package com.mosiler.web.admin.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base32;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.mosiler.web.admin.domain.reservationVO;
import com.mosiler.web.admin.service.reservationService;

public class adminUtil {
	
	private final static Logger logger = LogManager.getLogger(reservationService.class);
	static String className = "adminUtil";

	/**
	 * 로그인한 클라이언트 IP 체크
	 * @return String
	 */
	public static String getUserIp() throws Exception {
		String ip = null;
        
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();

        ip = request.getHeader("X-Forwarded-For");
        
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
            ip = request.getHeader("Proxy-Client-IP"); 
        } 
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
            ip = request.getHeader("WL-Proxy-Client-IP"); 
        } 
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
            ip = request.getHeader("HTTP_CLIENT_IP"); 
        } 
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
            ip = request.getHeader("HTTP_X_FORWARDED_FOR"); 
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
            ip = request.getHeader("X-Real-IP"); 
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
            ip = request.getHeader("X-RealIP"); 
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
            ip = request.getHeader("REMOTE_ADDR");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
            ip = request.getRemoteAddr(); 
        }
		
		return ip;
	}	
	
	/**
	 * OTP 키 생성
	 * @return String
	 */
	public static String generateSecretKey() {
        byte[] buffer = new byte[5 + 5 * 5];
         
        new Random().nextBytes(buffer);
 
        Base32 codec = new Base32();
        byte[] secretKey = Arrays.copyOf(buffer, 10);
        byte[] bEncodedKey = codec.encode(secretKey);
         
        // 생성된 Key
        String encodedKey = new String(bEncodedKey);
         
        System.out.println("encodedKey : " + encodedKey);
        
        return encodedKey;
	}
	
	/**
	 * OTP 키 검증
	 * @return String
	 */
	public static boolean checkCode(String secret, long code, long t) throws NoSuchAlgorithmException, InvalidKeyException {
		Base32 codec = new Base32();
		byte[] decodedKey = codec.decode(secret);

		int window = 3;

		for(int i = -window; i <= window; ++i) {
			long hash = verify_code(decodedKey, t + i);
 
			if(hash == code) 
				return true;
		}
 
        return false;
	}
	
	/**
	 * OTP 키 검증
	 * @return int
	 */
    private static int verify_code(byte[] key, long t) throws NoSuchAlgorithmException, InvalidKeyException {
        byte[] data = new byte[8];
        long value = t;
        
        for (int i = 8; i-- > 0; value >>>= 8) {
            data[i] = (byte) value;
        }
 
        SecretKeySpec signKey = new SecretKeySpec(key, "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(signKey);
        byte[] hash = mac.doFinal(data);
 
        int offset = hash[20 - 1] & 0xF;
 
        long truncatedHash = 0;
        for (int i = 0; i < 4; ++i) {
            truncatedHash <<= 8;
           
            truncatedHash |= (hash[offset + i] & 0xFF);
        }
 
        truncatedHash &= 0x7FFFFFFF;
        truncatedHash %= 1000000;
 
        return (int) truncatedHash;
    }
    
	public static String getQRBarcodeURL(String user, String host, String secret) {
        String format = "http://chart.apis.google.com/chart?cht=qr&amp;chs=300x300&amp;chl=otpauth://totp/%s@%s%%3Fsecret%%3D%s&amp;chld=H|0";
         
        return String.format(format, user, host, secret);
    }
	
	/**
	 * 페이징 현재 페이지, 보여줄 페이지 계산 
	 * @param model
	 * @return String
	 */
	public static String[] calculatePagingIsNull(String nowPage, String cntPerPage) {
		String pageArray[] = new String[2];
		
		if(nowPage == null && cntPerPage == null) {
			nowPage = "1";
			cntPerPage = "10";
		} else if(nowPage == null) {
			nowPage = "1";
		} else if(cntPerPage == null) {
			cntPerPage = "10";
		}
		
		pageArray[0] = nowPage;
		pageArray[1] = cntPerPage;
		
		return pageArray;
	}
	
	/**
	 * 현재 날짜 추출 (yyyy-mm-dd)
	 * @param model
	 * @return String
	 */
	public static String nowDate() {
		SimpleDateFormat fmt = new SimpleDateFormat ( "yyyy-MM-dd");
		Date nowDate = new Date();
		
		return fmt.format(nowDate);
	}
	
	/**
	 * 비밀번호 암호화 (운영자 pwd)
	 * @param model
	 * @return String
	 */
	public static String convertSHA(String pwd) {
		String soltKey = "25420048288677";
		String convertPwd = "";
		
		try {
			int temp;
	
	        // digest 설정
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			digest.reset();
			digest.update((pwd+soltKey).getBytes("utf8"));
			
			// SHA1 암호화
			String sha = String.format("%040x", new BigInteger(1, digest.digest()));
			
			// 소문자를 대문자로 변경 (한 글자씩 뽑아서 소문자면 대문자로 변경)
	        for(int i = 0; i < sha.length(); i++) {
	        	temp =  (int) sha.charAt(i);
	        	
	        	if((97 <= temp) && (122 >= temp)) {
	        		convertPwd += (char) (temp-32);
	        	} else {
	        		convertPwd += (char) temp;
	        	}		
	        }
		} catch (Exception e) {
			logger.debug(" ==========" + className + " => convertSHA(String pwd) ==========");
			return null;
		}

		return convertPwd;
	}
	
	/**
	 * 비밀번호 암호화 (파트너 pwd)
	 * @param model
	 * @return String
	 */
	public static String driverConvertSHA(String pwd) {
		//String soltKey = "25420048288677";
		String convertPwd = "";
		
		try {
			int temp;
	
	        // digest 설정
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			digest.reset();
			digest.update((pwd).getBytes("utf8"));
			
			// SHA1 암호화
			String sha = String.format("%040x", new BigInteger(1, digest.digest()));
			
			// 소문자를 대문자로 변경 (한 글자씩 뽑아서 소문자면 대문자로 변경)
	        for(int i = 0; i < sha.length(); i++) {
	        	temp =  (int) sha.charAt(i);
	        	
	        	if((97 <= temp) && (122 >= temp)) {
	        		convertPwd += (char) (temp-32);
	        	} else {
	        		convertPwd += (char) temp;
	        	}		
	        }
		} catch (Exception e) {
			logger.debug(" ==========" + className + " => convertSHA(String pwd) ==========");
			return null;
		}

		return convertPwd;
	}
	
	 /**
	  * 숫자에 천단위마다 콤마 넣기
	  * @param int
	  * @return String
	  * */
	 public static String toNumFormat(int num) {
		 DecimalFormat df = new DecimalFormat("#,###");
	  
		 return df.format(num);
	 }

	 /**
	  * 두 지점 간의 거리 계산
	  * @param lat
	  * @param lng
	  * @param lat2
	  * @param lng2
	  * */
	 public static double getDistance(float lat1, float lng, float lat2, float lng2) {
		 double theta = lng - lng2;
		 double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		 
		 if(dist > 1) {
			 dist = 1;
		 }
		 
		 dist = Math.acos(dist);
		 dist = rad2deg(dist);
		 dist = dist * 60 * 1.1515;
     
		 // km 단위
		 dist = dist * 1.609344;

		 return dist;
    }
     
	 // This function converts decimal degrees to radians
	 private static double deg2rad(double deg) {
		 return (deg * Math.PI / 180.0);
	 }
 
	 // This function converts radians to decimal degrees
	 private static double rad2deg(double rad) {
		 return (rad * 180 / Math.PI);
	 }

	/**
	 * 두 지점 간의 각도 계산
	 * @param lat
	 * @param lng
	 * @param lat2
	 * @param lng2
	 * */
	public static double getAngle(float lat, float lng, float lat2, float lng2) {
		double rad = Math.atan2(lng2 - lng, lat2 - lat);
		
		return (rad*180)/Math.PI ;
	}
	
	/**
	 * redeemCode 자동 생성
	 * @param length
	 * */
	public static String getRedeemCode(int length) {
		StringBuffer buffer = new StringBuffer();
		Random random = new Random();
		
		String chars[] = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,0,1,2,3,4,5,6,7,8,9".split(",");
		
		for(int i=0; i<length; i++) {
			buffer.append(chars[random.nextInt(chars.length)]);
		}
		
		return buffer.toString();
	}	
	
	/**
	 * 운행 데이터 전송 받기
	 * @param locationList 
	 * @return 
	 * @return Json
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public static JSONObject getStorageDrivingData(reservationVO reservationVO) throws IOException, ParseException {
		JSONParser parser = new JSONParser(); 
		JSONObject jsonData = new JSONObject();
		
		try {
			// 좌표 데이터 가져올 주소
			String targetUrl = "https://mosilergps.blob.core.windows.net/lclog/"+reservationVO.getDrivingNo();
			
			// URL 세팅
			URL url = new URL(targetUrl); 
			HttpURLConnection connection = (HttpURLConnection) url.openConnection(); 
			
			// 헤더 세팅
			connection.setRequestMethod("GET"); 
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			connection.setRequestProperty("Accept", "application/json");
			
			// Response Code
			int responseCode = connection.getResponseCode(); 
			
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream())); 
			String inputLine;
			
			// 응답 데이터
			StringBuffer response = new StringBuffer(); 
			
			while ((inputLine = in.readLine()) != null) { 
				response.append(inputLine); 
			} 
			
			// 닫기
			in.close();
			
			System.out.println("ResponseCode : " + responseCode);
			// 확인용 출력
			System.out.println("Response Data : " + response.toString());
			
			String stringData = response.toString();
			
			if(stringData != "" && stringData != null && !"".equals(stringData)) {
				Object temp = parser.parse(stringData); 
				
				jsonData = (JSONObject) temp;		
				
				jsonData.put("Code", "200");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error DrivingNo : " + reservationVO.getDrivingNo());
			
			jsonData.put("Code", "404");
			jsonData.put("DrivingNo", reservationVO.getDrivingNo());
			
			System.out.println("Error jsonData : " + jsonData);
		}

		return jsonData;
	}
}
