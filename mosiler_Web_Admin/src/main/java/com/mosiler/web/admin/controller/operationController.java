package com.mosiler.web.admin.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mosiler.web.admin.domain.reservationVO;
import com.mosiler.web.admin.service.operationService;

@Controller
@RequestMapping("/operation")
public class operationController {

	@Autowired
	private menuController menuController;
	
	@Autowired
	private operationService operationService;
	
	/**
	 * 실시간 관제 (페이지 이동)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/realTimeControll")
	public String realTimeControll(Model model, HttpServletRequest request) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/operation/realTimeControll";
	}
	
	/**
	 * 실시간 관제 - 좌표 (10초마다 실행)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/drivingLocation")
	@ResponseBody
	public List<List<reservationVO>> drivingLocation(HttpServletRequest request) {
		return operationService.getDrivingLocation(request);
	}
	
	/**
	 * 실시간 관제 - 실시간 Count (10초마다 실행)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/drivingCnt")
	@ResponseBody
	public List<reservationVO> drivingCnt(HttpServletRequest request) {
		return operationService.getDrivingCnt(request);
	}
	
	/**
	 * 실시간 관제 - 실시간 운행 List (10초마다 실행)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/currentDrivingList")
	@ResponseBody
	public List<reservationVO> currentDrivingList(HttpServletRequest request) {
		return operationService.getCurrentDrivingList(request);
	}

	/**
	 * 실시간 관제 - 리스트 클릭 시 Map 중심좌표 이동
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/currentLatLng")
	@ResponseBody
	public List<reservationVO> currentLatLng(HttpServletRequest request) {
		return operationService.getCurrentLatLng(request);
	}
	
	/**
	 * 일별 운행 현황 (페이지 이동)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/operationDayList")
	public String operationDayList(Model model, reservationVO reservationVO, HttpServletRequest request) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/operation/operationDayList";
	}
	
	/**
	 * 일별 운행 현황 (금일 운행 예약/취소건, 결제금액, 운행 예상 금액, 정산금액)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/selectCount")
	@ResponseBody
	public List<reservationVO> selectCount(reservationVO reservationVO, HttpServletRequest request) {
		//String revStartDt = request.getParameter("revStartDt");
		
		// 금일 운행 예약/취소건, 결제금액, 운행 예상 금액, 정산금액
		List<reservationVO> operationDayCount = operationService.operationDayCount();

		return operationDayCount;
	}	
	
	/**
	 * 일별 운행 현황 (데이터 Select)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/selectData")
	@ResponseBody
	public List<reservationVO> selectData(reservationVO reservationVO, HttpServletRequest request) {
		String revStartDt = request.getParameter("revStartDt");

		// 일별 운행 현황
		List<reservationVO> selectData = operationService.selectDrivingData(revStartDt);

		return selectData;
	}

	/**
	 * 일별 운행 현황 (데이터 Update)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/updateReservationDt")
	@ResponseBody
	public int updateReservationDt(HttpServletRequest request) {
		int resultCode = operationService.updateReservationDt(request);
		
		return resultCode;
	}
	
	/**
	 * 일별 운행 현황 (데이터 Copy)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/reservationCopy")
	@ResponseBody
	public int reservationCopy(HttpServletRequest request, @RequestParam Map<String , Object> dataMap) {
		int resultCode = 0;
		boolean isStatus = operationService.reservationCopy(request, dataMap);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 일별 운행 현황 (예약 취소)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/cancelReservation")
	@ResponseBody
	public int cancelReservation(HttpServletRequest request) {
		operationService.cancelReservation(request);
		int resultCode = 200;
		
		return resultCode;
	}
	
	/**
	 * 전체 운행 현황
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/operationList")
	public String operationList(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/operation/operationList";
	}
	
	/**
	 * 출퇴근 관리
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/workOnOffTime")
	public String workOnOffTime(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/operation/workOnOffTime";
	}
	
	/**
	 * 사고 관리
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/operationAccident")
	public String operationAccident(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/operation/operationAccident";
	}
	
	/**
	 * 선클레임
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/operationClaim")
	public String operationClaim(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/operation/operationClaim";
	}
} 
