package com.mosiler.web.admin.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.mosiler.web.admin.domain.systemVO;
import com.mosiler.web.admin.mapper.system.systemMapper;
import com.mosiler.web.admin.util.adminUtil;

@Service
public class systemService {

	private final Logger logger = LogManager.getLogger(systemService.class);
	String className = "systemService";
	
	@Autowired
	private systemMapper systemMapper; 
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	// 어드민 유저 Count
	public int adminUserListCnt(systemVO systemVO) {
		int adminUserListCnt = 0;
		
		try {
			adminUserListCnt = systemMapper.adminUserListCnt(systemVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => adminUserListCnt() ==========");
			return 0;
		}
		
		return adminUserListCnt;
	}

	// 어드민 유저 List
	public List<systemVO> adminUserList(systemVO systemVO, int start, int end) {
		List<systemVO> adminUserList = null;
		
		try {
			// 페이징 세팅
			systemVO.setStart(start);
			systemVO.setEnd(end);
			
			adminUserList = systemMapper.adminUserList(systemVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => adminUserList() ==========");
			return null;
		}
		
		return adminUserList;
	}

	//운영자 정보 수정
	public boolean updateAdminUser(systemVO systemVO) {
		boolean isStatus = false;
		
		DefaultTransactionDefinition DTD = new DefaultTransactionDefinition();
		DTD.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus transactionStatus = transactionManager.getTransaction(DTD);
		
		try {
			// otpKey 변경
			if(!"".equals(systemVO.getOtpKey())) {
				// 기존 otpKey 조회
				int count = systemMapper.getOtpKeyCnt(systemVO); 
				
				// 기존에 없으면 insert
				if(count == 0) {
					systemMapper.insertOtpKey(systemVO);
				}
				// 기존에 있으면 update
				else {
					systemMapper.updateOtpKey(systemVO);
				}
			}
			
			// 비번만 따로 변경
			if(!"".equals(systemVO.getUserPw())) {
				// 비번 암호화
				String pwd = adminUtil.convertSHA(systemVO.getUserPw());
				// 다시 세팅
				systemVO.setUserPw(pwd);
				
				// 비번 업데이트
				systemMapper.updatePw(systemVO);
			}
			
			// 정보 업데이트
			systemMapper.updateAdminUser(systemVO);
			
			// Commit
			transactionManager.commit(transactionStatus);
			
			isStatus = true;	
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => updateAdminUser() ==========");

			// Rollback
			transactionManager.rollback(transactionStatus);
			return isStatus;
		}
		
		return isStatus;
	}

	// 비밀번호 초기화
	public boolean pwdReset(systemVO systemVO) {
		boolean isStatus = false;
		
		try {
			// 전화번호 암호화
			String pwd = adminUtil.convertSHA(systemVO.getPhoneNumber());
			// 다시 세팅
			systemVO.setUserPw(pwd);
			
			// 비번 업데이트
			systemMapper.updatePw(systemVO);
			
			isStatus = true;
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => pwdReset() ==========");
			return isStatus;
		}

		return isStatus;
	}

	// FAQ Count
	public int faqListCnt(systemVO systemVO) {
		int faqListCnt = 0;
		
		try {
			faqListCnt = systemMapper.faqListCnt(systemVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => faqListCnt() ==========");
			return 0;
		}
		
		
		return faqListCnt;
	}

	// FAQ List
	public List<systemVO> faqList(systemVO systemVO, int start, int end) {
		List<systemVO> faqList = null;
		
		try {
			// 페이징 세팅
			systemVO.setStart(start);
			systemVO.setEnd(end);
			
			faqList = systemMapper.faqList(systemVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => faqList() ==========");
			return null;
		}
		
		return faqList;
	}

	// FAQ 상세 내용
	public List<systemVO> faqDetail(systemVO systemVO) {
		List<systemVO> faqDetail = null;
		
		try {
			// FAQ 상세 내용
			faqDetail = systemMapper.faqDetail(systemVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => faqDetail() ==========");
			return null;
		}
		
		return faqDetail;
	}

	// FAQ 등록
	public boolean insertFaq(HttpServletRequest request, systemVO systemVO) {
		boolean isStatus = false;
		
		try {
			systemVO.setId(request.getParameter("id"));
			systemVO.setFaqParentCategory(request.getParameter("faqParentCategory"));
			systemVO.setFaqCategory(request.getParameter("faqCategory"));
			systemVO.setFaqTopYn(request.getParameter("faqTopYn"));
			systemVO.setFaqQuestion(request.getParameter("faqQuestion"));
			systemVO.setUseYn(request.getParameter("useYn"));
			systemVO.setFaqAnswer(request.getParameter("faqAnswer"));
			
			// FAQ 등록
			systemMapper.insertFaq(systemVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => insertFaq() ==========");
			return isStatus;
		}
		
		return isStatus;
	}
	
	// FAQ Update
	public boolean updateFaq(HttpServletRequest request, systemVO systemVO) {
		boolean isStatus = false;
		
		try {
			systemVO.setFaqNo(Integer.parseInt(request.getParameter("faqNo")));
			systemVO.setFaqParentCategory(request.getParameter("faqParentCategory"));
			systemVO.setFaqCategory(request.getParameter("faqCategory"));
			systemVO.setFaqTopYn(request.getParameter("faqTopYn"));
			systemVO.setFaqQuestion(request.getParameter("faqQuestion"));
			systemVO.setUseYn(request.getParameter("useYn"));
			systemVO.setFaqAnswer(request.getParameter("faqAnswer"));
			
			// FAQ Update
			systemMapper.updateFaq(systemVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => updateFaq() ==========");
			return isStatus;
		}
		
		return isStatus;
	}
}
