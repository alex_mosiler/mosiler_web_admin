package com.mosiler.web.admin.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder // builder를 사용할수 있게 합니다.
@Data // user 필드값의 getter/setter를 자동으로 생성합니다.
@NoArgsConstructor // 인자없는 생성자를 자동으로 생성합니다.
@AllArgsConstructor // 인자를 모두 갖춘 생성자를 자동으로 생성합니다.
public class customerVO {
	// 고객 현황 VO
	int customerNo;
	String customerName;
	String email;
	String phoneNumber;
	String homeAddress;
	String carName;
	String carNo;
	String remark;
	String regDt;
	int regId;
	String updateDt;
	int updateId;
	String deleteYn;
	String userId;
	String userPw;
	String acountType;
	String deviceId;
	String osType;
	String deviceRegId;
	String deleteDt;
	String pushYn;
	int hourPrice;
	String hourPrice1;
	int bizCompNo;
	String customerType;
	int bizUserLevel;
	
	// paging VO
	int count;
	int start;
	int end;
	int totalCnt;
	
	// 고객 상세 VO
	String rownum;
	int paymentFee;
	int useCount;
	String lastDriving;
	int drivingNo;
	String drivingType;
	String reservationDt;
	String startDt;
	String drivingDt;
	String drivingStatus;
	String paymentYn;
	String driverName;
	
	// 검색어 VO
	String searchType;
	String searchDateType;
	String searchKeyword;
	String startDate;
	String endDate;
	
	// 쿠폰 VO
	String redeemCode;
	String promotionCode;
	String description;
	int disCountRate;
	int disCountAmount;
	String expiredDt;
	String activatedDt;
	
	// 서비스 신청자 VO
	int id;
//	String phoneNumber;
//	String email;
	String availTime;
//	String regDt;
	String status;
//	String updateDt;
	String rev1;
	String rev2;
	String rev3;
	String rev4;
	String rev5;
	String comment;
	
	// 법인현황 VO
//	int bizCompNo;
	String compName;
	String bizNumber;
	String ceoName;
	String compAddress;
	int usePersonCount;
	String compUserId;
	String compUserPw;
	String contractChargePart;
	String contractChargePhoneNo;
	String contractChargeName;
	String contractChargeEmail;
	String paymentChargePart;
	String paymentChargePhoneNo;
	String paymentChargeName;
	String paymentChargeEmail;
	String paymentMethod;
	String paymentDay;
	String taxReciveEmail;
//	int hourPrice;
	String reMark;
	String useYn;
//	String deleteYn;
//	int regId;
//	String regDt;
	int updId;
	String UpdDt;
	String adminName;
	
	// 프로모션 VO
//	int id;
	int promotionNo;
	
	String type;
	int couponQty;
//	String startDt;
	String endDt;
//	String promotionCode;
//	String description
	int releasedCoupon;
	int usedCoupon;
	String userTargetType;
	int validDays;
//	String regDt;
//	String updateDt;
//	String status;
	int discountRate;
	int discountAmount;
	int isRate;
	String statusName;
	String userTargetTypeName;
	String typeName;
	String codeName;
	String descriptionString;
	String couponName;
	int conditionQty;
	String conditionType;
	int acctualDiscountAmount;
	int discountValue;
	String promotionName;
	int isRepeat;
	int userTargetNo;
	int ruleId;
	
	public int getRuleId() {
		return ruleId;
	}
	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}
	public int getUserTargetNo() {
		return userTargetNo;
	}
	public void setUserTargetNo(int userTargetNo) {
		this.userTargetNo = userTargetNo;
	}
	public int getIsRepeat() {
		return isRepeat;
	}
	public void setIsRepeat(int isRepeat) {
		this.isRepeat = isRepeat;
	}
	public String getPromotionName() {
		return promotionName;
	}
	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	public int getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(int discountValue) {
		this.discountValue = discountValue;
	}
	public int getPromotionNo() {
		return promotionNo;
	}
	public void setPromotionNo(int promotionNo) {
		this.promotionNo = promotionNo;
	}
	public String getUserTargetTypeName() {
		return userTargetTypeName;
	}
	public int getAcctualDiscountAmount() {
		return acctualDiscountAmount;
	}
	public void setAcctualDiscountAmount(int acctualDiscountAmount) {
		this.acctualDiscountAmount = acctualDiscountAmount;
	}
	public String getCouponName() {
		return couponName;
	}
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	public int getConditionQty() {
		return conditionQty;
	}
	public void setConditionQty(int conditionQty) {
		this.conditionQty = conditionQty;
	}
	public String getConditionType() {
		return conditionType;
	}
	public void setConditionType(String conditionType) {
		this.conditionType = conditionType;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getDescriptionString() {
		return descriptionString;
	}
	public void setDescriptionString(String descriptionString) {
		this.descriptionString = descriptionString;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getName() {
		return userTargetTypeName;
	}
	public void setUserTargetTypeName(String userTargetTypeName) {
		this.userTargetTypeName = userTargetTypeName;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getCouponQty() {
		return couponQty;
	}
	public void setCouponQty(int couponQty) {
		this.couponQty = couponQty;
	}
	public String getEndDt() {
		return endDt;
	}
	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}
	public int getReleasedCoupon() {
		return releasedCoupon;
	}
	public void setReleasedCoupon(int releasedCoupon) {
		this.releasedCoupon = releasedCoupon;
	}
	public int getUsedCoupon() {
		return usedCoupon;
	}
	public void setUsedCoupon(int usedCoupon) {
		this.usedCoupon = usedCoupon;
	}
	public String getUserTargetType() {
		return userTargetType;
	}
	public void setUserTargetType(String userTargetType) {
		this.userTargetType = userTargetType;
	}
	public int getValidDays() {
		return validDays;
	}
	public void setValidDays(int validDays) {
		this.validDays = validDays;
	}
	public int getDiscountRate() {
		return discountRate;
	}
	public void setDiscountRate(int discountRate) {
		this.discountRate = discountRate;
	}
	public int getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(int discountAmount) {
		this.discountAmount = discountAmount;
	}
	public int getIsRate() {
		return isRate;
	}
	public void setIsRate(int isRate) {
		this.isRate = isRate;
	}
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	public String getHourPrice1() {
		return hourPrice1;
	}
	public void setHourPrice1(String hourPrice1) {
		this.hourPrice1 = hourPrice1;
	}
	public String getCompName() {
		return compName;
	}
	public void setCompName(String compName) {
		this.compName = compName;
	}
	public String getBizNumber() {
		return bizNumber;
	}
	public void setBizNumber(String bizNumber) {
		this.bizNumber = bizNumber;
	}
	public String getCeoName() {
		return ceoName;
	}
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}
	public String getCompAddress() {
		return compAddress;
	}
	public void setCompAddress(String compAddress) {
		this.compAddress = compAddress;
	}
	public int getUsePersonCount() {
		return usePersonCount;
	}
	public void setUsePersonCount(int usePersonCount) {
		this.usePersonCount = usePersonCount;
	}
	public String getCompUserId() {
		return compUserId;
	}
	public void setCompUserId(String compUserId) {
		this.compUserId = compUserId;
	}
	public String getCompUserPw() {
		return compUserPw;
	}
	public void setCompUserPw(String compUserPw) {
		this.compUserPw = compUserPw;
	}
	public String getContractChargePart() {
		return contractChargePart;
	}
	public void setContractChargePart(String contractChargePart) {
		this.contractChargePart = contractChargePart;
	}
	public String getContractChargePhoneNo() {
		return contractChargePhoneNo;
	}
	public void setContractChargePhoneNo(String contractChargePhoneNo) {
		this.contractChargePhoneNo = contractChargePhoneNo;
	}
	public String getContractChargeName() {
		return contractChargeName;
	}
	public void setContractChargeName(String contractChargeName) {
		this.contractChargeName = contractChargeName;
	}
	public String getContractChargeEmail() {
		return contractChargeEmail;
	}
	public void setContractChargeEmail(String contractChargeEmail) {
		this.contractChargeEmail = contractChargeEmail;
	}
	public String getPaymentChargePart() {
		return paymentChargePart;
	}
	public void setPaymentChargePart(String paymentChargePart) {
		this.paymentChargePart = paymentChargePart;
	}
	public String getPaymentChargePhoneNo() {
		return paymentChargePhoneNo;
	}
	public void setPaymentChargePhoneNo(String paymentChargePhoneNo) {
		this.paymentChargePhoneNo = paymentChargePhoneNo;
	}
	public String getPaymentChargeName() {
		return paymentChargeName;
	}
	public void setPaymentChargeName(String paymentChargeName) {
		this.paymentChargeName = paymentChargeName;
	}
	public String getPaymentChargeEmail() {
		return paymentChargeEmail;
	}
	public void setPaymentChargeEmail(String paymentChargeEmail) {
		this.paymentChargeEmail = paymentChargeEmail;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPaymentDay() {
		return paymentDay;
	}
	public void setPaymentDay(String paymentDay) {
		this.paymentDay = paymentDay;
	}
	public String getTaxReciveEmail() {
		return taxReciveEmail;
	}
	public void setTaxReciveEmail(String taxReciveEmail) {
		this.taxReciveEmail = taxReciveEmail;
	}
	public String getReMark() {
		return reMark;
	}
	public void setReMark(String reMark) {
		this.reMark = reMark;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public int getUpdId() {
		return updId;
	}
	public void setUpdId(int updId) {
		this.updId = updId;
	}
	public String getUpdDt() {
		return UpdDt;
	}
	public void setUpdDt(String updDt) {
		UpdDt = updDt;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getSearchDateType() {
		return searchDateType;
	}
	public void setSearchDateType(String searchDateType) {
		this.searchDateType = searchDateType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAvailTime() {
		return availTime;
	}
	public void setAvailTime(String availTime) {
		this.availTime = availTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRev1() {
		return rev1;
	}
	public void setRev1(String rev1) {
		this.rev1 = rev1;
	}
	public String getRev2() {
		return rev2;
	}
	public void setRev2(String rev2) {
		this.rev2 = rev2;
	}
	public String getRev3() {
		return rev3;
	}
	public void setRev3(String rev3) {
		this.rev3 = rev3;
	}
	public String getRev4() {
		return rev4;
	}
	public void setRev4(String rev4) {
		this.rev4 = rev4;
	}
	public String getRev5() {
		return rev5;
	}
	public void setRev5(String rev5) {
		this.rev5 = rev5;
	}
	public int getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(int customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getHomeAddress() {
		return homeAddress;
	}
	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}
	public String getCarName() {
		return carName;
	}
	public void setCarName(String carName) {
		this.carName = carName;
	}
	public String getCarNo() {
		return carNo;
	}
	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public int getRegId() {
		return regId;
	}
	public void setRegId(int regId) {
		this.regId = regId;
	}
	public String getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(String updateDt) {
		this.updateDt = updateDt;
	}
	public int getUpdateId() {
		return updateId;
	}
	public void setUpdateId(int updateId) {
		this.updateId = updateId;
	}
	public String getDeleteYn() {
		return deleteYn;
	}
	public void setDeleteYn(String deleteYn) {
		this.deleteYn = deleteYn;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserPw() {
		return userPw;
	}
	public void setUserPw(String userPw) {
		this.userPw = userPw;
	}
	public String getAcountType() {
		return acountType;
	}
	public void setAcountType(String acountType) {
		this.acountType = acountType;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getOsType() {
		return osType;
	}
	public void setOsType(String osType) {
		this.osType = osType;
	}
	public String getDeviceRegId() {
		return deviceRegId;
	}
	public void setDeviceRegId(String deviceRegId) {
		this.deviceRegId = deviceRegId;
	}
	public String getDeleteDt() {
		return deleteDt;
	}
	public void setDeleteDt(String deleteDt) {
		this.deleteDt = deleteDt;
	}
	public String getPushYn() {
		return pushYn;
	}
	public void setPushYn(String pushYn) {
		this.pushYn = pushYn;
	}
	public int getHourPrice() {
		return hourPrice;
	}
	public void setHourPrice(int hourPrice) {
		this.hourPrice = hourPrice;
	}
	public int getBizCompNo() {
		return bizCompNo;
	}
	public void setBizCompNo(int bizCompNo) {
		this.bizCompNo = bizCompNo;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public int getBizUserLevel() {
		return bizUserLevel;
	}
	public void setBizUserLevel(int bizUserLevel) {
		this.bizUserLevel = bizUserLevel;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public int getTotalCnt() {
		return totalCnt;
	}
	public void setTotalCnt(int totalCnt) {
		this.totalCnt = totalCnt;
	}
	public String getRownum() {
		return rownum;
	}
	public void setRownum(String rownum) {
		this.rownum = rownum;
	}
	public int getPaymentFee() {
		return paymentFee;
	}
	public void setPaymentFee(int paymentFee) {
		this.paymentFee = paymentFee;
	}
	public int getUseCount() {
		return useCount;
	}
	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}
	public String getLastDriving() {
		return lastDriving;
	}
	public void setLastDriving(String lastDriving) {
		this.lastDriving = lastDriving;
	}
	public int getDrivingNo() {
		return drivingNo;
	}
	public void setDrivingNo(int drivingNo) {
		this.drivingNo = drivingNo;
	}
	public String getDrivingType() {
		return drivingType;
	}
	public void setDrivingType(String drivingType) {
		this.drivingType = drivingType;
	}
	public String getReservationDt() {
		return reservationDt;
	}
	public void setReservationDt(String reservationDt) {
		this.reservationDt = reservationDt;
	}
	public String getStartDt() {
		return startDt;
	}
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
	public String getDrivingDt() {
		return drivingDt;
	}
	public void setDrivingDt(String drivingDt) {
		this.drivingDt = drivingDt;
	}
	public String getDrivingStatus() {
		return drivingStatus;
	}
	public void setDrivingStatus(String drivingStatus) {
		this.drivingStatus = drivingStatus;
	}
	public String getPaymentYn() {
		return paymentYn;
	}
	public void setPaymentYn(String paymentYn) {
		this.paymentYn = paymentYn;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getSearchKeyword() {
		return searchKeyword;
	}
	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getRedeemCode() {
		return redeemCode;
	}
	public void setRedeemCode(String redeemCode) {
		this.redeemCode = redeemCode;
	}
	public String getPromotionCode() {
		return promotionCode;
	}
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getDisCountRate() {
		return disCountRate;
	}
	public void setDisCountRate(int disCountRate) {
		this.disCountRate = disCountRate;
	}
	public int getDisCountAmount() {
		return disCountAmount;
	}
	public void setDisCountAmount(int disCountAmount) {
		this.disCountAmount = disCountAmount;
	}
	public String getExpiredDt() {
		return expiredDt;
	}
	public void setExpiredDt(String expiredDt) {
		this.expiredDt = expiredDt;
	}
	public String getActivatedDt() {
		return activatedDt;
	}
	public void setActivatedDt(String activatedDt) {
		this.activatedDt = activatedDt;
	}
	
	@Override
	public String toString() {
		return "customerVO [customerNo=" + customerNo + ", customerName=" + customerName + ", email=" + email
				+ ", phoneNumber=" + phoneNumber + ", homeAddress=" + homeAddress + ", carName=" + carName + ", carNo="
				+ carNo + ", remark=" + remark + ", regDt=" + regDt + ", regId=" + regId + ", updateDt=" + updateDt
				+ ", updateId=" + updateId + ", deleteYn=" + deleteYn + ", userId=" + userId + ", userPw=" + userPw
				+ ", acountType=" + acountType + ", deviceId=" + deviceId + ", osType=" + osType + ", deviceRegId="
				+ deviceRegId + ", deleteDt=" + deleteDt + ", pushYn=" + pushYn + ", hourPrice=" + hourPrice
				+ ", bizCompNo=" + bizCompNo + ", customerType=" + customerType + ", bizUserLevel=" + bizUserLevel
				+ ", count=" + count + ", start=" + start + ", end=" + end + ", totalCnt=" + totalCnt + ", rownum="
				+ rownum + ", paymentFee=" + paymentFee + ", useCount=" + useCount + ", lastDriving=" + lastDriving
				+ ", drivingNo=" + drivingNo + ", drivingType=" + drivingType + ", reservationDt=" + reservationDt
				+ ", startDt=" + startDt + ", drivingDt=" + drivingDt + ", drivingStatus=" + drivingStatus
				+ ", paymentYn=" + paymentYn + ", driverName=" + driverName + ", searchType=" + searchType
				+ ", searchKeyword=" + searchKeyword + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", redeemCode=" + redeemCode + ", promotionCode=" + promotionCode + ", description=" + description
				+ ", disCountRate=" + disCountRate + ", disCountAmount=" + disCountAmount + ", expiredDt=" + expiredDt
				+ ", activatedDt=" + activatedDt + "]";
	}
}
