package com.mosiler.web.admin.domain;

import lombok.Data;

@Data // user 필드값의 getter/setter를 자동으로 생성합니다.
public class pagingVO {
	int nowPage;
	int startPage;
	int endPage;
	int total;
	int cntPerPage;
	int lastPage;
	int start;
	int end;
	int cntPage = 10;
	int offsetRow;
	int nextRow;
	
	public int getNowPage() {
		return nowPage;
	}

	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}

	public int getStartPage() {
		return startPage;
	}

	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}

	public int getEndPage() {
		return endPage;
	}

	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getCntPerPage() {
		return cntPerPage;
	}

	public void setCntPerPage(int cntPerPage) {
		this.cntPerPage = cntPerPage;
	}

	public int getLastPage() {
		return lastPage;
	}

	public void setLastPage(int lastPage) {
		this.lastPage = lastPage;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getCntPage() {
		return cntPage;
	}

	public void setCntPage(int cntPage) {
		this.cntPage = cntPage;
	}

	public int getOffsetRow() {
		return offsetRow;
	}

	public void setOffsetRow(int offsetRow) {
		this.offsetRow = offsetRow;
	}

	public int getNextRow() {
		return nextRow;
	}

	public void setNextRow(int nextRow) {
		this.nextRow = nextRow;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	@Override
	public String toString() {
		return "pagingVO [nowPage=" + nowPage + ", startPage=" + startPage + ", endPage=" + endPage + ", total=" + total
				+ ", cntPerPage=" + cntPerPage + ", lastPage=" + lastPage + ", start=" + start + ", end=" + end
				+ ", cntPage=" + cntPage + ", offsetRow=" + offsetRow + ", nextRow=" + nextRow + ", searchType="
				+ searchType + ", searchKeyword=" + searchKeyword + "]";
	}

	// 검색어 VO
	String searchType;
	String searchKeyword;
	
	public pagingVO() {
	
	}
	
	public pagingVO(int total, int nowPage, int cntPerPage) {
		setNowPage(nowPage);
		setCntPerPage(cntPerPage);
		setTotal(total);
		
		setLastPage((int) Math.ceil((double)total / (double)cntPerPage));
		setEndPage(((int)Math.ceil((double)nowPage / (double)cntPage)) * cntPage);
		
		if(getLastPage() < getEndPage()) {
			setEndPage(getLastPage());
		}
		
		setStartPage(getEndPage() - cntPage + 1);
		
		if(getStartPage() < 1) {
			setStartPage(1);
		}
		
		setEnd(nowPage * cntPerPage);
		setStart(getEnd() - cntPerPage + 1);
		
		setOffsetRow((nowPage * cntPerPage) - cntPerPage );
		setNextRow(cntPerPage);
	}
}
