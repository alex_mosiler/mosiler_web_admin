package com.mosiler.web.admin.mapper.menu;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.mosiler.web.admin.domain.menuVO;

@Repository("com.mosiler.web.admin.mapper.menu.menuMapper")
@Mapper
public interface menuMapper {
	List<menuVO> cateList();
	List<menuVO> depthList();
}
