package com.mosiler.web.admin.mapper.partner;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.mosiler.web.admin.domain.partnerVO;
import com.mosiler.web.admin.domain.reservationVO;

@Repository("com.mosiler.web.admin.mapper.partner.partnerMapper")
@Mapper
public interface partnerMapper {

	int partnerAssignmentCnt(partnerVO partnerVO);
	List<partnerVO> partnerAssignmentList(partnerVO partnerVO);
	List<partnerVO> selectPartnerList(partnerVO partnerVO);
	List<partnerVO> selectPartnerDetail(partnerVO partnerVO);
	List<partnerVO> selectDrivingDetail(partnerVO partnerVO);
	List<partnerVO> selectCsDetail(partnerVO partnerVO);
	List<partnerVO> selectDriverClassDetail(partnerVO partnerVO);
	List<partnerVO> selectDriverEvaluationDetail(partnerVO partnerVO);
	List<partnerVO> selectDriverIssuedDetail(partnerVO partnerVO);
	List<partnerVO> selectDriverAccdentDetail(partnerVO partnerVO);
	List<partnerVO> selectDriverViolationDetail(partnerVO partnerVO);
	int selectDrivingDetailCnt(partnerVO partnerVO);
	int selectDriverEvaluationDetailCnt(partnerVO partnerVO);
	int selectCsDetailCnt(partnerVO partnerVO);
	int selectDriverClassDetailCnt(partnerVO partnerVO);
	int selectDriverIssuedDetailCnt(partnerVO partnerVO);
	int selectDriverAccdentDetailCnt(partnerVO partnerVO);
	int selectDriverViolationDetailCnt(partnerVO partnerVO);
	void insertDriver(partnerVO partnerVO);

}
