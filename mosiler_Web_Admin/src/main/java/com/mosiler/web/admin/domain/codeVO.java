package com.mosiler.web.admin.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder // builder를 사용할수 있게 합니다.
@Data // user 필드값의 getter/setter를 자동으로 생성합니다.
@NoArgsConstructor // 인자없는 생성자를 자동으로 생성합니다.
@AllArgsConstructor // 인자를 모두 갖춘 생성자를 자동으로 생성합니다.
public class codeVO {
	// TB_CommonCode
	int codeNo;
	int parentCodeNo;
	String code;
	String codeName;
	String codeNameNum;
	String reMark;
	String etc1;
	String etc2;
	String etc3;
	int sortOrder;
	String useYn;
	String regDt;
	int regUserNo;
	String updateDt;
	int updateUserNo;
	int codeFee;
	int bizCompNo;
	String compName;
	
	public int getBizCompNo() {
		return bizCompNo;
	}
	public void setBizCompNo(int bizCompNo) {
		this.bizCompNo = bizCompNo;
	}
	public String getCompName() {
		return compName;
	}
	public void setCompName(String compName) {
		this.compName = compName;
	}
	public int getCodeNo() {
		return codeNo;
	}
	public void setCodeNo(int codeNo) {
		this.codeNo = codeNo;
	}
	public int getParentCodeNo() {
		return parentCodeNo;
	}
	public void setParentCodeNo(int parentCodeNo) {
		this.parentCodeNo = parentCodeNo;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getCodeNameNum() {
		return codeNameNum;
	}
	public void setCodeNameNum(String codeNameNum) {
		this.codeNameNum = codeNameNum;
	}
	public String getReMark() {
		return reMark;
	}
	public void setReMark(String reMark) {
		this.reMark = reMark;
	}
	public String getEtc1() {
		return etc1;
	}
	public void setEtc1(String etc1) {
		this.etc1 = etc1;
	}
	public String getEtc2() {
		return etc2;
	}
	public void setEtc2(String etc2) {
		this.etc2 = etc2;
	}
	public String getEtc3() {
		return etc3;
	}
	public void setEtc3(String etc3) {
		this.etc3 = etc3;
	}
	public int getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public int getRegUserNo() {
		return regUserNo;
	}
	public void setRegUserNo(int regUserNo) {
		this.regUserNo = regUserNo;
	}
	public String getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(String updateDt) {
		this.updateDt = updateDt;
	}
	public int getUpdateUserNo() {
		return updateUserNo;
	}
	public void setUpdateUserNo(int updateUserNo) {
		this.updateUserNo = updateUserNo;
	}
	public int getCodeFee() {
		return codeFee;
	}
	public void setCodeFee(int codeFee) {
		this.codeFee = codeFee;
	}
	
	@Override
	public String toString() {
		return "codeVO [codeNo=" + codeNo + ", parentCodeNo=" + parentCodeNo + ", code=" + code + ", codeName="
				+ codeName + ", codeNameNum=" + codeNameNum + ", reMark=" + reMark + ", etc1=" + etc1 + ", etc2=" + etc2
				+ ", etc3=" + etc3 + ", sortOrder=" + sortOrder + ", useYn=" + useYn + ", regDt=" + regDt
				+ ", regUserNo=" + regUserNo + ", updateDt=" + updateDt + ", updateUserNo=" + updateUserNo
				+ ", codeFee=" + codeFee + "]";
	}
}
