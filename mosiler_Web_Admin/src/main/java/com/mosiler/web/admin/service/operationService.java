package com.mosiler.web.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.mosiler.web.admin.domain.reservationVO;
import com.mosiler.web.admin.mapper.operation.operationMapper;
import com.mosiler.web.admin.mapper.reservation.reservationMapper;
import com.mosiler.web.admin.util.adminUtil;

@Service
public class operationService {
	private static List<JSONObject> locationList = new ArrayList<JSONObject>();
	
	private final Logger logger = LogManager.getLogger(operationService.class);
	String className = "operationService";
	
	@Autowired
	operationMapper operationMapper;
	
	@Autowired
	reservationMapper reservationMapper;

	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	// 출발 준비, 출발 지연 관련 데이터
	public List<reservationVO> realTimeStartData(reservationVO reservationVO) {
		List<reservationVO> driverLocationLog = null;
		
		// 해당 drivingNo의 제일 최신 location, 10번째 location 	
		// 운행 시작 후 10초가 지나면 무조건 2 row 출력
		// 10초 전이면 1 row 출력
		driverLocationLog = operationMapper.getDriverLocationLog(reservationVO);

		if(driverLocationLog.size() == 0) {
			// TB_DriverLocationLog 테이블에 좌표 정보가 없을 경우
			// TB_Driving 테이블에서 예약 시작 위치를 가져온다
			driverLocationLog = operationMapper.getDrivingData(reservationVO);
			
			// 위치 정보가 1개이기 때문에 0
			double distance = 0;
			
			// distance를 세팅 해줘야 Map에 보인다
			driverLocationLog.get(0).setDistance(distance);
		} else {
			// 거리 계산
			if(driverLocationLog.size() == 1) {
				// 위치 정보가 1개 일 때 위치는 0
				double distance = 0;
				//double distance = adminUtil.getDistance(driverLocationLog.get(0).getLat(), driverLocationLog.get(0).getLng(), 0, 0); 
				
				driverLocationLog.get(0).setDistance(distance);
			} else if(driverLocationLog.size() == 2) {
				// 두 지점 간의 거리 계산
				double distance = adminUtil.getDistance(driverLocationLog.get(0).getLat(), driverLocationLog.get(0).getLng(), driverLocationLog.get(1).getLat(), driverLocationLog.get(1).getLng()); 
				
				// 시속 계산하여 넘겨준다.
				driverLocationLog.get(0).setDistance(distance*360);
				
				// 각도까지 계산
				double angle = adminUtil.getAngle(driverLocationLog.get(0).getLat(), driverLocationLog.get(0).getLng(), driverLocationLog.get(1).getLat(), driverLocationLog.get(1).getLng()); 
				
				driverLocationLog.get(0).setAngle(angle);
			}
		}	
		
		return driverLocationLog;
	}
	
	// 운행중, 운행 연장 관련 데이터
	public List<reservationVO> realTimeDrivingData(reservationVO reservationVO) {
		List<reservationVO> driverLocationLog = null;
		
		try {
			//  {"DrivingNo":"88","Lat":37.5162143,"Lng":127.0290992,"X":-3.549126849975437E-4,"Y":-1.1423179967096075E-4,"Z":2.1380283214966767E-5
			//		,"DriverNo":"4849","LogDt":"2021-03-24T14:46:15Z","pressure":1010.95458984375}
			JSONObject jsonData = adminUtil.getStorageDrivingData(reservationVO);
			
			// 스토리지에서 파일을 쓰고 있으면 jsonData가 공백으로 넘어온다. 공백 처리 해줘야함
			// 스토리지에 파일 일기에 성공 했을 경우
			if(jsonData.size() != 0 && jsonData != null && !jsonData.get("Code").equals("404")) {
				// 일단 배열에 때려 넣는다.
				locationList.add(jsonData);
				
				for(int i = locationList.size(); 0 < i; i--) {
					int index = i - 2;

					if(index < 0) {
						continue;
					} else {
						if(locationList.get(index).get("DrivingNo").equals(jsonData.get("DrivingNo"))) {
							// drivingNo 세팅
							reservationVO.setDrivingNo(Integer.parseInt(String.valueOf(jsonData.get("DrivingNo"))));
							
							// TB_Driving 테이블에서 예약 시작 위치를 가져온다
							driverLocationLog = operationMapper.getDrivingData(reservationVO);
							
							// 좌표값 세팅
							float locationListLat 	= Float.parseFloat(String.valueOf(locationList.get(index).get("Lat")));
							float locationListLng 	= Float.parseFloat(String.valueOf(locationList.get(index).get("Lng")));
							float jsonDataLat 		= Float.parseFloat(String.valueOf(jsonData.get("Lat")));
							float jsonDataLng 	= Float.parseFloat(String.valueOf(jsonData.get("Lng")));
							
							// 두 지점 간의 거리 계산
							double temp = adminUtil.getDistance(locationListLat, locationListLng, jsonDataLat, jsonDataLng); 
							
							// 시속 계산하여 넘겨준다.
							double distance = temp*360;
							
							// 각도까지 계산
							double angle = adminUtil.getAngle(locationListLat, locationListLng, jsonDataLat, jsonDataLng); 
							
							driverLocationLog.get(0).setLat(jsonDataLat);
							driverLocationLog.get(0).setLng(jsonDataLng);
							driverLocationLog.get(0).setDistance(distance);
							driverLocationLog.get(0).setAngle(angle);			
							
							break;
						}
					}				
				}
			}
			// 스토리지에서 파일을 못 읽었을 경우 (file not found)
			else {
				// 읽지 못한 파일의 drivingNo
				String drivingNo = String.valueOf(jsonData.get("DrivingNo"));
				
				for(int i = locationList.size(); 0 < i; i--) {
					int index = i - 1;

					// 해당 drivingNo에 대해 배열에 있는 그 전 데이터 추출
					if(locationList.get(index).get("DrivingNo").equals(drivingNo)) {
						// drivingNo 세팅
						reservationVO.setDrivingNo(Integer.parseInt(drivingNo));

						// TB_Driving 테이블에서 예약 시작 위치를 가져온다
						driverLocationLog = operationMapper.getDrivingData(reservationVO);
					
						// 그 전 좌표
						float locationListLat 	= Float.parseFloat(String.valueOf(locationList.get(index).get("Lat")));
						float locationListLng 	= Float.parseFloat(String.valueOf(locationList.get(index).get("Lng")));

						// 위치 정보가 1개이기 때문에 0
						double distance = 0;
						
						driverLocationLog.get(0).setDistance(distance);
						
						driverLocationLog.get(0).setLat(locationListLat);
						driverLocationLog.get(0).setLng(locationListLng);
						
						break;
					}				
				}			
				
				// locationList에 해당 drivingNo가 없거나
				// 파일 자체가 없는 경우 (driverLocationLog가 Null인 경우, 위 for문에 안 걸리면 해당 베열에 데이터 안 들어감)
				if(driverLocationLog == null) {
					// 파일 자체가 없는 경우 TB_Driving 테이블에서 예약 시작 위치를 가져온다
					driverLocationLog = operationMapper.getDrivingData(reservationVO);
					
					// 위치 정보가 1개이기 때문에 0
					double distance = 0;
					
					// distance를 세팅 해줘야 Map에 보인다
					driverLocationLog.get(0).setDistance(distance);
				}
			}		
		} catch (Exception e) {
			e.printStackTrace();
		}

		return driverLocationLog;
	}
	
	// 실시간 관제 - 좌표 (10초마다 실행)
	public List<List<reservationVO>> getDrivingLocation(HttpServletRequest request) {
		reservationVO reservationVO = new reservationVO();
		String nowDate = request.getParameter("nowDate");
		String nextDate = request.getParameter("nextDate");
		
		reservationVO.setNowDate(nowDate);
		reservationVO.setNextDate(nextDate);
		
		// 실제 넘겨줄 데이터 리스트
		List<List<reservationVO>> locationList = new ArrayList<List<reservationVO> >();
		
		try {	
			List<reservationVO> driverLocationLog = null;
			
			// 현재 출발준비, 운행중, 운행연장, 출발지연 상태인 drivingNo, drivingStatus, Name 리스트
			List<reservationVO> drivingNoList = operationMapper.getDrivingNoList(reservationVO);
			
			for(int i = 0; i < drivingNoList.size(); i++) {
				// Name 세팅
				reservationVO.setName(drivingNoList.get(i).getName());
				
				// drivingNo 세팅
				reservationVO.setDrivingNo(drivingNoList.get(i).getDrivingNo());
				
				// 출발 준비
				if("ready".equals(reservationVO.getName())) {
					driverLocationLog = realTimeStartData(reservationVO);
				}
				// 운행중
				else if("driving".equals(reservationVO.getName())) {
					driverLocationLog = realTimeDrivingData(reservationVO);
				}
				// 운행 연장
				else if("extensiveDriving".equals(reservationVO.getName())) {
					driverLocationLog = realTimeDrivingData(reservationVO);
				}
				// 출발 지연
				else if("startDelay".equals(reservationVO.getName())) {
					driverLocationLog = realTimeStartData(reservationVO);
				}
				
				locationList.add(driverLocationLog);
			}
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => getDrivingLocation() ==========");
			return null;
		}
		
		return locationList;
	}
	
	// 실시간 관제 - 실시간 Count (10초마다 실행)
	public List<reservationVO> getDrivingCnt(HttpServletRequest request) {
		reservationVO reservationVO = new reservationVO();
		String nowDate = request.getParameter("nowDate");
		String nextDate = request.getParameter("nextDate");
		
		reservationVO.setNowDate(nowDate);
		reservationVO.setNextDate(nextDate);
		
		// 실시간 Count
		List<reservationVO> drivingCnt =  operationMapper.getDrivingCnt(reservationVO);
		
		return drivingCnt;
	}
	
	// 실시간 관제 - 실시간 운행 List (10초마다 실행)
	public List<reservationVO> getCurrentDrivingList(HttpServletRequest request) {
		reservationVO reservationVO = new reservationVO();
		String nowDate = request.getParameter("nowDate");
		String nextDate = request.getParameter("nextDate");
		String searchKeyword = request.getParameter("searchKeyword");

		reservationVO.setNowDate(nowDate);
		reservationVO.setNextDate(nextDate);
		
		if(!"".equals("searchKeyword") || searchKeyword != null) {
			reservationVO.setSearchKeyword(searchKeyword);
		}
		
		// 실시간 운행 List
		List<reservationVO> currentDrivingList =  operationMapper.getCurrentDrivingList(reservationVO);
		
		return currentDrivingList;
	}
	
	// 실시간 관제 - 리스트 클릭 시 Map 중심좌표 이동
	public List<reservationVO> getCurrentLatLng(HttpServletRequest request) {
		reservationVO reservationVO = new reservationVO();
		String drivingNo = request.getParameter("drivingNo");
		
		reservationVO.setDrivingNo(Integer.parseInt(drivingNo));
		
		return operationMapper.getCurrentLatLng(reservationVO);
	}
	
	// 금일 운행 예약/취소건, 결제금액, 운행 예상 금액, 정산금액
	public List<reservationVO> operationDayCount() {
		List<reservationVO> operationDayCount = null;
		reservationVO reservationVO = new reservationVO();
		
		try {
			// 금일 날짜 세팅
			reservationVO.setDate(adminUtil.nowDate());
			
			// 금일 운행 예약/취소건, 결제금액, 운행 예상 금액, 정산금액
			operationDayCount = operationMapper.operationDayCount(reservationVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => operationDayCount() ==========");
			return null;
		}
		
		return operationDayCount;
	}
	
	// 일별 운행 현황 Select
	public List<reservationVO> selectDrivingData(String revStartDt) {
		List<reservationVO> selectData = null;
		
		try {
			if(revStartDt == null) {
				revStartDt = adminUtil.nowDate();
			}
			
			// 일별 운행 현황 Select
			selectData = operationMapper.selectDrivingData(revStartDt);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => selectDrivingData() ==========");
			return null;
		}
		
		return selectData;
	}

	// 일별 운행 현황 Update (Calendar에서 데이터 변경 시)
	public int updateReservationDt(HttpServletRequest request) {
		int resultCode = 500;
		reservationVO reservationVO = new reservationVO();
		
		try {
			// 받은 값 세팅
			reservationVO.setDrivingNo(Integer.parseInt(request.getParameter("drivingNo")));
			reservationVO.setRevStartDt(request.getParameter("revStartDt"));
			reservationVO.setRevEndDt(request.getParameter("revEndDt"));
			
			// 일별 운행 현황 Update
			int resultRow = operationMapper.updateReservationDt(reservationVO);
					
			if(resultRow == 1) {
				resultCode = 200;
			}
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => updateReservationDt() ==========");
			return resultCode;
		}
		
		return resultCode;
	}

	// 일별 운행 현황 Copy (Calendar에서 데이터 변경 시)
	public boolean reservationCopy(HttpServletRequest request, Map<String, Object> dataMap) {
		boolean isStatus = false;
		reservationVO reservationVO = new reservationVO();
		
		DefaultTransactionDefinition DTD = new DefaultTransactionDefinition();
		DTD.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus transactionStatus = transactionManager.getTransaction(DTD);
		
		int length = 0; 			
		int payServiceFee = 0;
		
		try {
			// 받은 값 세팅
			reservationVO.setDrivingNo(Integer.parseInt((String) dataMap.get("data[drivingNo]")));
			reservationVO.setBizCompNo(Integer.parseInt((String) dataMap.get("data[bizCompNo]")));
			reservationVO.setCustomerNo(Integer.parseInt((String) dataMap.get("data[customerNo]")));
			reservationVO.setCustomerName((String) dataMap.get("data[customerName]"));
			reservationVO.setCustomerPhoneNo((String) dataMap.get("data[customerPhoneNo]"));
			reservationVO.setDrivingType((String) dataMap.get("data[drivingType]"));
			reservationVO.setInsuranceYn(request.getParameter("insuranceYn"));
			reservationVO.setRevStartDt(request.getParameter("revStartDt"));
			reservationVO.setRevEndDt(request.getParameter("revEndDt"));
			reservationVO.setPaymentType((String) dataMap.get("data[paymentType]"));
			reservationVO.setCouponCnt(Integer.parseInt((String) dataMap.get("data[couponCnt]")));
			reservationVO.setHourPrice(Integer.parseInt((String) dataMap.get("data[hourPrice]")));
			reservationVO.setMosilerComment((String) dataMap.get("data[mosilerComment]"));
			reservationVO.setCustomerComment((String) dataMap.get("data[customerComment]"));
			reservationVO.setDriverNo(Integer.parseInt((String) dataMap.get("data[driverNo]")));
			reservationVO.setPassenger((String) dataMap.get("data[passenger]"));
			reservationVO.setPassengerPhoneNo((String) dataMap.get("data[passengerPhoneNo]"));
			reservationVO.setStartAddress((String) dataMap.get("data[revStartAddress]"));
			reservationVO.setStartLat((String) dataMap.get("data[revStartLat]"));
			reservationVO.setStartLng((String) dataMap.get("data[revStartLng]"));
			reservationVO.setEndAddress((String) dataMap.get("data[revEndAddress]"));
			reservationVO.setEndLat((String) dataMap.get("data[revEndLat]"));
			reservationVO.setEndLng((String) dataMap.get("data[revEndLng]"));
			reservationVO.setRevStartAddress((String) dataMap.get("data[revStartAddress]"));
			reservationVO.setRevStartLat((String) dataMap.get("data[revStartLat]"));
			reservationVO.setRevStartLng((String) dataMap.get("data[revStartLng]"));
			reservationVO.setRevEndAddress((String) dataMap.get("data[revEndAddress]"));
			reservationVO.setRevEndLat((String) dataMap.get("data[revEndLat]"));
			reservationVO.setRevEndLng((String) dataMap.get("data[revEndLng]"));
			reservationVO.setWayPointAddress01((String) dataMap.get("data[wayPointAddress01]"));
			reservationVO.setWayPointLat01((String) dataMap.get("data[wayPointLat01]"));
			reservationVO.setWayPointLng01((String) dataMap.get("data[wayPointLng01]"));
			reservationVO.setWayPointAddress02((String) dataMap.get("data[wayPointAddress02]"));
			reservationVO.setWayPointLat02((String) dataMap.get("data[wayPointLat02]"));
			reservationVO.setWayPointLng02((String) dataMap.get("data[wayPointLng02]"));
			reservationVO.setPaidTicketYn("N");
			
			// 기존 예약 컨시어지 조회 (drivingNo 바뀌기 전, 기존 예약에서 조회 후 drivingNo 변경)
			List<reservationVO> conciergeList = reservationMapper.selectConcierge(reservationVO);
			
			if(conciergeList != null) {
				length = conciergeList.size();
				payServiceFee = length * 10000;
			}
			
			// 부가 서비스 금액 
			reservationVO.setPayServiceFee(payServiceFee);
			
			// 예약 등록
			reservationMapper.insertReservation(reservationVO);
			
			// drivingNo 채번
			int drivingNo = reservationMapper.selectDrivingNo(reservationVO);
			reservationVO.setDrivingNo(drivingNo);
			
			// 경유지 등록
			if(reservationVO.getWayPointAddress01() != "" || reservationVO.getWayPointAddress02() != "") {
				reservationMapper.insertWayPoint(reservationVO);
			}

			// 컨시어지
			if(length != 0) {
				for(int i = 0; i < length; i++) {
					reservationVO.setPayServiceNo(conciergeList.get(i).getPayServiceNo());
					reservationMapper.insertConcierge(reservationVO);
				}
			}

			// Commit
			transactionManager.commit(transactionStatus);
			isStatus = true;		
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => reservationCopy() ==========");
			
			// Rollback
			transactionManager.rollback(transactionStatus);
			return isStatus;
		}
		
		return isStatus;
	}	
	
	// 일별 운행 현황 예약 취소 (drivingStatus 변경)
	public int cancelReservation(HttpServletRequest request) {
		int resultCode = 500;
		reservationVO reservationVO = new reservationVO();
		
		try {
			// 받은 값 세팅
			reservationVO.setDrivingNo(Integer.parseInt(request.getParameter("drivingNo")));
			
			// 일별 운행 현황 예약 취소
			int resultRow = operationMapper.cancelReservation(reservationVO);
			
			if(resultRow == 1) {
				resultCode = 200;
			}	
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => cancelReservation() ==========");
			return resultCode;
		}
		
		return resultCode;
	}
}
