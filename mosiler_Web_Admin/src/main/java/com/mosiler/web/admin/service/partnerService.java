package com.mosiler.web.admin.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mosiler.web.admin.domain.partnerVO;
import com.mosiler.web.admin.domain.reservationVO;
import com.mosiler.web.admin.mapper.partner.partnerMapper;
import com.mosiler.web.admin.util.adminUtil;

@Service
public class partnerService {

	private final Logger logger = LogManager.getLogger(partnerService.class);
	String className = "partnerService";
	
	@Autowired
	private partnerMapper partnerMapper;
	
	// 파트너 배정 Count
	public int partnerAssignmentCnt(partnerVO partnerVO) {
		int partnerAssignmentCnt = 0;
		
		try {
			partnerAssignmentCnt = partnerMapper.partnerAssignmentCnt(partnerVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => partnerAssignmentCnt() ==========");
			return 0;
		}
		
		return partnerAssignmentCnt;
	}	
	
	// 파트너 배정
	public List<partnerVO> partnerAssignmentList(partnerVO partnerVO, int start, int end) {
		List<partnerVO> partnerAssignmentList = null;
		
		try {
			// 페이징 세팅
			partnerVO.setStart(start);
			partnerVO.setEnd(end);
			
			partnerAssignmentList = partnerMapper.partnerAssignmentList(partnerVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => partnerAssignmentList() ==========");
			return null;
		}

		return partnerAssignmentList;
	}

	// 파트너 현황
	public List<partnerVO> selectPartnerList(HttpServletRequest request, partnerVO partnerVO) {
		List<partnerVO> selectPartnerList = null;
		
		try {
			// 파트너 현황
			selectPartnerList = partnerMapper.selectPartnerList(partnerVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => selectPartnerList() ==========");
			return null;
		}

		return selectPartnerList;
	}

	// 파트너 등록
	public boolean insertDriver(HttpServletRequest request, partnerVO partnerVO) {
		boolean isStatus = false;
		
		try {
			String userName = request.getParameter("userName");
			String userId = request.getParameter("userId");
			String socialNumber = request.getParameter("socialNumber");
			String phoneNumber = request.getParameter("phoneNumber");
			String userPw = request.getParameter("userPw");
			String licenseNumber = request.getParameter("licenseNumber");
			String bankCode = request.getParameter("bankCode");
			String accountNumber = request.getParameter("accountNumber");
			String hobby = request.getParameter("hobby");
			String jobName = request.getParameter("jobName");
			String drivingYear = request.getParameter("drivingYear");
			String driverStatus = request.getParameter("driverStatus");
			String bizCompNo = request.getParameter("bizCompNo");
			String permitStatus = request.getParameter("permitStatus");
			String driverRemark = request.getParameter("driverRemark");
			
			partnerVO.setUserName(userName);
			partnerVO.setUserId(userId);
			partnerVO.setSocialNumber(socialNumber);
			partnerVO.setPhoneNumber(phoneNumber);
			partnerVO.setProfileImgName("");
			partnerVO.setUserPw(adminUtil.driverConvertSHA(userPw));
			partnerVO.setLicenseNumber(licenseNumber);
			partnerVO.setBankCode(bankCode);
			partnerVO.setAccountNumber(accountNumber);
			partnerVO.setHobby(hobby);
			partnerVO.setJobName(jobName);
			partnerVO.setDrivingYear(Integer.parseInt(drivingYear));
			partnerVO.setDriverStatus(driverStatus);
			partnerVO.setBizCompNo(Integer.parseInt(bizCompNo));
			partnerVO.setPermitStatus(permitStatus);
			partnerVO.setDriverRemark(driverRemark);
			
			
			// 이미지 업로드 만든 후에 집어 넣는다///////// 개발 필요
			partnerVO.setLicenseImgName("");
			partnerVO.setAccountImgName("");
			
			
			// 파트너 등록
			partnerMapper.insertDriver(partnerVO);
			
			isStatus = true;
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => insertDriver() ==========");
			return isStatus;
		}
		
		return isStatus;
	}
	
	// 파트너 상세 팝업
	public List<partnerVO> selectPartnerDetail(partnerVO partnerVO) {
		List<partnerVO> partnerDetail = null;
		
		try {
			// 파트너 상세 정보
			partnerDetail =  partnerMapper.selectPartnerDetail(partnerVO);

			if ("".equals(partnerDetail.get(0).getProfileImgName()) /* || partnerDetail.get(0).getProfileImgName().equals(null) */) {
				partnerDetail.get(0).setProfileImgName("/images/egovframework/com/adm/partner_photo.png");
			} else {
				partnerDetail.get(0).setProfileImgName("http://manage.mosiler.com/AttachFile/Profile//"+partnerDetail.get(0).getProfileImgName());
			}
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => selectPartnerDetail() ==========");
			return null;
		}

		return partnerDetail;
	}

	// // 파트너 상세 - 운행내역 Count
	public int selectDrivingDetailCnt(partnerVO partnerVO) {
		int drivingDetailCnt = 0;
		
		try {
			drivingDetailCnt = partnerMapper.selectDrivingDetailCnt(partnerVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => selectDrivingDetailCnt() ==========");
			return 0;
		}
		
		return drivingDetailCnt;
	}
	
	// 파트너 상세 - 운행내역
	public List<partnerVO> selectDrivingDetail(partnerVO partnerVO, int start, int end) {
		List<partnerVO> drivingDetail = null;
		
		try {
			// 페이징 세팅
			partnerVO.setStart(start);
			partnerVO.setEnd(end);
			
			// 파트너 상세 - 운행내역
			List<partnerVO> temp =  partnerMapper.selectDrivingDetail(partnerVO);
			// 시간 계산해서 넘겨줄 List
			drivingDetail = new ArrayList<partnerVO>();
			
			// 예약 시간 분 단위 계산
			for (partnerVO vo : temp) {
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
				
				// 예약 시작 시간
				Date timeStamp1 = sf.parse(vo.getRevStartDt());
				long startTime = timeStamp1.getTime();
				
				// 예약 종료 시간
				Date timeStamp2 = sf.parse(vo.getRevEndDt());
				long endTime = timeStamp2.getTime();
				
				// 시간 차
				long mills = endTime - startTime;
				long min = mills/60000;
				
				vo.setTimeStamp(min);
				
				drivingDetail.add(vo);
			}
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => selectDrivingDetail() ==========");
			return null;
		}

		return drivingDetail;
	}

	// 파트너 상세 - 상담내역 Count
	public int selectCsDetailCnt(partnerVO partnerVO) {
		int csDetailCnt = 0;
		
		try {
			csDetailCnt = partnerMapper.selectCsDetailCnt(partnerVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => selectCsDetailCnt() ==========");
			return 0;
		}
		
		return csDetailCnt;
	}
	// 파트너 상세 - 상담내역
	public List<partnerVO> selectCsDetail(partnerVO partnerVO, int start, int end) {
		List<partnerVO> csDetail = null;
		
		try {
			// 페이징 세팅
			partnerVO.setStart(start);
			partnerVO.setEnd(end);
			
			// 파트너 상세 - 상담내역
			csDetail =  partnerMapper.selectCsDetail(partnerVO);

		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => selectCsDetail() ==========");
			return null;
		}

		return csDetail;
	}

	// 파트너 상세 - 교육내역 Count
	public int selectDriverClassDetailCnt(partnerVO partnerVO) {
		int driverClassDetailCnt = 0;
		
		try {
			driverClassDetailCnt = partnerMapper.selectDriverClassDetailCnt(partnerVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => selectDriverClassDetailCnt() ==========");
			return 0;
		}
		
		return driverClassDetailCnt;
	}
	
	// 파트너 상세 - 교육내역
	public List<partnerVO> selectDriverClassDetail(partnerVO partnerVO, int start, int end) {
		List<partnerVO> driverClassDetail = null;
		
		try {
			// 페이징 세팅
			partnerVO.setStart(start);
			partnerVO.setEnd(end);
			
			// 파트너 상세 - 교육내역
			driverClassDetail =  partnerMapper.selectDriverClassDetail(partnerVO);

		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => driverClassDetail() ==========");
			return null;
		}

		return driverClassDetail;
	}

	// 파트너 상세 - 평가 Count
	public int selectDriverEvaluationDetailCnt(partnerVO partnerVO) {
		int driverEvaluationDetailCnt = 0;
		
		try {
			driverEvaluationDetailCnt = partnerMapper.selectDriverEvaluationDetailCnt(partnerVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => selectDriverEvaluationDetailCnt() ==========");
			return 0;
		}
		
		return driverEvaluationDetailCnt;
	}
	
	// 파트너 상세 - 평가
	public List<partnerVO> selectDriverEvaluationDetail(partnerVO partnerVO, int start, int end) {
		List<partnerVO> driverEvaluationDetail = null;
		
		try {
			// 페이징 세팅
			partnerVO.setStart(start);
			partnerVO.setEnd(end);
			
			// 파트너 상세 - 평가
			driverEvaluationDetail =  partnerMapper.selectDriverEvaluationDetail(partnerVO);

		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => selectDriverEvaluationDetail() ==========");
			return null;
		}

		return driverEvaluationDetail;
	}

	// 파트너 상세 - 물품이력 Count
	public int selectDriverIssuedDetailCnt(partnerVO partnerVO) {
		int driverIssuedDetailCnt = 0;
		
		try {
			driverIssuedDetailCnt = partnerMapper.selectDriverIssuedDetailCnt(partnerVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => selectDriverIssuedDetailCnt() ==========");
			return 0;
		}
		
		return driverIssuedDetailCnt;
	}
	
	// 파트너 상세 - 물품이력
	public List<partnerVO> selectDriverIssuedDetail(partnerVO partnerVO, int start, int end) {
		List<partnerVO> driverIssuedDetail = null;
		
		try {
			// 페이징 세팅
			partnerVO.setStart(start);
			partnerVO.setEnd(end);
			
			// 파트너 상세 - 물품이력
			driverIssuedDetail =  partnerMapper.selectDriverIssuedDetail(partnerVO);

		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => selectDriverIssuedDetail() ==========");
			return null;
		}

		return driverIssuedDetail;
	}
	
	// 파트너 상세 - 사고이력 Count
	public int selectDriverAccdentDetailCnt(partnerVO partnerVO) {
		int driverAccdentDetailCnt = 0;
		
		try {
			driverAccdentDetailCnt = partnerMapper.selectDriverAccdentDetailCnt(partnerVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => selectDriverAccdentDetailCnt() ==========");
			return 0;
		}
		
		return driverAccdentDetailCnt;
	}

	// 파트너 상세 - 사고이력
	public List<partnerVO> selectDriverAccdentDetail(partnerVO partnerVO, int start, int end) {
		List<partnerVO> driverAccdentDetail = null;
		
		try {
			// 페이징 세팅
			partnerVO.setStart(start);
			partnerVO.setEnd(end);
			
			// 파트너 상세 - 사고이력
			driverAccdentDetail =  partnerMapper.selectDriverAccdentDetail(partnerVO);

		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => selectDriverAccdentDetail() ==========");
			return null;
		}

		return driverAccdentDetail;
	}

	// 파트너 상세 - 교통위반 이력 Count
	public int selectDriverViolationDetailCnt(partnerVO partnerVO) {
		int driverViolationDetailCnt = 0;
		
		try {
			driverViolationDetailCnt = partnerMapper.selectDriverViolationDetailCnt(partnerVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => selectDriverViolationDetailCnt() ==========");
			return 0;
		}
		
		return driverViolationDetailCnt;
	}
	
	// 파트너 상세 - 교통위반 이력
	public List<partnerVO> selectDriverViolationDetail(partnerVO partnerVO, int start, int end) {
		List<partnerVO> driverViolationDetail = null;
		
		try {
			// 페이징 세팅
			partnerVO.setStart(start);
			partnerVO.setEnd(end);
			
			// 파트너 상세 - 사고이력
			driverViolationDetail =  partnerMapper.selectDriverViolationDetail(partnerVO);

		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => selectDriverViolationDetail() ==========");
			return null;
		}

		return driverViolationDetail;
	}
}
