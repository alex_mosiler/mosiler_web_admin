package com.mosiler.web.admin.service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mosiler.web.admin.domain.mainVO;
import com.mosiler.web.admin.domain.systemVO;
import com.mosiler.web.admin.mapper.main.mainMapper;
import com.mosiler.web.admin.mapper.system.systemMapper;
import com.mosiler.web.admin.util.adminUtil;

@Service
public class mainService {

	private final Logger logger = LogManager.getLogger(mainService.class);
	String className = "mainService";
	
	@Autowired
	private systemMapper systemMapper; 
	
	@Autowired
	private mainMapper mainMapper;

	// 존재하는 유저인지 Check
	public int checkLoginUser(HttpServletRequest request) {
		int userCnt = 0;
		mainVO mainVO = new mainVO();
		
		try {
			// pwd 암호화
			String pwd = adminUtil.convertSHA(request.getParameter("pwd"));
			
			if(pwd != null) {
				mainVO.setId(request.getParameter("id"));
				mainVO.setPwd(pwd);
		
				// 유저 Check (있으면 1)
				userCnt = mainMapper.selectAdminUser(mainVO);
			}
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => checkLoginUser() ==========");
			return 0;
		}
		
		return userCnt;
	}

	// OTP 키 비교
	public boolean compareOtpKey(HttpServletRequest request) {
		boolean isStatus = false;
		
		try {
			systemVO systemVO = new systemVO();
			
			// pwd 암호화
			String pwd = adminUtil.convertSHA(request.getParameter("pwd"));
			
			// 입력 받은 id, pw 세팅
			systemVO.setUserPw(pwd);			
			systemVO.setUserId(request.getParameter("id"));
			
			// 기존 OTP 키 조회
			String encodedKey = systemMapper.getOtpKey(systemVO);
			
			// 없으면 false
			if(encodedKey == null || "".equals(encodedKey)) {
				return isStatus;
			}
			// 있으면 비교
			else {
				String otpKey = request.getParameter("otpKey");
				long user_code = Integer.parseInt(otpKey);
				long time = new Date().getTime() / 30000;
				
				try {
		        	// 키, 코드, 시간으로 일회용 비밀번호가 맞는지 일치 여부 확인.
		        	boolean check_code = adminUtil.checkCode(encodedKey, user_code, time);
		        	
		        	if(check_code)
		        		isStatus = true;
				} catch (Exception e) {
					logger.debug(" ==========" + this.className + " => compareOtpKey() ==========");
					return isStatus;
		        }
			}			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => compareOtpKey() ==========");
			return false;
		}
		
		return isStatus;
	}
}
