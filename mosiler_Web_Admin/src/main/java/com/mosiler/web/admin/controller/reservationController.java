package com.mosiler.web.admin.controller;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mosiler.web.admin.domain.codeVO;
import com.mosiler.web.admin.domain.pagingVO;
import com.mosiler.web.admin.domain.reservationVO;
import com.mosiler.web.admin.service.codeService;
import com.mosiler.web.admin.service.reservationService;
import com.mosiler.web.admin.util.adminUtil;
import com.mosiler.web.admin.util.validation;

import net.sf.json.JSONArray;

@Controller
@RequestMapping("/reservation")
public class reservationController {

	@Autowired
	private menuController menuController;
	
	@Autowired
	private reservationService reservationService;
	
	@Autowired
	private codeService codeService;
	
	/**
	 * 전체 예약 현황
	 * @param 
	 * @return 
	 */
	@RequestMapping(value="/reservationList")
	public ModelAndView reservationList(Model model, pagingVO pagingVO, reservationVO reservationVO, HttpServletRequest request) throws ParseException {
		ModelAndView mav = new ModelAndView();
		
		int cnt = 0;
		
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);

		// 현재 페이지 번호 세팅
		String nowPage = request.getParameter("nowPage");
		String cntPerPage = request.getParameter("cntPerPage");
		
		// 배차 상태 구분 공통 코드
		List<codeVO> codeList1 = codeService.getDrivingStatusCodeList();
		
		// 운행 구분 공통 코드
		List<codeVO> codeList2 = codeService.getDrivingTypeCodeList();
		
		// VO Setting
		reservationVO = validation.reservationValidation(request, nowPage, cntPerPage);
		
		// 예약 현황 
		List<reservationVO> list = reservationService.selectReservationList(request, reservationVO);
		
		if(list.size() != 0) 
			cnt = list.get(0).getTotCnt();
		
		// 페이징 계산
		pagingVO = new pagingVO(cnt, reservationVO.getPage(), reservationVO.getPageSize());
		
		if(reservationVO.getStartDt().substring(0, 10).equals("2000-01-01")) {
			reservationVO.setStartDt("");
		}
		
		if(reservationVO.getEndDt().substring(0, 10).equals("2999-12-31")) {
			reservationVO.setEndDt("");
		}
		
		JSONArray reservationList = JSONArray.fromObject(list);
		
		mav.addObject("cateList", menuModel.getAttribute("cateList"));
		mav.addObject("depthList",menuModel.getAttribute("depthList"));
		mav.addObject("codeList1", codeList1);
		mav.addObject("codeList2", codeList2);
		mav.addObject("reservationList", reservationList);
		mav.addObject(pagingVO);
		mav.addObject(reservationVO);
		
		return mav;
	}
	
	/**
	 * 예약 현황 상세 팝업
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/reservationDetailpopUp")
	public String reservationDetailpopUp(Model model, reservationVO reservationVO, @RequestParam(value="drivingNo", required = false) int drivingNo) throws ParseException {
		
		// drivingNo 세팅
		reservationVO.setDrivingNo(drivingNo);
		
		// 할인율 구분 공통 코드
		List<codeVO> hourPriceCodeList = codeService.getHourPriceCodeList();
		
		// 운행 구분 공통 코드
		List<codeVO> codeList = codeService.getDrivingTypeCodeList();
		
		// 결제 수단 구분 공통 코드
		List<codeVO> paymentTypeCodeList = codeService.getPaymentTypeCodeList();
		
		// 컨시어지 구분 공통 코드
		List<codeVO> conciergeCodeList = codeService.getConciergeCodeList();
		
		// 예약 상세 정보
		List<reservationVO> reservationDetail = reservationService.selectReservationDetail(reservationVO);
		
		// 천 단위 콤마
		String hourPrice = adminUtil.toNumFormat(reservationDetail.get(0).getHourPrice());
		
		// 콤마 찍힌 hourPrice VO 세팅
		reservationVO.setHourPrice1(hourPrice);
		
		// customerNo VO 세팅
		reservationVO.setCustomerNo(reservationDetail.get(0).getCustomerNo());
		
		// 경유지 정보 (TB_DRVG_STOP_AREA)
		List<reservationVO> reservationWayPoint = reservationService.selectReservationWayPoint(reservationVO);
		
		// driverNo VO 세팅
		reservationVO.setDriverNo(reservationDetail.get(0).getDriverNo());
		
		// 파트너 등급
		String partnerLv = reservationService.selectPartnerLV(reservationVO);
		
		// partnerLv VO 세팅
		reservationVO.setPartnerLv(partnerLv);
		
		// 해당 운행의 컨시어지 조회
		List<reservationVO> concierge = reservationService.selectConcierge(reservationVO);
		JSONArray conciergeObject = JSONArray.fromObject(concierge);
		
		model.addAttribute("hourPriceCodeList", hourPriceCodeList);
		model.addAttribute("paymentTypeCodeList", paymentTypeCodeList);
		model.addAttribute("conciergeCodeList", conciergeCodeList);
		model.addAttribute("reservationDetail", reservationDetail);
		model.addAttribute("reservationWayPoint", reservationWayPoint);
		model.addAttribute("reservationVO", reservationVO);
		model.addAttribute("customerNo", reservationVO.getCustomerNo());
		model.addAttribute("conciergeObject", conciergeObject);
		model.addAttribute("codeList", codeList);
		
		return "/reservation/popup/reservationDetail-popUp";
	}
	
	/**
	 * 예약 등록 (고객 상세 팝업에서)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/insertReservation", method = RequestMethod.POST)
	@ResponseBody
	public int insertReservation(Model model, HttpServletRequest request, reservationVO reservationVO) {
		
		int resultCode;
		
		boolean isStatus = reservationService.insertReservation(request, reservationVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 예약 수정
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/updateReservation", method = RequestMethod.POST)
	@ResponseBody
	public int updateReservation(Model model, HttpServletRequest request, reservationVO reservationVO) {
		
		int resultCode;
		
		boolean isStatus = reservationService.updateReservation(request, reservationVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 예약 취소
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/cancelReservation", method = RequestMethod.POST)
	@ResponseBody
	public int cancelReservation(Model model, HttpServletRequest request, reservationVO reservationVO) {
		
		int resultCode;
		
		boolean isStatus = reservationService.cancelReservation(request, reservationVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 예약 등록 팝업 오픈 (예약 현황 화면에서)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/reservationRegistPopUp")
	public String reservationRegistPopUp(Model model) {
		
		// 운행 구분 공통 코드
		List<codeVO> codeList = codeService.getDrivingTypeCodeList();
		
		// 컨시어지 구분 공통 코드
		List<codeVO> conciergeCodeList = codeService.getConciergeCodeList();
		
		// 할인율 구분 공통 코드
		List<codeVO> hourPriceCodeList = codeService.getHourPriceCodeList();
		
		// 결제 수단 구분 공통 코드
		List<codeVO> paymentTypeCodeList = codeService.getPaymentTypeCodeList();
		
		model.addAttribute("codeList", codeList);
		model.addAttribute("conciergeCodeList", conciergeCodeList);
		model.addAttribute("hourPriceCodeList", hourPriceCodeList);
		model.addAttribute("paymentTypeCodeList", paymentTypeCodeList);
		
		return "/reservation/popup/reservationRegist-popUp";
	}
	
}
