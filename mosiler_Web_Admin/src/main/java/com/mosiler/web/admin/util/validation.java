package com.mosiler.web.admin.util;

import javax.servlet.http.HttpServletRequest;

import com.mosiler.web.admin.domain.partnerVO;
import com.mosiler.web.admin.domain.reservationVO;

public class validation {
	/**
	 * reservationVO Setting
	 * @param cntPerPage 
	 * @param nowPage 
	 * @param reservationVO
	 * @return reservationVO
	 */
	public static reservationVO reservationValidation(HttpServletRequest request, String nowPage, String cntPerPage) {
		reservationVO reservationVO = new reservationVO();
		
		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		reservationVO.setPage(Integer.parseInt(pageArray[0]));
		reservationVO.setPageSize(Integer.parseInt(pageArray[1]));
		
		// 프로시저에 맞게 파라미터 세팅
		// 배차 상태
		if(request.getParameter("drivingStatus") != null && request.getParameter("drivingStatus") != "") {
			reservationVO.setState(request.getParameter("drivingStatus"));
		} else {
			reservationVO.setState("");
		}
		// 운행 구분
		if(request.getParameter("drivingType") != null && request.getParameter("drivingType") != "") {
			reservationVO.setDrivingType(request.getParameter("drivingType"));
		} else {
			reservationVO.setDrivingType("");
		}
		// 검색 select box
		if(request.getParameter("searchType1") != null && request.getParameter("searchType1") != "") {
			reservationVO.setGubun(request.getParameter("searchType1"));
		} else {
			reservationVO.setGubun("");
		} 
		// 검색 keyword
		if(request.getParameter("searchKeyword1") != null && request.getParameter("searchKeyword1") != "") {
			reservationVO.setKeyword(request.getParameter("searchKeyword1"));
		} else {
			reservationVO.setKeyword("");
		}
		//  위치 select box
		if(request.getParameter("searchType2") != null && request.getParameter("searchType2") != "") {
			reservationVO.setAddress(request.getParameter("searchType2"));
		} else {
			reservationVO.setAddress("");
		}
		// 위치 keyword
		if(request.getParameter("searchKeyword2") != null && request.getParameter("searchKeyword2") != "") {
			reservationVO.setKeywordAddress(request.getParameter("searchKeyword2"));
		} else {
			reservationVO.setKeywordAddress("");
		}
		// 예약 시작 시간
		if(request.getParameter("hiddenStartDate") != null && request.getParameter("hiddenStartDate") != "") {
			reservationVO.setStartDt(request.getParameter("hiddenStartDate"));
		} else {
			reservationVO.setStartDt("2000-01-01");
		}
		// 예약 종료 시간
		if(request.getParameter("hiddenEndDate") != null && request.getParameter("hiddenEndDate") != "") {
			reservationVO.setEndDt(request.getParameter("hiddenEndDate"));
		} else {
			reservationVO.setEndDt("2999-12-31");
		}
		// 그 외 나머지
		reservationVO.setSort("startdt_desc");
		reservationVO.setIsRequestAirService("");
		
		return reservationVO;
	}

	/**
	 * partnerVO Setting
	 * @param cntPerPage 
	 * @param nowPage 
	 * @param partnerVO
	 * @return partnerVO
	 */
	public static partnerVO partnerValidation(HttpServletRequest request, String nowPage, String cntPerPage) {
		partnerVO partnerVO = new partnerVO();
		
		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		partnerVO.setPage(Integer.parseInt(pageArray[0]));
		partnerVO.setPageSize(Integer.parseInt(pageArray[1]));
		
		// 검색 keyword
		if(request.getParameter("searchKeyword") != null && request.getParameter("searchKeyword") != "") {
			partnerVO.setKeyword(request.getParameter("searchKeyword"));
		} else {
			partnerVO.setKeyword("");
		}
		// 검색 select box
		if(request.getParameter("searchType") != null && request.getParameter("searchType") != "") {
			partnerVO.setGubun(request.getParameter("searchType"));
		} else {
			partnerVO.setGubun("");
		} 
		// 드라이버 상태 select box
		if(request.getParameter("partnerStatus") != null && request.getParameter("partnerStatus") != "") {
			partnerVO.setState(request.getParameter("partnerStatus"));
		} else {
			partnerVO.setState("");
		}
		// 일반, 타다, 전부
		if(request.getParameter("permitStatus") != null && request.getParameter("permitStatus") != "") {
			partnerVO.setPermitStatus(request.getParameter("permitStatus"));
		} else {
			partnerVO.setPermitStatus("");
		}

		return partnerVO;
	}
}
