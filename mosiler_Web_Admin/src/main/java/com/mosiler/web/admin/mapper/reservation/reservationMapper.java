package com.mosiler.web.admin.mapper.reservation;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.mosiler.web.admin.domain.reservationVO;

@Repository("com.mosiler.web.admin.mapper.reservation.reservationMapper")
@Mapper
public interface reservationMapper {

	void insertReservation(reservationVO reservationVO);
	void insertWayPoint(reservationVO reservationVO);
	int selectDrivingNo(reservationVO reservationVO);
	void insertConcierge(reservationVO reservationVO);
	List<reservationVO> selectConcierge(reservationVO reservationVO);
	List<reservationVO> selectReservationList(reservationVO reservationVO);
	List<reservationVO> selectReservationDetail(reservationVO reservationVO);
	List<reservationVO> selectReservationWayPoint(reservationVO reservationVO);
	String selectPartnerLV(reservationVO reservationVO);
	void updateWayPoint(reservationVO reservationVO);
	void deleteConcierge(reservationVO reservationVO);
	void updateReservation(reservationVO reservationVO);
	void cancelReservation(reservationVO reservationVO);
	
}
