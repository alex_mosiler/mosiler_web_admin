package com.mosiler.web.admin.mapper.customer;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.mosiler.web.admin.domain.customerVO;

@Repository("com.mosiler.web.admin.mapper.customer.customerMapper")
@Mapper
public interface customerMapper {

	int customerListCnt(customerVO customerVO);
	List<customerVO> customerList(customerVO customerVO);
	List<customerVO> useInfo(customerVO customerVO);
	List<customerVO> customerInfo(customerVO customerVO);
	List<customerVO> customerUseInfo(customerVO customerVO);
	int customerUseInfoCnt(customerVO customerVO);
	int customerCouponListCnt(customerVO customerVO);
	List<customerVO> customerCouponList(customerVO customerVO);
	int applyServiceUserCnt(customerVO customerVO);
	List<customerVO> applyServiceUserList(customerVO customerVO);
	List<customerVO> applyServiceDetail(customerVO customerVO);
	void updateApplyServiceDetail(customerVO customerVO);
	int corporationStatusCnt(customerVO customerVO);
	List<customerVO> corporationStatusList(customerVO customerVO);
	List<customerVO> corporationStatusDetail(customerVO customerVO);
	void updateCorporationStatus(customerVO customerVO);
	void insertCorporationStatus(customerVO customerVO);
	int promotionListCnt(customerVO customerVO);
	List<customerVO> promotionList(customerVO customerVO);
	List<customerVO> promotionDetail(customerVO customerVO);
	List<customerVO> promotionRule(customerVO customerVO);
	List<customerVO> promotionCoupon(customerVO customerVO);
	int promotionRuleCnt(customerVO customerVO);
	int promotionCouponCnt(customerVO customerVO);
	void updatePromotion(customerVO customerVO);
	void insertPromotion(customerVO customerVO);
	void insertPromotionRule(customerVO customerVO);
	List<customerVO> ruleDetail(customerVO customerVO);
	void updatePromotionRule(customerVO customerVO);
	void cancelPromotionRule(customerVO customerVO);
	void cancelPromotion(customerVO customerVO);
	List<customerVO> getRuleList(customerVO customerVO);
	void insertPromotionCoupon(customerVO customerVO);
	List<customerVO> couponDetail(customerVO customerVO);
	void cancelPromotionCoupon(customerVO customerVO);
	void updatePromotionCoupom(customerVO customerVO);

}
