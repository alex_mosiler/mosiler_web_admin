package com.mosiler.web.admin.mapper.code;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.mosiler.web.admin.domain.codeVO;

@Repository("com.mosiler.web.admin.mapper.code.codeMapper")
@Mapper
public interface codeMapper {

	List<codeVO> getDrivingStatusCodeList();
	List<codeVO> getDrivingTypeCodeList();
	List<codeVO> getConciergeCodeList();
	List<codeVO> getHourPriceCodeList();
	List<codeVO> getPaymentTypeCodeList();
	List<codeVO> getPromotionTypeList();
	List<codeVO> getUserTargetTypeList();
	List<codeVO> getPromotionStatusList();
	List<codeVO> getConditionTypeList();
	List<codeVO> getPartnerStatusCodeList();
	List<codeVO> getPermitStatusCodeList();
	List<codeVO> getBankCodeList();
	List<codeVO> getBizCompList();
	
}
