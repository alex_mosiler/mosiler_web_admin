package com.mosiler.web.admin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mosiler.web.admin.domain.menuVO;
import com.mosiler.web.admin.mapper.menu.menuMapper;

@Service
public class menuService {
	
	@Autowired 
	private menuMapper menuMapper;
	
	// 상위 메뉴
	public List<menuVO> cateList() {
		return menuMapper.cateList();
	}

	// 하위 메뉴
	public List<menuVO> depthList() {
		return menuMapper.depthList();
	}
}
