package com.mosiler.web.admin.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.mosiler.web.admin.domain.reservationVO;
import com.mosiler.web.admin.mapper.reservation.reservationMapper;

@Service
public class reservationService {
	
	private final Logger logger = LogManager.getLogger(reservationService.class);
	String className = "reservationService";
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
	reservationMapper reservationMapper;

	// 예약 등록
	public boolean insertReservation(HttpServletRequest request, reservationVO reservationVO) {
		boolean isStatus = false;
		
		DefaultTransactionDefinition DTD = new DefaultTransactionDefinition();
		DTD.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus transactionStatus = transactionManager.getTransaction(DTD);
	
		int length = 0; 			
		int payServiceFee = 0;
		
		try {
			reservationVO.setBizCompNo(Integer.parseInt(request.getParameter("bizCompNo")));
			reservationVO.setCustomerNo(Integer.parseInt(request.getParameter("customerNo")));
			reservationVO.setCustomerName(request.getParameter("customerName"));
			reservationVO.setCustomerPhoneNo(request.getParameter("customerPhoneNumber"));
			reservationVO.setDrivingType(request.getParameter("drivingType"));
			reservationVO.setInsuranceYn(request.getParameter("insuranceYn"));
			reservationVO.setRevStartDt(request.getParameter("revStartDt"));
			reservationVO.setRevEndDt(request.getParameter("revEndDt"));
			reservationVO.setPaymentType(request.getParameter("paymentType"));
			reservationVO.setCouponCnt(Integer.parseInt(request.getParameter("couponCnt")));
			reservationVO.setHourPrice(Integer.parseInt(request.getParameter("hourPrice")));
			reservationVO.setMosilerComment(request.getParameter("adminRequest"));
			reservationVO.setCustomerComment(request.getParameter("customerRequest"));
			reservationVO.setDriverNo(Integer.parseInt(request.getParameter("driverNo")));
			reservationVO.setPassenger(request.getParameter("ridingName"));
			reservationVO.setPassengerPhoneNo(request.getParameter("ridingPhoneNumber"));
			reservationVO.setStartAddress(request.getParameter("address1"));
			reservationVO.setStartLat(request.getParameter("lat1"));
			reservationVO.setStartLng(request.getParameter("lng1"));
			reservationVO.setEndAddress(request.getParameter("address4"));
			reservationVO.setEndLat(request.getParameter("lat4"));
			reservationVO.setEndLng(request.getParameter("lng4"));
			reservationVO.setRevStartAddress(request.getParameter("address1"));
			reservationVO.setRevStartLat(request.getParameter("lat1"));
			reservationVO.setRevStartLng(request.getParameter("lng1"));
			reservationVO.setRevEndAddress(request.getParameter("address4"));
			reservationVO.setRevEndLat(request.getParameter("lat4"));
			reservationVO.setRevEndLng(request.getParameter("lng4"));
			reservationVO.setWayPointAddress(request.getParameter("address2"));
			reservationVO.setWayPointLat(request.getParameter("lat2"));
			reservationVO.setWayPointLng(request.getParameter("lng2"));
			reservationVO.setWayPointAddress01(request.getParameter("address2"));
			reservationVO.setWayPointLat01(request.getParameter("lat2"));
			reservationVO.setWayPointLng01(request.getParameter("lng2"));
			reservationVO.setWayPointAddress02(request.getParameter("address3"));
			reservationVO.setWayPointLat02(request.getParameter("lat3"));
			reservationVO.setWayPointLng02(request.getParameter("lng3"));
			reservationVO.setConciergeCodeList(request.getParameterValues("conciergeCodeList"));
			reservationVO.setPaidTicketYn("N");
			
			if(reservationVO.getConciergeCodeList() != null) {
				length = reservationVO.getConciergeCodeList().length;
				payServiceFee = length * 10000;
			}
			
			// 부가 서비스 금액 
			reservationVO.setPayServiceFee(payServiceFee);

			// 1. 예약 등록
			reservationMapper.insertReservation(reservationVO);
			
			// drivingNo 채번
			int drivingNo = reservationMapper.selectDrivingNo(reservationVO);
			reservationVO.setDrivingNo(drivingNo);
			
			// 2. 경유지 등록 (경유지 있을 경우)
			if(reservationVO.getWayPointAddress01() != "" || reservationVO.getWayPointAddress02() != "") {
				reservationMapper.insertWayPoint(reservationVO);
			}
			
			// 3. 부가 서비스 등록 (부가서비스 있을 경우)
			if(length != 0) {
				for(int i = 0; i < length; i++) {
					reservationVO.setPayServiceNo(Integer.parseInt(reservationVO.getConciergeCodeList()[i]));
					reservationMapper.insertConcierge(reservationVO);
				}
			}
			
			// Commit
			transactionManager.commit(transactionStatus);
			isStatus = true;		
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => insertReservation() ==========");
			
			// Rollback
			transactionManager.rollback(transactionStatus);
			return isStatus;
		}
		
		return isStatus;
	}
	
	//  예약 수정
	public boolean updateReservation(HttpServletRequest request, reservationVO reservationVO) {
		boolean isStatus = false;
		
		DefaultTransactionDefinition DTD = new DefaultTransactionDefinition();
		DTD.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus transactionStatus = transactionManager.getTransaction(DTD);
		
		int length = 0; 			
		int payServiceFee = 0;
		
		try {
			reservationVO.setCustomerNo(Integer.parseInt(request.getParameter("customerNo")));
			reservationVO.setCustomerName(request.getParameter("customerName"));
			reservationVO.setCustomerPhoneNo(request.getParameter("customerPhoneNo"));
			reservationVO.setDrivingType(request.getParameter("drivingType"));
			reservationVO.setRevStartDt(request.getParameter("revStartDt"));
			reservationVO.setRevEndDt(request.getParameter("revEndDt"));
			reservationVO.setStartAddress(request.getParameter("address1"));
			reservationVO.setStartLat(request.getParameter("lat1"));
			reservationVO.setStartLng(request.getParameter("lng1"));
			reservationVO.setEndAddress(request.getParameter("address4"));
			reservationVO.setEndLat(request.getParameter("lat4"));
			reservationVO.setEndLng(request.getParameter("lng4"));
			reservationVO.setRevEndAddress(request.getParameter("address4"));
			reservationVO.setRevEndLat(request.getParameter("lat4"));
			reservationVO.setRevEndLng(request.getParameter("lng4"));
			reservationVO.setPaymentType(request.getParameter("paymentType"));
			reservationVO.setCouponCnt(Integer.parseInt(request.getParameter("couponCnt")));
			reservationVO.setWayPointAddress(request.getParameter("address2"));
			reservationVO.setWayPointLat(request.getParameter("lat2"));
			reservationVO.setWayPointLng(request.getParameter("lng2"));
			reservationVO.setWayPointAddress01(request.getParameter("address2"));
			reservationVO.setWayPointLat01(request.getParameter("lat2"));
			reservationVO.setWayPointLng01(request.getParameter("lng2"));
			reservationVO.setWayPointAddress02(request.getParameter("address3"));
			reservationVO.setWayPointLat02(request.getParameter("lat3"));
			reservationVO.setWayPointLng02(request.getParameter("lng3"));
			reservationVO.setMosilerComment(request.getParameter("mosilerComment"));
			reservationVO.setCustomerComment(request.getParameter("customerComment"));
			reservationVO.setDriverNo(Integer.parseInt(request.getParameter("driverNo")));
			reservationVO.setPassenger(request.getParameter("passenger"));
			reservationVO.setPassengerPhoneNo(request.getParameter("passengerPhoneNo"));
			reservationVO.setHourPrice(Integer.parseInt(request.getParameter("hourPrice")));
			reservationVO.setPaidTicketYn("N");
			reservationVO.setBizCompNo(Integer.parseInt(request.getParameter("bizCompNo")));
			reservationVO.setDrivingNo(Integer.parseInt(request.getParameter("drivingNo")));
			reservationVO.setConciergeCodeList(request.getParameterValues("conciergeCodeList"));
			
			if(reservationVO.getConciergeCodeList() != null) {
				length = reservationVO.getConciergeCodeList().length;
				payServiceFee = length * 10000;
			}
			
			// 부가 서비스 금액 
			reservationVO.setPayServiceFee(payServiceFee);

			// 1. 예약 수정
			reservationMapper.updateReservation(reservationVO);
			
			// 2. 경유지 수정
			reservationMapper.updateWayPoint(reservationVO);
			
			// 3. 기존 부가 서비스 삭제
			reservationMapper.deleteConcierge(reservationVO);
			
			// 4. 부가 서비스 재 추가 
			if(length != 0) {
				for(int i = 0; i < length; i++) {
					reservationVO.setPayServiceNo(Integer.parseInt(reservationVO.getConciergeCodeList()[i]));
					reservationMapper.insertConcierge(reservationVO);
				}
			}
			
			// Commit
			transactionManager.commit(transactionStatus);
			isStatus = true;		
		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => updateReservation() ==========");
			
			// Rollback
			transactionManager.rollback(transactionStatus);
			return isStatus;
		}
		
		return isStatus;
	}

	// 예약 취소
	public boolean cancelReservation(HttpServletRequest request, reservationVO reservationVO) {
		boolean isStatus = false;
		
		try {
			String drivingNo = request.getParameter("drivingNo");
			String regId = request.getParameter("regId");
			
			reservationVO.setDrivingNo(Integer.parseInt(drivingNo));
			reservationVO.setRegId(Integer.parseInt(regId));
			
			reservationMapper.cancelReservation(reservationVO);
			
			isStatus = true;
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => cancelReservation() ==========");
			return isStatus;
		}
		
		return isStatus;
	}
	
	// 예약 현황
	public List<reservationVO> selectReservationList(HttpServletRequest request, reservationVO reservationVO) throws ParseException {
		List<reservationVO> reservationList = null;
		
		try {
			// 예약 현황
			List<reservationVO> temp = reservationMapper.selectReservationList(reservationVO);
			// 시간 계산해서 넘겨줄 List
			reservationList = new ArrayList<reservationVO>();
			
			// 예약 시간 분 단위 계산
			for (reservationVO vo : temp) {
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
				
				// 예약 시작 시간
				Date timeStamp1 = sf.parse(vo.getRevStartDt());
				long startTime = timeStamp1.getTime();
				
				// 예약 종료 시간
				Date timeStamp2 = sf.parse(vo.getRevEndDt());
				long endTime = timeStamp2.getTime();
				
				// 시간 차
				long mills = endTime - startTime;
				long min = mills/60000;
				
				vo.setTimeStamp(min);
				
				reservationList.add(vo);
			}
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => selectReservationList() ==========");
			return null;
		}

		return reservationList;
	}

	// 예약 상세 정보
	public List<reservationVO> selectReservationDetail(reservationVO reservationVO) throws ParseException {
		List<reservationVO> reservationDetail = null;
		
		try {
			// 예약 상세 정보
			List<reservationVO> temp =  reservationMapper.selectReservationDetail(reservationVO);
			// 시간 계산해서 넘겨줄 List
			reservationDetail = new ArrayList<reservationVO>();
			
			// 예약 시간 분 단위 계산
			for (reservationVO vo : temp) {
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
				
				// 예약 시작 시간
				Date timeStamp1 = sf.parse(vo.getRevStartDt());
				long startTime = timeStamp1.getTime();
				
				// 예약 종료 시간
				Date timeStamp2 = sf.parse(vo.getRevEndDt());
				long endTime = timeStamp2.getTime();
				
				// 시간 차
				long mills = endTime - startTime;
				long min = mills/60000;
				
				vo.setTimeStamp(min);
				
				reservationDetail.add(vo);
			}
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => selectReservationDetail() ==========");
			return null;
		}

		
		return reservationDetail;
	}

	// 경유지 정보
	public List<reservationVO> selectReservationWayPoint(reservationVO reservationVO) {
		List<reservationVO> selectReservationWayPoint = null;
		
		try {
			selectReservationWayPoint = reservationMapper.selectReservationWayPoint(reservationVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => selectReservationWayPoint() ==========");
			return null;
		}
		
		return selectReservationWayPoint;
	}

	// 파트너 등급
	public String selectPartnerLV(reservationVO reservationVO) {
		String selectPartnerLV = null;
		
		try {
			selectPartnerLV = reservationMapper.selectPartnerLV(reservationVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => selectPartnerLV() ==========");
			return null;
		}
		
		
		return selectPartnerLV;
	}

	// 컨시어지 조회
	public List<reservationVO> selectConcierge(reservationVO reservationVO) {
		List<reservationVO> selectConcierge = null;
		
		try {
			selectConcierge = reservationMapper.selectConcierge(reservationVO);
			
		} catch(Exception e) {
			logger.debug(" ==========" + this.className + " => selectConcierge() ==========");
			return null;
		}
		
		
		return selectConcierge;
	}
}
