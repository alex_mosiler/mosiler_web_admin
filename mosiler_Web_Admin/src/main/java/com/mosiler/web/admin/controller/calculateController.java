package com.mosiler.web.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/calculate")
public class calculateController {

	@Autowired
	private menuController menuController;
	
	/**
	 * 파트너 정산서
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/calculatePartner")
	public String operationList(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/calculate/calculatePartner";
	}
	
	/**
	 * 보증금 공제
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/depositDeduction")
	public String depositDeduction(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/calculate/depositDeduction";
	}
	
	/**
	 * 사고 공제
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/accidentDeduction")
	public String accidentDeduction(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/calculate/accidentDeduction";
	}
	
	/**
	 * 범칙금 공제
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/penaltyDeduction")
	public String penaltyDeduction(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/calculate/penaltyDeduction";
	}
	
	/**
	 * 위반 이력
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/violationHistory")
	public String violationHistory(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/calculate/violationHistory";
	}
	
	/**
	 * 금액 설정
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/setFee")
	public String setFee(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/calculate/setFee";
	}

}
