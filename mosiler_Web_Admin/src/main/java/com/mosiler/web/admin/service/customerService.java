package com.mosiler.web.admin.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mosiler.web.admin.domain.customerVO;
import com.mosiler.web.admin.mapper.customer.customerMapper;

@Service
public class customerService {
	
	private final Logger logger = LogManager.getLogger(customerService.class);
	String className = "customerService";
	
	@Autowired 
	private customerMapper customerMapper;

	// 고객 현황 Count
	public int customerListCnt(customerVO customerVO) {
		int customerListCnt = 0;
		
		try {
			customerListCnt = customerMapper.customerListCnt(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => customerListCnt() ==========");
			return 0;
		}
		
		return customerListCnt;
	}
	
	// 고객 현황
	public List<customerVO> customerList(customerVO customerVO, int start, int end) {
		List<customerVO> customerList = null;
		
		try {
			// 페이징 세팅
			customerVO.setStart(start);
			customerVO.setEnd(end);
			
			customerList = customerMapper.customerList(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => customerList() ==========");
			return null;
		}

		return customerList;
	}

	// 1, 이용금액, 이용건수, 최종 운행일
	public List<customerVO> useInfo(customerVO customerVO) {
		List<customerVO> useInfo = null;
		
		try {
			useInfo = customerMapper.useInfo(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => useInfo() ==========");
			return null;
		}
		
		return useInfo;
	}
	
	// 2. 가입 정보
	public List<customerVO> customerInfo(customerVO customerVO) {
		List<customerVO> customerInfo = null;
		
		try {
			customerInfo = customerMapper.customerInfo(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => customerInfo() ==========");
			return null;
		}
		
		return customerInfo;
	}
	
	// 사용 현황 Count
	public int customerUseInfoCnt(customerVO customerVO) {
		int customerUseInfoCnt = 0;
		
		try {
			customerUseInfoCnt = customerMapper.customerUseInfoCnt(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => customerUseInfoCnt() ==========");
			return 0;
		}
		
		return customerUseInfoCnt;
	}
	
	// 3. 사용 현황
	public List<customerVO> customerUseInfo(customerVO customerVO, int start, int end) {
		List<customerVO> customerUseInfo = null;
		
		try {
			// 페이징 세팅
			customerVO.setStart(start);
			customerVO.setEnd(end);
			
			customerUseInfo = customerMapper.customerUseInfo(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => customerUseInfo() ==========");
			return null;
		}

		return customerUseInfo;
	}
	
	// 쿠폰 현황 Count
	public int customerCouponListCnt(customerVO customerVO) {
		int customerCouponListCnt = 0;
		
		try {
			customerCouponListCnt = customerMapper.customerCouponListCnt(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => customerCouponListCnt() ==========");
			return 0;
		}
		
		return customerCouponListCnt;
	}
	
	// 4. 쿠폰 현황
	public List<customerVO> customerCouponList(customerVO customerVO) {
		List<customerVO> customerCouponList = null;
		
		try {
			customerCouponList = customerMapper.customerCouponList(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => customerCouponList() ==========");
			return null;
		}
		
		return customerCouponList;
	}

	// 서비스 신청자 Count
	public int applyServiceUserCnt(customerVO customerVO) {
		int applyServiceUserCnt = 0;
		
		try {
			applyServiceUserCnt = customerMapper.applyServiceUserCnt(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => applyServiceUserCnt() ==========");
			return 0;
		}

		return applyServiceUserCnt;
	}

	// 서비스 신청자 List
	public List<customerVO> applyServiceUserList(customerVO customerVO, int start, int end) {
		List<customerVO> applyServiceUserList = null;
		
		try {
			// 페이징 세팅
			customerVO.setStart(start);
			customerVO.setEnd(end);
			
			applyServiceUserList = customerMapper.applyServiceUserList(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => applyServiceUserList() ==========");
			return null;
		}
		
		return applyServiceUserList;
	}

	// 서비스 신청자 상세 팝업
	public List<customerVO> applyServiceDetail(customerVO customerVO) {
		List<customerVO> applyServiceDetail = null;
		
		try {
			applyServiceDetail = customerMapper.applyServiceDetail(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => applyServiceDetail() ==========");
			return null;
		}
		
		return applyServiceDetail;
	}

	// 서비스 신청자 내용 업데이트
	public boolean updateApplyServiceDetail(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setId(Integer.parseInt(request.getParameter("id")));
			customerVO.setCustomerName(request.getParameter("userName"));
			customerVO.setAvailTime(request.getParameter("availTime"));
			customerVO.setPhoneNumber(request.getParameter("phoneNumber"));
			customerVO.setStatus(request.getParameter("status"));
			customerVO.setEmail(request.getParameter("email"));
			customerVO.setComment(request.getParameter("comment"));
			
			// 내용 업데이트
			customerMapper.updateApplyServiceDetail(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => updateApplyServiceDetail() ==========");
			return isStatus;
		}
		
		return isStatus;
	}

	// 법인 현황 Count
	public int corporationStatusCnt(customerVO customerVO) {
		int corporationStatusCnt = 0;
		
		try {
			corporationStatusCnt = customerMapper.corporationStatusCnt(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => corporationStatusCnt() ==========");
			return 0;
		}
		
		return corporationStatusCnt;
	}

	// 법인 현황 List
	public List<customerVO> corporationStatusList(customerVO customerVO, int start, int end) {
		List<customerVO> corporationStatusList = null;
		
		try {
			// 페이징 세팅
			customerVO.setStart(start);
			customerVO.setEnd(end);
			
			corporationStatusList = customerMapper.corporationStatusList(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => corporationStatusList() ==========");
			return null;
		}
		
		return corporationStatusList;
	}

	// 법인 현황 상세 팝업
	public List<customerVO> corporationStatusDetail(customerVO customerVO) {
		List<customerVO> corporationStatusDetail = null;
		
		try {
			corporationStatusDetail = customerMapper.corporationStatusDetail(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => corporationStatusDetail() ==========");
			return null;
		}
		
		return corporationStatusDetail;
	}

	// 법인 현황 등록
	public boolean insertCorporationStatus(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setAdminName(request.getParameter("adminName"));
			customerVO.setCompName(request.getParameter("compName"));
			customerVO.setBizNumber(request.getParameter("bizNumber"));
			customerVO.setCeoName(request.getParameter("ceoName"));
			customerVO.setUsePersonCount(Integer.parseInt(request.getParameter("usePersonCount")));
			customerVO.setCompAddress(request.getParameter("compAddress"));			
			customerVO.setContractChargePart(request.getParameter("contractChargePart"));
			customerVO.setContractChargePhoneNo(request.getParameter("contractChargePhoneNo"));
			customerVO.setContractChargeName(request.getParameter("contractChargeName"));
			customerVO.setContractChargeEmail(request.getParameter("contractChargeEmail"));
			customerVO.setPaymentChargePart(request.getParameter("paymentChargePart"));
			customerVO.setPaymentChargePhoneNo(request.getParameter("paymentChargePhoneNo"));
			customerVO.setPaymentChargeName(request.getParameter("paymentChargeName"));
			customerVO.setPaymentChargeEmail(request.getParameter("paymentChargeEmail"));
			customerVO.setTaxReciveEmail(request.getParameter("taxReciveEmail"));
			customerVO.setUseYn(request.getParameter("useYn"));
			customerVO.setPaymentMethod(request.getParameter("paymentMethod"));
			customerVO.setPaymentDay(request.getParameter("paymentDay"));
			customerVO.setHourPrice(Integer.parseInt(request.getParameter("hourPrice")));
			customerVO.setRemark(request.getParameter("reMark"));
			
			// 내용 등록
			customerMapper.insertCorporationStatus(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => insertCorporationStatus() ==========");
			return isStatus;
		}
		
		return isStatus;
	}	
	
	// 법인 현황 업데이트
	public boolean updateCorporationStatus(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setAdminName(request.getParameter("adminName"));
			customerVO.setBizCompNo(Integer.parseInt(request.getParameter("bizCompNo")));
			customerVO.setCompName(request.getParameter("compName"));
			customerVO.setCeoName(request.getParameter("ceoName"));
			customerVO.setUsePersonCount(Integer.parseInt(request.getParameter("usePersonCount")));
			customerVO.setCompAddress(request.getParameter("compAddress"));			
			customerVO.setContractChargePart(request.getParameter("contractChargePart"));
			customerVO.setContractChargePhoneNo(request.getParameter("contractChargePhoneNo"));
			customerVO.setContractChargeName(request.getParameter("contractChargeName"));
			customerVO.setContractChargeEmail(request.getParameter("contractChargeEmail"));
			customerVO.setPaymentChargePart(request.getParameter("paymentChargePart"));
			customerVO.setPaymentChargePhoneNo(request.getParameter("paymentChargePhoneNo"));
			customerVO.setPaymentChargeName(request.getParameter("paymentChargeName"));
			customerVO.setPaymentChargeEmail(request.getParameter("paymentChargeEmail"));
			customerVO.setTaxReciveEmail(request.getParameter("taxReciveEmail"));
			customerVO.setUseYn(request.getParameter("useYn"));
			customerVO.setPaymentMethod(request.getParameter("paymentMethod"));
			customerVO.setPaymentDay(request.getParameter("paymentDay"));
			customerVO.setHourPrice(Integer.parseInt(request.getParameter("hourPrice")));
			customerVO.setRemark(request.getParameter("reMark"));
			
			// 내용 업데이트
			customerMapper.updateCorporationStatus(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => updateCorporationStatus() ==========");
			return isStatus;
		}
		
		return isStatus;
	}

	// 프로모션 Count
	public int promotionListCnt(customerVO customerVO) {
		int promotionListCnt = 0;
		
		try {
			promotionListCnt = customerMapper.promotionListCnt(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => applyServiceUserCnt() ==========");
			return 0;
		}

		return promotionListCnt;
	}

	// 프로모션 List
	public List<customerVO> promotionList(customerVO customerVO, int start, int end) {
		List<customerVO> promotionList = null;
		
		try {
			// 페이징 세팅
			customerVO.setStart(start);
			customerVO.setEnd(end);
			
			promotionList = customerMapper.promotionList(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => corporationStatusList() ==========");
			return null;
		}
		
		return promotionList;
	}

	// 프로모션 상세 팝업
	public List<customerVO> promotionDetail(customerVO customerVO) {
		List<customerVO> promotionDetail = null;
		
		try {
			promotionDetail = customerMapper.promotionDetail(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => promotionDetail() ==========");
			return null;
		}
		
		return promotionDetail;
	}

	// 프로모션 규칙 Count
	public int promotionRuleCnt(customerVO customerVO) {
		int promotionRuleCnt = 0;
		
		try {
			promotionRuleCnt = customerMapper.promotionRuleCnt(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => promotionRuleCnt() ==========");
			return 0;
		}

		return promotionRuleCnt;
	}
	
	// 프로모션 규칙 내역
	public List<customerVO> promotionRule(customerVO customerVO, int start, int end) {
		List<customerVO> promotionRule = null;
		
		try {
			// 페이징 세팅
			customerVO.setStart(start);
			customerVO.setEnd(end);
			
			promotionRule = customerMapper.promotionRule(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => promotionRule() ==========");
			return null;
		}
		
		return promotionRule;
	}

	// 프로모션 쿠폰 내역 Count
	public int promotionCouponCnt(customerVO customerVO) {
		int promotionCouponCnt = 0;
		
		try {
			promotionCouponCnt = customerMapper.promotionCouponCnt(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => promotionCouponCnt() ==========");
			return 0;
		}

		return promotionCouponCnt;
	}

	
	// 프로모션 쿠폰 내역 List
	public List<customerVO> promotionCoupon(customerVO customerVO, int start, int end) {
		List<customerVO> promotionCoupon = null;
		
		try {
			// 페이징 세팅
			customerVO.setStart(start);
			customerVO.setEnd(end);
			
			promotionCoupon = customerMapper.promotionCoupon(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => promotionCoupon() ==========");
			return null;
		}
		
		return promotionCoupon;
	}

	// 프로모션 등록
	public boolean insertPromotion(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setPromotionCode(request.getParameter("promotionCode"));
			customerVO.setType(request.getParameter("type"));
			customerVO.setUserTargetType(request.getParameter("userTargetType"));
			customerVO.setCouponQty(Integer.parseInt(request.getParameter("couponQty")));
			customerVO.setDiscountValue(Integer.parseInt(request.getParameter("discountValue")));
			customerVO.setStartDt(request.getParameter("startDt"));
			customerVO.setEndDt(request.getParameter("endDt"));
			customerVO.setStatus(request.getParameter("status"));
			customerVO.setIsRate(Integer.parseInt(request.getParameter("isRate")));
			customerVO.setDescriptionString(request.getParameter("descriptionString"));
			
			// 프로모션 등록
			customerMapper.insertPromotion(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => insertPromotion() ==========");
			return isStatus;
		}
		
		return isStatus;
	}
	
	// 프로모션 업데이트
	public boolean updatePromotion(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setPromotionNo(Integer.parseInt(request.getParameter("promotionNo")));
			customerVO.setPromotionCode(request.getParameter("promotionCode"));
			customerVO.setType(request.getParameter("type"));
			customerVO.setUserTargetType(request.getParameter("userTargetType"));
			customerVO.setStartDt(request.getParameter("startDt"));
			customerVO.setEndDt(request.getParameter("endDt"));
			customerVO.setCouponQty(Integer.parseInt(request.getParameter("couponQty")));
			customerVO.setStatus(request.getParameter("status"));
			//customerVO.setDiscountAmount(Integer.parseInt(request.getParameter("discountAmount")));
			customerVO.setIsRate(Integer.parseInt(request.getParameter("isRate")));
			customerVO.setDiscountValue(Integer.parseInt(request.getParameter("discountValue")));
			customerVO.setDescriptionString(request.getParameter("descriptionString"));
			
			// 프로모션 업데이트
			customerMapper.updatePromotion(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => updatePromotion() ==========");
			return isStatus;
		}
		
		return isStatus;
	}

	// 프로모션 삭제
	public boolean cancelPromotion(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setPromotionNo(Integer.parseInt(request.getParameter("promotionNo")));

			// 프로모션 삭제
			customerMapper.cancelPromotion(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => cancelPromotionRule() ==========");
			return isStatus;
		}
		
		return isStatus;
	}
	
	// 프로모션 규칙 등록
	public boolean insertPromotionRule(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setPromotionNo(Integer.parseInt(request.getParameter("promotionNo")));
			customerVO.setPromotionName(request.getParameter("promotionName"));
			customerVO.setPromotionCode(request.getParameter("promotionCode"));
			customerVO.setConditionType(request.getParameter("conditionType"));
			customerVO.setConditionQty(Integer.parseInt(request.getParameter("conditionQty")));
			customerVO.setExpiredDt(request.getParameter("expiredDt"));
			customerVO.setIsRepeat(Integer.parseInt(request.getParameter("isRepeat")));
			
			// 프로모션 규칙 등록
			customerMapper.insertPromotionRule(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => insertPromotionRule() ==========");
			return isStatus;
		}
		
		return isStatus;
	}

	// 프로모션 규칙 상세 팝업
	public List<customerVO> ruleDetail(customerVO customerVO) {
		List<customerVO> ruleDetail = null;
		
		try {
			ruleDetail = customerMapper.ruleDetail(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => ruleDetail() ==========");
			return null;
		}
		
		return ruleDetail;
	}

	// 프로모션 규칙 업데이트
	public boolean updatePromotionRule(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setId(Integer.parseInt(request.getParameter("ruleId")));
			customerVO.setPromotionName(request.getParameter("promotionName"));
			customerVO.setPromotionCode(request.getParameter("promotionCode"));
			customerVO.setConditionType(request.getParameter("conditionType"));
			customerVO.setConditionQty(Integer.parseInt(request.getParameter("conditionQty")));
			customerVO.setExpiredDt(request.getParameter("expiredDt"));
			customerVO.setIsRepeat(Integer.parseInt(request.getParameter("isRepeat")));
			
			// 프로모션 규칙 업데이트
			customerMapper.updatePromotionRule(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => updatePromotion() ==========");
			return isStatus;
		}
		
		return isStatus;
	}

	// 프로모션 규칙 삭제
	public boolean cancelPromotionRule(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setId(Integer.parseInt(request.getParameter("ruleId")));

			// 프로모션 규칙 삭제
			customerMapper.cancelPromotionRule(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => cancelPromotionRule() ==========");
			return isStatus;
		}
		
		return isStatus;
	}

	// 해당 프로모션의 Rule 정보
	public List<customerVO> getRuleList(customerVO customerVO) {
		List<customerVO> ruleList = null;
		
		try {
			ruleList = customerMapper.getRuleList(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => getRuleList() ==========");
			return null;
		}
		
		return ruleList;
	}

	// 프로모션 쿠폰 등록
	public boolean insertPromotionCoupon(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setPromotionNo(Integer.parseInt(request.getParameter("promotionId")));
			customerVO.setId(Integer.parseInt(request.getParameter("ruleId")));
			customerVO.setCustomerNo(Integer.parseInt(request.getParameter("customerNo")));
			customerVO.setRedeemCode(request.getParameter("redeemCode"));
			customerVO.setStartDt(request.getParameter("startDt"));
			customerVO.setEndDt(request.getParameter("endDt"));
			customerVO.setIsRate(Integer.parseInt(request.getParameter("isRate")));
			customerVO.setDiscountValue(Integer.parseInt(request.getParameter("discountValue")));

			// 프로모션 쿠폰 등록
			customerMapper.insertPromotionCoupon(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => insertPromotionCoupon() ==========");
			return isStatus;
		}
		
		return isStatus;
	}

	// 프로모션 쿠폰 상세 팝업
	public List<customerVO> couponDetail(customerVO customerVO) {
		List<customerVO> couponDetail = null;
		
		try {
			couponDetail = customerMapper.couponDetail(customerVO);
			
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => couponDetail() ==========");
			return null;
		}
		
		return couponDetail;
	}

	// 프로모션 쿠폰 업데이트
	public boolean updatePromotionCoupom(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setId(Integer.parseInt(request.getParameter("couponId")));
			customerVO.setRuleId(Integer.parseInt(request.getParameter("ruleId")));
			customerVO.setCustomerNo(Integer.parseInt(request.getParameter("customerNo")));
			customerVO.setStartDt(request.getParameter("startDt"));
			customerVO.setEndDt(request.getParameter("endDt"));
			customerVO.setIsRate(Integer.parseInt(request.getParameter("isRate")));
			customerVO.setDiscountValue(Integer.parseInt(request.getParameter("discountValue")));
			
			// 프로모션 쿠폰 업데이트
			customerMapper.updatePromotionCoupom(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => updatePromotionCoupom() ==========");
			return isStatus;
		}
		
		return isStatus;
	}
	
	// 프로모션 쿠폰 삭제
	public boolean cancelPromotionCoupon(HttpServletRequest request, customerVO customerVO) {
		boolean isStatus = false;

		try {
			customerVO.setId(Integer.parseInt(request.getParameter("couponId")));

			// 프로모션 규칙 삭제
			customerMapper.cancelPromotionCoupon(customerVO);
			
			isStatus = true;		
		} catch (Exception e) {
			logger.debug(" ==========" + this.className + " => cancelPromotionCoupon() ==========");
			return isStatus;
		}
		
		return isStatus;
	}
}
