package com.mosiler.web.admin.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonObject;
import com.mosiler.web.admin.domain.pagingVO;
import com.mosiler.web.admin.domain.systemVO;
import com.mosiler.web.admin.service.systemService;
import com.mosiler.web.admin.util.adminUtil;

import net.sf.json.JSONArray;

@Controller
@RequestMapping("/system")
public class systemController {

	@Autowired
	private menuController menuController;
	
	@Autowired
	private systemService systemService;
	
//	@Value("${localPath}")
//	String localPath;
//	
//	@Value("${realPath}")
//	String realPath;
	
	/**
	 * 운영자 관리
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/setAdminUser")
	public ModelAndView reservationList(Model model, HttpServletRequest request, systemVO systemVO, pagingVO pagingVO) {
		ModelAndView mav = new ModelAndView();
		
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);

		// 현재 페이지 번호 세팅
		String nowPage = request.getParameter("nowPage");
		String cntPerPage = request.getParameter("cntPerPage");
		
		// 검색어 세팅
		if(request.getParameter("searchType") != null && request.getParameter("searchKeyword") != null) {
			systemVO.setSearchType(request.getParameter("searchType"));
			systemVO.setSearchKeyword(request.getParameter("searchKeyword"));
		}
		
		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		// 어드민 유저 Count
		int adminUserListCnt = systemService.adminUserListCnt(systemVO);
		pagingVO = new pagingVO(adminUserListCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
		
		// 어드민 유저 List
		List<systemVO> list = systemService.adminUserList(systemVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
		
		JSONArray adminUserList = JSONArray.fromObject(list);
		
		mav.addObject("cateList", menuModel.getAttribute("cateList"));
		mav.addObject("depthList",menuModel.getAttribute("depthList"));
		mav.addObject("adminUserList", adminUserList);
		mav.addObject(pagingVO);
		mav.addObject(systemVO);
		
		return mav;
	}
	
	/**
	 * 운영자 정보 수정
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/updateAdminUser")
	@ResponseBody
	public int updateAdminUser(Model model, HttpServletRequest request, systemVO systemVO) {
		// VO 세팅
		systemVO.setUserNo(Integer.parseInt(request.getParameter("userNo")));		
		systemVO.setUserId(request.getParameter("userId"));
		systemVO.setUserPw(request.getParameter("userPw"));
		systemVO.setUserName(request.getParameter("userName"));
		systemVO.setEmail(request.getParameter("email"));
		systemVO.setPhoneNumber(request.getParameter("phoneNumber"));
		systemVO.setOtpKey(request.getParameter("otpKey"));
		
		int resultCode = 0;
		boolean isStatus = systemService.updateAdminUser(systemVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}

		return resultCode;
	}
	
	/**
	 * OTP 키 발급
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/getOtpKey")
	@ResponseBody
	public String getOtpKey() {
		String encodedKey = adminUtil.generateSecretKey();

	    return encodedKey;
	}
	
	/**
	 * 비밀번호 초기화
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/pwdReset")
	@ResponseBody
	public int pwdReset(HttpServletRequest request, systemVO systemVO) {
		// VO 세팅
		systemVO.setUserNo(Integer.parseInt(request.getParameter("userNo")));		
		systemVO.setPhoneNumber(request.getParameter("phoneNumber"));
		
		int resultCode = 0;
		boolean isStatus = systemService.pwdReset(systemVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}

		return resultCode;
	}
	
	/**
	 * 권한 관리
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/setAuthority")
	public String setAuthority(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/system/setAuthority";
	}
	
	/**
	 * 상품 관리
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/setProductFee")
	public String setProductFee(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/system/setProductFee";
	}
	
	/**
	 * 컨시어지 관리
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/setConcierge")
	public String setConcierge(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/system/setConcierge";
	}
	
	/**
	 * 쿠폰 관리
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/setCoupon")
	public String setCoupon(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/system/setCoupon";
	}
	
	/**
	 * 코드 관리
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/setCode")
	public String setCode(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/system/setCode";
	}
	
	/**
	 * 공지사항
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/setNotice")
	public String setNotice(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/system/setNotice";
	}
	
	/**
	 * FAQ
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/setFAQ")
	public ModelAndView setFAQ(Model model, HttpServletRequest request, systemVO systemVO, pagingVO pagingVO) {
		ModelAndView mav = new ModelAndView();
		
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);

		// 현재 페이지 번호 세팅
		String nowPage = request.getParameter("nowPage");
		String cntPerPage = request.getParameter("cntPerPage");
		
		// 검색어 세팅
		if(request.getParameter("searchKeyword") != null) {
			systemVO.setSearchKeyword(request.getParameter("searchKeyword"));
		}
		
		// 검색 조건 세팅
		systemVO.setFaqCategory(request.getParameter("faqCategory"));
		systemVO.setFaqTopYn(request.getParameter("faqTopYn"));
		systemVO.setUseYn(request.getParameter("useYn"));
		
		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		// FAQ Count
		int faqListCnt = systemService.faqListCnt(systemVO);
		pagingVO = new pagingVO(faqListCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
		
		// FAQ List
		List<systemVO> list =  systemService.faqList(systemVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
		
		JSONArray faqList = JSONArray.fromObject(list);

		mav.addObject("cateList", menuModel.getAttribute("cateList"));
		mav.addObject("depthList",menuModel.getAttribute("depthList"));
		mav.addObject("faqList", faqList);
		mav.addObject(pagingVO);
		mav.addObject(systemVO);
		
		return mav;
	}
	
	/**
	 * FAQ 등록 팝업
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/faqRegistPopUp")
	public String faqRegistPopUp() {
		return "/system/popup/faqRegist-popUp";
	}
	
	/** 
	 * FAQ 파일 업로드 (ckeditor)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/multiFileUpload", method = RequestMethod.POST)
	@ResponseBody
	public String fileUpload(HttpServletRequest request, HttpServletResponse response, MultipartHttpServletRequest multiFlie) throws Exception {
		HttpSession session = request.getSession(true);
		Object flag = session.getAttribute("loginChannel");
		
		JsonObject jsonData = new JsonObject();
		
		PrintWriter printWriter = null;
		OutputStream out = null;
		
		MultipartFile file = multiFlie.getFile("upload");
		
		if(file != null) {
			if(file.getSize() > 0 && StringUtils.isNotBlank(file.getName())) {
				try {
					String fileName = file.getOriginalFilename();
					byte[] bytes = file.getBytes();
					String uploadPath = "";
					String fileUrl = "";
					
					if("local".equals(flag)) {
						uploadPath = request.getServletContext().getRealPath("/temp_image");
					} else {
						uploadPath = "/home/ubuntu/mosilerWebAdmin/temp_image";
					}

					File uploadFile = new File(uploadPath);
					
					if(!uploadFile.exists()) {
						uploadFile.mkdir();
					}
					
					fileName = "FaqImage__"+UUID.randomUUID().toString()+"__"+fileName;
					uploadPath = uploadPath+"/"+fileName;
					
					out = new FileOutputStream(new File(uploadPath));
					
					out.write(bytes);
					
					printWriter = response.getWriter();
					response.setContentType("text/html");
					
					if("local".equals(flag)) {
						fileUrl = "/temp_image/"+fileName;
					} else {
						fileUrl = "/temp_image/"+fileName;
					}
					
					jsonData.addProperty("uploaded", 1);
					jsonData.addProperty("fileName", fileName);
					jsonData.addProperty("url", fileUrl);
					
					printWriter.println(jsonData);
					System.out.println(jsonData);
					
					System.out.println("uploadPath :: " + uploadPath);
					System.out.println("fileUrl :: " + fileUrl);
					System.out.println("request.getContextPath :: " + request.getContextPath());
					
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if(out != null)
						out.close();
					
					if(printWriter != null)
						printWriter.close();
				}
			}
		}
		
		return null;
	}

	/**
	 * FAQ 등록
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/insertFaq")
	@ResponseBody
	public int insertFaq(Model model, HttpServletRequest request, systemVO systemVO, pagingVO pagingVO) {
		
		int resultCode;
		
		boolean isStatus = systemService.insertFaq(request, systemVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * FAQ 상세 팝업
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/faqDetailpopUp")
	public String faqDetailpopUp(Model model, HttpServletRequest request, systemVO systemVO, pagingVO pagingVO) {
		// faqNo 세팅
		systemVO.setFaqNo(Integer.parseInt(request.getParameter("faqNo")));
		
		// FAQ 상세 내용
		List<systemVO> faqDetail = systemService.faqDetail(systemVO);
		
		model.addAttribute("faqNo", systemVO.getFaqNo());
		model.addAttribute("faqDetail", faqDetail);
		model.addAttribute("faqAnswer", faqDetail.get(0).getFaqAnswer());
		
		return "/system/popup/faqDetail-popUp";
	}
	
	/**
	 * FAQ Update
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/updateFaq")
	@ResponseBody
	public int updateFaq(Model model, HttpServletRequest request, systemVO systemVO, pagingVO pagingVO) {
		
		int resultCode;
		
		boolean isStatus = systemService.updateFaq(request, systemVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}	
	
	/**
	 * QNA
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/setQNA")
	public String setQNA(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/system/setQNA";
	}
}
