package com.mosiler.web.admin.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/payment")
public class paymentController {
	
	@Autowired
	private menuController menuController;
	
	/**
	 * 결제 현황
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/paymentList")
	public String reservationList(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/payment/paymentList";
	}
	
	/**
	 * 정액권 결제
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/commuterTicket")
	public String commuterTicket(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/payment/commuterTicket";
	}
	
	/**
	 * 상품 요금
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/productFee")
	public String productFee(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/payment/productFee";
	}
}
