package com.mosiler.web.admin.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.azure.storage.file.share.ShareDirectoryClient;
import com.azure.storage.file.share.ShareFileClient;
import com.azure.storage.file.share.ShareFileClientBuilder;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.BlobContainerPermissions;
import com.microsoft.azure.storage.blob.BlobContainerPublicAccessType;
import com.microsoft.azure.storage.blob.CloudAppendBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.mosiler.web.admin.domain.codeVO;
import com.mosiler.web.admin.domain.pagingVO;
import com.mosiler.web.admin.domain.partnerVO;
import com.mosiler.web.admin.service.codeService;
import com.mosiler.web.admin.service.partnerService;
import com.mosiler.web.admin.util.adminUtil;
import com.mosiler.web.admin.util.validation;

import net.sf.json.JSONArray;

@Controller
@RequestMapping("/partner")
public class partnerController {

	@Autowired
	private menuController menuController;
	
	@Autowired
	private partnerService partnerService;
	
	@Autowired
	private codeService codeService;
	
	/**
	 * 고객 상세 팝업 - Open 파트너 배정
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/partnerAssignment")
	public String partnerAssignment(Model model, pagingVO pagingVO,  partnerVO partnerVO
			, @RequestParam(value="customerNo", required = false) String customerNo
			, @RequestParam(value="tabId", required = false) String tabId
			, @RequestParam(value="flag", required = false) String flag
			, HttpServletRequest request
			, @RequestParam(value="nowPage", required = false) String nowPage
			, @RequestParam(value="cntPerPage", required = false) String cntPerPage) {

		if(customerNo != null && "0".equals(customerNo))
			partnerVO.setCustomerNo(Integer.parseInt(customerNo));
		
		partnerVO.setTabId(tabId);
		
		if(flag != null) {
			partnerVO.setFlag(flag);
		} else {
			partnerVO.setFlag(null);
		}
		
		// 검색어 세팅
		if(request.getParameter("searchType") != null && request.getParameter("searchKeyword") != null) {
			partnerVO.setSearchType(request.getParameter("searchType"));
			partnerVO.setSearchKeyword(request.getParameter("searchKeyword"));
		
			pagingVO.setSearchType(request.getParameter("searchType"));
			pagingVO.setSearchKeyword(request.getParameter("searchKeyword"));
		}
		
		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		// 배정 신청(1), 강제배정(2), 배정이력(3) Count
		int partnerAssignmentCnt = partnerService.partnerAssignmentCnt(partnerVO);
		pagingVO = new pagingVO(partnerAssignmentCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
	
		// 배정 신청(1), 강제배정(2), 배정이력(3) 
		List<partnerVO> partnerAssignmentList = partnerService.partnerAssignmentList(partnerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());

		model.addAttribute("customerNo", customerNo);
		model.addAttribute("paging", pagingVO);
		model.addAttribute("partnerAssignmentList", partnerAssignmentList);
		model.addAttribute("partnerVO", partnerVO);
		
		return "/partner/popup/partnerAssignment";
	}
	
	/**
	 * 고객 상세 팝업 - Open 파트너 멀티 배정
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/partnerMultiAssignment")
	public String partnerMultiAssignment() {
		
		return "/partner/popup/partnerMultiAssignment";
	}
	
	/**
	 * 파트너 현황
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/partnerList")
	public ModelAndView partnerList(Model model, HttpServletRequest request, partnerVO partnerVO, pagingVO pagingVO) {
		ModelAndView mav = new ModelAndView();
		
		int cnt = 0;
		
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);

		// 현재 페이지 번호 세팅
		String nowPage = request.getParameter("nowPage");
		String cntPerPage = request.getParameter("cntPerPage");
		
		// 운행 종류 구분 공통 코드
		List<codeVO> codeList1 = codeService.getPermitStatusCodeList();
		
		// 드라이버 상태 구분 공통 코드
		List<codeVO> codeList2 = codeService.getPartnerStatusCodeList();
		
		// VO Setting
		partnerVO = validation.partnerValidation(request, nowPage, cntPerPage);
		
		// 파트너 현황 
		List<partnerVO> list = partnerService.selectPartnerList(request, partnerVO);
		
		if(list.size() != 0) 
			cnt = list.get(0).getTotCnt();
		
		// 페이징 계산
		pagingVO = new pagingVO(cnt, partnerVO.getPage(), partnerVO.getPageSize());
		
		JSONArray partnerList = JSONArray.fromObject(list);

		mav.addObject("cateList", menuModel.getAttribute("cateList"));
		mav.addObject("depthList",menuModel.getAttribute("depthList"));
		mav.addObject("codeList1", codeList1);
		mav.addObject("codeList2", codeList2);
		mav.addObject("partnerList", partnerList);
		mav.addObject(pagingVO);
		mav.addObject(partnerVO);
		
		return mav;
	}
	
	/**
	 * 파트너 등록 팝업
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/partnerRegistPopUp")
	public String partnerRegistPopUp(Model model) {
		// 은행 종류 구분 공통 코드
		List<codeVO> codeList1 = codeService.getBankCodeList();
		
		// 드라이버 상태 구분 공통 코드
		List<codeVO> codeList2 = codeService.getPartnerStatusCodeList();
		
		// 소속법인 구분 공통 코드
		List<codeVO> codeList3 = codeService.getBizCompList();
				
		// 운행 종류 구분 공통 코드
		List<codeVO> codeList4 = codeService.getPermitStatusCodeList();
		
		model.addAttribute("codeList1", codeList1);
		model.addAttribute("codeList2", codeList2);
		model.addAttribute("codeList3", codeList3);
		model.addAttribute("codeList4", codeList4);
		
		return "/partner/popup/partnerRegist-popUp";
	}
	
	/**
	 * 파트너 등록 
	 * @param 
	 * @return 
	 * @throws Exception 
	 */
	@RequestMapping(value = "/insertDriver", method = RequestMethod.POST)
	@ResponseBody
	public int insertDriver(Model model, HttpServletRequest request, partnerVO partnerVO) throws Exception {
		int resultCode;
		
		boolean isStatus = partnerService.insertDriver(request, partnerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
	@ResponseBody
	public void fileUpload(HttpServletRequest request, @RequestParam(value="file", required = false) MultipartFile mf) throws IllegalStateException, IOException {
		String flag = request.getParameter("flag");
	
		Date date_now = new Date(System.currentTimeMillis());
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss"); 
		String originalFileName = mf.getOriginalFilename();
		
		String storageConnectionString = "DefaultEndpointsProtocol=https;"
			      + "AccountName=mosilergps;"
			      + "AccountKey=aoclQYIj3OgmznNS8oTTc01t/ra81AABTbsWsFL+I0dMMQjV1WF0W+xlBEWWdDQ3fOIrnvjLhMCYz/WKzvT3rw==";


		
		
		
		
		
		
		
		
//		ShareDirectoryClient dirClient = new ShareFileClientBuilder()
//	             .connectionString(storageConnectionString).shareName("test")
//	             .resourcePath("/AttachFile/License")
//	             .buildDirectoryClient();
//
//	        ShareFileClient fileClient = dirClient.getFileClient(originalFileName);
//	        fileClient.create(1024);
//	        fileClient.uploadFromFile(originalFileName);
		
		
		
	
		
		
		
		
//		if("license".equals(flag)) {
//			String originalFileName = mf.getOriginalFilename();
//			
//			String uploadPath = "https://manage.mosiler.com/AttachFile/License/"+originalFileName;
//			System.out.println(uploadPath);
//			 mf.transferTo(new File(uploadPath));
//		}
//
//		if("account".equals(flag)) {
//			String originalFileName = mf.getOriginalFilename();
//			
//			String uploadPath = "https://manage.mosiler.com/AttachFile/account/"+originalFileName;
//			
//			 mf.transferTo(new File(uploadPath));
//		}
	
	}
	
	
	
	/**
	 * 파트너 상세 팝업
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/partnerDetailpopUp")
	public String partnerDetailpopUp(Model model, partnerVO partnerVO, 
			@RequestParam(value="driverNo", required = false) int driverNo
			, @RequestParam(value="tabId", required = false) String tabId
			, HttpServletRequest request, pagingVO pagingVO) {
		
		// driverNo 세팅
		partnerVO.setDriverNo(driverNo);
		model.addAttribute("driverNo", driverNo);
		
		// 1. 파트너 상세 정보
		List<partnerVO> partnerDetail = partnerService.selectPartnerDetail(partnerVO);
		model.addAttribute("partnerDetail", JSONArray.fromObject(partnerDetail));
		
		// 현재 페이지 번호 세팅
		String nowPage = request.getParameter("nowPage");
		String cntPerPage = request.getParameter("cntPerPage"+tabId);
		
		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		if(tabId.equals("0")) {
			// 2. 운행내역 Count
			int cnt = partnerService.selectDrivingDetailCnt(partnerVO);
			pagingVO = new pagingVO(cnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
			
			// 2. 운행내역
			List<partnerVO> drivingDetail = partnerService.selectDrivingDetail(partnerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());

			model.addAttribute("drivingDetail", JSONArray.fromObject(drivingDetail));
			model.addAttribute("csDetail", "0");
			model.addAttribute("driverClassDetail", "0");
			model.addAttribute("driverEvaluationDetail", "0");
			model.addAttribute("driverIssuedDetail", "0");
			model.addAttribute("driverAccdentDetail", "0");
			model.addAttribute("driverViolationDetail", "0");
			
		} else if(tabId.equals("1")) {
			// 3. 상담내역 Count
			int cnt = partnerService.selectCsDetailCnt(partnerVO);
			pagingVO = new pagingVO(cnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
			
			// 3. 상담내역
			List<partnerVO> csDetail = partnerService.selectCsDetail(partnerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
			
			model.addAttribute("drivingDetail", "0");
			model.addAttribute("csDetail", JSONArray.fromObject(csDetail));
			model.addAttribute("driverClassDetail", "0");
			model.addAttribute("driverEvaluationDetail", "0");
			model.addAttribute("driverIssuedDetail", "0");
			model.addAttribute("driverAccdentDetail", "0");
			model.addAttribute("driverViolationDetail", "0");
			
		} else if(tabId.equals("2")) {
			// 4. 교육내역 Count
			int cnt = partnerService.selectDriverClassDetailCnt(partnerVO);
			pagingVO = new pagingVO(cnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
			
			// 4. 교육내역
			List<partnerVO> driverClassDetail = partnerService.selectDriverClassDetail(partnerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());

			model.addAttribute("drivingDetail", "0");
			model.addAttribute("csDetail", "0");
			model.addAttribute("driverClassDetail", JSONArray.fromObject(driverClassDetail));
			model.addAttribute("driverEvaluationDetail", "0");
			model.addAttribute("driverIssuedDetail", "0");
			model.addAttribute("driverAccdentDetail", "0");
			model.addAttribute("driverViolationDetail", "0");
			
		} else if(tabId.equals("3")) {
			// 5. 평가 Count
			int cnt = partnerService.selectDriverEvaluationDetailCnt(partnerVO);
			pagingVO = new pagingVO(cnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
			
			// 5. 평가
			List<partnerVO> driverEvaluationDetail = partnerService.selectDriverEvaluationDetail(partnerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
			
			model.addAttribute("drivingDetail", "0");
			model.addAttribute("csDetail", "0");
			model.addAttribute("driverClassDetail", "0");
			model.addAttribute("driverEvaluationDetail", JSONArray.fromObject(driverEvaluationDetail));
			model.addAttribute("driverIssuedDetail", "0");
			model.addAttribute("driverAccdentDetail", "0");
			model.addAttribute("driverViolationDetail", "0");
			
		} else if(tabId.equals("4")) {
			// 6. 물품이력 Count
			int cnt = partnerService.selectDriverIssuedDetailCnt(partnerVO);
			pagingVO = new pagingVO(cnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
			
			// 6. 물품이력
			List<partnerVO> driverIssuedDetail = partnerService.selectDriverIssuedDetail(partnerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
			
			model.addAttribute("drivingDetail", "0");
			model.addAttribute("csDetail", "0");
			model.addAttribute("driverClassDetail", "0");
			model.addAttribute("driverEvaluationDetail", "0");
			model.addAttribute("driverIssuedDetail", JSONArray.fromObject(driverIssuedDetail));
			model.addAttribute("driverAccdentDetail", "0");
			model.addAttribute("driverViolationDetail", "0");
			
		} else if(tabId.equals("5")) {
			// 7. 사고이력 Count
			int cnt = partnerService.selectDriverAccdentDetailCnt(partnerVO);
			pagingVO = new pagingVO(cnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
			
			// 7. 사고이력 
			List<partnerVO> driverAccdentDetail = partnerService.selectDriverAccdentDetail(partnerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
			
			model.addAttribute("tabId", tabId);
			
			model.addAttribute("drivingDetail", "0");
			model.addAttribute("csDetail", "0");
			model.addAttribute("driverClassDetail", "0");
			model.addAttribute("driverEvaluationDetail", "0");
			model.addAttribute("driverIssuedDetail", "0");
			model.addAttribute("driverAccdentDetail", JSONArray.fromObject(driverAccdentDetail));
			model.addAttribute("driverViolationDetail", "0");
			
		} else if(tabId.equals("6")) {
			// 7. 교통위반 이력 Count
			int cnt = partnerService.selectDriverViolationDetailCnt(partnerVO);
			pagingVO = new pagingVO(cnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
			
			// 8. 교통위반 이력
			List<partnerVO> driverViolationDetail = partnerService.selectDriverViolationDetail(partnerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());

			model.addAttribute("drivingDetail", "0");
			model.addAttribute("csDetail", "0");
			model.addAttribute("driverClassDetail", "0");
			model.addAttribute("driverEvaluationDetail", "0");
			model.addAttribute("driverIssuedDetail", "0");
			model.addAttribute("driverAccdentDetail","0");
			model.addAttribute("driverViolationDetail", JSONArray.fromObject(driverViolationDetail));
		}
		
		model.addAttribute("tabId", tabId);
		model.addAttribute("pagingVO", pagingVO);
		model.addAttribute("partnerVO", partnerVO);
		
		return "/partner/popup/partnerDetail-popUp";
	}
	
	/**
	 * 임시 파트너
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/temporaryPartner")
	public String temporaryPartner(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/partner/temporaryPartner";
	}
	
	/**
	 * 불합격자
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/failPartner")
	public String failPartner(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/partner/failPartner";
	}
	
	/**
	 * 배차 설정
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/setDispatch")
	public String setDispatch(Model model) {
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
		
		return "/partner/setDispatch";
	}
}
