package com.mosiler.web.admin.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder // builder를 사용할수 있게 합니다.
@Data // user 필드값의 getter/setter를 자동으로 생성합니다.
@NoArgsConstructor // 인자없는 생성자를 자동으로 생성합니다.
@AllArgsConstructor // 인자를 모두 갖춘 생성자를 자동으로 생성합니다.
public class partnerVO {
	String tabId;
	int customerNo;
	int driverNo;
	int drivingNo;
	int drivingCnt;
	String userName;
	String phoneNumber;
	String requestDt;
	String driverLv;
	long timeStamp;
	
	// 검색어 VO
	String searchType;
	String searchKeyword;
	
	// 페이징 VO
	int count;
	int start;
	int end;
	int totalCnt;
	int page;
	int pageSize;
	String flag;
	
	// 파트너 현황 VO
	String partnerStatus;
	String gubun;
	String permitStatus;
	String state;
	String keyword;
//	int driverNo;
	String userId;
	String userPw;
//	String userName;
//	Stirng phoneNumber;
	String profileImgName;
	String socialNumber;
	String licenseNumber; 
	String licenseImgName;
	String bankCode;
	String accountNumber; 
	String accountImgName;
	String lastAccessDate;  
	int drivingYear;
	String hobby;
	String jobName;
	String driverStatus;
	String drivingYn;
	String approvalDt;
	String pushYn;
	String driverRemark;
	String regDt;
	String updateDt;
	String driverLevelName;
	String apiDriverId;
	String apiResult;
	String driverStatusName;
//	int drivingCnt;
	float averagePoint;
	int ruwNum;
	int totCnt;
//	String permitStatus;
	String permitStatusName;
	String isJoin;
	int bizCompNo;
	String ssn;
	String driverLevel;
	String socialImgName;
	String englishName;
	String bankCodeName;	
	String	 compName;
	String totalChargeAmount;
	
	// 파트너 현황 - 운행 내역
	String rownum;
	String customerName;
	String startAddress;
	String endAddress;
	String drivingTypeName;
	String revStartDt;
	String revEndDt;
	String paymentYn;
	int paymentAmount;
	
	// 파트너 현황 - 상담 내역
	int csNo;
	String comment;
	String drivingDate;
	String drivingStatusName;

	// 파트너 현황 - 교육 내역
	int idx;
	String classCode;
	String drivingGrade;
	String transformGrade;
	String lookGrade;
	int rid;
	int uid;
	String status;
	String gDate;
	String className;
	String statusName;
	
	// 파트너 현황 - 평가
	String driverName;
	int dropOff;
	int totalAmount;
	int peakAmount;
	int incentive;
	String IsApply;
	int incomeTax;
	int localTax;
	int grossIncentive;
	String Type;
	
	// 파트너 현황 - 물품 이력
	int issuedNo;
	String date;
	int itemNo;
	int qty;
	String itemName;
	
	// 파트너 현황 - 사고 이력
	int accdentNo;
	String carPNo;
	String insuranceNo;
	String actDate;
	String isHurt;
	String partCar;
	String location;
	String aCarPNo;
	String aName;
	String aInsuranceNo;
	String aisHurt;
	String aPartCar;
	String result;
	
	// 파트너 현황 - 교통위반 이력
	int violationNo;
	String vioType;
	String vioDate;
	String vioComment;
	String vioLocation;
	String resType;
	String vioAmount;
	String isDivide;
	String month;
	String isDone;
	String divideAmount;
	String resTypeName;
	String vioTypeName;
	
	public String getResTypeName() {
		return resTypeName;
	}

	public void setResTypeName(String resTypeName) {
		this.resTypeName = resTypeName;
	}

	public String getVioTypeName() {
		return vioTypeName;
	}

	public void setVioTypeName(String vioTypeName) {
		this.vioTypeName = vioTypeName;
	}

	public int getViolationNo() {
		return violationNo;
	}

	public void setViolationNo(int violationNo) {
		this.violationNo = violationNo;
	}

	public String getVioType() {
		return vioType;
	}

	public void setVioType(String vioType) {
		this.vioType = vioType;
	}

	public String getVioDate() {
		return vioDate;
	}

	public void setVioDate(String vioDate) {
		this.vioDate = vioDate;
	}

	public String getVioComment() {
		return vioComment;
	}

	public void setVioComment(String vioComment) {
		this.vioComment = vioComment;
	}

	public String getVioLocation() {
		return vioLocation;
	}

	public void setVioLocation(String vioLocation) {
		this.vioLocation = vioLocation;
	}

	public String getResType() {
		return resType;
	}

	public void setResType(String resType) {
		this.resType = resType;
	}

	public String getVioAmount() {
		return vioAmount;
	}

	public void setVioAmount(String vioAmount) {
		this.vioAmount = vioAmount;
	}

	public String getIsDivide() {
		return isDivide;
	}

	public void setIsDivide(String isDivide) {
		this.isDivide = isDivide;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getIsDone() {
		return isDone;
	}

	public void setIsDone(String isDone) {
		this.isDone = isDone;
	}

	public String getDivideAmount() {
		return divideAmount;
	}

	public void setDivideAmount(String divideAmount) {
		this.divideAmount = divideAmount;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getPaymentYn() {
		return paymentYn;
	}

	public void setPaymentYn(String paymentYn) {
		this.paymentYn = paymentYn;
	}

	public int getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(int paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public int getAccdentNo() {
		return accdentNo;
	}

	public void setAccdentNo(int accdentNo) {
		this.accdentNo = accdentNo;
	}

	public String getCarPNo() {
		return carPNo;
	}

	public void setCarPNo(String carPNo) {
		this.carPNo = carPNo;
	}

	public String getInsuranceNo() {
		return insuranceNo;
	}

	public void setInsuranceNo(String insuranceNo) {
		this.insuranceNo = insuranceNo;
	}

	public String getActDate() {
		return actDate;
	}

	public void setActDate(String actDate) {
		this.actDate = actDate;
	}

	public String getIsHurt() {
		return isHurt;
	}

	public void setIsHurt(String isHurt) {
		this.isHurt = isHurt;
	}

	public String getPartCar() {
		return partCar;
	}

	public void setPartCar(String partCar) {
		this.partCar = partCar;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getaCarPNo() {
		return aCarPNo;
	}

	public void setaCarPNo(String aCarPNo) {
		this.aCarPNo = aCarPNo;
	}

	public String getaName() {
		return aName;
	}

	public void setaName(String aName) {
		this.aName = aName;
	}

	public String getaInsuranceNo() {
		return aInsuranceNo;
	}

	public void setaInsuranceNo(String aInsuranceNo) {
		this.aInsuranceNo = aInsuranceNo;
	}

	public String getAisHurt() {
		return aisHurt;
	}

	public void setAisHurt(String aisHurt) {
		this.aisHurt = aisHurt;
	}

	public String getaPartCar() {
		return aPartCar;
	}

	public void setaPartCar(String aPartCar) {
		this.aPartCar = aPartCar;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public int getIssuedNo() {
		return issuedNo;
	}

	public void setIssuedNo(int issuedNo) {
		issuedNo = issuedNo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getItemNo() {
		return itemNo;
	}

	public void setItemNo(int itemNo) {
		this.itemNo = itemNo;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public int getDropOff() {
		return dropOff;
	}

	public void setDropOff(int dropOff) {
		this.dropOff = dropOff;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getPeakAmount() {
		return peakAmount;
	}

	public void setPeakAmount(int peakAmount) {
		this.peakAmount = peakAmount;
	}

	public int getIncentive() {
		return incentive;
	}

	public void setIncentive(int incentive) {
		this.incentive = incentive;
	}

	public String getIsApply() {
		return IsApply;
	}

	public void setIsApply(String isApply) {
		IsApply = isApply;
	}

	public int getIncomeTax() {
		return incomeTax;
	}

	public void setIncomeTax(int incomeTax) {
		this.incomeTax = incomeTax;
	}

	public int getLocalTax() {
		return localTax;
	}

	public void setLocalTax(int localTax) {
		this.localTax = localTax;
	}

	public int getGrossIncentive() {
		return grossIncentive;
	}

	public void setGrossIncentive(int grossIncentive) {
		this.grossIncentive = grossIncentive;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getDrivingGrade() {
		return drivingGrade;
	}

	public void setDrivingGrade(String drivingGrade) {
		this.drivingGrade = drivingGrade;
	}

	public String getTransformGrade() {
		return transformGrade;
	}

	public void setTransformGrade(String transformGrade) {
		this.transformGrade = transformGrade;
	}

	public String getLookGrade() {
		return lookGrade;
	}

	public void setLookGrade(String lookGrade) {
		this.lookGrade = lookGrade;
	}

	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getgDate() {
		return gDate;
	}

	public void setgDate(String gDate) {
		this.gDate = gDate;
	}

	public int getBizCompNo() {
		return bizCompNo;
	}

	public int getCsNo() {
		return csNo;
	}

	public void setCsNo(int csNo) {
		this.csNo = csNo;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getDrivingDate() {
		return drivingDate;
	}

	public void setDrivingDate(String drivingDate) {
		this.drivingDate = drivingDate;
	}

	public String getDrivingStatusName() {
		return drivingStatusName;
	}

	public void setDrivingStatusName(String drivingStatusName) {
		this.drivingStatusName = drivingStatusName;
	}

	public String getRownum() {
		return rownum;
	}

	public void setRownum(String rownum) {
		this.rownum = rownum;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getStartAddress() {
		return startAddress;
	}

	public void setStartAddress(String startAddress) {
		this.startAddress = startAddress;
	}

	public String getEndAddress() {
		return endAddress;
	}

	public void setEndAddress(String endAddress) {
		this.endAddress = endAddress;
	}

	public String getDrivingTypeName() {
		return drivingTypeName;
	}

	public void setDrivingTypeName(String drivingTypeName) {
		this.drivingTypeName = drivingTypeName;
	}

	public String getRevStartDt() {
		return revStartDt;
	}

	public void setRevStartDt(String revStartDt) {
		this.revStartDt = revStartDt;
	}

	public String getRevEndDt() {
		return revEndDt;
	}

	public void setRevEndDt(String revEndDt) {
		this.revEndDt = revEndDt;
	}

	public void setBizCompNo(int bizCompNo) {
		this.bizCompNo = bizCompNo;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getDriverLevel() {
		return driverLevel;
	}

	public void setDriverLevel(String driverLevel) {
		this.driverLevel = driverLevel;
	}

	public String getSocialImgName() {
		return socialImgName;
	}

	public void setSocialImgName(String socialImgName) {
		this.socialImgName = socialImgName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getBankCodeName() {
		return bankCodeName;
	}

	public void setBankCodeName(String bankCodeName) {
		this.bankCodeName = bankCodeName;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}

	public String getTotalChargeAmount() {
		return totalChargeAmount;
	}

	public void setTotalChargeAmount(String totalChargeAmount) {
		this.totalChargeAmount = totalChargeAmount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserPw() {
		return userPw;
	}

	public void setUserPw(String userPw) {
		this.userPw = userPw;
	}

	public String getProfileImgName() {
		return profileImgName;
	}

	public void setProfileImgName(String profileImgName) {
		this.profileImgName = profileImgName;
	}

	public String getSocialNumber() {
		return socialNumber;
	}

	public void setSocialNumber(String socialNumber) {
		this.socialNumber = socialNumber;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getLicenseImgName() {
		return licenseImgName;
	}

	public void setLicenseImgName(String licenseImgName) {
		this.licenseImgName = licenseImgName;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountImgName() {
		return accountImgName;
	}

	public void setAccountImgName(String accountImgName) {
		this.accountImgName = accountImgName;
	}

	public String getLastAccessDate() {
		return lastAccessDate;
	}

	public void setLastAccessDate(String lastAccessDate) {
		this.lastAccessDate = lastAccessDate;
	}

	public int getDrivingYear() {
		return drivingYear;
	}

	public void setDrivingYear(int drivingYear) {
		this.drivingYear = drivingYear;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDriverStatus() {
		return driverStatus;
	}

	public void setDriverStatus(String driverStatus) {
		this.driverStatus = driverStatus;
	}

	public String getDrivingYn() {
		return drivingYn;
	}

	public void setDrivingYn(String drivingYn) {
		this.drivingYn = drivingYn;
	}

	public String getApprovalDt() {
		return approvalDt;
	}

	public void setApprovalDt(String approvalDt) {
		this.approvalDt = approvalDt;
	}

	public String getPushYn() {
		return pushYn;
	}

	public void setPushYn(String pushYn) {
		this.pushYn = pushYn;
	}

	public String getDriverRemark() {
		return driverRemark;
	}

	public void setDriverRemark(String driverRemark) {
		this.driverRemark = driverRemark;
	}

	public String getRegDt() {
		return regDt;
	}

	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}

	public String getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(String updateDt) {
		this.updateDt = updateDt;
	}

	public String getDriverLevelName() {
		return driverLevelName;
	}

	public void setDriverLevelName(String driverLevelName) {
		this.driverLevelName = driverLevelName;
	}

	public String getApiDriverId() {
		return apiDriverId;
	}

	public void setApiDriverId(String apiDriverId) {
		this.apiDriverId = apiDriverId;
	}

	public String getApiResult() {
		return apiResult;
	}

	public void setApiResult(String apiResult) {
		this.apiResult = apiResult;
	}

	public String getDriverStatusName() {
		return driverStatusName;
	}

	public void setDriverStatusName(String driverStatusName) {
		this.driverStatusName = driverStatusName;
	}

	public float getAveragePoint() {
		return averagePoint;
	}

	public void setAveragePoint(float averagePoint) {
		this.averagePoint = averagePoint;
	}

	public int getRuwNum() {
		return ruwNum;
	}

	public void setRuwNum(int ruwNum) {
		this.ruwNum = ruwNum;
	}

	public int getTotCnt() {
		return totCnt;
	}

	public void setTotCnt(int totCnt) {
		this.totCnt = totCnt;
	}

	public String getPermitStatusName() {
		return permitStatusName;
	}

	public void setPermitStatusName(String permitStatusName) {
		this.permitStatusName = permitStatusName;
	}

	public String getIsJoin() {
		return isJoin;
	}

	public void setIsJoin(String isJoin) {
		this.isJoin = isJoin;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getGubun() {
		return gubun;
	}

	public void setGubun(String gubun) {
		this.gubun = gubun;
	}

	public String getPermitStatus() {
		return permitStatus;
	}

	public void setPermitStatus(String permitStatus) {
		this.permitStatus = permitStatus;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getPartnerStatus() {
		return partnerStatus;
	}

	public void setPartnerStatus(String partnerStatus) {
		this.partnerStatus = partnerStatus;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public int getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(int customerNo) {
		this.customerNo = customerNo;
	}

	public int getDriverNo() {
		return driverNo;
	}

	public void setDriverNo(int driverNo) {
		this.driverNo = driverNo;
	}

	public int getDrivingNo() {
		return drivingNo;
	}

	public void setDrivingNo(int drivingNo) {
		this.drivingNo = drivingNo;
	}

	public int getDrivingCnt() {
		return drivingCnt;
	}

	public void setDrivingCnt(int drivingCnt) {
		this.drivingCnt = drivingCnt;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRequestDt() {
		return requestDt;
	}

	public void setRequestDt(String requestDt) {
		this.requestDt = requestDt;
	}

	public String getDriverLv() {
		return driverLv;
	}

	public void setDriverLv(String driverLv) {
		this.driverLv = driverLv;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getTotalCnt() {
		return totalCnt;
	}

	public void setTotalCnt(int totalCnt) {
		this.totalCnt = totalCnt;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
}
