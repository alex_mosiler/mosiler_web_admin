package com.mosiler.web.admin.mapper.main;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.mosiler.web.admin.domain.mainVO;

@Repository("com.mosiler.web.admin.mapper.main.mainMapper")
@Mapper
public interface mainMapper {

	public int selectAdminUser(mainVO mainVO);
	
}
