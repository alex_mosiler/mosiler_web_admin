package com.mosiler.web.admin.domain;

import lombok.Data;

@Data
public class menuVO {
	int seq;
	int parentSeq;
	String menuNo;
	String menuTitle;
	int menuDepth;
	String menuUrl;
	String useYn;
	
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public int getParentSeq() {
		return parentSeq;
	}
	public void setParentSeq(int parentSeq) {
		this.parentSeq = parentSeq;
	}
	public String getMenuNo() {
		return menuNo;
	}
	public void setMenuNo(String menuNo) {
		this.menuNo = menuNo;
	}
	public String getMenuTitle() {
		return menuTitle;
	}
	public void setMenuTitle(String menuTitle) {
		this.menuTitle = menuTitle;
	}
	public int getMenuDepth() {
		return menuDepth;
	}
	public void setMenuDepth(int menuDepth) {
		this.menuDepth = menuDepth;
	}
	public String getMenuUrl() {
		return menuUrl;
	}
	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	
	@Override
	public String toString() {
		return "menuVO [seq=" + seq + ", parentSeq=" + parentSeq + ", menuNo=" + menuNo + ", menuTitle=" + menuTitle
				+ ", menuDepth=" + menuDepth + ", menuUrl=" + menuUrl + ", useYn=" + useYn + "]";
	}	
}
