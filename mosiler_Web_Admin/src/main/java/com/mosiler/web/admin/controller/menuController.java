package com.mosiler.web.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import com.mosiler.web.admin.domain.menuVO;
import com.mosiler.web.admin.service.menuService;

import net.sf.json.JSONArray;

@Controller
public class menuController {

	@Autowired
	private menuService menuService;
	
	/**
	 * 어드민 메뉴 (공통)
	 * @param 
	 * @return 
	 */
	public Model menuModel(Model model) {
		List<menuVO> cateList = menuService.cateList();
		List<menuVO> depthList = menuService.depthList();
		
		JSONArray cate = JSONArray.fromObject(cateList);
		JSONArray depth = JSONArray.fromObject(depthList);
		
		model.addAttribute("cateList", cate);
		model.addAttribute("depthList", depth);
		
		return model;
	}
}
