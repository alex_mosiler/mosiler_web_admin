package com.mosiler.web.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mosiler.web.admin.service.mainService;
import com.mosiler.web.admin.util.adminUtil;

@Controller
public class mainController {

	@Autowired
	mainService mainService;
	
	/**
	 * 로그인 페이지 이동
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/login")
	public String loginPage() {
		ModelAndView mav = new ModelAndView();
	    return "/login";
	}
	
	@RequestMapping(value = "/")
	public String loginPage1() {
		return "/login";
	}
	
	@RequestMapping(value = "/index")
	public String index() {
		return "/index";
	}
	
	/**
	 * 로그인 처리
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/procLogin")
	public String procLogin(HttpServletRequest request) throws Exception {
		String url = request.getRequestURL().toString();
		HttpSession session = request.getSession(true);
		
		// 유저정보 Check
		int userCnt = mainService.checkLoginUser(request);
		
		if(userCnt == 0) {
			// 유저 없을 시
			session.setAttribute("id", null);
			session.setAttribute("pwd", null);
			session.setAttribute("msg", "사용자 정보가 존재 하지 않습니다. 다시 입력해 주세요.");
		} else {
			/*if("alex".equals(request.getParameter("id"))) {
				session.setAttribute("id", request.getParameter("id"));	
				session.setAttribute("pwd", request.getParameter("pwd"));
				
				session.setAttribute("loginChannel", "local");
				
				// 세션 60분 유지
				session.setMaxInactiveInterval(60*60);
			} else {*/
				// OTP 키 비교
				boolean checkOtp = mainService.compareOtpKey(request);
				
				if(checkOtp) {
					String ip = adminUtil.getUserIp();
					
					System.out.println("Login IP : " + ip);
					
					// local
					if(ip.equals("0:0:0:0:0:0:0:1") || ip.equals("127.0.0.1")) {
						session.setAttribute("loginChannel", "local");
					} else {
						// dev
						if(url.contains("dev")) {
							session.setAttribute("loginChannel", "dev");
						} 
						// real
						else {
							session.setAttribute("loginChannel", "real");
						}
					}
					
					session.setAttribute("id", request.getParameter("id"));	
					session.setAttribute("pwd", request.getParameter("pwd"));
					
					// 세션 60분 유지
					session.setMaxInactiveInterval(60*60);
				} else {
					// OTP 키값 안 맞을 시
					session.setAttribute("id", null);
					session.setAttribute("pwd", null);
					session.setAttribute("msg", "OTP가 일치하지 않습니다. 다시 입력해 주세요.");
				}				
				/* } */
		}
		
		return "redirect:/";
	}

	/**
	 * 로그아웃 처리
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/logout")
	public String logout(HttpServletRequest request) throws Exception { 
		HttpSession session = request.getSession(true);
		
		session.invalidate();
		
		return "redirect:/";
	}
}
