package com.mosiler.web.admin.domain;

import java.math.BigInteger;
import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder // builder를 사용할수 있게 합니다.
@Data // user 필드값의 getter/setter를 자동으로 생성합니다.
@NoArgsConstructor // 인자없는 생성자를 자동으로 생성합니다.
@AllArgsConstructor // 인자를 모두 갖춘 생성자를 자동으로 생성합니다.
public class reservationVO {
	// TB_Driving VO
	int drivingNo;
	int driverNo;
	String driverName;
	String driverPhoneNumber;
	String driverPhoneNo;
	int customerNo;
	String customerName;
	String customerPhoneNo;
	String revStartDt;
	String revEndDt;
	String startDate;
	String endDate;
	String startAddress;
	String startLat;
	String startLng;
	String endAddress;
	String endLat;
	String endLng;
	String wayPointAddress;
	String wayPointLat;
	String wayPointLng;
	String wayPointAddress01;
	String wayPointLat01;
	String wayPointLng01;
	String wayPointAddress02;
	String wayPointLat02;
	String wayPointLng02;
	String wayPointAddress03;
	String wayPointLat03;
	String wayPointLng03;
	String wayPointAddress04;
	String wayPointLat04;
	String wayPointLng04;
	String drivingType;
	String drivingInfo;
	String drivingStatus;
	int chargeAmount;
	String paymentType;
	int discountRate;
	int couponCnt;
	int paymentFee;
	int driverCharge;
	int otherAmount;
	String paymentYn;
	int paymentAmount;
	int expensesTotalAmount;
	String mosilerComment;
	String customerComment;
	String reMark;
	String cancelDt;
	String regDt;
	String updateDt;
	int areaFee;
	int nightFee;
	String apiDrivingId;
	int preMiums;
	String delYn;
	String revStartAddress;
	String revStartLat;
	String revStartLng;
	String revEndAddress;
	String revEndLat;
	String revEndLng;
	String revWaypointAddress;
	String revWaypointLat;
	String revWaypointLng;
	String paidTicketYn;
	String passenger;
	String passengerPhoneNo;
	String urlLink;
	String carModel;
	String carNumner;
	String carMemo;
	int payServiceFee;
	int hourPrice;
	String hourPrice1;
	String billingYn;
	String billingDate;
	String paymentDate;
	int reserveCharge;
	int billingOtherAmount;
	String cardNumber;
	int BizCompNo;
	int noExpensesTotalAmount;
	String insuranceYn;
	String departureName;
	String releaseDt;
	String isRequestAirService;
	int regId;
	String regMethod;
	String cancelLimitDt;
	int couponId;
	int discountAmount;
	String dispatchTime;
	String[] conciergeCodeList;
	int payServiceNo;
	
	// 예약현황 VO
	int page;
	int pageSize;
	String gubun;
	String keyword;
	String state;
	String sort;
	String startDt;
	String endDt;
	String address;
	String keywordAddress;
	String drivingTypeName;
	String drivingStatusName;
	int requestCount;
	int requestStatus;
	String requestStatusName;
	String rowNum;
	int totCnt;
	String PaymentTypeName;
	String BankCode;
	String BankName;
	String CompName;
	String hour1;
	String incomeTax;
	String localTax;
	String deductuinAmount;
	String ticketBalance;
	String accountNumber;
	String partnerLv;
	long timeStamp;
	String wayPointYn;
	
	String date;
	int reservationCount; 
	int cancleReservationCount;
	int paymentAmmout;

	// 실시간 관제
	String nowDate;
	String nextDate;
	double distance;
	double angle;
	String name;
	String cnt;
	String flag;
	String searchKeyword;
	
	// TB_DriverLocationLog VO
	float lat;
	float lng;
	String locationDt;
	
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle = angle;
	}
	public String getSearchKeyword() {
		return searchKeyword;
	}
	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCnt() {
		return cnt;
	}
	public void setCnt(String cnt) {
		this.cnt = cnt;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getLng() {
		return lng;
	}
	public void setLng(float lng) {
		this.lng = lng;
	}
	public String getLocationDt() {
		return locationDt;
	}
	public void setLocationDt(String locationDt) {
		this.locationDt = locationDt;
	}
	public String getNowDate() {
		return nowDate;
	}
	public void setNowDate(String nowDate) {
		this.nowDate = nowDate;
	}
	public String getNextDate() {
		return nextDate;
	}
	public void setNextDate(String nextDate) {
		this.nextDate = nextDate;
	}
	public int getReservationCount() {
		return reservationCount;
	}
	public void setReservationCount(int reservationCount) {
		this.reservationCount = reservationCount;
	}
	public int getCancleReservationCount() {
		return cancleReservationCount;
	}
	public void setCancleReservationCount(int cancleReservationCount) {
		this.cancleReservationCount = cancleReservationCount;
	}
	public int getPaymentAmmout() {
		return paymentAmmout;
	}
	public void setPaymentAmmout(int paymentAmmout) {
		this.paymentAmmout = paymentAmmout;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getDrivingNo() {
		return drivingNo;
	}
	public void setDrivingNo(int drivingNo) {
		this.drivingNo = drivingNo;
	}
	public int getDriverNo() {
		return driverNo;
	}
	public void setDriverNo(int driverNo) {
		this.driverNo = driverNo;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getDriverPhoneNumber() {
		return driverPhoneNumber;
	}
	public void setDriverPhoneNumber(String driverPhoneNumber) {
		this.driverPhoneNumber = driverPhoneNumber;
	}
	public String getDriverPhoneNo() {
		return driverPhoneNo;
	}
	public void setDriverPhoneNo(String driverPhoneNo) {
		this.driverPhoneNo = driverPhoneNo;
	}
	public int getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(int customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerPhoneNo() {
		return customerPhoneNo;
	}
	public void setCustomerPhoneNo(String customerPhoneNo) {
		this.customerPhoneNo = customerPhoneNo;
	}
	public String getRevStartDt() {
		return revStartDt;
	}
	public void setRevStartDt(String revStartDt) {
		this.revStartDt = revStartDt;
	}
	public String getRevEndDt() {
		return revEndDt;
	}
	public void setRevEndDt(String revEndDt) {
		this.revEndDt = revEndDt;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartAddress() {
		return startAddress;
	}
	public void setStartAddress(String startAddress) {
		this.startAddress = startAddress;
	}
	public String getStartLat() {
		return startLat;
	}
	public void setStartLat(String startLat) {
		this.startLat = startLat;
	}
	public String getStartLng() {
		return startLng;
	}
	public void setStartLng(String startLng) {
		this.startLng = startLng;
	}
	public String getEndAddress() {
		return endAddress;
	}
	public void setEndAddress(String endAddress) {
		this.endAddress = endAddress;
	}
	public String getEndLat() {
		return endLat;
	}
	public void setEndLat(String endLat) {
		this.endLat = endLat;
	}
	public String getEndLng() {
		return endLng;
	}
	public void setEndLng(String endLng) {
		this.endLng = endLng;
	}
	public String getWayPointAddress() {
		return wayPointAddress;
	}
	public void setWayPointAddress(String wayPointAddress) {
		this.wayPointAddress = wayPointAddress;
	}
	public String getWayPointLat() {
		return wayPointLat;
	}
	public void setWayPointLat(String wayPointLat) {
		this.wayPointLat = wayPointLat;
	}
	public String getWayPointLng() {
		return wayPointLng;
	}
	public void setWayPointLng(String wayPointLng) {
		this.wayPointLng = wayPointLng;
	}
	public String getWayPointAddress01() {
		return wayPointAddress01;
	}
	public void setWayPointAddress01(String wayPointAddress01) {
		this.wayPointAddress01 = wayPointAddress01;
	}
	public String getWayPointLat01() {
		return wayPointLat01;
	}
	public void setWayPointLat01(String wayPointLat01) {
		this.wayPointLat01 = wayPointLat01;
	}
	public String getWayPointLng01() {
		return wayPointLng01;
	}
	public void setWayPointLng01(String wayPointLng01) {
		this.wayPointLng01 = wayPointLng01;
	}
	public String getWayPointAddress02() {
		return wayPointAddress02;
	}
	public void setWayPointAddress02(String wayPointAddress02) {
		this.wayPointAddress02 = wayPointAddress02;
	}
	public String getWayPointLat02() {
		return wayPointLat02;
	}
	public void setWayPointLat02(String wayPointLat02) {
		this.wayPointLat02 = wayPointLat02;
	}
	public String getWayPointLng02() {
		return wayPointLng02;
	}
	public void setWayPointLng02(String wayPointLng02) {
		this.wayPointLng02 = wayPointLng02;
	}
	public String getWayPointAddress03() {
		return wayPointAddress03;
	}
	public void setWayPointAddress03(String wayPointAddress03) {
		this.wayPointAddress03 = wayPointAddress03;
	}
	public String getWayPointLat03() {
		return wayPointLat03;
	}
	public void setWayPointLat03(String wayPointLat03) {
		this.wayPointLat03 = wayPointLat03;
	}
	public String getWayPointLng03() {
		return wayPointLng03;
	}
	public void setWayPointLng03(String wayPointLng03) {
		this.wayPointLng03 = wayPointLng03;
	}
	public String getWayPointAddress04() {
		return wayPointAddress04;
	}
	public void setWayPointAddress04(String wayPointAddress04) {
		this.wayPointAddress04 = wayPointAddress04;
	}
	public String getWayPointLat04() {
		return wayPointLat04;
	}
	public void setWayPointLat04(String wayPointLat04) {
		this.wayPointLat04 = wayPointLat04;
	}
	public String getWayPointLng04() {
		return wayPointLng04;
	}
	public void setWayPointLng04(String wayPointLng04) {
		this.wayPointLng04 = wayPointLng04;
	}
	public String getDrivingType() {
		return drivingType;
	}
	public void setDrivingType(String drivingType) {
		this.drivingType = drivingType;
	}
	public String getDrivingInfo() {
		return drivingInfo;
	}
	public void setDrivingInfo(String drivingInfo) {
		this.drivingInfo = drivingInfo;
	}
	public String getDrivingStatus() {
		return drivingStatus;
	}
	public void setDrivingStatus(String drivingStatus) {
		this.drivingStatus = drivingStatus;
	}
	public int getChargeAmount() {
		return chargeAmount;
	}
	public void setChargeAmount(int chargeAmount) {
		this.chargeAmount = chargeAmount;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public int getDiscountRate() {
		return discountRate;
	}
	public void setDiscountRate(int discountRate) {
		this.discountRate = discountRate;
	}
	public int getCouponCnt() {
		return couponCnt;
	}
	public void setCouponCnt(int couponCnt) {
		this.couponCnt = couponCnt;
	}
	public int getPaymentFee() {
		return paymentFee;
	}
	public void setPaymentFee(int paymentFee) {
		this.paymentFee = paymentFee;
	}
	public int getDriverCharge() {
		return driverCharge;
	}
	public void setDriverCharge(int driverCharge) {
		this.driverCharge = driverCharge;
	}
	public int getOtherAmount() {
		return otherAmount;
	}
	public void setOtherAmount(int otherAmount) {
		this.otherAmount = otherAmount;
	}
	public String getPaymentYn() {
		return paymentYn;
	}
	public void setPaymentYn(String paymentYn) {
		this.paymentYn = paymentYn;
	}
	public int getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(int paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public int getExpensesTotalAmount() {
		return expensesTotalAmount;
	}
	public void setExpensesTotalAmount(int expensesTotalAmount) {
		this.expensesTotalAmount = expensesTotalAmount;
	}
	public String getMosilerComment() {
		return mosilerComment;
	}
	public void setMosilerComment(String mosilerComment) {
		this.mosilerComment = mosilerComment;
	}
	public String getCustomerComment() {
		return customerComment;
	}
	public void setCustomerComment(String customerComment) {
		this.customerComment = customerComment;
	}
	public String getReMark() {
		return reMark;
	}
	public void setReMark(String reMark) {
		this.reMark = reMark;
	}
	public String getCancelDt() {
		return cancelDt;
	}
	public void setCancelDt(String cancelDt) {
		this.cancelDt = cancelDt;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(String updateDt) {
		this.updateDt = updateDt;
	}
	public int getAreaFee() {
		return areaFee;
	}
	public void setAreaFee(int areaFee) {
		this.areaFee = areaFee;
	}
	public int getNightFee() {
		return nightFee;
	}
	public void setNightFee(int nightFee) {
		this.nightFee = nightFee;
	}
	public String getApiDrivingId() {
		return apiDrivingId;
	}
	public void setApiDrivingId(String apiDrivingId) {
		this.apiDrivingId = apiDrivingId;
	}
	public int getPreMiums() {
		return preMiums;
	}
	public void setPreMiums(int preMiums) {
		this.preMiums = preMiums;
	}
	public String getDelYn() {
		return delYn;
	}
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}
	public String getRevStartAddress() {
		return revStartAddress;
	}
	public void setRevStartAddress(String revStartAddress) {
		this.revStartAddress = revStartAddress;
	}
	public String getRevStartLat() {
		return revStartLat;
	}
	public void setRevStartLat(String revStartLat) {
		this.revStartLat = revStartLat;
	}
	public String getRevStartLng() {
		return revStartLng;
	}
	public void setRevStartLng(String revStartLng) {
		this.revStartLng = revStartLng;
	}
	public String getRevEndAddress() {
		return revEndAddress;
	}
	public void setRevEndAddress(String revEndAddress) {
		this.revEndAddress = revEndAddress;
	}
	public String getRevEndLat() {
		return revEndLat;
	}
	public void setRevEndLat(String revEndLat) {
		this.revEndLat = revEndLat;
	}
	public String getRevEndLng() {
		return revEndLng;
	}
	public void setRevEndLng(String revEndLng) {
		this.revEndLng = revEndLng;
	}
	public String getRevWaypointAddress() {
		return revWaypointAddress;
	}
	public void setRevWaypointAddress(String revWaypointAddress) {
		this.revWaypointAddress = revWaypointAddress;
	}
	public String getRevWaypointLat() {
		return revWaypointLat;
	}
	public void setRevWaypointLat(String revWaypointLat) {
		this.revWaypointLat = revWaypointLat;
	}
	public String getRevWaypointLng() {
		return revWaypointLng;
	}
	public void setRevWaypointLng(String revWaypointLng) {
		this.revWaypointLng = revWaypointLng;
	}
	public String getPaidTicketYn() {
		return paidTicketYn;
	}
	public void setPaidTicketYn(String paidTicketYn) {
		this.paidTicketYn = paidTicketYn;
	}
	public String getPassenger() {
		return passenger;
	}
	public void setPassenger(String passenger) {
		this.passenger = passenger;
	}
	public String getPassengerPhoneNo() {
		return passengerPhoneNo;
	}
	public void setPassengerPhoneNo(String passengerPhoneNo) {
		this.passengerPhoneNo = passengerPhoneNo;
	}
	public String getUrlLink() {
		return urlLink;
	}
	public void setUrlLink(String urlLink) {
		this.urlLink = urlLink;
	}
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	public String getCarNumner() {
		return carNumner;
	}
	public void setCarNumner(String carNumner) {
		this.carNumner = carNumner;
	}
	public String getCarMemo() {
		return carMemo;
	}
	public void setCarMemo(String carMemo) {
		this.carMemo = carMemo;
	}
	public int getPayServiceFee() {
		return payServiceFee;
	}
	public void setPayServiceFee(int payServiceFee) {
		this.payServiceFee = payServiceFee;
	}
	public int getHourPrice() {
		return hourPrice;
	}
	public void setHourPrice(int hourPrice) {
		this.hourPrice = hourPrice;
	}
	public String getHourPrice1() {
		return hourPrice1;
	}
	public void setHourPrice1(String hourPrice1) {
		this.hourPrice1 = hourPrice1;
	}
	public String getBillingYn() {
		return billingYn;
	}
	public void setBillingYn(String billingYn) {
		this.billingYn = billingYn;
	}
	public String getBillingDate() {
		return billingDate;
	}
	public void setBillingDate(String billingDate) {
		this.billingDate = billingDate;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public int getReserveCharge() {
		return reserveCharge;
	}
	public void setReserveCharge(int reserveCharge) {
		this.reserveCharge = reserveCharge;
	}
	public int getBillingOtherAmount() {
		return billingOtherAmount;
	}
	public void setBillingOtherAmount(int billingOtherAmount) {
		this.billingOtherAmount = billingOtherAmount;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public int getBizCompNo() {
		return BizCompNo;
	}
	public void setBizCompNo(int bizCompNo) {
		BizCompNo = bizCompNo;
	}
	public int getNoExpensesTotalAmount() {
		return noExpensesTotalAmount;
	}
	public void setNoExpensesTotalAmount(int noExpensesTotalAmount) {
		this.noExpensesTotalAmount = noExpensesTotalAmount;
	}
	public String getInsuranceYn() {
		return insuranceYn;
	}
	public void setInsuranceYn(String insuranceYn) {
		this.insuranceYn = insuranceYn;
	}
	public String getDepartureName() {
		return departureName;
	}
	public void setDepartureName(String departureName) {
		this.departureName = departureName;
	}
	public String getReleaseDt() {
		return releaseDt;
	}
	public void setReleaseDt(String releaseDt) {
		this.releaseDt = releaseDt;
	}
	public String getIsRequestAirService() {
		return isRequestAirService;
	}
	public void setIsRequestAirService(String isRequestAirService) {
		this.isRequestAirService = isRequestAirService;
	}
	public int getRegId() {
		return regId;
	}
	public void setRegId(int regId) {
		this.regId = regId;
	}
	public String getRegMethod() {
		return regMethod;
	}
	public void setRegMethod(String regMethod) {
		this.regMethod = regMethod;
	}
	public String getCancelLimitDt() {
		return cancelLimitDt;
	}
	public void setCancelLimitDt(String cancelLimitDt) {
		this.cancelLimitDt = cancelLimitDt;
	}
	public int getCouponId() {
		return couponId;
	}
	public void setCouponId(int couponId) {
		this.couponId = couponId;
	}
	public int getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(int discountAmount) {
		this.discountAmount = discountAmount;
	}
	public String getDispatchTime() {
		return dispatchTime;
	}
	public void setDispatchTime(String dispatchTime) {
		this.dispatchTime = dispatchTime;
	}
	public String[] getConciergeCodeList() {
		return conciergeCodeList;
	}
	public void setConciergeCodeList(String[] conciergeCodeList) {
		this.conciergeCodeList = conciergeCodeList;
	}
	public int getPayServiceNo() {
		return payServiceNo;
	}
	public void setPayServiceNo(int payServiceNo) {
		this.payServiceNo = payServiceNo;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getGubun() {
		return gubun;
	}
	public void setGubun(String gubun) {
		this.gubun = gubun;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getStartDt() {
		return startDt;
	}
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
	public String getEndDt() {
		return endDt;
	}
	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getKeywordAddress() {
		return keywordAddress;
	}
	public void setKeywordAddress(String keywordAddress) {
		this.keywordAddress = keywordAddress;
	}
	public String getDrivingTypeName() {
		return drivingTypeName;
	}
	public void setDrivingTypeName(String drivingTypeName) {
		this.drivingTypeName = drivingTypeName;
	}
	public String getDrivingStatusName() {
		return drivingStatusName;
	}
	public void setDrivingStatusName(String drivingStatusName) {
		this.drivingStatusName = drivingStatusName;
	}
	public int getRequestCount() {
		return requestCount;
	}
	public void setRequestCount(int requestCount) {
		this.requestCount = requestCount;
	}
	public int getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(int requestStatus) {
		this.requestStatus = requestStatus;
	}
	public String getRequestStatusName() {
		return requestStatusName;
	}
	public void setRequestStatusName(String requestStatusName) {
		this.requestStatusName = requestStatusName;
	}
	public String getRowNum() {
		return rowNum;
	}
	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}
	public int getTotCnt() {
		return totCnt;
	}
	public void setTotCnt(int totCnt) {
		this.totCnt = totCnt;
	}
	public String getPaymentTypeName() {
		return PaymentTypeName;
	}
	public void setPaymentTypeName(String paymentTypeName) {
		PaymentTypeName = paymentTypeName;
	}
	public String getBankCode() {
		return BankCode;
	}
	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}
	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	public String getCompName() {
		return CompName;
	}
	public void setCompName(String compName) {
		CompName = compName;
	}
	public String getHour1() {
		return hour1;
	}
	public void setHour1(String hour1) {
		this.hour1 = hour1;
	}
	public String getIncomeTax() {
		return incomeTax;
	}
	public void setIncomeTax(String incomeTax) {
		this.incomeTax = incomeTax;
	}
	public String getLocalTax() {
		return localTax;
	}
	public void setLocalTax(String localTax) {
		this.localTax = localTax;
	}
	public String getDeductuinAmount() {
		return deductuinAmount;
	}
	public void setDeductuinAmount(String deductuinAmount) {
		this.deductuinAmount = deductuinAmount;
	}
	public String getTicketBalance() {
		return ticketBalance;
	}
	public void setTicketBalance(String ticketBalance) {
		this.ticketBalance = ticketBalance;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getPartnerLv() {
		return partnerLv;
	}
	public void setPartnerLv(String partnerLv) {
		this.partnerLv = partnerLv;
	}
	public long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getWayPointYn() {
		return wayPointYn;
	}
	public void setWayPointYn(String wayPointYn) {
		this.wayPointYn = wayPointYn;
	}
	
	@Override
	public String toString() {
		return "reservationVO [drivingNo=" + drivingNo + ", driverNo=" + driverNo + ", driverName=" + driverName
				+ ", driverPhoneNumber=" + driverPhoneNumber + ", driverPhoneNo=" + driverPhoneNo + ", customerNo="
				+ customerNo + ", customerName=" + customerName + ", customerPhoneNo=" + customerPhoneNo
				+ ", revStartDt=" + revStartDt + ", revEndDt=" + revEndDt + ", startDate=" + startDate + ", endDate="
				+ endDate + ", startAddress=" + startAddress + ", startLat=" + startLat + ", startLng=" + startLng
				+ ", endAddress=" + endAddress + ", endLat=" + endLat + ", endLng=" + endLng + ", wayPointAddress="
				+ wayPointAddress + ", wayPointLat=" + wayPointLat + ", wayPointLng=" + wayPointLng
				+ ", wayPointAddress01=" + wayPointAddress01 + ", wayPointLat01=" + wayPointLat01 + ", wayPointLng01="
				+ wayPointLng01 + ", wayPointAddress02=" + wayPointAddress02 + ", wayPointLat02=" + wayPointLat02
				+ ", wayPointLng02=" + wayPointLng02 + ", wayPointAddress03=" + wayPointAddress03 + ", wayPointLat03="
				+ wayPointLat03 + ", wayPointLng03=" + wayPointLng03 + ", wayPointAddress04=" + wayPointAddress04
				+ ", wayPointLat04=" + wayPointLat04 + ", wayPointLng04=" + wayPointLng04 + ", drivingType="
				+ drivingType + ", drivingInfo=" + drivingInfo + ", drivingStatus=" + drivingStatus + ", chargeAmount="
				+ chargeAmount + ", paymentType=" + paymentType + ", discountRate=" + discountRate + ", couponCnt="
				+ couponCnt + ", paymentFee=" + paymentFee + ", driverCharge=" + driverCharge + ", otherAmount="
				+ otherAmount + ", paymentYn=" + paymentYn + ", paymentAmount=" + paymentAmount
				+ ", expensesTotalAmount=" + expensesTotalAmount + ", mosilerComment=" + mosilerComment
				+ ", customerComment=" + customerComment + ", reMark=" + reMark + ", cancelDt=" + cancelDt + ", regDt="
				+ regDt + ", updateDt=" + updateDt + ", areaFee=" + areaFee + ", nightFee=" + nightFee
				+ ", apiDrivingId=" + apiDrivingId + ", preMiums=" + preMiums + ", delYn=" + delYn
				+ ", revStartAddress=" + revStartAddress + ", revStartLat=" + revStartLat + ", revStartLng="
				+ revStartLng + ", revEndAddress=" + revEndAddress + ", revEndLat=" + revEndLat + ", revEndLng="
				+ revEndLng + ", revWaypointAddress=" + revWaypointAddress + ", revWaypointLat=" + revWaypointLat
				+ ", revWaypointLng=" + revWaypointLng + ", paidTicketYn=" + paidTicketYn + ", passenger=" + passenger
				+ ", passengerPhoneNo=" + passengerPhoneNo + ", urlLink=" + urlLink + ", carModel=" + carModel
				+ ", carNumner=" + carNumner + ", carMemo=" + carMemo + ", payServiceFee=" + payServiceFee
				+ ", hourPrice=" + hourPrice + ", hourPrice1=" + hourPrice1 + ", billingYn=" + billingYn
				+ ", billingDate=" + billingDate + ", paymentDate=" + paymentDate + ", reserveCharge=" + reserveCharge
				+ ", billingOtherAmount=" + billingOtherAmount + ", cardNumber=" + cardNumber + ", BizCompNo="
				+ BizCompNo + ", noExpensesTotalAmount=" + noExpensesTotalAmount + ", insuranceYn=" + insuranceYn
				+ ", departureName=" + departureName + ", releaseDt=" + releaseDt + ", isRequestAirService="
				+ isRequestAirService + ", regId=" + regId + ", regMethod=" + regMethod + ", cancelLimitDt="
				+ cancelLimitDt + ", couponId=" + couponId + ", discountAmount=" + discountAmount + ", dispatchTime="
				+ dispatchTime + ", conciergeCodeList=" + Arrays.toString(conciergeCodeList) + ", payServiceNo="
				+ payServiceNo + ", page=" + page + ", pageSize=" + pageSize + ", gubun=" + gubun + ", keyword="
				+ keyword + ", state=" + state + ", sort=" + sort + ", startDt=" + startDt + ", endDt=" + endDt
				+ ", address=" + address + ", keywordAddress=" + keywordAddress + ", drivingTypeName=" + drivingTypeName
				+ ", drivingStatusName=" + drivingStatusName + ", requestCount=" + requestCount + ", requestStatus="
				+ requestStatus + ", requestStatusName=" + requestStatusName + ", rowNum=" + rowNum + ", totCnt="
				+ totCnt + ", PaymentTypeName=" + PaymentTypeName + ", BankCode=" + BankCode + ", BankName=" + BankName
				+ ", CompName=" + CompName + ", hour1=" + hour1 + ", incomeTax=" + incomeTax + ", localTax=" + localTax
				+ ", deductuinAmount=" + deductuinAmount + ", ticketBalance=" + ticketBalance + ", accountNumber="
				+ accountNumber + ", partnerLv=" + partnerLv + ", timeStamp=" + timeStamp + ", wayPointYn=" + wayPointYn
				+ ", date=" + date + ", reservationCount=" + reservationCount + ", cancleReservationCount="
				+ cancleReservationCount + ", paymentAmmout=" + paymentAmmout + ", nowDate=" + nowDate + ", nextDate="
				+ nextDate + ", distance=" + distance + ", name=" + name + ", cnt=" + cnt + ", flag=" + flag + ", lat="
				+ lat + ", lng=" + lng + ", locationDt=" + locationDt + "]";
	}
}
