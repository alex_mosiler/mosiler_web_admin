package com.mosiler.web.admin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mosiler.web.admin.domain.codeVO;
import com.mosiler.web.admin.mapper.code.codeMapper;

@Service
public class codeService {
	
	@Autowired 
	private codeMapper codeMapper;

	// 배차 상태 구분 코드
	public List<codeVO> getDrivingStatusCodeList() {
		return codeMapper.getDrivingStatusCodeList();
	}
	
	// 운행 구분 공통 코드
	public List<codeVO> getDrivingTypeCodeList() {
		return codeMapper.getDrivingTypeCodeList();
	}

	// 컨시어지 구분 공통 코드
	public List<codeVO> getConciergeCodeList() {
		return codeMapper.getConciergeCodeList();
	}

	// 할인율 구분 공통 코드
	public List<codeVO> getHourPriceCodeList() {
		return codeMapper.getHourPriceCodeList();
	}

	// 결제 수단 구분 공통 코드
	public List<codeVO> getPaymentTypeCodeList() {
		return codeMapper.getPaymentTypeCodeList();
	}
	
	// 프로모션 Type 공통 코드
	public List<codeVO> getPromotionTypeList() {
		return codeMapper.getPromotionTypeList();
	}

	// 프로모션 userTargetType 공통 코드
	public List<codeVO> getUserTargetTypeList() {
		return codeMapper.getUserTargetTypeList();
	}

	// 프로모션 Status 공통 코드
	public List<codeVO> getPromotionStatusList() {
		return codeMapper.getPromotionStatusList();
	}

	// 프로모션 규칙 조건 유형 공통 코드
	public List<codeVO> getConditionTypeList() {
		return codeMapper.getConditionTypeList();
	}

	// 드라이버 상태 공통 코드
	public List<codeVO> getPartnerStatusCodeList() {
		return codeMapper.getPartnerStatusCodeList();
	}

	// 운행 종류 구분 공통 코드
	public List<codeVO> getPermitStatusCodeList() {
		return codeMapper.getPermitStatusCodeList();
	}
	
	// 은행 종류 구분 공통 코드
	public List<codeVO> getBankCodeList() {
		return codeMapper.getBankCodeList();
	}
	
	// 소속 법인 구분 공통 코드
	public List<codeVO> getBizCompList() {
		return codeMapper.getBizCompList();
	}
}
