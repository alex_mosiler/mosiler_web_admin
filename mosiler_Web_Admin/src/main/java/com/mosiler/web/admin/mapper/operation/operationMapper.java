package com.mosiler.web.admin.mapper.operation;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.mosiler.web.admin.domain.reservationVO;

@Repository("com.mosiler.web.admin.mapper.operation.operationMapper")
@Mapper
public interface operationMapper {

	List<reservationVO> selectDrivingData(String revStartDt);
	int updateReservationDt(reservationVO reservationVO);
	int cancelReservation(reservationVO reservationVO);
	int reservationCopy(reservationVO reservationVO);
	List<reservationVO> operationDayCount(reservationVO reservationVO);
	List<reservationVO> getDrivingNoList(reservationVO reservationVO);
	List<reservationVO> getDriverLocationLog(reservationVO reservationVO);
	List<reservationVO> getDrivingCnt(reservationVO reservationVO);
	List<reservationVO> getCurrentDrivingList(reservationVO reservationVO);
	List<reservationVO> getDrivingData(reservationVO reservationVO);
	List<reservationVO> getCurrentLatLng(reservationVO reservationVO);

}
