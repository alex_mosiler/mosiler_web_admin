package com.mosiler.web.admin.mapper.system;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.mosiler.web.admin.domain.systemVO;

@Repository("com.mosiler.web.admin.mapper.system.systemMapper")
@Mapper
public interface systemMapper {

	public int adminUserListCnt(systemVO systemVO);
	public List<systemVO> adminUserList(systemVO systemVO);
	public void updatePw(systemVO systemVO);
	public void updateAdminUser(systemVO systemVO);
	public int getOtpKeyCnt(systemVO systemVO);
	public void insertOtpKey(systemVO systemVO);
	public void updateOtpKey(systemVO systemVO);
	public String getOtpKey(systemVO systemVO);
	public int faqListCnt(systemVO systemVO);
	public List<systemVO> faqList(systemVO systemVO);
	public List<systemVO> faqDetail(systemVO systemVO);
	public void updateFaq(systemVO systemVO);
	public void insertFaq(systemVO systemVO);

}
