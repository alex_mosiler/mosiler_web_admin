package com.mosiler.web.admin.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder // builder를 사용할수 있게 합니다.
@Data // user 필드값의 getter/setter를 자동으로 생성합니다.
@NoArgsConstructor // 인자없는 생성자를 자동으로 생성합니다.
@AllArgsConstructor // 인자를 모두 갖춘 생성자를 자동으로 생성합니다.
public class systemVO {
	// TB_Faq
	String id;
	int faqNo;
	String faqQuestion;
	String faqAnswer;
	String faqParentCategory;
	String faqCategory;
	String faqTopYn;
	String useYn;
	String regId2;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getFaqNo() {
		return faqNo;
	}
	public void setFaqNo(int faqNo) {
		this.faqNo = faqNo;
	}
	public String getFaqQuestion() {
		return faqQuestion;
	}
	public void setFaqQuestion(String faqQuestion) {
		this.faqQuestion = faqQuestion;
	}
	public String getFaqAnswer() {
		return faqAnswer;
	}
	public void setFaqAnswer(String faqAnswer) {
		this.faqAnswer = faqAnswer;
	}
	public String getFaqParentCategory() {
		return faqParentCategory;
	}
	public void setFaqParentCategory(String faqParentCategory) {
		this.faqParentCategory = faqParentCategory;
	}
	public String getFaqCategory() {
		return faqCategory;
	}
	public void setFaqCategory(String faqCategory) {
		this.faqCategory = faqCategory;
	}
	public String getFaqTopYn() {
		return faqTopYn;
	}
	public void setFaqTopYn(String faqTopYn) {
		this.faqTopYn = faqTopYn;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegId2() {
		return regId2;
	}
	public void setRegId2(String regId2) {
		this.regId2 = regId2;
	}

	// search VO
	String searchType;
	String searchKeyword;
	
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getSearchKeyword() {
		return searchKeyword;
	}
	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	// paging VO
	int count;
	int start;
	int end;
	int totalCnt;
	int rownum;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public int getTotalCnt() {
		return totalCnt;
	}
	public void setTotalCnt(int totalCnt) {
		this.totalCnt = totalCnt;
	}
	public int getRownum() {
		return rownum;
	}
	public void setRownum(int rownum) {
		this.rownum = rownum;
	}

	//  비번 변경용
	String userPw;
	
	public String getUserPw() {
		return userPw;
	}
	public void setUserPw(String userPw) {
		this.userPw = userPw;
	}
	
	// OTP
	String otpKey;
	
	public String getOtpKey() {
		return otpKey;
	}
	public void setOtpKey(String otpKey) {
		this.otpKey = otpKey;
	}
	
	// TB_Manager
	int userNo;
	String userId;
	String userName;
	String email;
	String phoneNumber;
	String reMark;
	String lastAccessDate;
	int authNo;
	String regDt;
	int regId;
	String updateDt;
	int updateId;
	
	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getReMark() {
		return reMark;
	}
	public void setReMark(String reMark) {
		this.reMark = reMark;
	}
	public String getLastAccessDate() {
		return lastAccessDate;
	}
	public void setLastAccessDate(String lastAccessDate) {
		this.lastAccessDate = lastAccessDate;
	}
	public int getAuthNo() {
		return authNo;
	}
	public void setAuthNo(int authNo) {
		this.authNo = authNo;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public int getRegId() {
		return regId;
	}
	public void setRegId(int regId) {
		this.regId = regId;
	}
	public String getUpdateDt() {
		return updateDt;
	}
	public void setUpdateDt(String updateDt) {
		this.updateDt = updateDt;
	}
	public int getUpdateId() {
		return updateId;
	}
	public void setUpdateId(int updateId) {
		this.updateId = updateId;
	}
	@Override
	public String toString() {
		return "systemVO [id=" + id + ", faqNo=" + faqNo + ", faqQuestion=" + faqQuestion + ", faqAnswer=" + faqAnswer
				+ ", faqParentCategory=" + faqParentCategory + ", faqCategory=" + faqCategory + ", faqTopYn=" + faqTopYn
				+ ", useYn=" + useYn + ", regId2=" + regId2 + ", searchType=" + searchType + ", searchKeyword="
				+ searchKeyword + ", count=" + count + ", start=" + start + ", end=" + end + ", totalCnt=" + totalCnt
				+ ", rownum=" + rownum + ", userPw=" + userPw + ", otpKey=" + otpKey + ", userNo=" + userNo
				+ ", userId=" + userId + ", userName=" + userName + ", email=" + email + ", phoneNumber=" + phoneNumber
				+ ", reMark=" + reMark + ", lastAccessDate=" + lastAccessDate + ", authNo=" + authNo + ", regDt="
				+ regDt + ", regId=" + regId + ", updateDt=" + updateDt + ", updateId=" + updateId + "]";
	}
	
	
}
