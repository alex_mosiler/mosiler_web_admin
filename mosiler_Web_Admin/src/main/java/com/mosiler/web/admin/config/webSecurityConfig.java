//package com.mosiler.web.admin.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@Configuration
//@EnableWebSecurity
//public class webSecurityConfig extends WebSecurityConfigurerAdapter {
//	
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		http.authorizeRequests()
////		.antMatchers("/system/**")
////		.access("hasRole('ROLE_VIEW')")
//		.antMatchers("/").permitAll()
//		.anyRequest().authenticated()
//		.and()
//		.formLogin()
//		.loginPage("/login")
//		.defaultSuccessUrl("/index").permitAll()
//		.usernameParameter("sID")
//		.passwordParameter("sPassword")
//		.and()
//		.logout().permitAll();
//	}
//	
//	@Override
//	public void configure(WebSecurity web) throws Exception {
//	  web.ignoring().antMatchers("/images/**", "/css/**", "/js/**");
//	}
//}
