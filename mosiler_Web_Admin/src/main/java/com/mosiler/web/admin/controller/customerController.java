package com.mosiler.web.admin.controller;

import java.util.HashMap;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mosiler.web.admin.domain.codeVO;
import com.mosiler.web.admin.domain.customerVO;
import com.mosiler.web.admin.domain.pagingVO;
import com.mosiler.web.admin.service.codeService;
import com.mosiler.web.admin.service.customerService;
import com.mosiler.web.admin.util.adminUtil;

import net.sf.json.JSONArray;

@Controller
@RequestMapping("/customer")
public class customerController {

	@Autowired
	private menuController menuController;
	
	@Autowired
	private customerService customerService;
	
	@Autowired
	private codeService codeService;

	/**
	 * 고객 현황
	 * @param 
	 * @return 
	 */
	@RequestMapping(value="/customerList")
	//@ResponseBody
	public ModelAndView customerList(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		
		// 현재 페이지 번호 세팅
		String nowPage = request.getParameter("nowPage");
		String cntPerPage = request.getParameter("cntPerPage");
		
		// 검색어 세팅
		if(request.getParameter("searchType") != null && request.getParameter("searchKeyword") != null) {
			customerVO.setSearchType(request.getParameter("searchType"));
			customerVO.setSearchKeyword(request.getParameter("searchKeyword"));
		}

		// 검색 조건 세팅
		customerVO.setCustomerType(request.getParameter("customerType"));
		customerVO.setOsType(request.getParameter("osType"));
		customerVO.setStartDate(request.getParameter("startDate"));
		customerVO.setEndDate(request.getParameter("endDate"));
		
		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		// 고객 현황 Count
		int customerListCnt = customerService.customerListCnt(customerVO);
		pagingVO = new pagingVO(customerListCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
		
		// 고객 현황
		List<customerVO> list = customerService.customerList(customerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
	
		JSONArray customerList = JSONArray.fromObject(list);
		
		mav.addObject("cateList", menuModel.getAttribute("cateList"));
		mav.addObject("depthList",menuModel.getAttribute("depthList"));
		mav.addObject("customerList", customerList);
		mav.addObject(pagingVO);
		mav.addObject(customerVO);

		return mav;

//		// 메뉴 호출 (공통)
//		Model menuModel = menuController.menuModel(model);
//		
//		// 검색어 세팅
//		if(request.getParameter("searchType") != null && request.getParameter("searchKeyword") != null) {
//			customerVO.setSearchType(request.getParameter("searchType"));
//			customerVO.setSearchKeyword(request.getParameter("searchKeyword"));
//		}
//		
//		// 검색 조건 세팅
//		customerVO.setCustomerType(request.getParameter("customerType"));
//		customerVO.setOsType(request.getParameter("osType"));
//		customerVO.setStartDate(request.getParameter("startDate"));
//		customerVO.setEndDate(request.getParameter("endDate"));
//		
//		// 페이징 계산
//		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
//		
//		// 고객 현황 Count
//		int customerListCnt = customerService.customerListCnt(customerVO);
//
//		pagingVO = new pagingVO(customerListCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
//		
//		// 고객 현황
//		List<customerVO> list = customerService.customerList(customerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
//		
//		JSONArray customerList = JSONArray.fromObject(list);
//		
//		model.addAttribute("cateList", menuModel.getAttribute("cateList"));
//		model.addAttribute("depthList",menuModel.getAttribute("depthList"));
//		model.addAttribute("pagingVO", pagingVO);
//		model.addAttribute("customerList", customerList);
//		model.addAttribute("customerVO", customerVO);
//		
//		return model;	
	}
	
	/**
	 * 고객 상세 팝업
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/customerDetailpopUp")
	public String customerDetailpopUp(Model model, pagingVO pagingVO, customerVO customerVO
			, @RequestParam(value="nowPage", required = false) String nowPage
			, @RequestParam(value="cntPerPage", required = false) String cntPerPage
			, @RequestParam(value="customerNo", required = false) String customerNo) {
		
		// customerNo 세팅
		customerVO.setCustomerNo(Integer.parseInt(customerNo));

		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		// 1. 이용금액, 이용건수, 최종 운행일  
		List<customerVO> useInfo = customerService.useInfo(customerVO);

		// 2. 가입 정보
		List<customerVO> customerInfo = customerService.customerInfo(customerVO);
	
		// 사용 현황 Count
		int customerUseInfoCnt = customerService.customerUseInfoCnt(customerVO);
		pagingVO = new pagingVO(customerUseInfoCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
		
		// 3. 사용 현황
		List<customerVO> customerUseInfo = customerService.customerUseInfo(customerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
		
		// 쿠폰 내역 Count
		int couponListCnt = customerService.customerCouponListCnt(customerVO);
		
		// 4. 쿠폰 내역
		List<customerVO> couponList = customerService.customerCouponList(customerVO);

		model.addAttribute("customerNo", customerVO.getCustomerNo());
		model.addAttribute("useInfo", useInfo);
		model.addAttribute("customerInfo", customerInfo);
		model.addAttribute("paging", pagingVO);
		model.addAttribute("customerUseInfo", customerUseInfo);
		model.addAttribute("couponList", couponList);
		
		return "/customer/popup/customerDetail-popUp";
	}
	
	/**
	 * 고객 상세 팝업 - 고객 등급
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/customerGrade")
	public String customerGrade(Model model, @RequestParam(value="customerNo", required = false ) String customerNo) {
		model.addAttribute("customerNo", customerNo);
		
		return "/customer/popup/customerGrade-popUp";
	}
	
	/**
	 * 고객 상세 팝업 - 예약 등록
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/customerReservationpopUp")
	public String customerReservation(Model model, customerVO customerVO, codeVO codeVO, @RequestParam(value="customerNo", required = false) String customerNo) {
		
		// customerNo 세팅
		customerVO.setCustomerNo(Integer.parseInt(customerNo));

		// 운행 구분 공통 코드
		List<codeVO> codeList = codeService.getDrivingTypeCodeList();
		
		// 컨시어지 구분 공통 코드
		List<codeVO> conciergeCodeList = codeService.getConciergeCodeList();
		
		// 할인율 구분 공통 코드
		List<codeVO> hourPriceCodeList = codeService.getHourPriceCodeList();
		
		// 결제 수단 구분 공통 코드
		List<codeVO> paymentTypeCodeList = codeService.getPaymentTypeCodeList();
		
		// 고객 이름, 전화번호
		List<customerVO> customerInfo = customerService.customerInfo(customerVO);
		
		model.addAttribute("customerNo", customerVO.getCustomerNo());
		model.addAttribute("codeList", codeList);
		model.addAttribute("conciergeCodeList", conciergeCodeList);
		model.addAttribute("hourPriceCodeList", hourPriceCodeList);
		model.addAttribute("paymentTypeCodeList", paymentTypeCodeList);
		model.addAttribute("customerInfo", customerInfo);
		
		return "/customer/popup/customerReservation-popUp";
	}
	
	/**
	 * 고객 상세 팝업 - Open Map
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/reservationMap")
	public String reservationMap(HttpServletRequest request, Model model, @RequestParam(value="num", required = false) String num) {
		model.addAttribute(num);
		
		return "/reservation/reservationMap";
	}

	/**
	 * 고객 선택 팝업 (예약등록)
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/customerSelectPopup")
	public String customerSelectPopup(Model model, HttpServletRequest request
			, customerVO customerVO, pagingVO pagingVO
			, @RequestParam(value="nowPage", required = false) String nowPage
			, @RequestParam(value="cntPerPage", required = false) String cntPerPage) {
		
		// 검색어 세팅
		if(request.getParameter("searchType") != null && request.getParameter("searchKeyword") != null) {
			customerVO.setSearchType(request.getParameter("searchType"));
			customerVO.setSearchKeyword(request.getParameter("searchKeyword"));
		}
		
		// 검색 조건 세팅
		customerVO.setCustomerType(request.getParameter("customerType"));
		customerVO.setOsType(request.getParameter("osType"));
		customerVO.setStartDate(request.getParameter("startDate"));
		customerVO.setEndDate(request.getParameter("endDate"));
		
		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		// 고객 현황 Count
		int customerListCnt = customerService.customerListCnt(customerVO);
		pagingVO = new pagingVO(customerListCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
		
		// 고객 현황
		List<customerVO> customerList = customerService.customerList(customerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
		
		model.addAttribute("paging", pagingVO);
		model.addAttribute("customerList", customerList);
		model.addAttribute("customerVO", customerVO);
		
		return "/customer/popup/customerSelect-popUp";
	}
	
	/**
	 * 서비스 신청자
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/applyServiceUser")
	public ModelAndView applyServiceUser(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		
		// 현재 페이지 번호 세팅
		String nowPage = request.getParameter("nowPage");
		String cntPerPage = request.getParameter("cntPerPage");
		
		// 검색어 세팅
		if(request.getParameter("searchType") != null && request.getParameter("searchKeyword") != null) {
			customerVO.setSearchType(request.getParameter("searchType"));
			customerVO.setSearchKeyword(request.getParameter("searchKeyword"));
		}

		// 검색 조건 세팅
		customerVO.setStatus(request.getParameter("status"));
		customerVO.setStartDate(request.getParameter("startDate"));
		customerVO.setEndDate(request.getParameter("endDate"));
		customerVO.setSearchDateType(request.getParameter("searchDateType"));
		
		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		// 서비스 신청자 Count
		int applyServiceUserCnt = customerService.applyServiceUserCnt(customerVO);
		pagingVO = new pagingVO(applyServiceUserCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
		
		// 서비스 신청자 List
		List<customerVO> list = customerService.applyServiceUserList(customerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
	
		JSONArray applyServiceUserList = JSONArray.fromObject(list);
		
		mav.addObject("cateList", menuModel.getAttribute("cateList"));
		mav.addObject("depthList",menuModel.getAttribute("depthList"));
		mav.addObject("applyServiceUserList", applyServiceUserList);
		mav.addObject(pagingVO);
		mav.addObject(customerVO);

		return mav;
	}
	
	/**
	 * 서비스 신청자 상세 팝업
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/applyServiceDetailPopUp")
	public String applyServiceDetailPopUp(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
		// id 세팅
		customerVO.setId(Integer.parseInt(request.getParameter("id")));
		
		// 상세 내역 조회 
		List<customerVO> applyServiceDetail = customerService.applyServiceDetail(customerVO);
		
		model.addAttribute("applyServiceDetail", applyServiceDetail);
		
		return "/customer/popup/applyServiceDetail-popUp";
	}
	
	/**
	 * 서비스 신청자 내용 업데이트
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/updateApplyServiceDetail")
	@ResponseBody
	public int updateApplyServiceDetail(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.updateApplyServiceDetail(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 법인 현황
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/corporationStatus")
	public ModelAndView corporationStatus(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		
		// 현재 페이지 번호 세팅
		String nowPage = request.getParameter("nowPage");
		String cntPerPage = request.getParameter("cntPerPage");
		
		// 검색어 세팅
		if(request.getParameter("searchType") != null && request.getParameter("searchKeyword") != null) {
			customerVO.setSearchType(request.getParameter("searchType"));
			customerVO.setSearchKeyword(request.getParameter("searchKeyword"));
		}

		// 검색 조건 세팅
		//customerVO.setUseYn(request.getParameter("useYn"));
		
		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		// 서비스 신청자 Count
		int corporationStatusCnt = customerService.corporationStatusCnt(customerVO);
		pagingVO = new pagingVO(corporationStatusCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
		
		// 서비스 신청자 List
		List<customerVO> list = customerService.corporationStatusList(customerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
	
		JSONArray corporationStatusList = JSONArray.fromObject(list);
		
		mav.addObject("cateList", menuModel.getAttribute("cateList"));
		mav.addObject("depthList",menuModel.getAttribute("depthList"));
		mav.addObject("corporationStatusList", corporationStatusList);
		mav.addObject(pagingVO);
		mav.addObject(customerVO);

		return mav;
	}
	
	/**
	 * 법인 현황 상세 팝업
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/corporationStatusDetailpopUp")
	public String corporationStatusDetailpopUp(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
		// bizCompNo 세팅
		customerVO.setBizCompNo(Integer.parseInt(request.getParameter("bizCompNo")));
		
		// 할인율 구분 공통 코드
		List<codeVO> hourPriceCodeList = codeService.getHourPriceCodeList();
		
		// 상세 내역 조회 
		List<customerVO> corporationStatusDetail = customerService.corporationStatusDetail(customerVO);
		
		// 천 단위 콤마
		String hourPrice = adminUtil.toNumFormat(corporationStatusDetail.get(0).getHourPrice());
		
		// 콤마 찍힌 hourPrice VO 세팅
		customerVO.setHourPrice1(hourPrice);
		
		
		// 추후 추가할 내용
		// 예약된 운행
		// 종료된 운행
		// 이용자 현황
		// 드라이버 현황
		// 월결제 현황
		
		
		
		model.addAttribute("corporationStatusDetail", corporationStatusDetail);
		model.addAttribute("hourPriceCodeList", hourPriceCodeList);
		model.addAttribute("customerVO", customerVO);
		
		return "/customer/popup/corporationStatusDetail-popUp";
	}
	
	/**
	 * 법인 현황 등록 팝업 - Open popup
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/corporationRegistPopUp")
	public String corporationRegistPopUp(Model model, @RequestParam(value="num", required = false) String num) {
		// 할인율 구분 공통 코드
		List<codeVO> hourPriceCodeList = codeService.getHourPriceCodeList();
		
		model.addAttribute("hourPriceCodeList", hourPriceCodeList);
		
		return "/customer/popup/corporationRegist-popUp";
	}	
	
	/**
	 * 법인 현황 등록
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/insertCorporationStatus")
	@ResponseBody
	public int insertCorporationStatus(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.insertCorporationStatus(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 법인 현황 업데이트
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/updateCorporationStatus")
	@ResponseBody
	public int updateCorporationStatus(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.updateCorporationStatus(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 프로모션
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/promotion")
	public ModelAndView promotion(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		
		// 메뉴 호출 (공통)
		Model menuModel = menuController.menuModel(model);
		
		// 현재 페이지 번호 세팅
		String nowPage = request.getParameter("nowPage");
		String cntPerPage = request.getParameter("cntPerPage");
		
		// 검색어 세팅
		if(request.getParameter("searchType") != null && request.getParameter("searchKeyword") != null) {
			customerVO.setSearchType(request.getParameter("searchType"));
			customerVO.setSearchKeyword(request.getParameter("searchKeyword"));
		}
		
		// 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		// 프로모션 Count
		int promotionListCnt = customerService.promotionListCnt(customerVO);
		pagingVO = new pagingVO(promotionListCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
		
		// 프로모션 List
		List<customerVO> list = customerService.promotionList(customerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
	
		JSONArray promotionList = JSONArray.fromObject(list);
		
		mav.addObject("cateList", menuModel.getAttribute("cateList"));
		mav.addObject("depthList",menuModel.getAttribute("depthList"));
		mav.addObject("promotionList", promotionList);
		mav.addObject(pagingVO);
		mav.addObject(customerVO);

		return mav;
	}
	
	/**
	 * 프로모션 상세 팝업
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/promotionDetailPopUp")
	public String promotionDetailPopUp(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
		// id 세팅
		customerVO.setId(Integer.parseInt(request.getParameter("promotionId")));
		
		// 상세 내역 조회 
		List<customerVO> promotionDetail = customerService.promotionDetail(customerVO);
/*		
		// (규칙 내역) 현재 페이지 번호 세팅
		String nowPage = request.getParameter("nowPage");
		String cntPerPage = request.getParameter("cntPerPage");
		
		// (규칙 내역) 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);
		
		// 규칙 내역 Count
		int promotionRuleCnt = customerService.promotionRuleCnt(customerVO);
		pagingVO = new pagingVO(promotionRuleCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
*/		
		// 규칙 내역 List
		List<customerVO> list = customerService.promotionRule(customerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
		
		JSONArray promotionRule = JSONArray.fromObject(list);
		
		// (쿠폰 내역) 현재 페이지 번호 세팅
		String nowPage = request.getParameter("nowPage");
		String cntPerPage = request.getParameter("cntPerPage	");
		
		// (쿠폰 내역) 페이징 계산
		String[] pageArray = adminUtil.calculatePagingIsNull(nowPage, cntPerPage);

		// 쿠폰 내역 Count
		int promotionCouponCnt = customerService.promotionCouponCnt(customerVO);
		pagingVO = new pagingVO(promotionCouponCnt, Integer.parseInt(pageArray[0]), Integer.parseInt(pageArray[1]));
		
		// 쿠폰 내역
		List<customerVO> list1 = customerService.promotionCoupon(customerVO, pagingVO.getOffsetRow(), pagingVO.getNextRow());
		
		JSONArray promotionCoupon = JSONArray.fromObject(list1);
		
		model.addAttribute("promotionId", customerVO.getId());
		model.addAttribute("promotionDetail", promotionDetail);
		model.addAttribute("promotionRule", promotionRule);
		model.addAttribute("promotionCoupon", promotionCoupon);
		model.addAttribute("pagingVO", pagingVO);
		model.addAttribute("customerVO", customerVO);
		model.addAttribute("tabId", request.getParameter("tabId"));
		
		return "/customer/popup/promotionDetail-popUp";
	}
	
	/**
	 * 프로모션 등록 팝업 - Open popup
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/promotionRegistPopUp")
	public String promotionRegistPopUp(Model model, @RequestParam(value="num", required = false) String num) {
		// 프로모션 Type 공통 코드
		List<codeVO> promotionTypeList = codeService.getPromotionTypeList();
		
		// 프로모션 userTargetType 공통 코드
		List<codeVO> userTargetTypeList = codeService.getUserTargetTypeList();
		
		// 프로모션 status 공통 코드
		List<codeVO> promotionStatusList = codeService.getPromotionStatusList();
		
		model.addAttribute("promotionTypeList", promotionTypeList);
		model.addAttribute("userTargetTypeList", userTargetTypeList);
		model.addAttribute("promotionStatusList", promotionStatusList);
		
		return "/customer/popup/promotionRegist-popUp";
	}	
		
	/**
	 * 프로모션 등록
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/insertPromotion")
	@ResponseBody
	public int insertPromotion(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.insertPromotion(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 프로모션 업데이트
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/updatePromotion")
	@ResponseBody
	public int updatePromotion(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.updatePromotion(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 프로모션 규칙 삭제
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/cancelPromotion")
	@ResponseBody
	public int cancelPromotion(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.cancelPromotion(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 프로모션 규칙 등록 팝업 - Open popup
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/ruleRegistPopUp")
	public String ruleRegistPopUp(Model model, @RequestParam(value="promotionId", required = false) String promotionId) {
		// 프로모션 규칙 조건 유형 공통 코드
		List<codeVO> conditionTypeList = codeService.getConditionTypeList();

		model.addAttribute("promotionId", promotionId);
		model.addAttribute("conditionTypeList", conditionTypeList);
		
		return "/customer/popup/ruleRegist-popUp";
	}	
	
	/**
	 * 프로모션 규칙 등록
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/insertPromotionRule")
	@ResponseBody
	public int insertPromotionRule(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.insertPromotionRule(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 프로모션 규칙 상세 팝업
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/ruleDetailPopUp")
	public String ruleDetailPopUp(Model model, pagingVO pagingVO, pagingVO pagingVO1, customerVO customerVO, HttpServletRequest request) {
		// id 세팅 (Rule Id)
		customerVO.setId(Integer.parseInt(request.getParameter("id")));
		
		// 상세 내역 조회 
		List<customerVO> ruleDetail = customerService.ruleDetail(customerVO);

		// 프로모션 규칙 조건 유형 공통 코드
		List<codeVO> conditionTypeList = codeService.getConditionTypeList();
		
		model.addAttribute("ruleDetail", ruleDetail);
		model.addAttribute("conditionTypeList", conditionTypeList);
		
		return "/customer/popup/ruleDetail-popUp";
	}
	
	/**
	 * 프로모션 규칙 업데이트
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/updatePromotionRule")
	@ResponseBody
	public int updatePromotionRule(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.updatePromotionRule(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 프로모션 규칙 삭제
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/cancelPromotionRule")
	@ResponseBody
	public int cancelPromotionRule(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.cancelPromotionRule(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 프로모션 쿠폰 등록 팝업 - Open popup
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/couponRegistPopUp")
	public String couponRegistPopUp(Model model, @RequestParam(value="promotionId", required = false) String promotionId, customerVO customerVO) {
		customerVO.setPromotionNo(Integer.parseInt(promotionId));
		
		// 해당 프로모션의 Rule 정보
		List<customerVO> ruleList = customerService.getRuleList(customerVO);
		
		// redeemCode 추출
		String redeemCode = adminUtil.getRedeemCode(10);

		model.addAttribute("ruleList", ruleList);
		model.addAttribute("promotionId", promotionId);
		model.addAttribute("redeemCode", redeemCode);
		
		return "/customer/popup/couponRegist-popUp";
	}	
	
	/**
	 * 프로모션 쿠폰 등록
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/insertPromotionCoupon")
	@ResponseBody
	public int insertPromotionCoupon(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.insertPromotionCoupon(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 프로모션 쿠폰 상세 팝업
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/couponDetailPopUp")
	public String couponDetailPopUp(Model model, pagingVO pagingVO, pagingVO pagingVO1, customerVO customerVO, HttpServletRequest request) {
		// id 세팅 (Rule Id)
		customerVO.setId(Integer.parseInt(request.getParameter("id")));

		// 쿠폰 상세 내역 조회
		List<customerVO> couponDetail = customerService.couponDetail(customerVO);

		// promotionNo 세팅 (Promotion Id)
		customerVO.setPromotionNo(couponDetail.get(0).getPromotionNo());
		
		// 해당 프로모션의 Rule 정보
		List<customerVO> ruleList = customerService.getRuleList(customerVO);
		
		model.addAttribute("couponDetail", couponDetail);
		model.addAttribute("ruleList", ruleList);
		
		return "/customer/popup/couponDetail-popUp";
	}
	
	/**
	 * 프로모션 쿠폰 업데이트
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/updatePromotionCoupom")
	@ResponseBody
	public int updatePromotionCoupom(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.updatePromotionCoupom(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
	
	/**
	 * 프로모션 쿠폰 삭제
	 * @param 
	 * @return 
	 */
	@RequestMapping(value = "/cancelPromotionCoupon")
	@ResponseBody
	public int cancelPromotionCoupon(Model model, pagingVO pagingVO, customerVO customerVO, HttpServletRequest request) {
	
		int resultCode;
		
		boolean isStatus = customerService.cancelPromotionCoupon(request, customerVO);
		
		if(isStatus) {
			resultCode = 200;
		} else {
			resultCode = 500;
		}
		
		return resultCode;
	}
}
