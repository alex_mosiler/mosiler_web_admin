function makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2) 
{
	//tab으로 2뎁스 동적으로 움직여야하는경우
	var tabCodeStr = '';
	var tabArr = [];
	
	var $cateDiv = $('#cateDiv');
	var $depthDiv1 = $('#depthDiv1');
	var $depthDiv2 = $('#depthDiv2');
	
	var cateSeq = '';
	var depthSeq1 = '';
	var currTitle = '';
	
	var $selectSpan = $('<a href="javascript:void(0);" class="btn-dep"></a>');
	var $menuUl = $('<ul class="list"></ul>');
	$cateDiv.empty();
	$cateDiv.append($menuUl.clone());
	$(cateList).each(function(ic, oc){
		if(cateMenuCode == oc.menuNo){
			cateSeq = oc.seq;
			$cateDiv.prepend($selectSpan.clone().text(oc.menuTitle));
			currTitle += ' - ' + oc.menuTitle;
		}
		if(cateMenuCode == oc.menuNo){
			$('ul',$cateDiv).append('<li><a href="' + oc.menuUrl + '" class="on">'+oc.menuTitle+'</a></li>');	
		}else{
			$('ul',$cateDiv).append('<li><a href="' + oc.menuUrl + '">'+oc.menuTitle+'</a></li>');
		}
	});
	
	$depthDiv1.empty();
	$depthDiv1.append($menuUl.clone());
	$(depthList).each(function(i1, o1){
		if(cateSeq == o1.parentSeq){
			if(depthMenuCode1 == o1.menuNo){
				depthSeq1 = o1.seq;
				$depthDiv1.prepend($selectSpan.clone().text(o1.menuTitle));
				currTitle += ' - ' + o1.menuTitle;
			}
			if(depthMenuCode1 == o1.menuNo){
				$('ul',$depthDiv1).append('<li><a href="' + o1.menuUrl + '" class="on">'+o1.menuTitle+'</a></li>');					
			}else{
				$('ul',$depthDiv1).append('<li><a href="' + o1.menuUrl + '">'+o1.menuTitle+'</a></li>');
			}
		}
	});
	
	$depthDiv2.empty();
	$depthDiv2.append($menuUl.clone());
	$(depthList2).each(function(i2, o2){
		if(depthSeq1 == o2.parentSeq){
			if(depthMenuCode2 == o2.menuNo){
				//현재 메뉴에 대한 타이틀이나 공통적인 동작이 필요한 경우 추가한다.
				$depthDiv2.prepend($selectSpan.clone().text(o2.menuTitle));
				currTitle += ' - ' + o2.menuTitle;
			}
			if(depthMenuCode2 == o2.menuNo){
				$('ul',$depthDiv2).append('<li><a href="' + o2.menuUrl + '" class="on">'+o2.menuTitle+'</a></li>');					
			}else{
				$('ul',$depthDiv2).append('<li><a href="' + o2.menuUrl + '">'+o2.menuTitle+'</a></li>');
			}
			if(tabCodeStr!=''){
				tabArr.push(o2);
			}
		}
	});
	
	if($('.btn-dep',$depthDiv2).length){
		$('.btn-dep',$depthDiv2).addClass('last');
	}else{
		$('.btn-dep',$depthDiv1).addClass('last');
	}
	
	if(!$depthDiv2.text()){
		$depthDiv2.remove();
	}
	
	if(currTitle){
		$(document).attr('title', $(document).attr('title') + currTitle);
	}
	
	//gnb 메뉴도 연결부탁 - 이은경
	$(cateList).each(function(ic, oc){
		var $gnbMenuUl = $('#gnbMenuUl');
		PluscateSeq = "0"+oc.seq;  
		//html='<li><strong class="menu-tit">'+oc.TITLE+'</strong><ul class="menu">'; 
		html='<li><strong class="menu-tit">'+oc.menuTitle+'</strong>';
		
		if(PluscateSeq == 01) {
			html+= '<ul class="menu curr">';
		}else{
			html+= '<ul class="menu">';
		}			
		
		$(depthList).each(function(i1, o1){
			if(oc.seq == o1.parentSeq){
				if(o1.CHILD_CNT > 0){
					if(depthMenuCode1 == o1.menuNo){
						html+='<li class="current notopen"><a href="javascript:void(0);" class="more">'+o1.menuTitle+'</a>';	
					}else{
						html+='<li><a href="javascript:void(0);" class="more">'+o1.menuTitle+'</a>';
					}
					html+='<ul class="menu-detail">';
					$(depthList2).each(function(i2, o2){
						if(o1.seq == o2.parentSeq){
							if(depthMenuCode2 == o2.menuNo){
								html+='<li><a href="'+o2.menuUrl+'" class="on open">'+o2.menuTitle+'</a></li>';	
							}else{
								html+='<li><a href="'+o2.menuUrl+'">'+o2.menuTitle+'</a></li>';	
							}
						}
					});
					html+='</ul></li>';
				}else{
					if(depthMenuCode2 == o1.menuNo){
						html+='<li class="current"><a href="'+o1.menuUrl+'" class="on">'+o1.menuTitle+'</a></li>';	
					}else{
						html+='<li><a href="'+o1.menuUrl+'">'+o1.menuTitle+'</a></li>';	
					}						
				}
			}
		});
		html+='</ul></li>';
		$gnbMenuUl.append(html);
	});		
}