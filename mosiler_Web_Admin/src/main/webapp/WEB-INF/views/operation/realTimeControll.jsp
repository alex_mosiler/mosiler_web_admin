<!-- 실시간 관제 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 운행관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>     
	<style>
		html, body {height:100%;}	
		.bAddr {margin:10px !important;padding: 5px 5px 0 !important;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;z-index: 1000 !important}
		.bLong {margin:10px !important;padding: 5px !important;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;z-index: 1000 !important;border-top: 1px solid #ccc;;letter-spacing: 0;}
	</style>
	<!-- 카카오지도 lib  -->
 	<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=8b0f41af31fdb6d393a5383d14d4c12e&libraries=services,clusterer,drawing"></script>
	<!-- 카카오지도 api -->
 	<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=8b0f41af31fdb6d393a5383d14d4c12e"></script>
</head>

<body>
	<div id="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">
			<!-- Sidebar -->		
			<aside id="sidebar" class="sidebar" data-sidebar="over">
				<button type="button" class="btn btn-drawer sidebar-close" aria-controls="sidebar" aria-expanded="true"><span><i class="arw arw-slide2-prev primary">Sidebar 닫기</i></span></button>
				<div class="section-filters search">
			        <div class="sidebar-content-inner">
			            <form class="search">
			                <fieldset class="form-controls flexible">
								<span class="input-group type-search">
									<input type="text" name="searchKeyword" id="searchKeyword" value="" placeholder="고객명, 파트너명 검색" class="input-search">
									<span class="append"><button type="button" class="btn" onclick="currentDrivingList();"><span><i class="ico ico-search1">search</i></span></button></span>
								</span>
			                 </fieldset>
			            </form>
			        </div>
				</div>	
				<div class="section-filters summary">
					<div class="sidebar-content-inner grid gut-guide">
						<div class="col"><em id="currentDate"> </em> / <em id="currentTime"> </em></div>
						<div class="col">
							<button type="button" class="btn-ico" id="refreshBtn"><span><i class="ico ico-refresh1 white" style="opacity:.5">새로고침</i></span></button>
						</div>
						<div class="total col right">총 <em id="totalCnt">0</em>건</div>
					</div>
				</div>	
				<div class="section-filters shortcut" id="cntList">
 					<ul>
						<li class="shortcut-item warming is-active" style="background-color:#c0c0c0; border: 1px solid #c0c0c0; width:82px;">
							<a href="javascript:void(0);">
								<p class="shortcut-txt">출발준비</p>
								<p class="shortcut-link" id="readyCnt">0</p>
							</a>
						</li>
						<li class="shortcut-item operate is-active" style="background-color:#0080ff; border: 1px solid #0080ff; width:82px;">
							<a href="javascript:void(0);">
								<p class="shortcut-txt">운행중</p>
								<p class="shortcut-link" id="drivingCnt">0</p>
							</a>
						</li>
						<li class="shortcut-item renew is-active" style="width:82px;">
							<a href="javascript:void(0);">
								<p class="shortcut-txt">운행연장</p>
								<p class="shortcut-link" id= "extensiveDrivingCnt">0</p>
							</a>
						</li>
						<li class="shortcut-item delay is-active" style="width:82px;">
							<a href="javascript:void(0);">
								<p class="shortcut-txt">출발지연</p>
								<p class="shortcut-link" id="startDelay">0</p>
							</a>
						</li>	
						<!-- <li class="shortcut-item caution">
							<a href="javascript:void(0);">
								<p class="shortcut-txt">-</p>
								<p class="shortcut-link">-</p>

							</a>
						</li>
						<li class="shortcut-item caution">
							<a href="javascript:void(0);">
								<p class="shortcut-txt">-</p>
								<p class="shortcut-link">-</p>
							</a>
						</li>  -->
					</ul>
				</div>
				<div class="section-filters item-summary" id="listDiv">
				</div>				
			</aside>
			<button type="button" class="btn btn-drawer sidebar-open" aria-controls="sidebar" aria-expanded="false"><span><i class="arw arw-slide2-next primary">Sidebar 보기</i></span></button>
			<!-- //Sidebar -->	
			<!-- Map -->							
			<div id="map" class="map"></div>
			<div class="map-legend">
				<div class="drop up">
					<button type="button" aria-controls="drop4" aria-haspopup="listbox" aria-expanded="false" class="btn type4 drop-toggle"><span>지도범례<i class="arw arw-toggle1 dark"></i></span></button>
					<div id="drop4" class="drop-menu bg-white" style="width: 360px">
						<div class="grid ">
							<div class="col col-6">
								<ul class="list type4">
									<li class="item header">운행상태</li>
									<li class="item">
										<div class="grid cross-center">
											<div class="col col-6"><strong>출발준비</strong></div>
											<div class="col col-6"><span style="display: block;border: 2px solid #999;background-color: #fff;width: 50px;height: 10px;"></span></div>
										</div>
									</li>
									<li class="item">
										<div class="grid cross-center">
											<div class="col col-6"><strong>운행중</strong></div>
											<div class="col col-6"><span style="display: block;border: 2px solid #0676f4;background-color: #fff;width: 50px;height: 10px;"></span></div>
										</div>
									</li>
									<li class="item">
										<div class="grid cross-center">
											<div class="col col-6"><strong>운행연장</strong></div>
											<div class="col col-6"><span style="display: block;border: 2px solid #ffc801;background-color: #fff;width: 50px;height: 10px;"></span></div>
										</div>
									</li>	
									<li class="item">
										<div class="grid cross-center">
											<div class="col col-6"><strong>출발지연</strong></div>
											<div class="col col-6"><span style="display: block;border: 2px solid #ff0000;background-color: #fff;width: 50px;height: 10px;"></span></div>
										</div>
									</li>													
								</ul>							
							</div>
							<div class="col col-6">
								<ul class="list type4">
									<li class="item header">속도구분</li>
									<li class="item">
										<div class="grid cross-center">
											<div class="col col-6"><strong>정체</strong></div>
											<div class="col col-6"><img src="/images/egovframework/com/adm/marker_speed_yellow.png"></div>
										</div>
									</li>
									<li class="item">
										<div class="grid cross-center">
											<div class="col col-6"><strong>서행</strong></div>
											<div class="col col-6"><img src="/images/egovframework/com/adm/marker_speed_green.png"></div>
										</div>
									</li>
									<li class="item">
										<div class="grid cross-center">
											<div class="col col-6"><strong>평균</strong></div>
											<div class="col col-6"><img src="/images/egovframework/com/adm/marker_speed_blue.png"></div>
										</div>
									</li>	
									<li class="item">
										<div class="grid cross-center">
											<div class="col col-6"><strong>과속</strong></div>
											<div class="col col-6"><img src="/images/egovframework/com/adm/marker_speed_red.png"></div>
										</div>
									</li>													
								</ul>							
							</div>
						</div>

					</div>
				</div>
			</div>	
			</div>	
			<!-- //Map -->
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/index'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/operation/realTimeControll'">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->			
	</div>
</body>

<script type="text/javascript">
	$(document).ready(function(){
		// 로딩바 노출
		showLoadingBar();
		
		let today = new Date();   
		var currentDate = today.getFullYear()+"."+((today.getMonth()+1) < 10 ? '0'+(today.getMonth()+1) : (today.getMonth()+1))+"."+(today.getDate() < 10 ? '0'+today.getDate() : today.getDate());

		// 오늘 날짜, 시간 세팅
		$("#currentDate").text(currentDate);
		
		// 새로 고침 버튼
		$('#refreshBtn').click(function() {
		    location.reload();
		});
		
		// 검색 텍스트 박스 엔터키 방지 
		$('input[type="text"]').keydown(function() {
			if (event.keyCode === 13) {
				event.preventDefault();
			};
		});
		
		$("#map").empty();
		
		// 마커 정보 담을 배열
		var markerArray = [];
		
		// infowindow 정보 담을 배열
		var infowindowArray = [];
		
		// 지도를 표시할 div
		var mapContainer = document.getElementById('map'), 
		mapOption = {
			center : new kakao.maps.LatLng(37.516148, 127.029102), // 지도의 중심좌표
			level : 9 // 지도의 확대 레벨
		};
		
		// Map 생성
		var map = new kakao.maps.Map(mapContainer, mapOption);
		
		// 지도 확대 축소를 제어할 수 있는  줌 컨트롤을 생성합니다
		var zoomControl = new kakao.maps.ZoomControl();
		map.addControl(zoomControl, kakao.maps.ControlPosition.RIGHT);
		
		// 실시간 시간 표시
		setInterval(getNowTime, 1000);
		
		// 10초 단위로 서버 호출하여 실시간 좌표 조회
		setInterval(setDrivingLocation, 10000, map, markerArray, infowindowArray);
	});
	
	// 실시간 시간 표시
	function getNowTime() {
		let today = new Date();   
		var currentTime = (today.getHours() < 10 ? '0'+today.getHours() : today.getHours())+":"+(today.getMinutes() < 10 ? '0'+today.getMinutes() : today.getMinutes())+":"+(today.getSeconds() < 10 ? '0'+today.getSeconds() : today.getSeconds());
		
		$("#currentTime").text(currentTime);
	}
	
	// 실시간 좌표 조회
	function setDrivingLocation(map, markerArray, infowindowArray) {
		let today = new Date();   

		// DB로 넘겨줄 오늘, 내일 날짜
		var nowDate;
		var nextDate;
		
		// 내일 날짜
		var nextYear;
		var nextMonth;
		var nextDate;
		
		// 오늘 날짜
	  	var year = today.getFullYear();
		var month = today.getMonth() + 1;
	    var date = today.getDate();

	    if(month == 1) {
	    	month = '0' + month;
	    	
	    	if(date == 31) {
	    		nextYear = year;
	    		nextMonth = "02";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 2) {
	    	month = '0' + month;
	    	
			if(date == 28) {
				nextYear = year;
	    		nextMonth = "03";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 3) {
	    	month = '0' + month;
	    	
			if(date == 31) {
				nextYear = year;
	    		nextMonth = "04";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}	    	
	    } else if(month == 4) {
	    	month = '0' + month;
	    	
			if(date == 30) {
				nextYear = year;
	    		nextMonth = "05";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 5) {
	    	month = '0' + month;
	    	
			if(date == 31) {
				nextYear = year;
	    		nextMonth = "06";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 6) {
	    	month = '0' + month;
	    	
			if(date == 30) {
				nextYear = year;
	    		nextMonth = "07";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 7) {
	    	month = '0' + month;
	    	
			if(date == 31) {
				nextYear = year;
	    		nextMonth = "08";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 8) {
	    	month = '0' + month;
	    	
			if(date == 31) {
				nextYear = year;
	    		nextMonth = "09";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 9) {
	    	month = '0' + month;
	    	
			if(date == 30) {
				nextYear = year;
	    		nextMonth = "10";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 10) {
			if(date == 31) {
				nextYear = year;
	    		nextMonth = "11";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 11) {
			if(date == 30) {
				nextYear = year;
	    		nextMonth = "12";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 12) {
			if(date == 31) {
				nextYear = year + 1;
	    		nextMonth = "01";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    }
	    
    	nowDate = year + "-" + month + "-" + date + " 00:00:00.000";
    	nextDate = nextYear + "-" + nextMonth + "-" + nextDate + " 00:00:00.000";

		// 실시간 좌표
		$.ajax({
			url: "/operation/drivingLocation"
			, type: "POST"
			, data: {
				nowDate: nowDate
				, nextDate: nextDate
			}
			, success: function(dataList) {
				// marker 초기화
				if(markerArray != null) {
					for (var i = 0; i < markerArray.length; i++) {
						markerArray[i].setMap(null);

						if(i == (markerArray.length - 1)) {
							markerArray.length = 0;
						}
				    }    
				}
				
				// infowindow 초기화
				if(infowindowArray != null) {
					for (var i = 0; i < infowindowArray.length; i++) {
						infowindowArray[i].setMap(null);

						if(i == (infowindowArray.length - 1)) {
							infowindowArray.length = 0;
						}
				    }    
				}
				
				// 각 마커별로 infowindow 내용, 마커 이미지, 좌표를 담을 배열
				var positions =[];

				// 마커 이미지, 마커 좌표, 마커 내용 
				for(var i = 0; i < dataList.length; i++) {
					var imageSrc;
					var markerPosition;
					var content = "";
					var text;
					var temp = dataList[i];
					var angle;
					
					if(temp != null && temp != "") {
						angle = temp[0].angle;
						
						// seq, 각도 확인용
						//console.log(temp[0].drivingNo +" / " + temp[0].angle );
						
						// 마커 이미지 설정
						if(temp[0].flag == "ready") {
							text = "출발준비";
							
							imageSrc = "/images/egovframework/markerImage/ready_0_0.png";
							/* if(Math.ceil(temp[0].distance) < 30) {
								imageSrc = "/images/egovframework/markerImage/ready_60_90.png";
							} else if(Math.ceil(temp[0].distance) < 60) {
								imageSrc = "/images/egovframework/markerImage/ready_0_30.png";
							} else if(Math.ceil(temp[0].distance) < 90) {
								imageSrc = "/images/egovframework/markerImage/ready_30_60.png";
							} else if(Math.ceil(temp[0].distance) >= 90) {
								imageSrc = "/images/egovframework/markerImage/ready_90_200.png";
							}		 */					
						} else if(temp[0].flag == "driving") {
							text = "운행중";
							
							if(Math.ceil(temp[0].distance) < 30) {
								imageSrc = "/images/egovframework/markerImage/driving_60_90.png";
							} else if(Math.ceil(temp[0].distance) < 60) {
								imageSrc = "/images/egovframework/markerImage/driving_0_30.png";
							} else if(Math.ceil(temp[0].distance) < 90) {
								imageSrc = "/images/egovframework/markerImage/driving_30_60.png";
							} else if(Math.ceil(temp[0].distance) >= 90) {
								imageSrc = "/images/egovframework/markerImage/driving_90_200.png";
							}
						} else if(temp[0].flag == "extensiveDriving") {
							text = "운행연장"
							
							if(Math.ceil(temp[0].distance) < 30) {
								imageSrc = "/images/egovframework/markerImage/extensiveDriving_60_90.png";
							} else if(Math.ceil(temp[0].distance) < 60) {
								imageSrc = "/images/egovframework/markerImage/extensiveDriving_0_30.png";
							} else if(Math.ceil(temp[0].distance) < 90) {
								imageSrc = "/images/egovframework/markerImage/extensiveDriving_30_60.png";
							} else if(Math.ceil(temp[0].distance) >= 90) {
								imageSrc = "/images/egovframework/markerImage/extensiveDriving_90_200.png";
							}							
						} else if(temp[0].flag == "startDeley") {
							text = "출발지연";
							
							if(Math.ceil(temp[0].distance) < 30) {
								imageSrc = "/images/egovframework/markerImage/startDelay_60_90.png";
							} else if(Math.ceil(temp[0].distance) < 60) {
								imageSrc = "/images/egovframework/markerImage/startDelay_0_30.png";
							} else if(Math.ceil(temp[0].distance) < 90) {
								imageSrc = "/images/egovframework/markerImage/startDelay_30_60.png";
							} else if(Math.ceil(temp[0].distance) >= 90) {
								imageSrc = "/images/egovframework/markerImage/startDelay_90_200.png";
							}							
						}
						
						// 마커 좌표 설정
					 	if(temp[0].lat == null || temp[0].lng == null || temp[0].lat == 0.0 || temp[0].lng == 0.0) {
					 		markerPosition  = new kakao.maps.LatLng(temp[0].revStartLat, temp[0].revStartLng);
					 	} else if(temp.length == 1) {
					 		markerPosition  = new kakao.maps.LatLng(temp[0].lat.toFixed(5), temp[0].lng.toFixed(5)); 
					 	} else {
					 		markerPosition  = new kakao.maps.LatLng(temp[0].lat.toFixed(5), temp[0].lng.toFixed(5)); 
						}
					 	
						// 마커 내용 설정
						content += "<div class='bAddr'>";
						content += "	<div>- 예약번호 : "+temp[0].drivingNo+"</div>";
						content += "	<div>- 출발지 : "+temp[0].revStartAddress+"</div>";
						content += "	<div>- 도착지 : "+temp[0].revEndAddress+"</div>";
						content += "</div>";
						content += "<div class='bLong'>";
						content += "	<div>- 고객명 : "+temp[0].customerName + " / " + temp[0].customerPhoneNo+"</div>";
						content += "	<div>- 파트너 : "+temp[0].driverName + " / " + temp[0].driverPhoneNumber+"</div>";
						content += "	<div>- 운행상태 : "+text+"</div>";
						content += "</div>";
						
						// positions 세팅
						positions.push({
							content : content
							, latlng : markerPosition
							, imageSrc : imageSrc
							, angle : angle
						});
					}
				}
								
/*				
				// 지도가 확대 또는 축소되면 마지막 파라미터로 넘어온 함수를 호출하도록 이벤트를 등록합니다
				kakao.maps.event.addListener(map, 'zoom_changed', function() {        
				    // 지도의 현재 레벨을 얻어옵니다
				    var level = map.getLevel();
				    
				    var message = '현재 지도 레벨은 ' + level + ' 입니다';
					console.log(message);
				});
*/			
				
				// 설정된 마커 정보를 Map에 표시
				for(var i = 0; i < positions.length; i++) {
					// 설정된 마커 이미지의 크기
					var imageSize = new kakao.maps.Size(40, 40);
				     
					// 마커 위치 조정
					var imageOption = new kakao.maps.Point(20, 20); 
					
				 	// 설정된 마커 이미지 삽입
					var markerImage = new kakao.maps.MarkerImage(positions[i].imageSrc, imageSize, imageOption);
				 	
				 	// 마커의 모든 내용을 표시
					var marker = new kakao.maps.Marker({
				        map : map 
				        , position : positions[i].latlng 
				        , image : markerImage
				    });
				
				 	// marker 확인용
				 	//console.log(marker);
				 	
				 	// 이미지 회전
				 	// marker 안에 fa: img 부분을 가져와서 넣는다. 
				 	// 추후에 바뀌면 img를 가지고 있는 태그를 찾아서 바꿔준다.
					$(marker.fa).css('transform','');
					$(marker.fa).css('transform','rotateZ('+Math.ceil(positions[i].angle)+'deg)');

					// 마커 내용 표시
					var infowindow = new kakao.maps.InfoWindow({
				        content : positions[i].content 
				        , disableAutoPan : true
				    });
					
					// 이벤트 등록
					kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
				    kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
					
				    // 마커 정보를 배열에 저장
				    markerArray.push(marker);
				    
				    // infowindow 정보를 배열에 저장
				    infowindowArray.push(infowindow)
				}
				
				// 마커 내용 infowindow Open
				function makeOverListener(map, marker, infowindow) {
				    return function() {
				        infowindow.open(map, marker);
				    };
				}

				// 마커 내용 infowindow Close
				function makeOutListener(infowindow) {
				    return function() {
				        infowindow.close();
				    };
				}
				
				// 로딩바 숨김
				hideLoadingBar();
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
		        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
		    }
		}); 				

		// 실시간 Count
		$.ajax({
			url: "/operation/drivingCnt"
			, type: "POST"
			, data: {
				nowDate: nowDate
				, nextDate: nextDate
			}
			, success: function(dataList) {
				for(var i = 0; i < dataList.length; i++) {
					// 무조건 5개 row만 나온다
					if(i == 0) {
						// 운행 준비
						$("#readyCnt").text(dataList[1].cnt);
					} else if(i == 1) {
						// 운행중
						$("#drivingCnt").text(dataList[3].cnt);
					} else if(i == 2) {
						// 운행 연장
						$("#extensiveDrivingCnt").text(dataList[2].cnt);
					} else if(i == 3) {
						// 출발지연
						$("#startDelay").text(dataList[0].cnt);
					}
					
					$("#totalCnt").text(Number(dataList[0].cnt) + Number(dataList[1].cnt) + Number(dataList[2].cnt) + Number(dataList[3].cnt));
				}
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
		        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
		    }
		}); 			
		
		// 실시간 운행 List (공통으로 호출하기 위해 함수로 뺌 - 검색어 처리)
		currentDrivingList();
	}
	
	// 실시간 운행 List (공통으로 호출하기 위해 함수로 뺌 - 검색어 처리)
	function currentDrivingList() {
		let today = new Date();   

		// DB로 넘겨줄 오늘, 내일 날짜
		var nowDate;
		var nextDate;
		
		// 내일 날짜
		var nextYear;
		var nextMonth;
		var nextDate;
		
		// 오늘 날짜
	  	var year = today.getFullYear();
		var month = today.getMonth() + 1;
	    var date = today.getDate();

	    if(month == 1) {
	    	month = '0' + month;
	    	
	    	if(date == 31) {
	    		nextYear = year;
	    		nextMonth = "02";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 2) {
	    	month = '0' + month;
	    	
			if(date == 28) {
				nextYear = year;
	    		nextMonth = "03";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 3) {
	    	month = '0' + month;
	    	
			if(date == 31) {
				nextYear = year;
	    		nextMonth = "04";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}	    	
	    } else if(month == 4) {
	    	month = '0' + month;
	    	
			if(date == 30) {
				nextYear = year;
	    		nextMonth = "05";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 5) {
	    	month = '0' + month;
	    	
			if(date == 31) {
				nextYear = year;
	    		nextMonth = "06";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 6) {
	    	month = '0' + month;
	    	
			if(date == 30) {
				nextYear = year;
	    		nextMonth = "07";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 7) {
	    	month = '0' + month;
	    	
			if(date == 31) {
				nextYear = year;
	    		nextMonth = "08";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 8) {
	    	month = '0' + month;
	    	
			if(date == 31) {
				nextYear = year;
	    		nextMonth = "09";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 9) {
	    	month = '0' + month;
	    	
			if(date == 30) {
				nextYear = year;
	    		nextMonth = "10";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 10) {
			if(date == 31) {
				nextYear = year;
	    		nextMonth = "11";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 11) {
			if(date == 30) {
				nextYear = year;
	    		nextMonth = "12";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    } else if(month == 12) {
			if(date == 31) {
				nextYear = year + 1;
	    		nextMonth = "01";
	    		nextDate = "01";
	    	} else {
	    		nextYear = year;
	    		nextMonth = month;
	    		nextDate = date + 1;
	    	}
	    }
	    
    	nowDate = year + "-" + month + "-" + date + " 00:00:00.000";
    	nextDate = nextYear + "-" + nextMonth + "-" + nextDate + " 00:00:00.000";
		
		var searchKeyword = $("#searchKeyword").val();
		
		$.ajax({
			url: "/operation/currentDrivingList"
			, type: "POST"
			, data: {
				nowDate: nowDate
				, nextDate: nextDate
				, searchKeyword: searchKeyword
			}
			, success: function(dataList) {
				$("#listArea").remove();
				var html = "";
				
				html += "<ul id='listArea'>";
				
				for(var i = 0; i < dataList.length; i++) {
					var text;
					
					if(dataList[i].flag == "ready") {
						text = "<span class='txt-warming'>출발준비";
					} else if(dataList[i].flag == "driving") {
						text = "<span class='txt-operate'>운행중";
					} else if(dataList[i].flag == "startDelay") {
						text = "<span class='txt-delay'>출발지연";
					} else if(dataList[i].flag == "extensiveDriving") {
						text = "<span class='txt-renew'>운행연장"
					}
					
					html += "<li class='summary-box'>";
					html += "	<a href='javascript:void(0);' onclick='setCenter("+dataList[i].drivingNo+")'>";
					html += "		<h5>"+dataList[i].drivingNo+"</h5>";
					html += "		<dl class='service-list'>";
					html +=	"			<dd><label class='label type3 mar-r5'>출발지</label>"+dataList[i].revStartAddress+"</dd>";
					html +=	"			<dd><label class='label type3 mar-r5'>도착지</label>"+dataList[i].revEndAddress+"</dd>";
					html +=	"			<dd><label class='label type3 mar-r5'>고객</label>"+dataList[i].customerName+" / "+dataList[i].customerPhoneNo+"</dd>";
					html +=	"			<dd><label class='label type3 mar-r5'>파트너</label>"+dataList[i].driverName+" / "+dataList[i].driverPhoneNumber+"</dd>";
					html +=	"			<dd><label class='label type3 mar-r5'>운행상태</label>"+text+"</span></dd>";
					html += "		</dl>";
					html += "	</a>";
					html += "</li>";
				}
				
				html += "</ul>";
				
				$("#listDiv").append(html);
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
		        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
		    }
		});			
	}
	
	// 리스트 클릭 시 Map 중심 좌표 세팅
	function setCenter(drivingNo) {
			// 해당 drivingNo의 현재 좌표를 구해온다
			/* $.ajax({
			url: "/operation/currentLatLng"
			, type: "POST"
			, data: {
				drivingNo: drivingNo
			}
			, success: function(dataList) {
				if(dataList == "") {
					alert("해당 운행의 좌표 데이터가 수집 되지 않았습니다. 잠시 후 다시 시도해 주세요.");
				} else {
					var moveLatLon = new kakao.maps.LatLng(dataList[0].lat, dataList[0].lng);
					//console.log(moveLatLon);
					//console.log(map);
					//map.setCenter(moveLatLon);   
				}
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
		        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
		    }
		}); */
	}
	
	// 로딩바 노출
	function showLoadingBar() {
		var maskHeight = $(document).height(); 
		var maskWidth = window.document.body.clientWidth; 
		
		var mask = "<div id='mask' style='position:absolute; z-index:9000; background-color:#000000; display:none; left:0; top:0;'></div>";
		var loadingImg = '';
		loadingImg += "<div id='loadingImg' style='position:absolute; left:50%; top:40%; display:none; z-index:10000;'>"; 
		loadingImg += "	 <img style='width:80px; height:80px;' src='/images/egovframework/markerImage/loadingbar3.gif'/>"; 
		loadingImg += "</div>";

		$('body').append(mask).append(loadingImg);

		$('#mask').css({ 
			'width' : maskWidth 
			, 'height': maskHeight 
			, 'opacity' : '0.3' 
		}); 
		
		$('#mask').show(); 
		$('#loadingImg').show();
	}
	
	// 로딩바 숨김
	function hideLoadingBar() {
		$('#mask, #loadingImg').hide(); 
		$('#mask, #loadingImg').remove();
	}
</script>
</html>