<!-- 일별 운행 현황 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 운행관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<link rel='stylesheet'  href= '../lib/main.css' />
   	<link rel="stylesheet" href="../lib/jquery-ui.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>	
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>    	
   	<script src="/js/util.js"></script>
	<script src='../lib/main.js'></script>
	<script src="../lib/jquery-ui.min.js"></script>
	<script src="//code.jquery.com/jquery.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

 	<style>	
		.fc-myCustomButton1-button {background: none !important;border: none !important;cursor: auto !important;font-size: 16px !important;font-weight: bold !important;letter-spacing: -1px !important;color: #222 !important;padding: 0 !important;line-height: 0 !important;}
		.fc-myCustomButton2-button {background: none !important;border: none !important;cursor: auto !important;font-size: 14px !important;letter-spacing: -1px !important;color: #222 !important;padding: 0 !important;line-height: 0  !important;}
		.fc-myCustomButton3-button {background: #72a53a !important;border: 1px solid #72a53a !important;cursor: auto !important;} 
		.fc-myCustomButton4-button {background: #cccc00 !important;border: 1px solid #cccc00 !important;cursor: auto !important;} 
		.fc-myCustomButton5-button {background: #508ae6 !important;border: 1px solid #508ae6 !important;cursor: auto !important;}
		.fc-myCustomButton6-button {background: #ec6e3c !important;border: 1px solid #ec6e3c !important;cursor: auto !important;}
		.fc-myCustomButton7-button {background: #d19a43 !important;border: 1px solid #d19a43 !important;cursor: auto !important;}
		.fc-myCustomButton8-button {background: #686bb1 !important;border: 1px solid #686bb1 !important;cursor: auto !important;} 
		.fc-myCustomButton9-button {background: #48b4e1 !important;border: 1px solid #48b4e1 !important;cursor: auto !important;} 
		.fc-myCustomButton10-button {background: #546d82 !important;border: 1px solid #546d82 !important;cursor: auto !important;} 
		.fc-datagrid-body {background: #f2eeef !important}
	</style>  	
	
	<!-- 메뉴구현부  -->
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			} 
			
			var cateList = ${cateList};
			var depthList = ${depthList};
			
			var cateMenuCode = 'M'+'04'+'0000';
			var depthMenuCode1 = 'M'+'04'+'02'+'00';
			var depthMenuCode2 = 'M040201';
	
			makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2);

			let today = new Date();   
			var nowDate = today.getFullYear()+"-"+((today.getMonth()+1) < 10 ? '0'+(today.getMonth()+1) : (today.getMonth()+1))+"-"+(today.getDate() < 10 ? '0'+today.getDate() : today.getDate());

			setCount(nowDate);
			setCalendar(nowDate);

			// 이전 버튼
			$(".fc-next-button").click(function(){
				var checkDate = $(".fc-toolbar-title").text();
				var nowDate = changeDate(checkDate);

				setCount(nowDate);
				setCalendar(nowDate);
			});

			// 다음 버튼
		    $(".fc-prev-button").click(function(){
				var checkDate = $(".fc-toolbar-title").text();
				var nowDate = changeDate(checkDate);

				setCount(nowDate);
				setCalendar(nowDate);
			});

			$(".ui-button ui-widget").click(function(){
				setCount(nowDate);
				setCalendar(nowDate);
			})
		});
	</script> 
	
	<script type="text/javascript">
		// 달력 그리는 부분
		function setCalendar(nowDate) {
			let today = new Date();
			var dataList = getData(nowDate);
			var resources = getResources(dataList);
			var events = getEvents(dataList);
			var calendarEl = document.getElementById('calendar');
	
			// Start 
			var calendar = new FullCalendar.Calendar(calendarEl, {
				now: nowDate
				, scrollTime: '00:00'
				, editable: true 
			  	, aspectRatio: 1.8
			  	, height: 'auto'
			    , locale: 'ko'
				, customButtons: {
	    	    	myCustomButton1: {
		    	     	text: '차량운행현황'    	      
			    	}
    				, myCustomButton2: {
			    	 	text: '조회기준일시 : ' + today.toLocaleString()    	      
				    }
				    , myCustomButton3: {
		    	      	text: '골프'    	      
		    	    }
		    	    , myCustomButton4: {
		    	      	text: '키즈'
			    	}
			    	, myCustomButton5: {
		    	      	text: '비즈'
			    	}
			    	, myCustomButton6: {
		    	      	text: '일반'
			    	}
			    	, myCustomButton7: {
		    	      	text: '실버'
			    	}
			    	, myCustomButton8: {
		    	      	text: '쇼핑'
			    	}
			    	, myCustomButton9: {
		    	      	text: '에어'
			    	}
			    	, myCustomButton10: {
		    	      	text: '고정운행'
			    	}				    					    					    							    				    				    	    
			    }	      
			    , headerToolbar: {
			        left: 'myCustomButton1 myCustomButton2',
			        center: 'prev title next',
			        right: 'myCustomButton3,myCustomButton4,myCustomButton5,myCustomButton6,myCustomButton7,myCustomButton8,myCustomButton9,myCustomButton10'
			    }
		    	, initialView: 'resourceTimelineDay'
			    , expandRows: false // 주어진 뷰의 행이 전체 높이를 차지하지 않으면 크기에 맞게 확장된다. (default:false)
			    , slotMinWidth: true
			    , eventOverlap: true //드래그 및 크기 조정중인 이벤트가 서로 겹칠 수 있는지 여부를 결정 (default:true)
			    , navLinks: true	// 날짜(숫자)를 선택했을 때 그 날짜에 해당하는 세부 스케줄로 넘어갈 수 있게 할수 있다. (default:false)
			    , selectable : true // 설정을 true로 하게 되면, 날짜를 드래그를 통해 영역을 지정할 수 있게 된다	.
			    , resourceAreaWidth: '10%' 
			    , views: {
			    	resourceTimelineThreeDays: {
				        type: 'resourceTimeline'
					    , duration: { days: 3 }
			    		, buttonText: '3 days'
			        }
			    }
			    , resourceAreaHeaderContent: '드라이버명 (연락처)'
			    , resources: resources
		     	, events: events
		     	, resourceOrder: 'title'
		     	, eventClick: function(info) {
					// 예약 정보 상세 팝업
					reservationDetailModal(dataList[info.event.id], nowDate);
		     	}
		     	, eventResize: function(info) {
					// 예약 날짜 변경
					reservationUpdateDt(dataList[info.event.id], info.event.start, info.event.end, nowDate)
			     }
			     , eventDrop: function(info) {
					$('#modal2').dialog ({
		    			autoOpen: true
		    			, resizable: false
		    			, width: "300px"
			    		, buttons: {
		    				"등록": function() { 
		    					reservationCopy(dataList[info.event.id], info.event.start, info.event.end, nowDate);	
							}
							, "수정": function() { 
								// 예약 날짜 변경
								reservationUpdateDt(dataList[info.event.id], info.event.start, info.event.end, nowDate);	
							}
		        		}
	        			, close: function() {
	        				setCount(nowDate);
	        				setCalendar(nowDate);
				        }
	        			, modal: true
		  			});  
				}
			});	// End
	
		    calendar.render();
		}
		
		// 금일 운행 예약/취소건, 결제금액, 운행 예상 금액, 정산금액
		function setCount(nowDate) {
			$.ajax({
				url: "/operation/selectCount"
				, type: "POST"
				, async: false
				, data: {
					//revStartDt: nowDate
				}
				, success: function(jsonData) {
					var nowPayment = String(jsonData[0].PaymentFee);
					var nowPredict = String(jsonData[0].ReserveCharge);
					var nowCalculate = String(jsonData[0].PaymentAmmout);
					
					$("#nowReservation").text(jsonData[0].ReservationCount);
					$("#nowCancel").text(jsonData[0].CancleReservationCount);
					$("#nowPayment").text(nowPayment.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
					$("#nowPredict").text(nowPredict.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
					$("#nowCalculate").text(nowCalculate.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
				}
				, error: function(data, status, opt)
			    {
					alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
			        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			    }
			}); 
		}
		
		// 해당 날짜의 운행 현황
		function getData(nowDate) {
			var dataList;

			$.ajax({
				url: "/operation/selectData"
				, type: "POST"
				, async: false
				, data: {
					revStartDt: nowDate
				}
				, success: function(jsonData) {
					dataList = jsonData;
				}
				, error: function(data, status, opt)
			    {
					alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
			        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			    }
			}); 

			return dataList;
		}

		// (calendar) resources 세팅
		function getResources(dataList) {
			var resources = []; 
			var arr = [];
			
			for(var i = 0; i < dataList.length; i++) {
				if(arr.indexOf(dataList[i].driverNo) < 0) {
					resources.push({ id: dataList[i].driverNo, title: dataList[i].driverName+" ("+dataList[i].driverPhoneNumber+")"});
					arr.push(dataList[i].driverNo);
				}
			}

			return resources;
		}

		// (calendar) events 세팅
		function getEvents(dataList) {
			var events = [];
			var arr = [];
			var color = "";

			for(var i = 0; i < dataList.length; i++) {
				// 운행 구분에 따른 색깔 추가
				if(dataList[i].drivingType == 1) {
					// 일반
					color = "#ec6e3c";
				} else if(dataList[i].drivingType == 3) {
					// 골프
					color = "#72a53a";
				} else if(dataList[i].drivingType == 21) {
					// 키즈
					color = "#cccc00";
				} else if(dataList[i].drivingType == 22) {
					// 실버
					color = "#d19a43";
				} else if(dataList[i].drivingType == 23) {
					// 에어
					color = "#48b4e1";
				} else if(dataList[i].drivingType == 24) {
					// 비즈
					color = "#508ae6";
				} else if(dataList[i].drivingType == 25) {
					// 쇼핑
					color = "#686bb1";
				} else if(dataList[i].drivingType == 300) {
					// 고정 운행
					color = "#546d82";
				}

				events.push({id: i, resourceId: dataList[i].driverNo, title: dataList[i].customerName+" ( "+dataList[i].customerPhoneNo+" ) ", start: dataList[i].revStartDt, end: dataList[i].revEndDt, backgroundColor: color, borderColor: color});
			}

			return events;
		}

		// 날짜형식 변경
		function changeDate(checkDate) {
			var array = checkDate.split(" ");
			
			var y = array[0].replace( /년/gi, '');
			var m = array[1].replace( /월/gi, '');
			var d = array[2].replace( /일/gi, '');

			if(m < 10) {
				m = "0"+m;
			}

			if(d < 10) {
				d = "0"+d;
			}

			var nowDate = y+"-"+m+"-"+d;

			return nowDate;
		}

		// 예약 정보 상세 팝업
		function reservationDetailModal(data, nowDate) {
			$(".popup_type1").remove();

			var html = "";
			
			$('#modal').dialog ({
    			autoOpen: false
    			, resizable: false
    			, width: "500px"
        		, buttons: {
    				"예약 취소": function() { 
    					cancelReservation(data, nowDate);
					}
        		}
    			, close: function() {
    				setCount(nowDate);
    				setCalendar(nowDate);
		        }
		        , modal: true
  			});

			var drivingType = "";
			
			// drivingType 세팅
			if(data.drivingType == 1) {
				drivingType = "일반";
			} else if(data.drivingType == 3) {
				drivingType = "골프";
			} else if(data.drivingType == 21) {
				drivingType = "키즈";
			} else if(data.drivingType == 22) {
				drivingType = "실버";
			} else if(data.drivingType == 23) {
				drivingType = "에어";
			} else if(data.drivingType == 24) {
				drivingType = "비즈";
			} else if(data.drivingType == 25) {
				drivingType = "공항 홈발렛";
			} else if(data.drivingType == 300) {
				drivingType = "고정운행";
			}
			
			html += "	<section class='popup_type1' tabindex='0' z-index=''>";
			html += "		<div class='popup-body'>";
			html += "			<div class='popup-cont'>";
			html += "				<section class='section'>";
			html += "					<div class='tbl-type1 tbl-lefted'>";
			html += "						<table class='dataGrid'>";
			html += "							<colgroup>";
			html += "								<col width='30%''>";
			html += "								<col width='*'>";
			html += "							</colgroup>";
			html += "							<tbody>";
			html += "								<tr>";
			html += "									<th scope='row'>운행 타입</th>";
			html += "									<td>"+drivingType+"</td>";			
			html += "								</tr>";
			html += "								<tr>";
			html += "									<th scope='row'>고객명</th>";
			html += "									<td>"+data.customerName+"</td>";			
			html += "								</tr>";
			html += "								<tr>";
			html += "									<th scope='row'>고객 연락처</th>";
			html += "									<td>"+data.customerPhoneNo+"</td>";			
			html += "								</tr>";
			html += "								<tr>";
			html += "									<th scope='row'>파트너명</th>";
			html += "									<td>"+data.driverName+"</td>";			
			html += "								</tr>";
			html += "								<tr>";
			html += "									<th scope='row'>파트너 연락처</th>";
			html += "									<td>"+data.driverPhoneNumber+"</td>";			
			html += "								</tr>";
			html += "								<tr>";
			html += "									<th scope='row'>예약 시작 시간</th>";
			html += "									<td>"+dateToString(new Date(data.revStartDt))+"</td>";			
			html += "								</tr>";
			html += "								<tr>";
			html += "									<th scope='row'>예약 종료 시간</th>";
			html += "									<td>"+dateToString(new Date(data.revEndDt))+"</td>";			
			html += "								</tr>";
			html += "								<tr>";
			html += "									<th scope='row'>출발지</th>";
			html += "									<td>"+data.revStartAddress+"</td>";			
			html += "								</tr>";
			html += "								<tr>";
			html += "									<th scope='row'>도착지</th>";
			html += "									<td>"+data.revEndAddress+"</td>";			
			html += "								</tr>";
			html += "							</tbody>";
			html += "						</table>";			
			html += "					</div>";
			html += "				</section>";
			html += "			</div>";
			html += "		</div>";
			html += "	</section>";

			$("#modal").append(html);
			$('#modal').dialog('open');
		}

		// 예약 취소
		function cancelReservation(data, nowDate) {
			if(confirm("예약 취소 하시겠습니까?")) {
				$.ajax({
					url: "/operation/cancelReservation"
					, type: "POST"
					, async: false
					, data: {
						drivingNo: data.drivingNo
					}
					, success: function(resultCode) {
						if(resultCode == 200) {
							alert("예약 취소 되었습니다.");

							$('#modal').dialog('close');
							setCount(nowDate);
							setCalendar(nowDate);
						} else {
							alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");

							$('#modal').dialog('close');
							setCount(nowDate);
							setCalendar(nowDate);
						}
					}
					, error: function(data, status, opt) {
						alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");

						$('#modal2').dialog('close');
						setCount(nowDate);
						setCalendar(nowDate);
						
				        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
				    }
				}); 
			} 
		}
		
		// 스크롤 했을 경우 데이터 업데이트 (시작 시간, 종료 시간)
		function reservationUpdateDt(data, start, end, nowDate) {
 			var drivingNo = data.drivingNo;
			var startDt = dateToString(start);
			var endDt = dateToString(end)

  			$.ajax({
				url: "/operation/updateReservationDt"
				, type: "POST"
				, async: false
				, data: {
					drivingNo: drivingNo
					, revStartDt: startDt
					, revEndDt: endDt
				}
				, success: function(resultCode) {
					if(resultCode == 200) {
						alert("수정 되었습니다.");

						$('#modal2').dialog('close');
						setCount(nowDate);
						setCalendar(nowDate);
					} else {
						alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");

						$('#modal2').dialog('close');
						setCount(nowDate);
						setCalendar(nowDate);
					}
				}
				, error: function(data, status, opt) {
					alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");

					$('#modal2').dialog('close');
					setCount(nowDate);
					setCalendar(nowDate);
					
			        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			    }
			});
		}

		// 스크롤 했을 경우 데이터 Copy (시작 시간, 종료 시간)
		function reservationCopy(data, start, end, nowDate) {
			var startDt = dateToString(start);
			var endDt = dateToString(end)
			
   			$.ajax({
				url: "/operation/reservationCopy"
				, type: "POST"
				, async: false
				, data: {
					data: data
					, revStartDt: startDt
					, revEndDt: endDt
				}
				, success: function(resultCode) {
					if(resultCode == 200) {
						alert("복사 되었습니다.");

						$('#modal2').dialog('close');
						setCount(nowDate);
						setCalendar(nowDate);
					} else {
						alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");

						$('#modal2').dialog('close');
						setCount(nowDate);
						setCalendar(nowDate);
					}
				}
				, error: function(data, status, opt) {
					alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");

					$('#modal2').dialog('close');
					setCount(nowDate);
					setCalendar(nowDate);
					
			        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			    }
			}); 
		}
		
		// 대한민국 표준 시 format 변경
		function dateToString(format) {	
		    var year = format.getFullYear();
			var month = format.getMonth() + 1;
		    var date = format.getDate();
			var hour = format.getHours();
			var min = format.getMinutes();
			var sec = format.getSeconds();

		    if(month < 10) 
		    	month = '0' + month;
			
		    if(date < 10) 
		    	date = '0' + date;

		    if(hour < 10) 
		    	hour = '0' + hour;

		    if(min < 10) 
		    	min = '0' + min;

		    if(sec < 10) 
		    	sec = '0' + sec;

		    console.log(year + "-" + month + "-" + date + " " + hour + ":" + min + ":" + sec);
		    
			return year + "-" + month + "-" + date + " " + hour + ":" + min + ":" + sec;
		}
	</script>
</head>

<body>
	<div class="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">					
			<%@ include file="../header2.jsp" %>
			<!-- Content -->
			<div id="content" class="in-sec">
				<section class="section">				
					<div class="status-box green grid flex-between cross-center">
						<div class="col"></div>
						<div class="col">	
							<ul>
								<li>
									<a href="javascript:void(0);" class="status-link">
										<span class="status-txt">금일 운행 예약건</span>
										<span class="status-qty" id="nowReservation"></span> 건
									</a>
								</li>
								<li>
									<a href="javascript:void(0);" class="status-link">
										<span class="status-txt">금일 운행 예약 취소건</span>
										<span class="status-qty" id="nowCancel"></span> 건
									</a>
								</li>
								<li>
									<a href="javascript:void(0);" class="status-link">
										<span class="status-txt">금일 결제금액</span>
										<span class="status-qty" id="nowPayment"></span> 원
									</a>
								</li>
								<li>
									<a href="javascript:void(0);" class="status-link">
										<span class="status-txt">금일 운행 예상금액</span>
										<span class="status-qty" id="nowPredict"></span> 원
									</a>
								</li>
								<li>
									<a href="javascript:void(0);" class="status-link">
										<span class="status-txt">금일 정산금액</span>
										<span class="status-qty" id="nowCalculate"></span> 원
									</a>
								</li>											
							</ul>
						</div>
						<div class="col"></div>										
					</div>
				</section>	
				<section class="section">	
					<div id="calendar"></div>
				</section>
			</div>
			<!-- // Content -->
			<div id="modal" title="예약 정보"></div>
			<div id="modal2" title="알림"></div>
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/index'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/operation/realTimeControll'">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->		
	</div>
</body>
</html>