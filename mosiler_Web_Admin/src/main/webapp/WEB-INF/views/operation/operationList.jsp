<!-- 전체 운행 현황 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 운행관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>  
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>    	
   	<script src="/js/util.js"></script>
   	
	<!-- 메뉴구현부  -->
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			var cateList = ${cateList};
			var depthList = ${depthList};
			
			var cateMenuCode = 'M'+'04'+'0000';
			var depthMenuCode1 = 'M'+'04'+'03'+'00';
			var depthMenuCode2 = 'M040301';
	
			makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2);
		});
	</script>   	
</head>

<body>
	<div class="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">					
			<%@ include file="../header2.jsp" %>
			<!-- Content -->
			<div id="content" class="in-sec">
				<!-- section -->
				<section class="section">
					<div class="status-box green">
						<ul>
							<li>
								<a href="javascript:void(0);" class="status-link">
									<span class="status-txt">운행준비</span>
									<span class="status-qty">1,050</span> 건
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="status-link">
									<span class="status-txt">현장도착</span>
									<span class="status-qty">250</span> 건
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="status-link">
									<span class="status-txt">확인필요</span>
									<span class="status-qty">1,250</span> 건
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="status-link">
									<span class="status-txt">운행중</span>
									<span class="status-qty">1,050</span> 건
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="status-link">
									<span class="status-txt">운행연장</span>
									<span class="status-qty">250</span> 건
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="status-link">
									<span class="status-txt">완료지연</span>
									<span class="status-qty">1,250</span> 건
								</a>
							</li>										
						</ul>										
					</div>
				</section>
				<!-- //section -->			
				<section class="section mar-t10">
					<div class="form-search grid flex-center cross-center">
						<ul>
							<li>
								<label class="form-tit">구분</label>
								<div class="data-group">
									<select name="" id="sSelect1" class="select h30">
									    <option value="">전체</option>
									</select>									
								</div>
							</li>
							<li>
								<label class="form-tit">상품구분</label>
								<div class="data-group">
									<select name="sSelect1" id="sSelect1" class="select h30">
									    <option value="">전체</option>
									</select>
								</div>
							</li>
							<li>
								<label class="form-tit">운행상태</label>
								<div class="data-group">
									<select name="sSelect1" id="sSelect1" class="select h30">
									    <option value="">전체</option>
									</select>
								</div>
							</li>	
							<li>
								<label class="form-tit">보험</label>
								<div class="data-group">
									<select name="sSelect1" id="sSelect1" class="select h30">
									    <option value="">전체</option>
									</select>
								</div>
							</li>																					
							<li>
								<label class="form-tit">기간</label>
								<div class="data-group">
									<select name="sSelect1" id="sSelect1" class="select h30">
									    <option value="">예약일</option>
									</select>									
									<input type="date" name="" id="inputDate" value="2020-05-03" placeholder="" title="" class="input h30 input-date"><span class="form-split">-</span><input type="date" name="" id="inputDate" value="2020-05-03" placeholder="" title="" class="input h30 input-date">
								</div>
							</li>
							<li>
								<label class="form-tit">검색</label>
								<div class="data-group">
									<select name="sSelect1" id="sSelect1" class="select h30">
									    <option value="">선택일</option>
									</select>									
									<input type="text" name="" id="inputDate" value="고객명/휴대폰번호/이메일" placeholder="" title="" class="input h30 w200">
								</div>
							</li>								
						</ul>
						<div class="Mmar-t10">
							<button type="button" class="btn-search"><span>검색</span></button>
						</div>
			
					</div>
				</section>
				<!-- section -->
				<section class="section">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h2 class="h2">전체운행현황</h2>
						</div>
						<div class="col right">
							<button type="button" class="btn type1 secondary"><span>엑셀다운로드</span></button>
						</div>			
					</div>
					<div class="Mguide">
   						모바일에서 표를 좌우로 스크롤 할 수 있습니다. 
  					</div>
					<div class="gtbl-list l15">
						<table class="x-auto">
							<caption></caption>
							<thead>
								<tr>
									<th scope="col">번호</th>
									<th scope="col">구분</th>
									<th scope="col">예약번호</th>
									<th scope="col">고객명</th>
									<th scope="col">상품구분</th>
									<th scope="col">출발지</th>
									<th scope="col">예약일시</th>
									<th scope="col">파트너</th>
									<th scope="col">운행일</th>
									<th scope="col">시작</th>
									<th scope="col">종료</th>
									<th scope="col">운행시간(분)</th>
									<th scope="col">운행상태</th>
									<th scope="col">지도</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>9999</td>
									<td>운행</td>
									<td><a href="javascript:;" onclick="window.open('M040301-P01.html','','width=1000,height=700');return false">링크걸림</a></td>
									<td>홍*동</td>
									<td>상품A</td>
									<td class="align-l">서울 강남구</td>
									<td>YYYY-MM-DD hh:mm</td>
									<td>김기사</td>
									<td>YYYY-MM-DD</td>
									<td></td>
									<td></td>
									<td></td>
									<td>확인필요</td>
									<td></td>
								</tr>	
								<tr>
									<td>9999</td>
									<td>운행</td>
									<td>20200204p0101</td>
									<td>홍*동</td>
									<td>상품A</td>
									<td class="align-l">서울 강남구</td>
									<td>YYYY-MM-DD hh:mm</td>
									<td>김기사</td>
									<td>YYYY-MM-DD</td>
									<td></td>
									<td></td>
									<td></td>
									<td>확인필요</td>
									<td></td>
								</tr>	
								<tr>
									<td>9999</td>
									<td>운행</td>
									<td>20200204p0101</td>
									<td>홍*동</td>
									<td>상품A</td>
									<td class="align-l">서울 강남구</td>
									<td>YYYY-MM-DD hh:mm</td>
									<td>김기사</td>
									<td>YYYY-MM-DD</td>
									<td></td>
									<td></td>
									<td></td>
									<td>확인필요</td>
									<td></td>
								</tr>	
								<tr>
									<td>9999</td>
									<td>운행</td>
									<td>20200204p0101</td>
									<td>홍*동</td>
									<td>상품A</td>
									<td class="align-l">서울 강남구</td>
									<td>YYYY-MM-DD hh:mm</td>
									<td>김기사</td>
									<td>YYYY-MM-DD</td>
									<td></td>
									<td></td>
									<td></td>
									<td>확인필요</td>
									<td></td>
								</tr>	
								<tr>
									<td>9999</td>
									<td>운행</td>
									<td>20200204p0101</td>
									<td>홍*동</td>
									<td>상품A</td>
									<td class="align-l">서울 강남구</td>
									<td>YYYY-MM-DD hh:mm</td>
									<td>김기사</td>
									<td>YYYY-MM-DD</td>
									<td></td>
									<td></td>
									<td></td>
									<td>확인필요</td>
									<td></td>
								</tr>	
								<tr>
									<td>9999</td>
									<td>운행</td>
									<td>20200204p0101</td>
									<td>홍*동</td>
									<td>상품A</td>
									<td class="align-l">서울 강남구</td>
									<td>YYYY-MM-DD hh:mm</td>
									<td>김기사</td>
									<td>YYYY-MM-DD</td>
									<td></td>
									<td></td>
									<td></td>
									<td>확인필요</td>
									<td></td>
								</tr>	
								<tr>
									<td>9999</td>
									<td>운행</td>
									<td>20200204p0101</td>
									<td>홍*동</td>
									<td>상품A</td>
									<td class="align-l">서울 강남구</td>
									<td>YYYY-MM-DD hh:mm</td>
									<td>김기사</td>
									<td>YYYY-MM-DD</td>
									<td></td>
									<td></td>
									<td></td>
									<td>확인필요</td>
									<td></td>
								</tr>	
								<tr>
									<td>9999</td>
									<td>운행</td>
									<td>20200204p0101</td>
									<td>홍*동</td>
									<td>상품A</td>
									<td class="align-l">서울 강남구</td>
									<td>YYYY-MM-DD hh:mm</td>
									<td>김기사</td>
									<td>YYYY-MM-DD</td>
									<td></td>
									<td></td>
									<td></td>
									<td>확인필요</td>
									<td></td>
								</tr>	
								<tr>
									<td>9999</td>
									<td>운행</td>
									<td>20200204p0101</td>
									<td>홍*동</td>
									<td>상품A</td>
									<td class="align-l">서울 강남구</td>
									<td>YYYY-MM-DD hh:mm</td>
									<td>김기사</td>
									<td>YYYY-MM-DD</td>
									<td></td>
									<td></td>
									<td></td>
									<td>확인필요</td>
									<td></td>
								</tr>	
								<tr>
									<td>9999</td>
									<td>운행</td>
									<td>20200204p0101</td>
									<td>홍*동</td>
									<td>상품A</td>
									<td class="align-l">서울 강남구</td>
									<td>YYYY-MM-DD hh:mm</td>
									<td>김기사</td>
									<td>YYYY-MM-DD</td>
									<td></td>
									<td></td>
									<td></td>
									<td>확인필요</td>
									<td></td>
								</tr>	
								<tr>
									<td>9999</td>
									<td>운행</td>
									<td>20200204p0101</td>
									<td>홍*동</td>
									<td>상품A</td>
									<td class="align-l">서울 강남구</td>
									<td>YYYY-MM-DD hh:mm</td>
									<td>김기사</td>
									<td>YYYY-MM-DD</td>
									<td></td>
									<td></td>
									<td></td>
									<td>확인필요</td>
									<td></td>
								</tr>																																																																																																																																																																																																																																																																																																																																														
							</tbody>	
						</table>
					</div>
					<div class="tbl-footer grid flex-between cross-center">
						<div class="col total">페이지 <em>1</em>/129 결과 <em>2,577</em>건</div>
						<div class="col">
							<div class="paging paging-basic">
							    <div class="page-group">
							        <button type="button" class="btn first" title="처음 페이지" disabled="disabled"><span><i class="arw arw-board1-first gray"></i></span></button>
							        <button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
							    </div>
								<div class="page-state">
									<span class="curPage">1</span>
									<span class="totPage">3</span>
								</div>	
							    <ul class="num-group">
							        <li><button type="button" class="btn" title="1 페이지" aria-current="true" disabled="disabled"><span>1</span></button></li>
							        <li><button type="button" class="btn" title="2 페이지"><span>2</span></button></li>
							        <li><button type="button" class="btn" title="3 페이지"><span>3</span></button></li>
							    </ul>
							    <div class="page-group">
							        <button type="button" class="btn next" title="다음 페이지"><span><i class="arw arw-board1-next gray"></i></span></button>
							        <button type="button" class="btn last" title="마지막 페이지"><span><i class="arw arw-board1-last gray"></i></span></button>
							    </div>
						    </div>
						</div>
						<div class="col per-page">
							<select name="" id="" class="select h30">
							    <option value="">10</option>
							    <option value="">20</option>
							    <option value="">30</option>
							    <option value="">40</option>
							    <option value="">50</option>
							    <option value="">100</option>
							</select>	
						</div>			
					</div>		
				</section>
				<!-- //section -->
			</div>	
			<!-- //Content -->
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/index'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/operation/realTimeControll'">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->		
	</div>
</body>
</html>