<!-- 파트너 현황 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 파트너관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>    	
   	<script src="/js/util.js"></script>
   	
	<!-- 메뉴구현부  -->
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			var cateList = ${cateList};
			var depthList = ${depthList};
			
			var cateMenuCode = 'M'+'02'+'0000';
			var depthMenuCode1 = 'M'+'02'+'04'+'00';
			var depthMenuCode2 = 'M020401';
			
			makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2);
		});
	</script>   	
</head>

<body>
	<div class="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">					
			<!-- breadcrumb -->
			<div class="breadcrumb">
				<div class="in-sec">
					<div class="home"><a href="../index.html"></a></div>
					<div class="bread-menu">
						<div class="depth dep01" id="cateDiv"></div>
						<div class="depth dep02" id="depthDiv1"></div>					
					</div>
					<button type="button" class="popup-open" aria-haspopup="dialog" onclick="ui.popup.open('popupBasic', this);" tabindex="0"><i class="allmenu"></i></button>
				</div>
			</div>
			<!-- //breadcrumb -->		
			<!-- Content -->
			<div id="content" class="in-sec mar-t-10">		
				<div class="section grid gut-20">
					<div class="col col-8 order2">
						<!-- 검색영역 -->
						<section>
							<div class="form-search grid flex-center cross-center">
								<ul>
									<li>
										<label class="form-tit">알림그룹</label>
										<div class="data-group">
											<select name="" id="sSelect1" class="select h30">
											    <option value="">전체</option>
											    <option value="">1순위</option>
											    <option value="">2순위</option>
											    <option value="">3순위</option>
											</select>									
										</div>
									</li>	
									<li>
										<label class="form-tit">기간</label>
										<div class="data-group">
											<select name="sSelect1" id="sSelect1" class="select h30">
												<option value="">교육이수일</option>
											    <option value="">최종운행일</option>											    
											</select>									
											<input type="date" name="" id="inputDate" value="2020-05-03" placeholder="" title="" class="input h30 input-date"><span class="form-split">-</span><input type="date" name="" id="inputDate" value="2020-05-03" placeholder="" title="" class="input h30 input-date">
										</div>
									</li>									
									<li>
										<label class="form-tit">검색</label>
										<div class="data-group">
											<select name="sSelect1" id="sSelect1" class="select h30">
											    <option value="">선택</option>
											</select>									
											<input type="text" name="" id="inputDate" value="파트너명/휴대폰번호" placeholder="" title="" class="input h30 w200">
										</div>
									</li>								
								</ul>
								<div>
									<button type="button" class="btn-search"><span>검색</span></button>
								</div>					
							</div>
						</section>
						<!-- section -->					
						<section class="section">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">자동배차 설정현황</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>운행내역 리스트</caption>
									<thead>
										<tr>
											<th scope="col">
												<span class="check type1">
												    <input type="checkbox" name="sCheckall" id="sCheckall" value="">
												    <label for="sCheckall"><span class="blind">전체선택</span></label>
												</span>									
											</th>									
											<th scope="col">번호</th>
											<th scope="col">파트너</th>
											<th scope="col">휴대폰번호</th>
											<th scope="col">지역</th>
											<th scope="col">운행구분</th>
											<th scope="col">운행건</th>
											<th scope="col">지명건</th>
											<th scope="col">알림그룹</th>
											<th scope="col">알림간격시간</th>
											<th scope="col">교육이수일</th>									
											<th scope="col">최종운행일</th>											
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck01" id="sCheck01" value="">
												    <label for="sCheck01"><span class="blind">선택</span></label>
												</span>									
											</td>										
											<td>9999</td>
											<td>김기사</td>
											<td>010-12345-5678</td>
											<td class="align-l">서울 강남구</td>
											<td>ALL</td>
											<td>NNNN</td>
											<td>NNNN</td>
											<td>1순위</td>
											<td>30분</td>
											<td>YYYY-MM-DD</td>
											<td>YYYY-MM-DD</td>
										</tr>																																																																																																																																																																																																																																																																																																																																																				
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div class="col total">페이지 <em>1</em>/129 결과 <em>2,577</em>건</div>
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									        <button type="button" class="btn first" title="처음 페이지" disabled="disabled"><span><i class="arw arw-board1-first gray"></i></span></button>
									        <button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    </div>
										<div class="page-state">
											<span class="curPage">1</span>
											<span class="totPage">3</span>
										</div>	
									    <ul class="num-group">
									        <li><button type="button" class="btn" title="1 페이지" aria-current="true" disabled="disabled"><span>1</span></button></li>
									        <li><button type="button" class="btn" title="2 페이지"><span>2</span></button></li>
									        <li><button type="button" class="btn" title="3 페이지"><span>3</span></button></li>
									    </ul>
									    <div class="page-group">
									        <button type="button" class="btn next" title="다음 페이지"><span><i class="arw arw-board1-next gray"></i></span></button>
									        <button type="button" class="btn last" title="마지막 페이지"><span><i class="arw arw-board1-last gray"></i></span></button>
									    </div>
								    </div>
								</div>
								<div class="col per-page">
									<select name="" id="" class="select h30">
									    <option value="">10</option>
									    <option value="">20</option>
									    <option value="">30</option>
									    <option value="">40</option>
									    <option value="">50</option>
									    <option value="">100</option>
									</select>	
								</div>			
							</div>
						</section>
					</div>				
					<div class="col col-4">
						<section>	
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">자동배차 기능설정</h2>
								</div>		
							</div>														
							<div class="tbl-type1 tbl-lefted">
								<table>
									<colgroup>
										<col width="25%">
										<col width="15%">
										<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="row" rowspan="3">알림 그룹 지정</th>
											<th scope="row">1순위</th>
											<td>
												<span class="check type1 w80">
												    <input type="checkbox" name="sCheck10" id="sCheck10" value="" checked>
												    <label for="sCheck10"><span>대권역</span></label>
												</span>	
												<span class="check type1 w80">
												    <input type="checkbox" name="sCheck11" id="sCheck11" value="">
												    <label for="sCheck11"><span>소권역</span></label>
												</span>																								
											</td>									
										</tr>
										<tr>
											<th scope="row">2순위</th>
											<td>
												<span class="check type1 w80">
												    <input type="checkbox" name="sCheck10" id="sCheck10" value="" checked>
												    <label for="sCheck10"><span>운행구분</span></label>
												</span>	
												<span class="check type1 w80">
												    <input type="checkbox" name="sCheck11" id="sCheck11" value="">
												    <label for="sCheck11"><span>운행건</span></label>
												</span>	
												<span class="check type1 w80">
												    <input type="checkbox" name="sCheck11" id="sCheck11" value="">
												    <label for="sCheck11"><span>지명건</span></label>
												</span>																								
											</td>									
										</tr>											
										<tr>
											<th scope="row">3순위</th>
											<td>
												<span class="check type1 w80">
												    <input type="checkbox" name="sCheck10" id="sCheck10" value="" checked>
												    <label for="sCheck10"><span>사고율</span></label>
												</span>	
												<span class="check type1 w80">
												    <input type="checkbox" name="sCheck11" id="sCheck11" value="">
												    <label for="sCheck11"><span>최종운행일</span></label>
												</span>												
											</td>								
										</tr>
										<tr>
											<th scope="row">알림 간격 시간</th>
											<td colspan="2">
												<span class="radio type1 w70">
												    <input type="radio" name="sRadio11" id="sRadio11" value="" checked>
												    <label for="sRadio11"><span>10분</span></label>
												</span>	
												<span class="radio type1 w70">
												    <input type="radio" name="sRadio12" id="sRadio12" value="">
												    <label for="sRadio12"><span>30분</span></label>
												</span>	
												<span class="radio type1 w70">
												    <input type="radio" name="sRadio13" id="sRadio13" value="">
												    <label for="sRadio13"><span>1시간</span></label>
												</span>																																			
											</td>							
										</tr>												
										<tr>
											<th scope="row">알림 우선권 부여</th>
											<td colspan="2">
												<span class="radio type1 w70">
												    <input type="radio" name="sRadio21" id="sRadio21" value="" checked>
												    <label for="sRadio21"><span>1순위</span></label>
												</span>	
												<span class="radio type1 w70">
												    <input type="radio" name="sRadio22" id="sRadio22" value="">
												    <label for="sRadio22"><span>2순위</span></label>
												</span>	
												<span class="radio type1 w70">
												    <input type="radio" name="sRadio23" id="sRadio23" value="">
												    <label for="sRadio23"><span>3순위</span></label>
												</span>									
											</td>											
										</tr>																																																																													
									</tbody>
								</table>
							</div>
							<div class="form-info">
							    <p>알림 우선권 부여되는 자는 교육이수 완료 후 3개월 이내 파트너에 한함</p>
							</div>														
						</section>	
						<section class="section">	
							<div class="grid cross-center">
								<div class="col right">
									<button type="button" class="btn type1 primary"><span>저장</span></button>
									<button type="button" class="btn type1 secondary"><span>취소</span></button>
								</div>			
							</div>
						</section>																		
					</div>
				</div>
			</div>	
			<!-- //Content -->
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/index'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/operation/realTimeControll'">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->		
	</div>
</body>
</html>