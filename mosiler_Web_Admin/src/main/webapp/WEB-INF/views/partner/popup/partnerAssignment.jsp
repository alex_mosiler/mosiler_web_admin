<!--  파트너 배정 팝업 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 예약관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
   	
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			$("#customerNo").val(${customerNo});
			
			// 엔터키 이벤트 
			$("#searchKeyword"+${partnerVO.tabId}).keypress(function(e) {
				if(e.which == 13) {
					search(${partnerVO.tabId});
				}
			});

			if(${partnerVO.flag == 'Y'}) {
				$("#tab11").attr("disabled", true);
				$("#tab22").attr("disabled", true);
				$("#tab33").attr("disabled", true);
			} else {
				//alert("고객 화면");
			}

			if(${partnerVO.tabId} == 1) {
				$("#tabNav11").attr("aria-selected", false);
				$("#tabNav11").attr("class", "tab is-selected");
				$("#tabContent11").attr("aria-hidden", false);
				$("#tabContent11").attr("class", "tab-content is-visible")

				$("#tabNav12").attr("aria-selected", true);
				$("#tabNav12").attr("class", "tab");
				$("#tabContent12").attr("aria-hidden", true);
				$("#tabContent12").attr("class", "tab-content")

				$("#tabNav13").attr("aria-selected", true);
				$("#tabNav13").attr("class", "tab");
				$("#tabContent13").attr("aria-hidden", false);
				$("#tabContent13").attr("class", "tab-content")
			} else if(${partnerVO.tabId} == 2) {
				$("#tabNav12").attr("aria-selected", false);
				$("#tabNav12").attr("class", "tab is-selected");
				$("#tabContent12").attr("aria-hidden", false);
				$("#tabContent12").attr("class", "tab-content is-visible")

				$("#tabNav11").attr("aria-selected", true);
				$("#tabNav11").attr("class", "tab");
				$("#tabContent11").attr("aria-hidden", true);
				$("#tabContent11").attr("class", "tab-content")

				$("#tabNav13").attr("aria-selected", true);
				$("#tabNav13").attr("class", "tab");
				$("#tabContent13").attr("aria-hidden", false);
				$("#tabContent13").attr("class", "tab-content")
			} else {
				$("#tabNav13").attr("aria-selected", false);
				$("#tabNav13").attr("class", "tab is-selected");
				$("#tabContent13").attr("aria-hidden", false);
				$("#tabContent13").attr("class", "tab-content is-visible")

				$("#tabNav12").attr("aria-selected", true);
				$("#tabNav12").attr("class", "tab");
				$("#tabContent12").attr("aria-hidden", true);
				$("#tabContent12").attr("class", "tab-content")

				$("#tabNav11").attr("aria-selected", true);
				$("#tabNav11").attr("class", "tab");
				$("#tabContent11").attr("aria-hidden", false);
				$("#tabContent11").attr("class", "tab-content")
			}
			
			// 페이징 버튼 처리 (추후 post로 변경하자)
			partnerAssignmentListPageLink();
		});

		// 각 탭 클릭 시
		function tabClick(tabId) {
			var customerNo = $("#customerNo").val();
			location.href = "/partner/partnerAssignment?tabId="+tabId+"&customerNo="+customerNo;  
		}

		// 검색 이벤트
		function search(tabId) {
			var searchKeyword = $("#searchKeyword"+tabId).val();
			var searchType = $("#searchType"+tabId).val();
			var customerNo = $("#customerNo").val();
			var param = [];

			param.push("searchType="+searchType);
			param.push("searchKeyword="+searchKeyword);
			param.push("tabId="+tabId);
			param.push("customerNo="+customerNo);

			var url = "/partner/partnerAssignment"+"?"+param.join("&");
			location.href = url;
		}

		// 배정 버튼 클릭 시
		function selPartner(index) {
			opener.document.getElementById("driverNo").value = $("#partnerNo"+index).val();
			opener.document.getElementById("userName").value = $("#partnerName"+index).val();
			opener.document.getElementById("phoneNumber").value = $("#partnerPhoneNumber"+index).val();
			opener.document.getElementById("partnerLv").value = $("#partnerLv"+index).val();
			// 사고율 추가

			self.close();
		}
		
		// 페이징 버튼 처리 (추후 post로 변경하자)
		function partnerAssignmentListPageLink() {
			$("#firstPage"+${partnerVO.tabId}).attr("onclick", "location.href='/partner/partnerAssignment?nowPage=1&cntPerPage=${paging.cntPerPage}&searchType=${partnerVO.searchType}&searchKeyword=${partnerVO.searchKeyword}&tabId=${partnerVO.tabId}&customerNo=${partnerVO.customerNo}'");
			$("#prevPage"+${partnerVO.tabId}).attr("onclick", "location.href='/partner/partnerAssignment?nowPage=${paging.nowPage-1}&cntPerPage=${paging.cntPerPage}&searchType=${partnerVO.searchType}&searchKeyword=${partnerVO.searchKeyword}&tabId=${partnerVO.tabId}&customerNo=${partnerVO.customerNo}'");
			$("#nextPage"+${partnerVO.tabId}).attr("onclick", "location.href='/partner/partnerAssignment?nowPage=${paging.nowPage+1}&cntPerPage=${paging.cntPerPage}&searchType=${partnerVO.searchType}&searchKeyword=${partnerVO.searchKeyword}&tabId=${partnerVO.tabId}&customerNo=${partnerVO.customerNo}'");
			$("#lastPage"+${partnerVO.tabId}).attr("onclick", "location.href='/partner/partnerAssignment?nowPage=${paging.lastPage}&cntPerPage=${paging.cntPerPage}&searchType=${partnerVO.searchType}&searchKeyword=${partnerVO.searchKeyword}&tabId=${partnerVO.tabId}&customerNo=${partnerVO.customerNo}'")
		}

   		// 닫기 버튼
 		function selfClose() {
			self.close();
		}
	</script>   	   	
</head>

<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<input type="hidden" id="customerNo" value="">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">파트너 배정</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<!-- S: Tab Area -->
				<div class="tab-nav type1">
				    <ul class="tab-list" role="tablist">
				        <li id="tabNav11" role="tab" aria-controls="tabContent11" aria-selected="true" class="tab">
				        	<button type="button" class="btn" id="tab11" onclick="tabClick(1)"><span>배정신청</span></button>
				        </li>
				        <li id="tabNav12" role="tab" aria-controls="tabContent12" aria-selected="true" class="tab">
				        	<button type="button" class="btn" id="tab22" onclick="tabClick(2)"><span>강제배정</span></button>
				        </li>
				        <li id="tabNav13" role="tab" aria-controls="tabContent13" aria-selected="false" class="tab">
				        	<button type="button" class="btn" id="tab33" onclick="tabClick(3)"><span>배정이력</span></button>
				        </li>			        
				    </ul>
				</div>
				<div class="tab-container">
				    <!-- S: 배정신청 -->
				    <div id="tabContent11" aria-labelledby="tabNav11" aria-hidden="false" class="tab-content is-visible">
						<section class="section">
							<div class="form-search grid flex-center cross-center">
								<ul class="col">
									<li>
										<label class="form-tit">검색</label>
										<div class="data-group">
											<select name="sSelect1" id="searchType1" class="select h30">
											    <option value="partnerName" 	<c:if test="${partnerVO.searchType eq 'partnerName'}">selected</c:if>>파트너명</option>
											    <option value="phoneNumber"	<c:if test="${partnerVO.searchType eq 'phoneNumber'}">selected</c:if>>휴대폰번호</option>
											</select>									
											<input type="text" name="" id="searchKeyword1" value="${partnerVO.searchKeyword}" placeholder="" title="" class="input h30 w200">
										</div>
									</li>								
								</ul>
								<div class="col">
									<button type="button" class="btn-search" onclick="search(1);"><span>검색</span></button>
								</div>
							</div>
						</section>
						<section class="section mar-t10">
							<div class="gtbl-list l7">
								<table class="">
									<caption></caption>
									<thead>
										<tr>
											<th scope="col">번호</th>
											<th scope="col">파트너</th>
											<th scope="col">휴대폰번호</th>
											<th scope="col">신청일시</th>
											<th scope="col">선택</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="partnerAssignmentList" items="${partnerAssignmentList}" varStatus="status">
											<tr>
												<td><input type="hidden" id="partnerNo1${status.index}" value= "${partnerAssignmentList.driverNo}">${partnerAssignmentList.rownum}</td>
												<td><input type="hidden" id="partnerName1${status.index}" value="${partnerAssignmentList.userName}">${partnerAssignmentList.userName}</td>
												<td><input type="hidden" id="partnerPhoneNumber1${status.index}" value="${partnerAssignmentList.phoneNumber}">"${partnerAssignmentList.phoneNumber}</td>
												<input type="hidden" id="partnerLv1${status.index}" value="${partnerAssignmentList.driverLv}">
												<td>${partnerAssignmentList.requestDt}</td>
												<td><button type="button" onclick="selPartner(1${status.index});" class="btn type4 primary">배정</button></td>
											</tr>	
										</c:forEach>																																																																																																																																			
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-center cross-center">
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									    	<c:choose>
									    		<c:when test="${paging.nowPage == 1 }">
									    			<button type="button" class="btn first" title="처음 페이지" disabled="disabled" ><span><i class="arw arw-board1-first gray"></i></span></button>
									        		<button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    		</c:when>
									    		<c:otherwise>
									    			<button type="button" id="firstPage1" class="btn first" title="처음 페이지"  onclick=""><span><i class="arw arw-board1-first gray"></i></span></button>
									        		<button type="button" id="prevPage1" class="btn prev" title="이전 페이지" onclick=""><span><i class="arw arw-board1-prev gray"></i></span></button>
									    		</c:otherwise>
									    	</c:choose>	
									    </div>
									    <ul class="num-group">
									   		<c:forEach begin="${paging.startPage}" end="${paging.endPage}" var="page"> 
									   			<c:choose>
									   				<c:when test="${page == paging.nowPage}">
									   					<li><button type="button" class="btn" aria-current="true" disabled="disabled"><span>${page}</span></button></li>
									   				</c:when>
									   				<c:when test="${page != paging.nowPage}">
									   					<li><button type="button" id="e" class="btn" onclick="location.href='/partner/partnerAssignment?nowPage=${page}&cntPerPage=${paging.cntPerPage}&searchType=${partnerVO.searchType}&searchKeyword=${partnerVO.searchKeyword}&tabId=${partnerVO.tabId}&customerNo=${partnerVO.customerNo}'"><span>${page}</span></button></li>
									   				</c:when>
									   			</c:choose>
									   		</c:forEach> 
									    </ul>
									    <div class="page-group">
									    	<c:choose>
									    		<c:when test="${paging.nowPage == paging.lastPage}">
									    			<button type="button" class="btn next" title="다음 페이지" disabled="disabled" ><span><i class="arw arw-board1-next gray"></i></span></button>
									        		<button type="button" class="btn last" title="마지막 페이지" disabled="disabled"><span><i class="arw arw-board1-last gray"></i></span></button>
									    		</c:when>
									    		<c:otherwise>
									    			<button type="button" id="nextPage1" class="btn next" title="다음 페이지" onclick=""><span><i class="arw arw-board1-next gray"></i></span></button>
									        		<button type="button" id="lastPage1" class="btn last" title="마지막 페이지" onclick=""><span><i class="arw arw-board1-last gray"></i></span></button>
									    		</c:otherwise>
									    	</c:choose>
									    </div>
								    </div>
								</div>
							</div>		
						</section>
				    </div>
				    <!-- E: 배정신청 -->
				    <!-- S: 강제배정 -->
				    <div id="tabContent12" aria-labelledby="tabNav12" aria-hidden="true" class="tab-content">
						<section class="section">
							<div class="form-search grid flex-center cross-center">
								<ul class="col">
									<li>
										<label class="form-tit">검색</label>
										<div class="data-group">
											<select name="sSelect1" id="searchType2" class="select h30">
											    <option value="partnerName"	<c:if test="${partnerVO.searchType eq 'partnerName'}">selected</c:if>>파트너명</option>
											    <option value="phoneNumber" <c:if test="${partnerVO.searchType eq 'phoneNumber'}">selected</c:if>>휴대폰번호</option>
											</select>									
											<input type="text" name="" id="searchKeyword2" value="${partnerVO.searchKeyword}" placeholder="" title="" class="input h30 w200">
										</div>
									</li>								
								</ul>
								<div class="col">
									<button type="button" class="btn-search" onclick="search(2);"><span>검색</span></button>
								</div>
							</div>
						</section>
						<section class="section mar-t10">
							<div class="gtbl-list l7">
								<table class="">
									<caption></caption>
									<thead>	
										<tr>
											<th scope="col">번호</th>
											<th scope="col">파트너</th>
											<th scope="col">휴대폰번호</th>
											<th scope="col">선택</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="partnerAssignmentList" items="${partnerAssignmentList}" varStatus="status">
											<tr>
												<td><input type="hidden" id="partnerNo2${status.index}" value= "${partnerAssignmentList.driverNo}">${partnerAssignmentList.rownum}</td>
												<td><input type="hidden" id="partnerName2${status.index}" value="${partnerAssignmentList.userName}">${partnerAssignmentList.userName}</td>
												<td><input type="hidden" id="partnerPhoneNumber2${status.index}" value="${partnerAssignmentList.phoneNumber}">${partnerAssignmentList.phoneNumber}</td>
												<input type="hidden" id="partnerLv2${status.index}" value="${partnerAssignmentList.driverLv}">
												<td><button type="button" onclick="selPartner(2${status.index});" class="btn type4 primary">배정</button></td>
											</tr>	
										</c:forEach>														
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-center cross-center">
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									    	<c:choose>
									    		<c:when test="${paging.nowPage == 1 }">
									    			<button type="button" class="btn first" title="처음 페이지" disabled="disabled" ><span><i class="arw arw-board1-first gray"></i></span></button>
									        		<button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    		</c:when>
									    		<c:otherwise>
									    			<button type="button" id="firstPage2" class="btn first" title="처음 페이지"  onclick=""><span><i class="arw arw-board1-first gray"></i></span></button>
									        		<button type="button" id="prevPage2" class="btn prev" title="이전 페이지" onclick=""><span><i class="arw arw-board1-prev gray"></i></span></button>
									    		</c:otherwise>
									    	</c:choose>	
									    </div>
									    <ul class="num-group">
									   		<c:forEach begin="${paging.startPage}" end="${paging.endPage}" var="page"> 
									   			<c:choose>
									   				<c:when test="${page == paging.nowPage}">
									   					<li><button type="button" class="btn" aria-current="true" disabled="disabled"><span>${page}</span></button></li>
									   				</c:when>
									   				<c:when test="${page != paging.nowPage}">
									   					<li><button type="button" id="e" class="btn" onclick="location.href='/partner/partnerAssignment?nowPage=${page}&cntPerPage=${paging.cntPerPage}&searchType=${partnerVO.searchType}&searchKeyword=${partnerVO.searchKeyword}&tabId=${partnerVO.tabId}&customerNo=${partnerVO.customerNo}'"><span>${page}</span></button></li>
									   				</c:when>
									   			</c:choose>
									   		</c:forEach> 
									    </ul>
									    <div class="page-group">
									    	<c:choose>
									    		<c:when test="${paging.nowPage == paging.lastPage}">
									    			<button type="button" class="btn next" title="다음 페이지" disabled="disabled" ><span><i class="arw arw-board1-next gray"></i></span></button>
									        		<button type="button" class="btn last" title="마지막 페이지" disabled="disabled"><span><i class="arw arw-board1-last gray"></i></span></button>
									    		</c:when>
									    		<c:otherwise>
									    			<button type="button" id="nextPage2" class="btn next" title="다음 페이지" onclick=""><span><i class="arw arw-board1-next gray"></i></span></button>
									        		<button type="button" id="lastPage2" class="btn last" title="마지막 페이지" onclick=""><span><i class="arw arw-board1-last gray"></i></span></button>
									    		</c:otherwise>
									    	</c:choose>
									    </div>
								    </div>
								</div>
							</div>		
						</section>
				    </div>
				    <!-- E: 강제배정  -->
				    <!-- S: 배정이력 -->
				    <div id="tabContent13" aria-labelledby="tabNav13" aria-hidden="true" class="tab-content">
						<section class="section">
							<div class="form-search grid flex-center cross-center">
								<ul class="col">
									<li>
										<label class="form-tit">검색</label>
										<div class="data-group">
											<select name="sSelect1" id="searchType3" class="select h30">
											    <option value="partnerName" 	<c:if test="${partnerVO.searchType eq 'partnerName'}">selected</c:if>>파트너명</option>
											    <option value="phoneNumber" <c:if test="${partnerVO.searchType eq 'phoneNumber'}">selected</c:if>>휴대폰번호</option>
											</select>									
											<input type="text" name="" id="searchKeyword3" value="${partnerVO.searchKeyword}" placeholder="" title="" class="input h30 w200">
										</div>
									</li>								
								</ul>
								<div class="col">
									<button type="button" class="btn-search" onclick="search(3);"><span>검색</span></button>
								</div>
							</div>
						</section>
						<section class="section mar-t10">
							<div class="gtbl-list l7">
								<table class="">
									<caption></caption>
									<thead>
										<tr>
											<th scope="col">번호</th>
											<th scope="col">파트너</th>
											<th scope="col">휴대폰번호</th>
											<th scope="col">운행횟수</th>
											<th scope="col">선택</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="partnerAssignmentList" items="${partnerAssignmentList}" varStatus="status">
											<tr>
												<td><input type="hidden" id="partnerNo3${status.index}" value= "${partnerAssignmentList.driverNo}">${partnerAssignmentList.rownum}</td>
												<td><input type="hidden" id="partnerName3${status.index}" value="${partnerAssignmentList.userName}">${partnerAssignmentList.userName}</td>
												<td><input type="hidden" id="partnerPhoneNumber3${status.index}" value="${partnerAssignmentList.phoneNumber}">${partnerAssignmentList.phoneNumber}</td>
												<input type="hidden" id="partnerLv3${status.index}" value="${partnerAssignmentList.driverLv}">
												<td>${partnerAssignmentList.drivingCnt}</td>
												<td><button type="button" onclick="selPartner(3${status.index});" class="btn type4 primary">배정</button></td>
											</tr>	
										</c:forEach>																		
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-center cross-center">
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									    	<c:choose>
									    		<c:when test="${paging.nowPage == 1 }">
									    			<button type="button" class="btn first" title="처음 페이지" disabled="disabled" ><span><i class="arw arw-board1-first gray"></i></span></button>
									        		<button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    		</c:when>
									    		<c:otherwise>
									    			<button type="button" id="firstPage3" class="btn first" title="처음 페이지"  onclick=""><span><i class="arw arw-board1-first gray"></i></span></button>
									        		<button type="button" id="prevPage3" class="btn prev" title="이전 페이지" onclick=""><span><i class="arw arw-board1-prev gray"></i></span></button>
									    		</c:otherwise>
									    	</c:choose>	
									    </div>
									    <ul class="num-group">
									   		<c:forEach begin="${paging.startPage}" end="${paging.endPage}" var="page"> 
									   			<c:choose>
									   				<c:when test="${page == paging.nowPage}">
									   					<li><button type="button" class="btn" aria-current="true" disabled="disabled"><span>${page}</span></button></li>
									   				</c:when>
									   				<c:when test="${page != paging.nowPage}">
									   					<li><button type="button" id="e" class="btn" onclick="location.href='/partner/partnerAssignment?nowPage=${page}&cntPerPage=${paging.cntPerPage}&searchType=${partnerVO.searchType}&searchKeyword=${partnerVO.searchKeyword}&tabId=${partnerVO.tabId}&customerNo=${partnerVO.customerNo}'"><span>${page}</span></button></li>
									   				</c:when>
									   			</c:choose>
									   		</c:forEach> 
									    </ul>
									    <div class="page-group">
									    	<c:choose>
									    		<c:when test="${paging.nowPage == paging.lastPage}">
									    			<button type="button" class="btn next" title="다음 페이지" disabled="disabled" ><span><i class="arw arw-board1-next gray"></i></span></button>
									        		<button type="button" class="btn last" title="마지막 페이지" disabled="disabled"><span><i class="arw arw-board1-last gray"></i></span></button>
									    		</c:when>
									    		<c:otherwise>
									    			<button type="button" id="nextPage3" class="btn next" title="다음 페이지" onclick=""><span><i class="arw arw-board1-next gray"></i></span></button>
									        		<button type="button" id="lastPage3" class="btn last" title="마지막 페이지" onclick=""><span><i class="arw arw-board1-last gray"></i></span></button>
									    		</c:otherwise>
									    	</c:choose>
									    </div>
								    </div>
								</div>
							</div>		
						</section>
				    </div>
				    <!-- E: 배정이력  -->			    
				</div>
				<!-- e: Tab Area -->				
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" onclick="selfClose();" class="btn type3 primary"><span>확인</span></button>
				<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</body>
</html>