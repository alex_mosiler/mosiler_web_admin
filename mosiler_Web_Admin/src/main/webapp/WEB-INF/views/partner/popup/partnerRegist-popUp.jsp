<!--  파트너 등록 팝업 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 파트너관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
</head>
<script type="text/javascript">
	$(document).ready(function() {
		var id = "${id}";
		
		/* if(id == "") {
			location.href = "/";
		} */
	});
	
	function fileUpload(num) {
		var form = $("#uploadForm_"+num)[0];
		var data = new FormData(form);
		
		 $.ajax({
	        type: "POST",
	        enctype: 'multipart/form-data',
	        url: "/partner/fileUpload",
	        data: data,
	        processData: false,
	        contentType: false,
	        cache: false,
	        timeout: 600000,
	        success: function (data) {
	        	
	        	alert('success')
	        },
	        error: function (e) {
	       
	            alert('fail');
	        }
	    });
		
		
	}
	
	// 파트너 등록
	function insertDriver() {
		var userName			= $("#userName").val();
		var userId				= $("#userId").val();
		var socialNumber		= $("#socialNumber").val();
		var phone1				= $("#phone1").val();
		var phone2				= $("#phone2").val();
		var phone3				= $("#phone3").val();
		var phoneNumber; 	
		var userPw				= $("#userPw").val();
		var licenseNumber	= $("#licenseNumber").val();
		var bankCode			= $("#bankCode").val();
		var accountNumber	= $("#accountNumber").val();
		var hobby				= $("#hobby").val();
		var jobName			= $("#jobName").val();
		var drivingYear			= $("#drivingYear").val();
		var driverStatus		= $("#driverStatus").val();
		var bizCompNo		= $("#bizCompNo").val();
		var permitStatus		= $("#permitStatus").val();
		var driverRemark		= $("textarea#driverRemark").val();
		
		if(userName == "") {
			alert("드라이버 성명을 입력해 주세요.");
			return false;
		}
		if(userId == "") {
			alert("아이디(이메일)을 입력해 주세요.");
			return false;
		}
		if(socialNumber == "") {
			alert("주민등록번호를 입력해 주세요.");
			return false;
		}
		if(phone1 == "" || phone2 == "" || phone3 == "") {
			alert("휴대폰 번호를 입력해 주세요.");
			return false;
		} else {
			phoneNumber = phone1+phone2+phone3;
		}
		if(userPw == "") {
			alert("비밀번호를 주세요.");
			return false;
		}
		if(licenseNumber == "") {
			alert("운전면허 번호를 입력해 주세요.");
			return false;
		}
		if(bankCode == "") {
			alert("은행을 선택해 주세요.");
			return false;
		}
		if(accountNumber == "") {
			alert("통장번호를 입력해 주세요.");
			return false;
		}
		if(bizCompNo == "") {
			bizCompNo = 0;
		}
		
		$.ajax({
			url: "/partner/insertDriver"
			, type: "POST"
			, data: {
				userName: userName
				, userId: userId
				, socialNumber: socialNumber
				, phoneNumber: phoneNumber
				, userPw: userPw
				, licenseNumber: licenseNumber
				, bankCode: bankCode
				, accountNumber: accountNumber
				, hobby: hobby
				, jobName: jobName
				, drivingYear: drivingYear
				, driverStatus: driverStatus
				, bizCompNo: bizCompNo
				, permitStatus: permitStatus
				, driverRemark: driverRemark
			}
			, success: function(data) {
				if(data == 200) {
					alert("파트너 등록이 완료 되었습니다.");
					
					self.close();
				} else {
					alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
				}

				opener.location.reload();
				//self.close();
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
				//self.close();
				
				console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			}
		});
	}
	
	// 닫기 버튼
	function selfClose() {
		self.close();
	}
</script>
<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">파트너 등록</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">	
				<section class="section" id="loginForm">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h3 class="h3">기본정보</h3>
						</div>	
					</div>
					<div class="tbl-type2 tbl-lefted">
						<table>
							<colgroup>
								<col width="15%">
								<col width="*">
								<col width="15%">
								<col width="*">
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">드라이버 성명</th>
									<td>
										<input type="text" name="userName" id="userName" value="" placeholder="이름 입력" title="" class="input h25 w300">
									</td>	
									<th scope="row">아이디 (이메일)</th>
									<td class="form vertical">
										<div>
											<input type="text" name="userId" id="userId" value="" placeholder="이메일 입력" title="" class="input h25 w300">
										</div>							
									</td>					
								</tr>
								<tr>
									<th scope="row">주민등록번호</th>
									<td>
										<input type="text" name="socialNumber" id="socialNumber" value="" placeholder="주민등록번호 입력" title="" class="input h25 w300">							
									</td>	
									<th scope="row">휴대전화</th>
									<td>
										<div>
											<input type="text" name="phone1" id="phone1" value="" placeholder="" title="" class="input h25 w60">
											<i class="form-split">-</i>
											<input type="text" name="phone2" id="phone2" value="" placeholder="" title="" class="input h25 w60">
											<i class="form-split">-</i>
											<input type="text" name="phone3" id="phone3" value="" placeholder="" title="" class="input h25 w60">
											<!-- <span class="static is-errored" role="alert">숫자만 입력하세요.</span> -->
										</div>										
									</td>										
								</tr>						
								<tr>
									<th scope="row">비밀번호</th>
									<td class="form vertical">
										<div>
											<input type="text" name="userPw" id="userPw" value="" placeholder="비밀번호 입력" title="" class="input h25 w300">
										</div>
									</td>
									<th scope="row"></th>
									<td class="form vertical">
										<div>
											
										</div>
									</td>
								</tr>		
							</tbody>
						</table>
					</div>
				</section>
				<section class="section" id="loginForm">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h3 class="h3">운전면허증 등록</h3>
						</div>	
					</div>
					<div class="tbl-type2 tbl-lefted">
						<table>
							<colgroup>
								<col width="15%">
								<col width="*">
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">운전면허 번호</th>
									<td>
										<form method="post" enctype="multipart/form-data" id="uploadForm_0">
											<input type="hidden" id="flag" name="flag" value="license">
												<span class="file">
													<input type="text" id="licenseNumber" name="licenseNumber" class="input h25 w200" title="" placeholder="운전면허 번호 입력">
												</span>
											<input type="file" name="file"><input type="button" onclick="fileUpload(0);" value="업로드" class="btn type4 secondary">
										</form>						
									</td>						
								</tr>	
								<tr>
									<th scope="row">통장 번호</th>
									<td>
										<form method="post" enctype="multipart/form-data" id="uploadForm_1">
											<input type="hidden" id="flag" name="flag" value="account">
												<span class="file">
													<select name="bankCode" id="bankCode" class="select h30" >
														<option value="">선택</option>
														<c:forEach items="${codeList1}" var="list" varStatus="status">
															<option value="${list.code}">${list.codeName}</option>
														</c:forEach>
													</select>
													<input type="text" id="accountNumber" name="accountNumber" class="input h25 w200" title="" placeholder="통장 번호 입력">
												</span>
											<input type="file" name="file"><input type="button" onclick="fileUpload(1);" value="업로드" class="btn type4 secondary">
										</form>						
									</td>						
								</tr>			
							</tbody>
						</table>
					</div>
				</section>
				<section class="section" id="loginForm">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h3 class="h3">추가정보</h3>
						</div>	
					</div>
					<div class="tbl-type2 tbl-lefted">
						<table>
							<colgroup>
								<col width="15%">
								<col width="35%">
								<col width="15%">
								<col width="*">							
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">취미</th>
									<td><input type="text" name="hobby" id="hobby" value="" placeholder="취미 입력" title="" class="input h25 w300"></td>
									<th scope="row">직업</th>
									<td><input type="text" name="jobName" id="jobName" value="" placeholder="직업 입력" title="" class="input h25 w300"></td>
								</tr>		
								<tr>
									<th scope="row">운전경력</th>
									<td>
										<select name="drivingYear" id="drivingYear" class="select h25">
											<c:forEach var="i" begin="1" end="99">
												<option value="${i}">${i}년</option>
											</c:forEach>
										</select>									
									</td>
									<th scope="row">승인 상태</th>
									<td>
										<select name="driverStatus" id="driverStatus" class="select h30" >
											<c:forEach items="${codeList2}" var="list" varStatus="status">
												<option value="${list.code}">${list.codeName}</option>
											</c:forEach>
										</select>								
									</td>
								</tr>
								<tr>
									<th scope="row">소속 법인</th>
									<td>
										<select name="bizCompNo" id="bizCompNo" class="select h30" >
											<option value="">선택</option>
											<c:forEach items="${codeList3}" var="list" varStatus="status">
												<option value="${list.bizCompNo}">${list.compName}</option>
											</c:forEach>
										</select>
									</td>
									<th scope="row">운행 종류</th>
									<td>
										<select name="permitStatus" id="permitStatus" class="select h30" >
											<c:forEach items="${codeList4}" var="list" varStatus="status">
												<option value="${list.code}">${list.codeName}</option>
											</c:forEach>
										</select>
									</td>
								</tr>	
								<tr>
									<th scope="row">자기 소개</th>
									<td colspan="4">
										<div class="form-controls flexible">
											<textarea rows="3" id="driverRemark" cols="50" class="input type1" value=""></textarea>
										</div>
									</td>
								</tr>																																		
							</tbody>
						</table>
					</div>
				</section>									
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" class="btn type3 primary" onclick="insertDriver();"><span>저장</span></button>
				<button type="button" class="btn type3 secondary" onclick="selfClose();"><span>취소</span></button>
				<!-- last button 
				<button type="button" class="btn type3 primary"><span>확인</span></button>
				<button type="button" class="btn type3 secondary"><span>취소</span></button>
				-->
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>

<!-- S : Alert Popup -->
<div id="popupAlert" class="popup-wrap type-alert" role="alertdialog" aria-hidden="false">
	<section class="popup type1" tabindex="0" style="width: 400px">
		<div class="popup-head">
			<h3>이메일 본인 인증</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<section class="section">
					<p class="form-desc mar-b10">이메일로 인증번호가 발송완료 되었습니다.<br>
					받는 메일서버의 상황에 따라 최대 5분 정도 소요됩니다.<br>
					받으신 인증번호를 하단에 입력하세요.</p>
					<input type="text" name="" id="" value="" placeholder="인증번호 입력" title="" class="input h25 W100">
				</section>				
			</div>
		</div>
		<div class="popup-foot">
			<div class="btn-area flex-center">
				<div>
					<button type="button" class="btn type3 primary" onclick="ui.popup.close('popupAlert')"><span>확인</span></button>
					<button type="button" class="btn type3 secondary" onclick="ui.popup.close('popupAlert')"><span>취소</span></button>
				</div>				
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="제목 팝업닫기" onclick="ui.popup.close('popupAlert')"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>
	<div class="dimmer"></div>
</div>
<!-- E : Alert Popup -->
</body>
</html>