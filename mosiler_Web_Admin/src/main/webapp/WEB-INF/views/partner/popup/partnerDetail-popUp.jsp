<!--  파트너 상세 팝업 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 파트너관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>    	
   	<script src="/js/util.js"></script>
</head>
<script type="text/javascript">
	$(document).ready(function() {
		var id = "${id}";
		
		if(id == "") {
			location.href = "/";
		} 

		var tabId = ${tabId};
		var driverNo = ${driverNo}; 
		
		// driverNo 항시 세팅
		$("#driverNo").val(driverNo);
		
		// 현재 페이지 번호 세팅
		$("input[name=nowPage]").val(${pagingVO.nowPage});
		
		// 현재 탭 번호 세팅
		$("#tabId").val(tabId);
		
		// 탭 전환 시 UI 변경
		for(var i=0; i < 7; i++) {
			if(i == tabId) {
				$("#tabNav"+i).attr("aria-selected", true);
				$("#tabNav"+i).attr("class", "tab is-selected");
				
				$("#tabContent"+i).attr("aria-hidden", false);
				$("#tabContent"+i).attr("class", "tab-content is-visible");
			} else {
				$("#tabNav"+i).attr("aria-selected", false);
				$("#tabNav"+i).attr("class", "tab");
				
				$("#tabContent"+i).attr("aria-hidden", true);
				$("#tabContent"+i).attr("class", "tab-content");
			}
		}
		
		if(tabId == "0") {
			var drivingDetail = ${drivingDetail};	
		
			// 운행내역 리스트
			makeDrivingDetailContents(drivingDetail);
			makePagingVar(0);
			
		} else if(tabId == "1") {
			var csDetail = ${csDetail};	
			
			// 상담내역 리스트
			makeCsDetailContents(csDetail);
			makePagingVar(1);
			
		} else if(tabId == "2") {
			var driverClassDetail = ${driverClassDetail};	
			
			// 교육내역 리스트
			makeDriverClassDetailContents(driverClassDetail);
			makePagingVar(2);
			
		}  else if(tabId == "3") {
			var driverEvaluationDetail = ${driverEvaluationDetail};	
			
			// 평가 리스트
			makeDriverEvaluationDetailContents(driverEvaluationDetail);
			makePagingVar(3);
			
		}  else if(tabId == "4") {
			var driverIssuedDetail = ${driverIssuedDetail};	
			
			// 물품이력 리스트
			makeDriverIssuedDetailContents(driverIssuedDetail);
			makePagingVar(4);
			
		}  else if(tabId == "5") {
			var driverAccdentDetail = ${driverAccdentDetail};	
			
			// 사고이력 리스트
			makeDriverAccdentDetailContents(driverAccdentDetail);
			makePagingVar(5);
			
		}  else if(tabId == "6") {
			var driverViolationDetail	 = ${driverViolationDetail};	
			
			// 교통위반 이력 리스트
			makeDriverViolationDetailContents(driverViolationDetail);
			makePagingVar(6);
		}
	});
	
	// 운행내역 리스트
	function makeDrivingDetailContents(drivingDetail) {
		var html = "";
		var cntPerPage = ${pagingVO.cntPerPage};
		
		for(var i = 0; i < cntPerPage; i++) {
			if(drivingDetail[i] == null) {
				break;
			} else {
				var paymentAmount;
				
				if(drivingDetail[i].paymentYn == "N") {
					paymentAmount = "미정산";
				} else {
					paymentAmount = drivingDetail[i].paymentAmount
				}
				
				//var originPhoneNumber = customerList[i].phoneNumber;
				//var parsePhoneNumber = originPhoneNumber.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
				
				//html += "<tr style='cursor: pointer' onclick=\"window.open('/customer/customerDetailpopUp?"
				//			 +"customerNo="+customerList[i].customerNo+"','','width=1130,height=900');return true\">";
				html += "<tr>";
				html += "	<input type='hidden' id='drivingNo' 		name='drivingNo' 	value='"+drivingDetail[i].drivingNo+"'>";
				html += "	<input type='hidden' id='driverNo' 			name='driverNo' 		value='"+drivingDetail[i].driverNo+"'>";
				html += "	<input type='hidden' id='customerNo' 	name='customerNo'	value='"+drivingDetail[i].customerNo+"'>";
				html += "	<td>"+drivingDetail[i].rownum+"</td>";
				html += "	<td>"+drivingDetail[i].customerName+"</td>";          
				html += "	<td class='align-l'>"+drivingDetail[i].startAddress+"</td>";
				html += "	<td class='align-l'>"+drivingDetail[i].endAddress+"</td>";
				
				html += "	<td>"+drivingDetail[i].drivingTypeName+"</td>";
				html += "	<td>"+drivingDetail[i].revStartDt.substring(0, 10)+"</td>";
				html += "	<td>"+drivingDetail[i].revStartDt.substring(11, 16)+"</td>";
				html += "	<td>"+drivingDetail[i].revEndDt.substring(11, 16)+"</td>";
				html += "	<td>"+drivingDetail[i].timeStamp+"분</td>";
				html += "	<td>"+drivingDetail[i].averagePoint+"</td>";
				html += "	<td>"+paymentAmount+"</td>";
				html += "</tr>";
			}
		}
		
		$("#drivingDetailDiv").append(html);		
	}
	
	// 상담내역 리스트
	function makeCsDetailContents(csDetail) {
		var html = "";
		//var cntPerPage = ${pagingVO.cntPerPage};
		
		for(var i = 0; i < csDetail.length; i++) {
			if(csDetail[i] == null) {
				break;
			} else {
				//var originPhoneNumber = customerList[i].phoneNumber;
				//var parsePhoneNumber = originPhoneNumber.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
				
				//html += "<tr style='cursor: pointer' onclick=\"window.open('/customer/customerDetailpopUp?"
				//			 +"customerNo="+customerList[i].customerNo+"','','width=1130,height=900');return true\">";
				html += "<tr>";
				html += "	<input type='hidden' id='csNo' 			name='csNo'			value='"+csDetail[i].customerNo+"'>";
				html += "	<input type='hidden' id='drivingNo' 	name='drivingNo' 	value='"+csDetail[i].drivingNo+"'>";
				html += "	<input type='hidden' id='driverNo' 		name='driverNo' 		value='"+csDetail[i].driverNo+"'>";
				html += "	<td>"+csDetail[i].rownum+"</td>";
				html += "	<td>"+csDetail[i].customerName+"</td>";          
				html += "	<td class='align-l'>"+csDetail[i].comment+".......</td>";
				html += "	<td class='align-l'>"+csDetail[i].startAddress+"</td>";
				html += "	<td class='align-l'>"+csDetail[i].endAddress+"</td>";
				html += "	<td>"+csDetail[i].drivingTypeName+"</td>";
				html += "	<td>"+csDetail[i].drivingDate+"</td>";
				html += "	<td>"+csDetail[i].drivingStatusName+"</td>";
				html += "	<td>"+csDetail[i].regDt+"</td>";
				html += "</tr>";
			}
		}
		
		$("#csDetailDiv").append(html);				
	}
	
	// 교육내역 리스트
	function makeDriverClassDetailContents(driverClassDetail) {
		var html = "";
		//var cntPerPage = ${pagingVO.cntPerPage};
		
		for(var i = 0; i < driverClassDetail.length; i++) {
			if(driverClassDetail[i] == null) {
				break;
			} else {
				//var originPhoneNumber = customerList[i].phoneNumber;
				//var parsePhoneNumber = originPhoneNumber.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
				
				//html += "<tr style='cursor: pointer' onclick=\"window.open('/customer/customerDetailpopUp?"
				//			 +"customerNo="+customerList[i].customerNo+"','','width=1130,height=900');return true\">";
				html += "<tr>";
				html += "	<input type='hidden' id='idx' 				name='idx'				value='"+driverClassDetail[i].idx+"'>";
				html += "	<input type='hidden' id='driverNo' 		name='driverNo' 		value='"+driverClassDetail[i].driverNo+"'>";
				html += "	<td>"+driverClassDetail[i].className+"</td>";          
				html += "	<td>"+driverClassDetail[i].drivingGrade+"</td>";
				html += "	<td>"+driverClassDetail[i].transformGrade+"</td>";
				html += "	<td>"+driverClassDetail[i].lookGrade+"</td>";
				html += "	<td class='align-l'>"+driverClassDetail[i].comment.substring(0, 50)+"......</td>";
				html += "	<td>"+driverClassDetail[i].statusName+"</td>";
				html += "	<td>"+driverClassDetail[i].regDt.substring(0, 10)+"</td>";
				html += "</tr>";
			}
		}
		
		$("#driverClassDetailDiv").append(html);				
	}
	
	// 평가 리스트
	function makeDriverEvaluationDetailContents(driverEvaluationDetail) {
		var html = "";
		//var cntPerPage = ${pagingVO.cntPerPage};
		
		for(var i = 0; i < driverEvaluationDetail.length; i++) {
			if(driverEvaluationDetail[i] == null) {
				break;
			} else {
				var type;
				var isApply;
				
				if(driverEvaluationDetail[i].type == "1") {
					type = "인센티브";
				} else if (driverEvaluationDetail[i].type == "2") {
					type = "팁";
				} else {
					type ="";
				}

				if(driverEvaluationDetail[i].isApply == true) {
					isApply = "적용";
				} else {
					isApply = "";
				}
				
				//var originPhoneNumber = customerList[i].phoneNumber;
				//var parsePhoneNumber = originPhoneNumber.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
				
				//html += "<tr style='cursor: pointer' onclick=\"window.open('/customer/customerDetailpopUp?"
				//			 +"customerNo="+customerList[i].customerNo+"','','width=1130,height=900');return true\">";
				html += "<tr>";
				html += "	<input type='hidden' id='idx' 				name='idx'				value='"+driverEvaluationDetail[i].idx+"'>";
				html += "	<input type='hidden' id='driverNo' 		name='driverNo' 		value='"+driverEvaluationDetail[i].driverNo+"'>";
				html += "	<td>"+driverEvaluationDetail[i].rownum+"</td>";
				html += "	<td>"+driverEvaluationDetail[i].revStartDt+"</td>"          
				html += "	<td>"+driverEvaluationDetail[i].revEndDt+"	</td>";
				html += "	<td>"+driverEvaluationDetail[i].driverLevel+"</td>";
				html += "	<td>"+driverEvaluationDetail[i].incentive+"</td>";
				html += "	<td>"+driverEvaluationDetail[i].incomeTax+"</td>";
				html += "	<td>"+driverEvaluationDetail[i].localTax+"</td>";
				html += "	<td>"+driverEvaluationDetail[i].grossIncentive+"</td>";
				html += "	<td>"+isApply+"</td>";
				html += "	<td>"+type+"</td>";
				html += "</tr>";
			}
		}
		
		$("#driverEvaluationDetailDiv").append(html);				
	}
	
	// 물품이력 리스트
	function makeDriverIssuedDetailContents(driverIssuedDetail) {
		var html = "";
		//var cntPerPage = ${pagingVO.cntPerPage};
		
		for(var i = 0; i < driverIssuedDetail.length; i++) {
			if(driverIssuedDetail[i] == null) {
				break;
			} else {
				//var originPhoneNumber = customerList[i].phoneNumber;
				//var parsePhoneNumber = originPhoneNumber.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
				
				//html += "<tr style='cursor: pointer' onclick=\"window.open('/customer/customerDetailpopUp?"
				//			 +"customerNo="+customerList[i].customerNo+"','','width=1130,height=900');return true\">";
				html += "<tr>";
				html += "	<input type='hidden' id='issuedNo' 	name='issuedNo'		value='"+driverIssuedDetail[i].issuedNo+"'>";
				html += "	<input type='hidden' id='driverNo' 		name='driverNo' 		value='"+driverIssuedDetail[i].driverNo+"'>";
				html += "	<input type='hidden' id='itemNo' 		name='itemNo' 		value='"+driverIssuedDetail[i].itemNo+"'>";
				html += "	<td>"+driverIssuedDetail[i].itemName+"</td>";         
				html += "	<td>"+driverIssuedDetail[i].date+"</td>";
				html += "	<td>"+driverIssuedDetail[i].qty+"</td>";
				html += "	<td class='align-l'>"+driverIssuedDetail[i].comment.substring(0, 100)+"......</td>";
				html += "	<td>"+driverIssuedDetail[i].regDt.substring(0, 10)+"</td>";
				html += "</tr>";
			}
		}
		
		$("#driverIssuedDetailDiv").append(html);				
	}
	
	// 사고이력 리스트
	function makeDriverAccdentDetailContents(driverAccdentDetail) {
		var html = "";
		//var cntPerPage = ${pagingVO.cntPerPage};
		
		for(var i = 0; i < driverAccdentDetail.length; i++) {
			if(driverAccdentDetail[i] == null) {
				break;
			} else {
				//var originPhoneNumber = customerList[i].phoneNumber;
				//var parsePhoneNumber = originPhoneNumber.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
				
				//html += "<tr style='cursor: pointer' onclick=\"window.open('/customer/customerDetailpopUp?"
				//			 +"customerNo="+customerList[i].customerNo+"','','width=1130,height=900');return true\">";
				html += "<tr>";
				html += "	<input type='hidden' id='accdentNo' 	name='accdentNo'	value='"+driverAccdentDetail[i].accdentNo+"'>";
				html += "	<input type='hidden' id='driverNo' 		name='driverNo' 		value='"+driverAccdentDetail[i].driverNo+"'>";
				html += "	<input type='hidden' id='drivingNo' 	name='drivingNo' 	value='"+driverAccdentDetail[i].drivingNo+"'>";
				html += "	<td>"+driverAccdentDetail[i].customerName+"<br>"+driverAccdentDetail[i].carPNo+"</td>";          
				html += "	<td>"+driverAccdentDetail[i].actDate+"<br>"+driverAccdentDetail[i].insuranceNo+"</td>";       
				html += "	<td>"+driverAccdentDetail[i].isHurt+"<br>"+driverAccdentDetail[i].partCar+"</td>";       
				html += "	<td>"+driverAccdentDetail[i].aCarPNo+"<br>"+driverAccdentDetail[i].aInsuranceNo+"</td>"; 
				html += "	<td>"+driverAccdentDetail[i].aisHurt+"<br>"+driverAccdentDetail[i].aPartCar+"</td>"; 
				html += "	<td>"+driverAccdentDetail[i].location+"</td>";   
				html += "	<td>"+driverAccdentDetail[i].comment+"<br>"+driverAccdentDetail[i].result+"</td>"; 
				html += "	<td>"+driverAccdentDetail[i].statusName+"</td>";   
				html += "</tr>";
			}
		}
		
		$("#driverAccdentDetailDiv").append(html);			
	}
	
	// 교통위반 이력 리스트
	function makeDriverViolationDetailContents(driverViolationDetail) {
		var html = "";
		//var cntPerPage = ${pagingVO.cntPerPage};
		
		for(var i = 0; i < driverViolationDetail.length; i++) {
			if(driverViolationDetail[i] == null) {
				break;
			} else {
				//var originPhoneNumber = customerList[i].phoneNumber;
				//var parsePhoneNumber = originPhoneNumber.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
				
				//html += "<tr style='cursor: pointer' onclick=\"window.open('/customer/customerDetailpopUp?"
				//			 +"customerNo="+customerList[i].customerNo+"','','width=1130,height=900');return true\">";
				html += "<tr>";
				html += "	<input type='hidden' id='violationNo' 	name='violationNo'	value='"+driverViolationDetail[i].violationNo+"'>";
				html += "	<input type='hidden' id='driverNo' 		name='driverNo' 		value='"+driverViolationDetail[i].driverNo+"'>";
				html += "	<input type='hidden' id='drivingNo' 	name='drivingNo' 	value='"+driverViolationDetail[i].drivingNo+"'>";
				html += "	<td>"+driverViolationDetail[i].carPNo+"</td>"; 
				html += "	<td>"+driverViolationDetail[i].customerName+"</td>"; 
				html += "	<td>"+driverViolationDetail[i].vioDate+"</td>"; 
				html += "	<td>"+driverViolationDetail[i].vioTypeName+"</td>"; 
				html += "	<td>"+driverViolationDetail[i].resTypeName+"</td>"; 
				html += "	<td>"+driverViolationDetail[i].vioAmount+"</td>"; 
				html += "	<td>"+driverViolationDetail[i].divideAmount+"</td>"; 
				html += "	<td>"+driverViolationDetail[i].isDone+"</td>"
				html += "	<td>"+driverViolationDetail[i].isDivide+"</td>"
				html += "	<td>"+driverViolationDetail[i].regDt.substring(0,10)+"</td>"
				html += "</tr>";
			}
		}
		
		$("#driverViolationDetailDiv").append(html);					
	}
	
	// 페이징 바
	function makePagingVar(num) {
		var nowPage = ${pagingVO.nowPage};
		var startPage = ${pagingVO.startPage};
		var endPage = ${pagingVO.endPage};
		var lastPage = ${pagingVO.lastPage};
	
		console.log(num);
		
		var html = "";
		
		html += "<div class=\"col'\">";
		html += "	<div class=\"paging paging-basic\">";
		html += "		<div class=\"page-group\">";
		
		if(nowPage == 1) {
			html += "		<button type=\"button\" class=\"btn first\" title=\"처음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
			html += "		<button type=\"button\" class=\"btn prev\" title=\"이전 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
		} else {
			html += "		<button type=\"button\" id=\"firstPage\" class=\"btn first\" title=\"처음 페이지\"  onclick='arrowBtn(1);'><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
			html += "		<button type=\"button\" id=\"prevPage\" class=\"btn prev\" title=\"이전 페이지\" onclick='arrowBtn(2);'><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
		}
		
		html += "		</div>";
		html += "		<ul class=\"num-group\">";
		
		for(var i = startPage; i <= endPage; i++) {
			if(i == nowPage) {
				html += "	<li><button type=\"button\" class=\"btn\" aria-current=\"true\" disabled=\"disabled\"><span>"+i+"</span></button></li>";
			} else {
				html += "	<li><button type=\"button\" id=\"e\" class=\"btn\" onclick='numberBtn("+i+");'><span>"+i+"</span></button></li>"
			}
		}
		
		html += "		</ul>";
		html += "		<div class=\"page-group\">";
		
		if(nowPage == lastPage) {
			html += "		<button type=\"button\" class=\"btn next\" title=\"다음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-next gray\"></i></span></button>";
			html += "		<button type=\"button\" class=\"btn last\" title=\"마지막 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
		} else {
			html += "		<button type=\"button\" id=\"nextPage\" class=\"btn next\" title=\"다음 페이지\" onclick='arrowBtn(3);'><span><i class=\"arw arw-board1-next gray\"></i></span></button>";			
			html += "		<button type=\"button\" id=\"lastPage\" class=\"btn last\" title=\"마지막 페이지\" onclick='arrowBtn(4);'><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
		}
		
		html += "		</div>";		
		html += "	</div>";
		html += "</div>";	
		
		$("#pagingVar"+num).append(html);
	}

	// 한 페이지에 보여줄 row수 전환 
	function cntPerPageChange() {
		// 현재 페이지 번호 저장
		$("input[name=nowPage]").val(${pagingVO.nowPage});
		
		// submit
		document.partnerDetailForm.submit(); 
	}

	// 페이징 번호 버튼
	function numberBtn(num) {
		// 전활될 페이지 번호
		$("input[name=nowPage]").val(num);
		
		// submit
		document.partnerDetailForm.submit(); 
	}
	
	// 페이징 화살표 버튼
	function arrowBtn(num) {
		var nowPage = ${pagingVO.nowPage};
		
		if(num == 1) {
			// 처음
			nowPage = 1;
		} else if(num == 2) {
			// 이전
			nowPage = nowPage - 1;
		} else if(num == 3) {
			// 다음
			nowPage = nowPage + 1;
		} else {
			// 마지막
			nowPage = ${pagingVO.lastPage};
		}
		
		// 전활될 페이지 번호
		$("input[name=nowPage]").val(nowPage);
		
		// submit
		document.partnerDetailForm.submit(); 
	}
	
	// 탭 전환
	function changeTab(num) {
		$("#tabId").val(num);

		// submit
		document.partnerDetailForm.submit(); 
	}
	
	// 닫기 버튼
	function selfClose() {
		self.close();
	}
</script>
<body>
<form name="partnerDetailForm" id="partnerDetailForm" method="post" action="/partner/partnerDetailpopUp" onsubmit="return false">
<input type="hidden" id="tabId" name="tabId">
<input type="hidden" id="driverNo" name="driverNo">
<input type="hidden" name="nowPage"> 
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">파트너상세</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<section class="section">
					<c:forEach var="partnerDetail" items="${partnerDetail}" varStatus="idx">
						<div class="grid gut-guide">
							<div class="col col-2">
								<div class="photo-wrap">	
									<div class="photo">
								        <div class="img">
								        	<img src="${partnerDetail.profileImgName}" style="max-width: 100%; height: auto;">
								        </div>
								    </div>
								    <div class="mar-t5">
								        <button class="btn type4">수정</button>
								        <button class="btn type4">삭제</button>
								    </div>  
								</div>								
							</div>
							<div class="col col-10">
								<div class="tbl-type1 tbl-lefted">
									<table>
										<colgroup>
											<col width="17%">
											<col width="*">
											<col width="17%">
											<col width="30%">
										</colgroup>
										<tbody>
											<tr>
												<th scope="row">이름</th>
												<td>${partnerDetail.userName}</td>
												<th scope="row">이메일(아이디)</th>
												<td>${partnerDetail.userId}</td>										
											</tr>
											<tr>
												<th scope="row">휴대폰 번호</th>
												<td>${partnerDetail.phoneNumber}</td>
												<th scope="row">등록일시</th>
												<td>${partnerDetail.regDt}</td>										
											</tr>											
											<tr>
												<th scope="row">승인 상태</th>
												<td>
													${partnerDetail.driverStatusName}
													<c:if test="${partnerDetail.approvalDt != null}">
														(${partnerDetail.approvalDt}) 	
													</c:if>
												</td>												
												<th scope="row">최종 접속 일시</th>
												<td>${partnerDetail.lastAccessDate}</td>									
											</tr>
											<tr>
												<th scope="row">운행 횟수</th>
												<td>${partnerDetail.drivingCnt}</td>												
												<th scope="row">총 정산금액</th>
												<td>${partnerDetail.totalChargeAmount}</td>									
											</tr>												
											<!-- <tr>
												<th scope="row">보험키</th>
												<td colspan="3">123456789012, 123456789012, 123456789012</td>											
											</tr>										
											<tr>
												<th scope="row">운전면허증</th>
												<td colspan="3">12-345678-90
													<button type="button" class="btn-ico type2" title="사진다운"><span><i class="ico ico-photo1">사진 다운</i></span></button>
													<span class="file">
														<input type="text" id="sFilesName11" class="input h25" placeholder="운전면허증_김기사.jpg" title="찾기된 파일명" readonly="readonly">
														<label for="sFiles1" class="btn type4 secondary type-attach" role="button">
															<span><input type="file" name="sFiles1" id="sFiles1" value="찾아보기" tabindex="-1" aria-hidden="true" onchange="fileAttachSrc(this, event)">찾기</span>
														</label>
													</span>	
												</td>
											</tr>
											<tr>	
												<th scope="row">운전경력 증명서</th>
												<td colspan="3">발급일 YYYY-MM-DD	
													<button type="button" class="btn-ico type2" title="사진다운"><span><i class="ico ico-photo1">사진 다운</i></span></button>												
													<span class="file">
														<input type="text" id="sFilesName11" class="input h25" placeholder="운전경력증명서_김기사.jpg" title="찾기된 파일명" readonly="readonly">
														<label for="sFiles1" class="btn type4 secondary type-attach" role="button">
															<span><input type="file" name="sFiles1" id="sFiles1" value="찾아보기" tabindex="-1" aria-hidden="true" onchange="fileAttachSrc(this, event)">찾기</span>
														</label>
													</span>	
													<button type="button" class="btn type4 primary"><span>승인</span></button>											
												</td>
											</tr>										
											<tr>
												<th scope="row">은행정보</th>
												<td colspan="3">은행명/계좌번호
													<button type="button" class="btn-ico type2" title="사진다운"><span><i class="ico ico-photo1">사진 다운</i></span></button>
													<span class="file">
														<input type="text" id="sFilesName11" class="input h25" placeholder="통장사본_김기사.jpg" title="찾기된 파일명" readonly="readonly">
														<label for="sFiles1" class="btn type4 secondary type-attach" role="button">
															<span><input type="file" name="sFiles1" id="sFiles1" value="찾아보기" tabindex="-1" aria-hidden="true" onchange="fileAttachSrc(this, event)">찾기</span>
														</label>
													</span>											
												</td>
											</tr>
											<tr>	
												<th scope="row">주민등록 등본</th>
												<td colspan="3">
													<button type="button" class="btn-ico type2" title="사진다운"><span><i class="ico ico-photo1">사진 다운</i></span></button>
													<span class="file">
														<input type="text" id="sFilesName11" class="input h25" placeholder="주민등록번호_김기사.jpg" title="찾기된 파일명" readonly="readonly">
														<label for="sFiles1" class="btn type4 secondary type-attach" role="button">
															<span><input type="file" name="sFiles1" id="sFiles1" value="찾아보기" tabindex="-1" aria-hidden="true" onchange="fileAttachSrc(this, event)">찾기</span>
														</label>
													</span>											
												</td>											
											</tr> -->
											<tr>
												<th scope="row">면허증 번호</th>
												<td>${partnerDetail.licenseNumber}</td>
												<th scope="row">계좌 정보</th>
												<td>${partnerDetail.bankCodeName} &nbsp; ${partnerDetail.accountNumber}</td>											
											</tr>
											<tr>
												<th scope="row">직업 / 취미</th>
												<td>
													${partnerDetail.jobName} 
													<c:if test="${partnerDetail.hobby != null}">
														/ ${partnerDetail.hobby}
													</c:if>
												</td>
												<th scope="row">운전 경력</th>
												<td>${partnerDetail.drivingYear}년</td>											
											</tr>
											<tr>
												<th scope="row">소속 법인</th>
												<td>${partnerDetail.compName}</td>
												<th scope="row">평균 평점</th>
												<td>${partnerDetail.averagePoint}</td>											
											</tr>
											<%-- <tr>
												<th scope="row">디바이스 구분</th>
										--		<td>${partnerDetail.}</td>
												<th scope="row">디바이스 ID</th>
									--			<td>${partnerDetail.}</td>											
											</tr> --%>
											<tr>
												<th scope="row">권한</th>
												<td>${partnerDetail.permitStatusName}</td>
												<th scope="row">주민등록번호</th>
												<td>${partnerDetail.ssn}</td>											
											</tr>
											<tr>
												<th scope="row">자기 소개</th>
												<td>${partnerDetail.driverRemark}</td>
											</tr>
											<!-- <tr>
												<th scope="row">간편로그인 연동</th>
												<td>
													<i class="ico ico-sns facebook">facebook</i>
													<i class="ico ico-sns mail">mail</i>
													<i class="ico ico-sns navermail">navermail</i>
													<i class="ico ico-sns gmail">gmail</i>
													<i class="ico ico-sns naver">naver</i>
													<i class="ico ico-sns kakao">kakao</i>													
												</td>
												<th scope="row">교관</th>
												<td>
													<span class="switch type1">
													    <input type="checkbox" name="sSwitch2" id="sSwitch22" value="" checked>
													    <label for="sSwitch22"></label>
													</span>											
												</td>											
											</tr>
											<tr>
												<th scope="row">기타</th>
												<td colspan="3">
													<button type="button" class="btn type4 primary"><span>자기소개</span></button>		
													<button type="button" class="btn type4 primary"><span>계약서</span></button>									
												</td>							
											</tr>		 -->																																																																														
										</tbody>
									</table>
								</div>		
							</div>
						</div>
					</c:forEach>		
				</section>										
				<section class="section">
					<!-- S: Tab Area -->
					<div class="tab-nav type1">
					    <ul class="tab-list" role="tablist">
					        <li id="tabNav0" role="tab" aria-controls="tabContent0" aria-selected="true" class="tab is-selected">
					        	<button type="button" class="btn" onclick="changeTab(0);"><span>운행내역</span></button>
					        </li>
					        <li id="tabNav1" role="tab" aria-controls="tabContent0" aria-selected="false" class="tab">
					        	<button type="button" class="btn" onclick="changeTab(1);"><span>상담내역</span></button>
					        </li>
					        <li id="tabNav2" role="tab" aria-controls="tabContent2" aria-selected="false" class="tab">
					        	<button type="button" class="btn" onclick="changeTab(2);"><span>교육내역</span></button>
					        </li>
					        <li id="tabNav3" role="tab" aria-controls="tabContent3" aria-selected="false" class="tab">
					        	<button type="button" class="btn" onclick="changeTab(3);"><span>평가</span></button>
					        </li>
					        <li id="tabNav4" role="tab" aria-controls="tabContent4" aria-selected="false" class="tab">
					        	<button type="button" class="btn" onclick="changeTab(4);"><span>물품이력</span></button>
					        </li>
					        <li id="tabNav5" role="tab" aria-controls="tabContent5" aria-selected="false" class="tab">
					        	<button type="button" class="btn" onclick="changeTab(5);"><span>사고이력</span></button>
					        </li>
					        <li id="tabNav6" role="tab" aria-controls="tabContent6" aria-selected="false" class="tab">
					        	<button type="button" class="btn" onclick="changeTab(6)"><span>교통위반 이력</span></button>
					        </li>
					    </ul>
					</div>
					<div class="tab-container">
					    <div id="tabContent0" aria-labelledby="tabNav0" aria-hidden="false" class="tab-content is-visible">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">운행내역</h2>
								</div>
							</div>						    
							<div class="gtbl-list l15">
								<table>
									<caption>운행내역</caption>
									<thead>
										<tr>
											<th>번호</th>
											<th>고객명</th>
											<th>출발지</th>
											<th>도착지</th>
											<th>운행 구분</th>
											<th>운행 일자</th>
											<th>시작</th>
											<th>종료</th>
											<th>운행 시간(분)</th>
											<th>운행 평가</th>
											<th>정산 금액</th>
										</tr>
									</thead>
									<tbody id="drivingDetailDiv">
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
								<div id="pagingVar0" class="col"></div>
								<div class="col per-page">
									<select name="cntPerPage0" id="cntPerPage0" class="select h30" onChange="cntPerPageChange()">
									    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
									    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
									    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
									    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
									    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
									    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
									</select>	
								</div>			
							</div>
					    </div>
					    <div id="tabContent1" aria-labelledby="tabNav1" aria-hidden="true" class="tab-content">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">상담내역</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>상담내역</caption>
									<thead>
										<tr>
											<th>번호</th>
											<th>고객명</th>
											<th>상담 내용</th>
											<th>출발지</th>
											<th>도착지</th>
											<th>운행 구분</th>
											<th>운행일</th>
											<th>상태</th>
											<th>등록일</th>
										</tr>
									</thead>
									<tbody id="csDetailDiv">
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
								<div id="pagingVar1" class="col"></div>
								<div class="col per-page">
									<select name="cntPerPage1" id="cntPerPage1" class="select h30" onChange="cntPerPageChange()">
									    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
									    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
									    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
									    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
									    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
									    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
									</select>
								</div>			
							</div>
					    </div>
					   	<div id="tabContent2" aria-labelledby="tabNav2" aria-hidden="true" class="tab-content">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">교육내역</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>교육내역</caption>
									<thead>
										<tr>
											<th>교육명</th>
											<th>운전 등급</th>
											<th>의전 등급</th>
											<th>P 등급</th>
											<th>비고</th>
											<th>상태</th>
											<th>교육일</th>
										</tr>
									</thead>
									<tbody id="driverClassDetailDiv">
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
								<div id="pagingVar2" class="col"></div>
								<div class="col per-page">
									<select name="cntPerPage2" id="cntPerPage2" class="select h30" onChange="cntPerPageChange()">
									    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
									    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
									    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
									    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
									    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
									    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
									</select>
								</div>			
							</div>
					    </div>
					   	<div id="tabContent3" aria-labelledby="tabNav3" aria-hidden="true" class="tab-content">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">평가</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>평가</caption>
									<thead>
										<tr>
											<th>시작일</th>
											<th>운행 시작 시간</th>
											<th>운행 종료 시간</th>
											<th>등급</th>
											<th>인센티브</th>
											<th>세금</th>
											<th>지방세</th>
											<th>지급금</th>
											<th>대다??</th>
											<th>기타 정산</th>
										</tr>
									</thead>
									<tbody id="driverEvaluationDetailDiv">
									</tbody>		
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
								<div id="pagingVar3" class="col"></div>
								<div class="col per-page">
									<select name="cntPerPage3" id="cntPerPage3" class="select h30" onChange="cntPerPageChange()">
									    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
									    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
									    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
									    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
									    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
									    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
									</select>
								</div>			
							</div>
					    </div>
					    <div id="tabContent4" aria-labelledby="tabNav4" aria-hidden="true" class="tab-content">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">물품이력</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>물품이력</caption>
									<thead>
										<tr>
											<th>품명</th>
											<th>날짜</th>
											<th>수량</th>
											<th>비고</th>
											<th>등록일</th>
										</tr>
									</thead>
									<tbody id="driverIssuedDetailDiv">
									</tbody>		
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
								<div id="pagingVar4" class="col"></div>
								<div class="col per-page">
									<select name="cntPerPage4" id="cntPerPage4" class="select h30" onChange="cntPerPageChange()">
									    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
									    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
									    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
									    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
									    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
									    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
									</select>
								</div>			
							</div>
					    </div>
					    <div id="tabContent5" aria-labelledby="tabNav5" aria-hidden="true" class="tab-content">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">사고이력</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>사고이력</caption>
									<thead>
										<tr>
											<th>고객명 <br> 차량번호</th>
											<th>사고일시 <br> 보험접수 번호</th>
											<th>탑승자 부상 여부 <br> 차량 파손 부위</th>
											<th>상대 차량번호 <br> 상대 보험번호</th>
											<th>상대 부상 여부 <br> 상대 차량 파손 부위</th>
											<th>사고 위치</th>
											<th>사고 내용 <br> 사고 결과</th>
											<th>상태</th>
										</tr>
									</thead>
									<tbody id="driverAccdentDetailDiv">
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
								<div id="pagingVar5" class="col"></div>
								<div class="col per-page">
									<select name="cntPerPage5" id="cntPerPage5" class="select h30" onChange="cntPerPageChange()">
									    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
									    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
									    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
									    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
									    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
									    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
									</select>
								</div>			
							</div>
					    </div>
					    <div id="tabContent6" aria-labelledby="tabNav6" aria-hidden="true" class="tab-content">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">교통위반 이력</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>교통위반 이력</caption>
									<thead>
										<tr>
											<th>차량 번호</th>
											<th>고객명</th>
											<th>위반 일시</th>
											<th>구분</th>
											<th>위반 구분</th>
											<th>청구 금액</th>
											<th>납부 금액</th>
											<th>납부 여부</th>
											<th>납부 여부</th>
											<th>등록일</th>
										</tr>
									</thead>
									<tbody id="driverViolationDetailDiv">
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
								<div id="pagingVar6" class="col"></div>
								<div class="col per-page">
									<select name="cntPerPage6" id="cntPerPage6" class="select h30" onChange="cntPerPageChange()">
									    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
									    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
									    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
									    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
									    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
									    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
									</select>
								</div>			
							</div>
					    </div>
					</div>
					<!-- e: Tab Area -->	
				</section>
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" onclick="selfClose();" class="btn type3 primary"><span>확인</span></button>
				<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</form>
</body>
</html>