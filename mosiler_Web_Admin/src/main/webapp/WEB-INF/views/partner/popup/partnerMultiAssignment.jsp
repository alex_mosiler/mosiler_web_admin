<!--  파트너 멀티 배정 팝업 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 예약관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
</head>

<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">파트너 멀티배정</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<div class="grid flex-center cross-center">
					<div class="col col-5">
						<section class="section form-search">
							<div class=" input-group has-append">								
								<input type="text" name="" id="" value="" placeholder="파트너명,휴대폰번호" title="" class="input h30">
								<span class="append"><button type="button" class="btn"><span><i class="ico ico-search1 dark">search</i></span></button></span>
							</div>					
						</section>
						<section class="section mar-t10">
							<div class="gtbl-list l10">
								<table class="">
									<caption></caption>
									<thead>
										<tr>
											<th scope="col">선택</th>
											<th scope="col">번호</th>
											<th scope="col">파트너</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck11" id="sCheck11" value="">
												    <label for="sCheck11"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>김기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck12" id="sCheck12" value="">
												    <label for="sCheck12"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>이기사</td>
											<td>010-1234-5678</td>
										</tr>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck13" id="sCheck13" value="">
												    <label for="sCheck13"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>최기사</td>
											<td>010-1234-5678</td>
										</tr>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck14" id="sCheck14" value="">
												    <label for="sCheck14"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>박기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck15" id="sCheck15" value="">
												    <label for="sCheck15"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>공기사</td>
											<td>010-1234-5678</td>
										</tr>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck16" id="sCheck16" value="">
												    <label for="sCheck16"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>김기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck17" id="sCheck17" value="">
												    <label for="sCheck17"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>이기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck18" id="sCheck18" value="">
												    <label for="sCheck18"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>박기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck19" id="sCheck19" value="">
												    <label for="sCheck19"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>공기사</td>
											<td>010-1234-5678</td>
										</tr>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck20" id="sCheck20" value="">
												    <label for="sCheck20"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>김기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck21" id="sCheck21" value="">
												    <label for="sCheck21"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>이기사</td>
											<td>010-1234-5678</td>
										</tr>																																																																																																																																																																									
									</tbody>	
								</table>
							</div>
						</section>				
					</div>
					<div class="col col-2">
						<div class="align-c ">
							<p><button type="button" class="btn-ico type3"><span><i class="arw arw-shuttle-next"></i></span></button></p>
							<p class="mar-t10"><button type="button" class="btn-ico type3"><span><i class="arw arw-shuttle-prev"></i></span></button></p>
						</div>
					</div>
					<div class="col col-5">
						<section class="section form-search">
							<div class=" input-group has-append">								
								<input type="text" name="" id="" value="" placeholder="파트너명,휴대폰번호" title="" class="input h30">
								<span class="append"><button type="button" class="btn"><span><i class="ico ico-search1 dark">search</i></span></button></span>
							</div>					
						</section>
						<section class="section mar-t10">
							<div class="gtbl-list l10">
								<table class="">
									<caption></caption>
									<thead>
										<tr>
											<th scope="col">선택</th>
											<th scope="col">번호</th>
											<th scope="col">파트너</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck51" id="sCheck51" value="">
												    <label for="sCheck51"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>김기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck52" id="sCheck52" value="">
												    <label for="sCheck52"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>이기사</td>
											<td>010-1234-5678</td>
										</tr>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck53" id="sCheck53" value="">
												    <label for="sCheck53"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>최기사</td>
											<td>010-1234-5678</td>
										</tr>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck54" id="sCheck54" value="">
												    <label for="sCheck54"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>박기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck55" id="sCheck55" value="">
												    <label for="sCheck55"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>공기사</td>
											<td>010-1234-5678</td>
										</tr>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck56" id="sCheck56" value="">
												    <label for="sCheck56"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>김기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck57" id="sCheck57" value="">
												    <label for="sCheck57"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>이기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck58" id="sCheck58" value="">
												    <label for="sCheck58"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>박기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck59" id="sCheck59" value="">
												    <label for="sCheck59"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>공기사</td>
											<td>010-1234-5678</td>
										</tr>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck60" id="sCheck60" value="">
												    <label for="sCheck60"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>김기사</td>
											<td>010-1234-5678</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck61" id="sCheck61" value="">
												    <label for="sCheck61"><span class="blind">선택</span></label>
												</span>								
											</td>									
											<td>이기사</td>
											<td>010-1234-5678</td>
										</tr>																																																																																																																																																																									
									</tbody>	
								</table>
							</div>
						</section>								
					</div>
				</div>
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" class="btn type3 primary"><span>확인</span></button>
				<button type="button" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</body>
</html>