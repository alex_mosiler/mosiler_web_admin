<!-- 법인 현황 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 고객관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>    	
   	<script src="/js/util.js"></script>
   	
	<!-- 메뉴구현부  -->
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			var corporationStatusList = ${corporationStatusList};
			var cateList = ${cateList};
			var depthList = ${depthList};
			
			var cateMenuCode = 'M'+'01'+'0000';
			var depthMenuCode1 = 'M'+'01'+'03'+'00';
			var depthMenuCode2 = 'M010301';
		
			makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2);
			
			// 현재 페이지 번호 세팅
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			// 엔터키 이벤트 
		 	$("#searchKeyword").keypress(function(e) {
				if(e.which == 13) {
					search();
				}
			}); 

			// 리스트
			makeContents(corporationStatusList);
			// 페이징
			makePagingVar();
		});
		
		// 리스트
		function makeContents(corporationStatusList) {
			var html = "";
			var cntPerPage = ${pagingVO.cntPerPage};
			
			for(var i = 0; i < cntPerPage; i++) {
				if(corporationStatusList[i] == null) {
					break;
				} else {
					var useYn;
					var paymentMethod;
					
					if(corporationStatusList[i].useYn == "Y") {
						useYn = "사용";
					} else {
						useYn = "사용 안함";
					}
					
					if(corporationStatusList[i].paymentMethod == "1") {
						paymentMethod = "현금 결제";
					} else {
						paymentMethod = "카드 결제";
					}
					
					html += "<tr style='cursor: pointer' onclick=\"window.open('/customer/corporationStatusDetailpopUp?"
								 +"bizCompNo="+corporationStatusList[i].bizCompNo+"','','width=1000,height=700');return true\">";
					html += "	<td>"+corporationStatusList[i].rownum+"</td>"     
					html += "	<td>"+corporationStatusList[i].compName+"</td>";
					html += "	<td>"+corporationStatusList[i].bizNumber+"</td>"          
					html += "	<td>"+corporationStatusList[i].ceoName+"</td>";
					html += "	<td class='align-l'>"+corporationStatusList[i].compAddress+"</td>";
					html += "	<td>"+corporationStatusList[i].contractChargePart+"</td>";
					html += "	<td>"+corporationStatusList[i].contractChargePhoneNo+"</td>";
					html += "	<td>"+corporationStatusList[i].contractChargeName+"</td>";
					html += "	<td>"+corporationStatusList[i].contractChargeEmail+"</td>";
					html += "	<td>"+corporationStatusList[i].paymentChargePart+"</td>";
					html += "	<td>"+corporationStatusList[i].paymentChargePhoneNo+"</td>";
					html += "	<td>"+corporationStatusList[i].paymentChargeName+"</td>";
					html += "	<td>"+corporationStatusList[i].paymentChargeEmail+"</td>";
					html += "	<td>"+useYn+"</td>";
					html += "	<td>"+paymentMethod+"</td>";
					html += "	<td>"+corporationStatusList[i].paymentDay+"일</td>";
					html += "	<td>0건</td>";
					html += "	<td>"+corporationStatusList[i].regDt+"</td>";
					html += "</tr>";
				}
			}
			
			$("#listDiv").append(html);
		}

		// 페이징 바
		function makePagingVar() {
			var nowPage = ${pagingVO.nowPage};
			var startPage = ${pagingVO.startPage};
			var endPage = ${pagingVO.endPage};
			var lastPage = ${pagingVO.lastPage};
		
			var html = "";
			
			html += "<div class=\"col'\">";
			html += "	<div class=\"paging paging-basic\">";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == 1) {
				html += "		<button type=\"button\" class=\"btn first\" title=\"처음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn prev\" title=\"이전 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"firstPage\" class=\"btn first\" title=\"처음 페이지\"  onclick='arrowBtn(1);'><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" id=\"prevPage\" class=\"btn prev\" title=\"이전 페이지\" onclick='arrowBtn(2);'><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			}
			
			html += "		</div>";
			html += "		<ul class=\"num-group\">";
			
			for(var i = startPage; i <= endPage; i++) {
				if(i == nowPage) {
					html += "	<li><button type=\"button\" class=\"btn\" aria-current=\"true\" disabled=\"disabled\"><span>"+i+"</span></button></li>";
				} else {
					html += "	<li><button type=\"button\" id=\"e\" class=\"btn\" onclick='numberBtn("+i+");'><span>"+i+"</span></button></li>"
				}
			}
			
			html += "		</ul>";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == lastPage) {
				html += "		<button type=\"button\" class=\"btn next\" title=\"다음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-next gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn last\" title=\"마지막 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"nextPage\" class=\"btn next\" title=\"다음 페이지\" onclick='arrowBtn(3);'><span><i class=\"arw arw-board1-next gray\"></i></span></button>";			
				html += "		<button type=\"button\" id=\"lastPage\" class=\"btn last\" title=\"마지막 페이지\" onclick='arrowBtn(4);'><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			}
			
			html += "		</div>";		
			html += "	</div>";
			html += "</div>";	
			
			$("#pagingVar").append(html);
		}	
		
		// 검색
 		function search() {
			//submit
			document.corporationStatusForm.submit();
		} 

		// 검색 초기화
		function keywordReset() {
			$("#useYn").val("");
			$("#searchKeyword").val("");
		}
		
		// 한 페이지에 보여줄 row수 전환 
		function cntPerPageChange() {
			// 현재 페이지 번호 저장
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			// submit
			document.corporationStatusForm.submit(); 
		}

		// 페이징 번호 버튼
		function numberBtn(num) {
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(num);
			
			// submit
			document.corporationStatusForm.submit(); 
		}
		
		// 페이징 화살표 버튼
		function arrowBtn(num) {
			var nowPage = ${pagingVO.nowPage};
			
			if(num == 1) {
				// 처음
				nowPage = 1;
			} else if(num == 2) {
				// 이전
				nowPage = nowPage - 1;
			} else if(num == 3) {
				// 다음
				nowPage = nowPage + 1;
			} else {
				// 마지막
				nowPage = ${pagingVO.lastPage};
			}
			
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(nowPage);
			
			// submit
			document.corporationStatusForm.submit(); 
		}
	</script>   	
</head>

<body>
<form name="corporationStatusForm" id="corporationStatusForm" method="post" action="/customer/corporationStatus" onsubmit="return false">
	<div class="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">					
		<%@ include file="../header2.jsp" %>
			<!-- Content -->
			<div id="content" class="in-sec">
				<input type="hidden" name="nowPage">
				<section class="section">
					<div class="form-search grid flex-center cross-center">
						<ul>
							<li>
								<label class="form-tit">사용여부</label>
								<div class="data-group">
									<select name="useYn" id="useYn" class="select h30">
										<option value="">전체</option>
									    <option value="Y"  <c:if test="${customerVO.useYn eq 'Y'}">selected</c:if>>사용</option>
									    <option value="N" <c:if test="${customerVO.useYn eq 'N'}">selected</c:if>>사용안함</option>
									</select>
								</div>
							</li>
							<li>
								<label class="form-tit">검색</label>
								<div class="data-group">
									<select name="searchType" id="searchType" class="select h30">
									    <option value="type1">법인명</option>
									</select>									
									<input type="text" name="searchKeyword" id="searchKeyword" value="${customerVO.searchKeyword}" placeholder="" title="" class="input h30 w200">
								</div>
							</li>								
						</ul>
						<div class="Mmar-t10">
							<button type="button" class="btn-search" onclick="search();"><span>검색</span></button>
						</div>&emsp;
						<div class="Mmar-t10">
							<button type="button" class="btn-search" onclick="keywordReset();"><span id="searchButton">초기화</span></button>
						</div>		
					</div>
				</section>
				<!-- section -->
				<section class="section">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h2 class="h2">법인현황</h2>
						</div>
						<div class="col right">
							<button type="button" class="btn type1 primary" onclick="window.open('/customer/corporationRegistPopUp', '', 'width=1000, height=650')"><span>법인등록</span></button>
							<!-- <button type="button" class="btn type1 primary"><span>수정</span></button>
							<button type="button" class="btn type1 primary"><span>삭제</span></button> -->
							<button type="button" class="btn type1 secondary"><span>엑셀다운로드</span></button>
						</div>			
					</div>
					<div class="Mguide">
   						모바일에서 표를 좌우로 스크롤 할 수 있습니다. 
  					</div>
					<div class="gtbl-list l15">
						<table class="x-auto">
							<caption></caption>
							<thead>
								<tr>
									<th scope="col" rowspan="2">
										<!-- 
										<span class="check type1">
										    <input type="checkbox" name="sCheckall" id="sCheckall" value="">
										    <label for="sCheckall"><span class="blind">전체선택</span></label>
										</span>	
										 -->									
									</th>
									<!-- <th scope="col" rowspan="2">번호</th> -->
									<th scope="col" rowspan="2">법인명</th>
									<th scope="col" rowspan="2">사업자번호</th>
									<th scope="col" rowspan="2">대표자</th>
									<th scope="col" rowspan="2">주소</th>
									<th scope="col" colspan="4">계약담당</th>
									<th scope="col" colspan="4">결제담당</th>
									<th scope="col" rowspan="2">사용</th>
									<th scope="col" rowspan="2">결제수단</th>
									<th scope="col" rowspan="2">결제일</th>
									<th scope="col" rowspan="2">이용건수</th>
									<th scope="col" rowspan="2">등록일</th>
								</tr>
								<tr>
									<th scope="col" class="header">부서</th>
									<th scope="col" class="header">연락처</th>
									<th scope="col" class="header">이름</th>
									<th scope="col" class="header">이메일</th>
									<th scope="col" class="header">부서</th>
									<th scope="col" class="header">연락처</th>
									<th scope="col" class="header">이름</th>
									<th scope="col" class="header">이메일</th>
								</tr>								
							</thead>
							<tbody id="listDiv">
							</tbody>
						</table>
					</div>
					<div class="tbl-footer grid flex-between cross-center">
						<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
						<div id="pagingVar" class="col">
						</div>
						<div class="col per-page">
							<select name="cntPerPage" id="cntPerPage" class="select h30" onChange="cntPerPageChange()">
							    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
							    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
							    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
							    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
							    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
							    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
							</select>	
						</div>			
					</div>	
				</section>
				<!-- //section -->
			</div>	
			<!-- //Content -->
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/index'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/operation/realTimeControll'">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->		
	</div>
</form>
</body>
</html>