<!-- 고객 현황 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 고객관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>
   	<script src="/js/util.js"></script>  
   	  	
	<!-- 메뉴구현부  -->
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			var customerList = ${customerList};	
			var cateList = ${cateList};
			var depthList = ${depthList};
			
			var cateMenuCode = 'M'+'01'+'0000';
			var depthMenuCode1 = 'M'+'01'+'01'+'00';
			var depthMenuCode2 = 'M010101';
			
			makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2);

			// 현재 페이지 번호 세팅
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			// 엔터키 이벤트 
		 	$("#searchKeyword").keypress(function(e) {
				if(e.which == 13) {
					search();
				}
			}); 

			// 리스트
			makeContents(customerList);
			// 페이징
			makePagingVar();
		});

		// 리스트
		function makeContents(customerList) {
			var html = "";
			var cntPerPage = ${pagingVO.cntPerPage};
			
			for(var i = 0; i < cntPerPage; i++) {
				if(customerList[i] == null) {
					break;
				} else {
					var originPhoneNumber = customerList[i].phoneNumber;
					var parsePhoneNumber = originPhoneNumber.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
					
					html += "<tr style='cursor: pointer' onclick=\"window.open('/customer/customerDetailpopUp?"
								 +"customerNo="+customerList[i].customerNo+"','','width=1130,height=900');return true\">";
					html += "	<td>"+customerList[i].rownum+"</td>";
					html += "	<td>"+customerList[i].customerName+"</td>"          
					html += "	<td>"+parsePhoneNumber+"</td>";
					html += "	<td>"+customerList[i].email+"</td>";
					html += "	<td class='align-l'>"+customerList[i].homeAddress+"</td>";
					html += "	<td>0</td>";
					html += "	<td>"+customerList[i].count+"</td>";
					html += "	<td>"+customerList[i].endDate+"</td>";
					html += "	<td>"+customerList[i].regDt+"</td>";
					html += "</tr>";
				}
			}
			
			$("#listDiv").append(html);
		}

		// 페이징 바
		function makePagingVar() {
			var nowPage = ${pagingVO.nowPage};
			var startPage = ${pagingVO.startPage};
			var endPage = ${pagingVO.endPage};
			var lastPage = ${pagingVO.lastPage};
		
			var html = "";
			
			html += "<div class=\"col'\">";
			html += "	<div class=\"paging paging-basic\">";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == 1) {
				html += "		<button type=\"button\" class=\"btn first\" title=\"처음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn prev\" title=\"이전 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"firstPage\" class=\"btn first\" title=\"처음 페이지\"  onclick='arrowBtn(1);'><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" id=\"prevPage\" class=\"btn prev\" title=\"이전 페이지\" onclick='arrowBtn(2);'><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			}
			
			html += "		</div>";
			html += "		<ul class=\"num-group\">";
			
			for(var i = startPage; i <= endPage; i++) {
				if(i == nowPage) {
					html += "	<li><button type=\"button\" class=\"btn\" aria-current=\"true\" disabled=\"disabled\"><span>"+i+"</span></button></li>";
				} else {
					html += "	<li><button type=\"button\" id=\"e\" class=\"btn\" onclick='numberBtn("+i+");'><span>"+i+"</span></button></li>"
				}
			}
			
			html += "		</ul>";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == lastPage) {
				html += "		<button type=\"button\" class=\"btn next\" title=\"다음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-next gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn last\" title=\"마지막 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"nextPage\" class=\"btn next\" title=\"다음 페이지\" onclick='arrowBtn(3);'><span><i class=\"arw arw-board1-next gray\"></i></span></button>";			
				html += "		<button type=\"button\" id=\"lastPage\" class=\"btn last\" title=\"마지막 페이지\" onclick='arrowBtn(4);'><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			}
			
			html += "		</div>";		
			html += "	</div>";
			html += "</div>";	
			
			$("#pagingVar").append(html);
		}

		// 검색
 		function search() {
			//submit
			document.customerForm.submit();
		} 

		// 검색 초기화
		function keywordReset() {
			$("#searchKeyword").val("");
			$("#startDate").val("");
			$("#endDate").val("");
			$("#customerType option:eq(0)").prop("selected", true);
			$("#osType option:eq(0)").prop("selected", true);
			$("#coupon option:eq(0)").prop("selected", true);
			$("#searchType option:eq(0)").prop("selected", true);
		}
		
		// 한 페이지에 보여줄 row수 전환 
		function cntPerPageChange() {
			// 현재 페이지 번호 저장
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			// submit
			document.customerForm.submit(); 
		}

		// 페이징 번호 버튼
		function numberBtn(num) {
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(num);
			
			// submit
			document.customerForm.submit(); 
		}
		
		// 페이징 화살표 버튼
		function arrowBtn(num) {
			var nowPage = ${pagingVO.nowPage};
			
			if(num == 1) {
				// 처음
				nowPage = 1;
			} else if(num == 2) {
				// 이전
				nowPage = nowPage - 1;
			} else if(num == 3) {
				// 다음
				nowPage = nowPage + 1;
			} else {
				// 마지막
				nowPage = ${pagingVO.lastPage};
			}
			
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(nowPage);
			
			// submit
			document.customerForm.submit(); 
		}
	</script>   	
</head>

<body>
<form name="customerForm" id="customerForm" method="post" action="/customer/customerList" onsubmit="return false">
	<div class="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">					
			<%@ include file="../header2.jsp" %>
			<!-- Content -->
			<div id="content" class="in-sec">
				<section class="section">
					<input type="hidden" name="nowPage">
					<div class="form-search grid flex-center cross-center">
						<ul>
							<li>
								<label class="form-tit">고객 유형</label>
								<div class="data-group">
									<select name="customerType" id="customerType" class="select h30">
									    <option value="">전체</option>
									    <option value="1" <c:if test="${customerVO.customerType eq '1'}">selected</c:if>>개인</option>
									    <option value="2" <c:if test="${customerVO.customerType eq '2'}">selected</c:if>>법인</option>
									</select>									
								</div>
							</li>
							<li>
								<label class="form-tit">OS 타입</label>
								<div class="data-group">
									<select name="osType" id="osType" class="select h30">
									    <option value="">전체</option>
									     <option value="A" <c:if test="${customerVO.osType eq 'A'}">selected</c:if>>Android</option>
									      <option value="I" <c:if test="${customerVO.osType eq 'I'}">selected</c:if>>iOS</option>
									</select>
								</div>
							</li>	 
							<li>
								<label class="form-tit">쿠폰(미개발)</label>
								<div class="data-group">
									<select name="sSelect1" id="coupon" class="select h30">
									    <option value="">사용</option>
									    <option value="">사용안함</option>
									</select>
								</div>
							</li>																					
							<li>
								<label class="form-tit">가입일</label>
								<div class="data-group">
									<input type="date" name="startDate" id="startDate" value="${customerVO.startDate }" placeholder="" title="" class="input h30 input-date"><span class="form-split">-
									</span><input type="date" name="endDate" id="endDate" value="${customerVO.endDate }" placeholder="" title="" class="input h30 input-date">
								</div>
							</li>
							<li>
								<label class="form-tit">검색</label>
								<div class="data-group">
									<select name="searchType" id="searchType" class="select h30">
									    <option value="customerName" 	<c:if test="${customerVO.searchType eq 'customerName'}">selected</c:if>>고객명</option>
									    <option value="phoneNumber" 	<c:if test="${customerVO.searchType eq 'phoneNumber'}">selected</c:if>>휴대폰번호</option>
									    <option value="email" 				<c:if test="${customerVO.searchType eq 'email'}">selected</c:if>>이메일</option>
									</select>									
									<input type="text" name="searchKeyword" id="searchKeyword" value="${customerVO.searchKeyword}" placeholder="고객명/휴대폰번호/이메일" title="" class="input h30 w200">
								</div>
							</li>									
						</ul>
						<div class="Mmar-t10">
							<button type="button" class="btn-search" onclick="search();"><span id="searchButton">검색</span></button>
						</div>&emsp;
						<div class="Mmar-t10">
							<button type="button" class="btn-search" onclick="keywordReset();"><span id="searchButton">초기화</span></button>
						</div>			
					</div>
				</section>
				<!-- section -->
				<section class="section">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h2 class="h2">고객회원현황</h2>
						</div>
						<div class="col mar-l10">
							<i class="ico ico-user vip">VIP</i>VIP
							<i class="ico ico-user green">정액권사용자</i>정액권사용자
						</div>
						<div class="col right">
							<button type="button" class="btn type1 primary"><span>엑셀다운로드</span></button>
						</div>			
					</div>
					<div class="Mguide">
   						모바일에서 표를 좌우로 스크롤 할 수 있습니다. 
  					</div>
					<div class="gtbl-list l15">
						<table class="x-auto">
							<caption></caption>
							<thead>
								<tr>
									<th scope="col">번호</th>
									<th scope="col">고객명</th>
									<th scope="col">휴대폰번호</th>
									<th scope="col">이메일</th>
									<th scope="col">지역</th>
									<th scope="col">정액권(분)</th>
									<th scope="col">이용건수(건)</th>
									<th scope="col">최종운행일</th>
									<th scope="col">가입일</th>
								</tr>
							</thead>
							<tbody id="listDiv">
							</tbody>	
						</table>
					</div>
					<div class="tbl-footer grid flex-between cross-center">
						<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
						<div id="pagingVar" class="col">
						</div>
						<div class="col per-page">
							<select name="cntPerPage" id="cntPerPage" class="select h30" onChange="cntPerPageChange()">
							    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
							    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
							    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
							    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
							    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
							    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
							</select>	
						</div>			
					</div>		
				</section>
				<!-- //section -->
			</div>	
			<!-- //Content -->
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/index'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/operation/realTimeControll'">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->			
	</div>
</form>
</body>
</html>