<!-- 프로모션 등록 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 고객관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		var id = "${id}";
		
		if(id == "") {
			location.href = "/";
		} else {
			$("#id").val(id);
		}
	});	

	// 프로모션 등록
	function insertPromotion() {
		var promotionCode	= $("#promotionCode").val();
		var type					= $("#type").val();
		var userTargetType	= $("#userTargetType").val();
		var couponQty			= $("#couponQty").val();
		var discountValue;
		var startDt				= $("#startDate").val();
		var endDt				= $("#endDate").val();
		var status				= $("#promotionStatus").val();
		var descriptionString = $("textarea#description").val();
		var isRate				= $('input[name="isRate"]:checked').val();
		
		if(isRate == 1) {
			discountValue = $("#discountValue").val();

			if(discountValue > 100 || discountValue < 0) {
				alert("할인율을 확인해 주세요.");
				return false;
			}
		} else {
			discountValue = $("#discountValue").val();
		}
		
		if(promotionCode == "") {
			alert("규칙명을 입력해 주세요.");
			return false;
		} 
		if(type == "") {
			alert("프로모션 유형을 선택해 주세요.");
			return false;
		}
		if(userTargetType == "") {
			alert("대상을 선택해 주세요.");
			return false;
		}
		if(couponQty == "" && type == 2) {
			alert("프로모션 유형이 Manual인 경우 쿠폰 수량을 입력해야 합니다.");
			return false;
		}
		if(discountValue == "") {
			alert("할인 수치를 입력해 주세요.");
			return false;
		}
		if(startDt == "") {
			alert("시작일을 입력해 주세요.");
			return false;
		}
		if(endDt == "") {
			alert("종료일을 입력해 주세요.");
			return false;
		}
		if(status == "") {
			alert("상태를 선택해 주세요.");
			return false;
		}
		
		if(couponQty == "") {
			couponQty = 0;	
		}
		
		$.ajax({
			url: "/customer/insertPromotion"
			, type: "POST"
			, data: {
				promotionCode: promotionCode
				, type: type
				, userTargetType: userTargetType
				, couponQty: couponQty
				, discountValue: discountValue
				, startDt: startDt+" 00:00:00.000"
				, endDt: endDt+" 00:00:00.000"
				, status: status
				, descriptionString: descriptionString
				, isRate: isRate
			}
			, success: function(data) {
				if(data == 200) {
					alert("프로모션이 등록 되었습니다.");
					
					self.close();
				} else {
					alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
				}

				opener.location.reload();
				self.close();
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
				self.close();
				
				console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			}
		}); 				
	}
	
	// 숫자만 입력 가능
	function setNum(obj) {
		val = obj.value;
		re = /[^0-9]/gi;
		
		obj.value = val.replace(re, "");
	}
	
	// 닫기 버튼
	function selfClose() {
		self.close();
	}
</script>
<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">프로모션 등록</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<section class="section">
					<div class="tbl-type1 tbl-lefted">
						<table>
							<colgroup>
								<col width="25%">
								<col width="13%">
								<col width="*">
								<col width="13%">
								<col width="30%">
							</colgroup>
							<tbody>
								<tr>
									<th scope="row" >규칙명</th>
									<td colspan="4">
										<div class="form-controls flexible">
											<input type="text" name="promotionCode" id="promotionCode" value="" placeholder="" class="input type1 h25">
										</div>	
									</td>								
								</tr>
								<tr>
									<th scope="row" >프로모션 유형</th>
									<td colspan="4">
										<select name="type" id="type" class="select h25" style="width:100%;">
											<option value="">선택</option>
											<c:forEach items="${promotionTypeList}" var="list" varStatus="status">
												<option value="${list.code}">${list.codeName}</option>
											</c:forEach>
										</select>																		
									</td>								
								</tr>
								<tr>
									<th scope="row">대상</th>
									<td colspan="4">
										<select name="userTargetType" id="userTargetType" class="select h25" style="width:100%;">
											<option value="">선택</option>
											<c:forEach items="${userTargetTypeList}" var="list" varStatus="status">
												<option value="${list.code}">${list.codeName}</option>
											</c:forEach>
										</select>				
									</td>
								</tr>									
								<tr>
									<th scope="row">쿠폰 수량</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="couponQty" id="couponQty" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
								</tr>	
								<tr>
									<th scope="row" >할인</th>
									<td colspan="4">
										<input type="radio" name="isRate" value="1" checked>율
										<input type="radio" name="isRate" value="0" >양
										&nbsp;
										<input type="text" id="discountValue" name="discountValue" onkeyup="setNum(this);" placeholder="할인 수치 입력" class="input type1 h25" style="width:96px;">
									</td>
								</tr>	
								<tr>
									<th scope="row">시작일</th>
									<td>
										<input type="date" name="startDate" id="startDate" value="" placeholder="" title="" class="input h30 input-date">
									</td>												
								</tr>		
								<tr>
									<th scope="row">종료일</th>
									<td>
										<input type="date" name="endDate" id="endDate" value="" placeholder="" title="" class="input h30 input-date">
									</td>	
								</tr>		
								<tr>
									<th scope="row">상태</th>
									<td colspan="4">
										<select name="promotionStatus" id="promotionStatus" class="select h25" style="width:100%;">
											<option value="">선택</option>
											<c:forEach items="${promotionStatusList}" var="list" varStatus="status">
												<option value="${list.code}">${list.codeName}</option>
											</c:forEach>
										</select>				
									</td>
								</tr>	
								<tr>
									<th scope="row">설명</th>
									<td colspan="4">
										<textarea cols="20" id="description" name="description" rows="2" class="input type1" style="width: 100%"></textarea>			
									</td>
								</tr>																																		
							</tbody>
						</table>
					</div>
				</section>																
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" onclick="insertPromotion();" class="btn type3 primary"><span>등록</span></button>
				<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</body>
</html>