<!-- 서비스 신청자 상세 팝업 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 고객관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		var id = "${id}";
		
		if(id == "") {
			location.href = "/";
		}
	});

	// 상담 내용 Update
	function updateApplyServiceDetail() {
		var id 					= $("#id").val();
		var userName 			= $("#userName").val();
		var availTime 			= $("#availTime").val();
		var phoneNumber 	= $("#phoneNumber").val();
		var status 				= $("#status").val();
		var email 				= $("#email").val();
		var comment 			= $("textarea#comment").val();

		$.ajax({
			url: "/customer/updateApplyServiceDetail"
			, type: "POST"
			, data: {
				id: id
				, userName: userName 
				, availTime: availTime 
				, phoneNumber: phoneNumber
				, status: status
				, email: email
				, comment: comment
			}
			, success: function(data) {
				if(data == 200) {
					alert("상담 내역이 수정 되었습니다.");
					
					self.close();
				} else {
					alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
				}
	
				opener.location.reload();
				self.close();
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
				self.close();
				
				console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			}
		}); 
	}
	
	// 닫기 버튼
	function selfClose() {
	self.close();
	}
</script>
<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">전화예약 상담현황</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<section class="section">
					<div class="tbl-type1 tbl-lefted">
						<table>
							<colgroup>
								<col width="15%">
								<col width="*">
								<col width="15%">
								<col width="35%">
							</colgroup>
							<tbody>
							<c:forEach var="applyServiceDetail" items="${applyServiceDetail}" varStatus="idx">
								<input type="hidden" id="id" value="${applyServiceDetail.id}">
								<tr>
									<th scope="row" class="required">고객명</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="userName" id="userName" value="${applyServiceDetail.userName}" placeholder="" class="input type1 h25">
										</div>
									</td>
									<th scope="row">통화가능시간</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="availTime" id="availTime" value="${applyServiceDetail.availTime}" placeholder="" class="input type1 h25">
										</div>
									</td>										
								</tr>
								<tr>
									<th scope="row" class="required">전화번호</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="phoneNumber" id="phoneNumber" value="${applyServiceDetail.phoneNumber}" placeholder="" class="input type1 h25">
										</div>
									</td>
									<th scope="row">상태</th>
									<td>
										<select name="status" id="status" class="select type1 h25">
										    <option value="wait"		<c:if test="${applyServiceDetail.status eq 'wait'}">selected</c:if>>상담대기</option>
										    <option value="complete" <c:if test="${applyServiceDetail.status eq 'complete'}">selected</c:if>>상담완료</option>
										    <option value="hold"		<c:if test="${applyServiceDetail.status eq 'hold'}">selected</c:if>>보류</option>
										    <option value="cancel" 		<c:if test="${applyServiceDetail.status eq 'cancel'}">selected</c:if>>취소</option>
										</select>
									</td>										
								</tr>
								<tr>
									<th scope="row">이메일주소</th>
									<td colspan="3">
										<div class="form-controls flexible">
											<input type="text" name="email" id="email" value="${applyServiceDetail.email}" placeholder="" class="input type1 h25">
										</div>
									</td>									
								</tr>
								<tr>
									<th scope="row">상담내용</th>
									<td colspan="3">
										<div class="form-controls flexible">
											<textarea rows="3" cols="50" class="input type1" id="comment" value="${applyServiceDetail.comment}">${applyServiceDetail.comment}</textarea>
										</div>
									</td>
								</tr>		
							</c:forEach>																																						
							</tbody>
						</table>
					</div>
				</section>
				<section class="section mar-t10">
					<div class="grid">						
						<div class="col right">
							<button type="button" onclick="updateApplyServiceDetail();" class="btn type1 primary"><span>저장</span></button>
							<button type="button" class="btn type1 secondary"><span>사용자전환</span></button>
						</div>
					</div>
				</section>
				<section class="section">
					<div class="tbl-type1 tbl-lefted">
						<table>
							<colgroup>
								<col width="15%">
								<col width="*">
								<col width="15%">
								<col width="35%">
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">고객유형</th>
									<td>
										<div class="form-controls flexible">
											<select name="" id="" class="select type1 h25 mar-r5">
											    <option value="">개인</option>
											    <option value="">법인</option>
											</select>
											<select name="" id="" class="select type1 h25" disabled="">
				                                <option value="">선택</option>
				                                <option value="">Master</option>
				                                <option value="">Sub</option>
				                            </select>
			                            </div>								
									</td>
									<th scope="row">법인</th>
									<td>
										<div class="form-controls flexible">
											<select name="" id="" class="select type1 h25" disabled="">
				                                <option value="">선택</option>
			                                    <option value="">주식회사 더프라자</option>
			                                    <option value="">주식회사 카카오브이엑스</option>
			                                    <option value="">(주) 그린웍스</option>
			                                    <option value="">코오롱글로벌(주) 강남지점</option>
			                                    <option value="">(주)다우바이오메디카</option>
			                                    <option value="">(주)테스트</option>
			                                    <option value="">하이메디</option>
			                                    <option value="">(주)카이오</option>
			                                    <option value="">롯데미래전략연구소 주식회사</option>
			                                    <option value="">(주)마지막삼십분</option>
			                                    <option value="">(주)브이씨엔씨</option>
				                            </select>
			                            </div>
									</td>										
								</tr>
								<tr>
									<th scope="row">계정유형</th>
									<td>N/A</td>
									<th scope="row">기업유형</th>
									<td>기업</td>										
								</tr>
								<tr>
									<th scope="row" class="required">고객명</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="" id="" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
									<th scope="row" class="required">전화번호</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="" id="" value="" placeholder="- 제외하고 입력" class="input type1 h25">
										</div>
									</td>																	
								</tr>
								<tr>
									<th scope="row">ID</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="" id="" value="" placeholder="" class="input type1 h25 mar-r5">
											<button class="btn type4 primary"><span>중복확인</span></button>
										</div>
									</td>
									<th scope="row">이메일 주소</th>
									<td>
										<div class="form-controls flexible">
											<input type="" name="" id="" value="" placeholder="" class="input type1 h25">
										</div>
									</td>																	
								</tr>
								<tr>
									<th scope="row">비밀번호</th>
									<td>
										<div class="form-controls flexible">
											<input type="password" name="" id="" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
									<th scope="row">비밀번호 확인</th>
									<td>
										<div class="form-controls flexible">
											<input type="password" name="" id="" value="" placeholder="" class="input type1 h25">
										</div>
									</td>																	
								</tr>							
								<tr>
									<th scope="row">보유차랑</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="" id="" value="" placeholder="차량명" class="input type1 h25 mar-r5">
											<input type="text" name="" id="" value="" placeholder="차량번호" class="input type1 h25 W45">
										</div>								
									</td>
									<th scope="row">적용단가</th>
									<td>
										<select name="" id="" class="select type1 h25">
		                                    <option value="20000">20,000 원</option>
		                                    <option value="18000">18,000 원</option>
		                                    <option value="16500">16,500 원</option>
		                                    <option value="16000">16,000 원</option>
		                                    <option value="15400">15,400 원</option>
		                                    <option value="14300">14,300 원</option>
		                                    <option value="13200">13,200 원</option>
		                                    <option value="12500">12,500 원</option>
		                                    <option value="12100">12,100 원</option>
		                                    <option value="11000">11,000 원</option>
		                                    <option value="10000">10,000 원</option>
			                            </select>
									</td>																	
								</tr>
								<tr>
									<th scope="row">집주소</th>
									<td colspan="3">
										<div class="row">
											<input type="text" name="sAddress" id="sAddress1" value="" placeholder="" title="우편번호" class="input type1 h25 w100">
											<button type="button" class="btn type4"><span>우편번호</span></button>
										</div>
										<div class="row">
											<input type="text" name="sAddress" id="sAddress2" value="" placeholder="" title="기본주소" class="input type1 h25 W100">
										</div>
										<div class="row">
											<input type="text" name="sAddress" id="sAddress3" value="" placeholder="" title="상세주소" class="input type1 h25 W100">
										</div>									
									</td>
								</tr>	
								<tr>
									<th scope="row">비고</th>
									<td colspan="3">
										<div class="form-controls flexible">
											<textarea rows="3" cols="50" class="input type1"></textarea>
										</div>
									</td>
								</tr>																																																											
							</tbody>
						</table>
					</div>
				</section>																			
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" onclick="selfClose();" class="btn type3 primary"><span>확인</span></button>
				<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</body>
</html>