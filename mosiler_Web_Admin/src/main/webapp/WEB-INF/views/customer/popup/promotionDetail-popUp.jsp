<!-- 프로모션 상세 팝업 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 고객관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>

  	<script type="text/javascript">
 		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			 
			var promotionRule = ${promotionRule};	
			var promotionCoupon = ${promotionCoupon};
			var promotionId = "${customerVO.id}"
			var tabId = ${tabId};
			
			// 현재 페이지 번호 세팅
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			$("#promotionId").val(promotionId);
			
			// 규칙 내역 리스트
			makeContents(promotionRule);
			
			// 쿠폰 내역 리스트
			makeContents1(promotionCoupon);
			makePagingVar();
		}); 

		// 규칙 내역 리스트
		function makeContents(promotionRule) {
			var html = "";

			for(var i = 0; i < promotionRule.length; i++) {
				if(promotionRule[i] == null) {
					break;
				} else {
					html += "<tr style='cursor: pointer' onclick=\"window.open('/customer/ruleDetailPopUp?"
						 		+"id="+promotionRule[i].id+"','','width=500,height=375');return true\">";
					html += "	<td>"+promotionRule[i].rownum+"</td>";
					html += "	<td>"+promotionRule[i].couponName+"</td>"          
					html += "	<td>"+promotionRule[i].promotionCode+"</td>";
					html += "	<td>"+promotionRule[i].conditionQty+"</td>";
					html += "	<td>"+promotionRule[i].codeName+"</td>";
					html += "	<td>"+promotionRule[i].expiredDt+"</td>";
					html += "	<td>"+promotionRule[i].regDt+"</td>";
					html += "</tr>"; 
				}
			}
			
			$("#promotionRuleDiv").append(html);
		}

		// 쿠폰 내역 리스트
		function makeContents1(promotionCoupon) {
			var html = "";
			var cntPerPage = "${pagingVO.cntPerPage}";

			for(var i = 0; i < cntPerPage; i++) {
				if(promotionCoupon[i] == null) {
					break;
				} else {
					var codeName;
					var customerName;
					var startDt;
					var endDt;
					var acctualDiscountAmount;
					
					if(promotionCoupon[i].codeName == null) {
						codeName = ""
					} else {
						codeName = promotionCoupon[i].codeName;
					}
					
					if(promotionCoupon[i].customerName == null) {
						customerName = "";
					} else {
						customerName = promotionCoupon[i].customerName
					}
					
					if(promotionCoupon[i].startDt == null) {
						startDt = "";
					} else {
						startDt = promotionCoupon[i].startDt;
					}

					if(promotionCoupon[i].endDt == null) {
						endDt = ""
					} else {
						endDt = promotionCoupon[i].endDt;
					}

					if(promotionCoupon[i].acctualDiscountAmount == null) {
						acctualDiscountAmount = "";
					} else {
						acctualDiscountAmount = promotionCoupon[i].acctualDiscountAmount;
					}
					
					html += "<tr style='cursor: pointer' onclick=\"window.open('/customer/couponDetailPopUp?"
				 				+"id="+promotionCoupon[i].id+"','','width=500,height=375');return true\">";
					html += "	<td>"+promotionCoupon[i].rownum+"</td>";
					html += "	<td>"+promotionCoupon[i].redeemCode+"</td>"          
					html += "	<td>"+codeName+"</td>";
					html += "	<td>"+customerName+"</td>";
					html += "	<td>"+startDt+"</td>";
					html += "	<td>"+endDt+"</td>";
					html += "	<td>"+promotionCoupon[i].discountAmount+"</td>";
					html += "	<td>"+acctualDiscountAmount+"</td>";
					html += "	<td>"+promotionCoupon[i].regDt+"</td>";
					html += "</tr>"; 
				}
			}
			
			$("#promotionCouponDiv").append(html);
		}
		
		// 쿠폰 내역 페이징 바
		function makePagingVar() {
			var nowPage = ${pagingVO.nowPage};
			var startPage = ${pagingVO.startPage};
			var endPage = ${pagingVO.endPage};
			var lastPage = ${pagingVO.lastPage};
		
			var html = "";
			
			html += "<div class=\"col'\">";
			html += "	<div class=\"paging paging-basic\">";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == 1) {
				html += "		<button type=\"button\" class=\"btn first\" title=\"처음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn prev\" title=\"이전 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"firstPage\" class=\"btn first\" title=\"처음 페이지\"  onclick='arrowBtn(1);'><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" id=\"prevPage\" class=\"btn prev\" title=\"이전 페이지\" onclick='arrowBtn(2);'><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			}
			
			html += "		</div>";
			html += "		<ul class=\"num-group\">";
			
			for(var i = startPage; i <= endPage; i++) {
				if(i == nowPage) {
					html += "	<li><button type=\"button\" class=\"btn\" aria-current=\"true\" disabled=\"disabled\"><span>"+i+"</span></button></li>";
				} else {
					html += "	<li><button type=\"button\" id=\"e\" class=\"btn\" onclick='numberBtn("+i+");'><span>"+i+"</span></button></li>"
				}
			}
			
			html += "		</ul>";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == lastPage) {
				html += "		<button type=\"button\" class=\"btn next\" title=\"다음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-next gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn last\" title=\"마지막 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"nextPage\" class=\"btn next\" title=\"다음 페이지\" onclick='arrowBtn(3);'><span><i class=\"arw arw-board1-next gray\"></i></span></button>";			
				html += "		<button type=\"button\" id=\"lastPage\" class=\"btn last\" title=\"마지막 페이지\" onclick='arrowBtn(4);'><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			}
			
			html += "		</div>";		
			html += "	</div>";
			html += "</div>";	
			
			$("#pagingVar").append(html);
		}

		// 한 페이지에 보여줄 row수 전환 
		function cntPerPageChange() {
			// tabId 저장
			$("#tabId").val(1);
			
			// 현재 페이지 번호 저장
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			// submit
			document.promotionDetailForm.submit(); 
		}

		// 페이징 번호 버튼
		function numberBtn(num) {
			// tabId 저장
			$("#tabId").val(1);
			
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(num);
			
			// submit
			document.promotionDetailForm.submit(); 
		}
		
		// 페이징 화살표 버튼
		function arrowBtn(num) {
			// tabId 저장
			$("#tabId").val(1);
			
			var nowPage = ${pagingVO.nowPage};
			
			if(num == 1) {
				// 처음
				nowPage = 1;
			} else if(num == 2) {
				// 이전
				nowPage = nowPage - 1;
			} else if(num == 3) {
				// 다음
				nowPage = nowPage + 1;
			} else {
				// 마지막
				nowPage = ${pagingVO.lastPage};
			}
			
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(nowPage);
			
			// submit
			document.promotionDetailForm.submit(); 
		}
		
		// 프로모션 삭제
		function cancelPromotion() {
			var promotionNo = $("#promotionNo").val();
			
			if(confirm("해당 프로모션을 삭제 하시겠습니까?")) {
				$.ajax({
					url: "/customer/cancelPromotion"
					, type: "POST"
					, async: false
					, data: {
						promotionNo: promotionNo
					}
					, success: function(data) {
						if(data == 200) {
							alert("프로모션이 삭제 되었습니다.");
							
							self.close();
						} else {
							alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
						}

						opener.location.reload();
						self.close();
					}
					, error: function(data, status, opt) {
						alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
						self.close();
						
						console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
				    }
				}); 
			} 
		}
		
		// 프로모션 수정
		function updatePromotion() {
			var promotionNo 	= $("#promotionNo").val();
			var promotionCode 	= $("#promotionCode").val();
			var type 				= $("#type").val();
			var userTargetType 	= $("#userTargetType").val();
			var startDt				= $("#startDate").val();
			var endDt				= $("#endDate").val();
			var couponQty 		= $("#couponQty").val();
			var status 				= $("#status").val();
			var descriptionString = $("#description").val();
			//var discountAmount = $("#discountAmount").val();
			var isRate				= $('input[name="isRate"]:checked').val();
			var discountValue;

			if(isRate == 1) {
				discountValue = $("#discountValue").val();

				if(discountValue > 100 || discountValue < 0) {
					alert("할인율을 확인해 주세요.");
					return false;
				}
			} else {
				discountValue = $("#discountValue").val();
			}
			
			if(couponQty == "") {
				couponQty = 0;	
			}
			
			$.ajax({
				url: "/customer/updatePromotion"
				, type: "POST"
				, data: {
					promotionNo: promotionNo
					, promotionCode: promotionCode
					, type: type
					, userTargetType: userTargetType
					, startDt: startDt+" 00:00:00.000"
					, endDt: endDt+" 00:00:00.000"
					, couponQty: couponQty
					, status: status
					//, discountAmount: discountAmount
					, descriptionString: descriptionString
					, discountValue: discountValue
					, isRate: isRate
				}
				, success: function(data) {
					if(data == 200) {
						alert("프로모션이 수정 되었습니다.");
						
						self.close();
					} else {
						alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
					}

					opener.location.reload();
					self.close();
				}
				, error: function(data, status, opt) {
					alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
					self.close();
					
					console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
				}
			}); 					
		}
		
		// 숫자만 입력 가능
		function setNum(obj) {
			val = obj.value;
			re = /[^0-9]/gi;
			
			obj.value = val.replace(re, "");
		}
		
		// 닫기 버튼
 		function selfClose() {
			self.close();
		}
	</script>  
</head>

<body>
<form name="promotionDetailForm" id="promotionDetailForm" method="post" action="/customer/promotionDetailPopUp" onsubmit="return false">
<input type="hidden" id="tabId" name="tabId">
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">프로모션 상세</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<section class="section">
					<input type="hidden" id="promotionId" name="promotionId">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h3 class="h3">프로모션 정보</h3>
						</div>	
					</div>
					<div class="tbl-type1 tbl-lefted">
						<table>
							<colgroup>
								<col width="15%">
								<col width="*">
								<col width="15%">
								<col width="35%">
							</colgroup>
							<tbody>
								<c:forEach var="promotionDetail" items="${promotionDetail}" varStatus="idx">
									<input type="hidden" id="promotionNo" value="${promotionDetail.promotionNo}">
									<tr>
										<th scope="row">규칙명</th>
										<td>
											<input type="text" id="promotionCode" name="promotionCode" class="input h25" value="${promotionDetail.promotionCode}" style="width:350px;">
										</td>
										<th scope="row">프로모션 유형</th>
										<td>
											<select name="type" id="type" class="select h30" style="width:350px;">
												<option value="1"  <c:if test="${promotionDetail.typeName eq 'Auto'}">selected</c:if>>Auto</option>
											    <option value="2" <c:if test="${promotionDetail.typeName eq 'Manual'}">selected</c:if>>Manual</option>
											</select>
										</td>										
									</tr>
									<tr>
										<th scope="row">대상</th>
										<td>
											<select name="userTargetType" id="userTargetType" class="select h30" style="width:350px;">
												<option value="1"  <c:if test="${promotionDetail.userTargetTypeName eq 'Customer'}">selected</c:if>>Customer</option>
											    <option value="2" <c:if test="${promotionDetail.userTargetTypeName eq 'Driver'}">selected</c:if>>Driver</option>
											</select>
										</td>
										<th scope="row">쿠폰 수량</th>
										<td>
											<input type="text" id="couponQty" onkeyup="setNum(this);" class="input h25" value="${promotionDetail.couponQty}" style="width:350px;">
										</td>	<!-- name="couponQty" -->		
									</tr>											
									<tr>
										<th scope="row">시작일</th>
										<td>
											<input type="date" name="startDate" id="startDate" value="${promotionDetail.startDt}" placeholder="" title="" class="input h30 input-date" style="width:350px;">
										</td>												
										<th scope="row">종료일</th>
										<td>
											<input type="date" name="endDate" id="endDate" value="${promotionDetail.endDt}" placeholder="" title="" class="input h30 input-date" style="width:350px;">
										</td>									
									</tr>
									<tr>
										<th scope="row">상태</th>
										<td>
											<select name="status" id="status" class="select h30" style="width:350px;">
												<option value="1"  <c:if test="${promotionDetail.statusName eq 'New'}">selected</c:if>>New</option>
											    <option value="2" <c:if test="${promotionDetail.statusName eq 'Run'}">selected</c:if>>Run</option>
											    <option value="3" <c:if test="${promotionDetail.statusName eq 'End'}">selected</c:if>>End</option>
											</select>
										</td>
										<th scope="row">할인</th>
										<td>
											<input type="radio" name="isRate" value="1" <c:if test="${promotionDetail.isRate eq true}">checked</c:if>>율
											<input type="radio" name="isRate" value="0" <c:if test="${promotionDetail.isRate eq false}">checked</c:if>>양
											&nbsp;
											<c:if test="${promotionDetail.isRate eq true}">
												<input type="text" id="discountValue" name="discountValue" onkeyup="setNum(this);" class="input h25" value="${promotionDetail.discountRate}" style="width:280px;">
											</c:if>
											<c:if test="${promotionDetail.isRate eq false}">
												<input type="text" id="discountValue" name="discountValue" onkeyup="setNum(this);" class="input h25" value="${promotionDetail.discountAmount}" style="width:280px;">
											</c:if>
										</td>						
									</tr>
									<tr>
										<th scope="row">발급된 쿠폰 수</th>
										<td>${promotionDetail.releasedCoupon}</td>
										<th scope="row">사용된 쿠폰 수</th>
										<td>${promotionDetail.usedCoupon}</td>		
									</tr>
									<tr>
										<th scope="row">설명</th>
										<td colspan="3">
											<input type="text" id="description" name="description" class="input h25" value="${promotionDetail.description}" style="width:890px;">
										</td>
									</tr>										
								</c:forEach>								
							</tbody>
						</table>
					</div>
				</section>
				<section class="section mar-t10">
					<div class="grid">						
						<div class="col right">
							<button type="button" onclick="updatePromotion();" class="btn type1 primary"><span>수정</span></button>
							<button type="button" onclick="cancelPromotion();" class="btn type1 primary" style="background-color:red;"><span>삭제</span></button>
							<!-- <button type="button" onclick="selfClose();" class="btn type1 secondary"><span>닫기</span></button> -->
						</div>
					</div>
				</section>					
				
				<section class="section">
					<!-- S: Tab Area -->
					<div class="tab-nav type1">
					    <ul class="tab-list" role="tablist">
					    	<c:if test="${tabId eq 0 }">
					    		<li id="tabNav11" role="tab" aria-controls="tabContent11" aria-selected="true" class="tab is-selected">
					        		<button type="button" class="btn"><span>규칙내역</span></button>
					        	</li>
					    	</c:if>
					    	<c:if test="${tabId eq 1 }">
					    		<li id="tabNav11" role="tab" aria-controls="tabContent11" aria-selected="false" class="tab">
					        		<button type="button" class="btn"><span>규칙내역</span></button>
					        	</li>
					    	</c:if>
					    	<c:if test="${tabId eq 0 }">
					    		<li id="tabNav12" role="tab" aria-controls="tabContent12" aria-selected="false" class="tab">
						        	<button type="button" class="btn" id="couponList"><span>쿠폰내역</span></button>
						        </li>
					    	</c:if>
					    	<c:if test="${tabId eq 1 }">
								<li id="tabNav12" role="tab" aria-controls="tabContent12"aria-selected="true" class="tab is-selected">
						        	<button type="button" class="btn" id="couponList"><span>쿠폰내역</span></button>
						        </li>
					    	</c:if>
					    	<li style="margin-left:700px;">
					        	<button type="button" class="btn type1 primary" onclick="window.open('/customer/ruleRegistPopUp?promotionId=${promotionId}', '', 'width=500, height=375')"><span>규칙 추가</span></button>
					        </li>
					        &nbsp; 
					        <li>
					        	<button type="button" class="btn type1 primary" onclick="window.open('/customer/couponRegistPopUp?promotionId=${promotionId}', '', 'width=500, height=375')"><span>쿠폰 추가</span></button>
					        </li>
					    </ul>
					</div>
					<div class="tab-container">
						<c:if test="${tabId eq 0 }">
							<div id="tabContent11" aria-labelledby="tabNav11" aria-hidden="false" class="tab-content is-visible">			    
								<div class="gtbl-list l15">
									<table>
										<caption>프로모션</caption>
										<thead>
											<tr>
												<th>번호</th>
												<th>이름</th>
												<th>규칙명</th>
												<th>조건 수량</th>
												<th>조건 유형</th>
												<th>쿠폰 만료</th>
												<th>등록일</th>
											</tr>
										</thead>
										<tbody id="promotionRuleDiv">																																																																																																																																																																																																																																																														
										</tbody>	
									</table>
								</div>
						    </div>
						</c:if>
						<c:if test="${tabId eq 1 }">
							<div id="tabContent11" aria-labelledby="tabNav11" aria-hidden="true" class="tab-content">			    
								<div class="gtbl-list l15">
									<table>
										<caption>프로모션</caption>
										<thead>
											<tr>
												<th>번호</th>
												<th>이름</th>
												<th>규칙명</th>
												<th>조건 수량</th>
												<th>조건 유형</th>
												<th>쿠폰 만료</th>
												<th>등록일</th>
											</tr>
										</thead>
										<tbody id="promotionRuleDiv">																																																																																																																																																																																																																																																														
										</tbody>	
									</table>
								</div>
						    </div>
					     </c:if>
					    <!-- E: 규칙내역 -->
					    <!-- S: 쿠폰내역 -->
					    <c:if test="${tabId eq 0 }">
	   					    <div id="tabContent12" aria-labelledby="tabNav12" aria-hidden="true" class="tab-content">
						    	<input type="hidden" name="nowPage"> 
						 		<div class="gtbl-list l15">
									<table>
										<caption>쿠폰</caption>
										<thead>
											<tr>
												<th>번호</th>
												<th>쿠폰 코드</th>
												<th>규칙명</th>
												<th>사용자</th>
												<th>활성화 날짜</th>
												<th>유효 기간</th>
												<th>할인</th>
												<th>실제 할인금액</th>
												<th>등록일</th>
											</tr>
										</thead>
										<tbody id="promotionCouponDiv">																																																																																																																																																																																																																																												
										</tbody>	
									</table>
								</div>
								<div class="tbl-footer grid flex-between cross-center">
									<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
									<div id="pagingVar" class="col">
									</div>
									<div class="col per-page">
										<select name="cntPerPage" id="cntPerPage" class="select h30" onChange="cntPerPageChange()">
										    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
										    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
										    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
										    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
										    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
										    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
										</select>	
									</div>			
								</div>
						    </div>
						</c:if>
						<c:if test="${tabId eq 1 }">
						    <div id="tabContent12" aria-labelledby="tabNav12" aria-hidden="false" class="tab-content is-visible">			    
						    	<input type="hidden" name="nowPage"> 
						 		<div class="gtbl-list l15">
									<table>
										<caption>쿠폰</caption>
										<thead>
											<tr>
												<th>번호</th>
												<th>쿠폰 코드</th>
												<th>규칙명</th>
												<th>사용자</th>
												<th>활성화 날짜</th>
												<th>유효 기간</th>
												<th>할인</th>
												<th>실제 할인금액</th>
												<th>등록일</th>
											</tr>
										</thead>
										<tbody id="promotionCouponDiv">																																																																																																																																																																																																																																												
										</tbody>	
									</table>
								</div>
								<div class="tbl-footer grid flex-between cross-center">
									<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
									<div id="pagingVar" class="col">
									</div>
									<div class="col per-page">
										<select name="cntPerPage" id="cntPerPage" class="select h30" onChange="cntPerPageChange()">
										    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
										    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
										    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
										    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
										    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
										    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
										</select>	
									</div>			
								</div>
						    </div>						
						 </c:if>

					    <!-- E: 쿠폰내역  -->
					</div>
					<!-- e: Tab Area -->	
				</section>
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" onclick="selfClose();" class="btn type3 primary"><span>확인</span></button>
				<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>
</div>
</form>
</body>
</html>