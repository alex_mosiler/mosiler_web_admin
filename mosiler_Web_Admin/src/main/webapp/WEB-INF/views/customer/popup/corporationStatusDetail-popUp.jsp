<!-- 법인 현황 상세 팝업 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 고객관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		var id = "${id}";
		
		if(id == "") {
			location.href = "/";
		} else {
			$("#id").val(id);
		}
	});	

	// 법인 현황 업데이트
	function updateCorporationStatus() {
		var adminName 					= $("#id").val();
		var bizCompNo 					= $("#bizCompNo").val(); 
		var compName 					= $("#compName").val();
		var ceoName 						= $("#ceoName").val();
		var usePersonCount 				= $("#usePersonCount").val(); 
		var compAddress 				= $("#compAddress").val();
		var contractChargePart 			= $("#contractChargePart").val();
		var contractChargePhoneNo 	= $("#contractChargePhoneNo").val();
		var contractChargeName 		= $("#contractChargeName").val();
		var contractChargeEmail 		= $("#contractChargeEmail").val();
		var paymentChargePart 		= $("#paymentChargePart").val();
		var paymentChargePhoneNo 	= $("#paymentChargePhoneNo").val();
		var paymentChargeName 		= $("#paymentChargeName").val();
		var paymentChargeEmail 		= $("#paymentChargeEmail").val();
		var taxReciveEmail 				= $("#taxReciveEmail").val();
		var useYn 							= $("#useYn").val();
		var paymentMethod 			= $("#paymentMethod").val();
		var paymentDay 					= $("#paymentDay").val(); 
		var hourPrice 						= $("#hourPrice").val(); 
		var reMark							= $("textarea#reMark").val();

		if(paymentDay <= 0 || paymentDay >31) {
			alert("결제일은 1 ~ 31 사이의 숫자만 입력해야 합니다.");
			return false;
		}
		if(hourPrice == "") {
			alert("할인율을 확인해 주세요.");
			return false;
		}
		
		$.ajax({
			url: "/customer/updateCorporationStatus"
			, type: "POST"
			, data: {
				adminName: adminName
				, bizCompNo: bizCompNo
				, compName: compName
				, ceoName: ceoName
				, usePersonCount: usePersonCount
				, compAddress: compAddress
				, contractChargePart: contractChargePart
				, contractChargePhoneNo: contractChargePhoneNo
				, contractChargeName: contractChargeName
				, contractChargeEmail: contractChargeEmail
				, paymentChargePart: paymentChargePart
				, paymentChargePhoneNo: paymentChargePhoneNo
				, paymentChargeName: paymentChargeName
				, paymentChargeEmail: paymentChargeEmail
				, taxReciveEmail: taxReciveEmail
				, useYn: useYn
				, paymentMethod: paymentMethod
				, paymentDay: paymentDay
				, hourPrice: $.trim(hourPrice.replace(/,/g, ''))	
				, reMark: reMark
			}
			, success: function(data) {
				if(data == 200) {
					alert("법인 고객정보가 수정 되었습니다.");
					
					self.close();
				} else {
					alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
				}

				opener.location.reload();
				self.close();
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
				self.close();
				
				console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			}
		}); 		
	}

	// 숫자만 입력 가능
	function setNum(obj) {
		val = obj.value;
		re = /[^0-9]/gi;
		
		obj.value = val.replace(re, "");
	}
	
	// 닫기 버튼
	function selfClose() {
		self.close();
	}
</script>
<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">법인 고객정보 상세</h3>
		</div>
		<input type="hidden" id="id" value="">
		<div class="popup-body">
			<div class="popup-cont">
				<c:forEach var="corporationStatusDetail" items="${corporationStatusDetail}" varStatus="idx">
				<section class="section">
					<input type="hidden" id="bizCompNo" value="${corporationStatusDetail.bizCompNo}">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h3 class="h3">법인정보</h3>
						</div>	
					</div>			
					<div class="tbl-type1 tbl-lefted">
						<table>
							<colgroup>
								<col width="17%">
								<col width="13%">
								<col width="*">
								<col width="13%">
								<col width="30%">
							</colgroup>
							<tbody>
								<tr>
									<th scope="row" class="required">법인명</th>
									<td colspan="2">
										<input type="text" id="compName" name="compName" class="input h25" value="${corporationStatusDetail.compName}" style="width:350px;">
									</td>
									<th scope="row" class="required">사업자번호</th>
									<td>
										${corporationStatusDetail.bizNumber}
									</td>										
								</tr>
								<tr>
									<th scope="row" class="required">대표자</th>
									<td colspan="2">
										<input type="text" id="ceoName" name="ceoName" class="input h25" value="${corporationStatusDetail.ceoName}" style="width:350px;">
									</td>
									<th scope="row">사용인원</th>
									<td>
										<input type="text" id="usePersonCount" name="usePersonCount" onkeyup="setNum(this);" class="input h25" value="${corporationStatusDetail.usePersonCount}" style="width:50px;"> 명
									</td>										
								</tr>
								<tr>
									<th scope="row">주소</th>
									<td colspan="4">
										<input type="text" id="compAddress" name="compAddress" class="input h25" value="${corporationStatusDetail.compAddress}" style="width:737px;">
									</td>
								</tr>									
								<tr>
									<th scope="row" rowspan="2">계약담당자</th>
									<th scope="row">부서</th>
									<td>
										<input type="text" id="contractChargePart" name="contractChargePart" class="input h25" value="${corporationStatusDetail.contractChargePart}" style="width:230px;">
									</td>
									<th scope="row">연락처</th>
									<td>
										<input type="text" id="contractChargePhoneNo" name="contractChargePhoneNo" class="input h25" value="${corporationStatusDetail.contractChargePhoneNo}" style="width:230px;">
									</td>
								</tr>
								<tr>
									<th scope="row">이름</th>
									<td>
										<input type="text" id="contractChargeName" name="contractChargeName" class="input h25" value="${corporationStatusDetail.contractChargeName}" style="width:230px;">
									</td>
									<th scope="row">e-mail</th>
									<td>
										<input type="text" id="contractChargeEmail" name="contractChargeEmail" class="input h25" value="${corporationStatusDetail.contractChargeEmail}" style="width:230px;">
									</td>
								</tr>
								<tr>
									<th scope="row" rowspan="2">결제담당자</th>
									<th scope="row">부서</th>
									<td>
										<input type="text" id="paymentChargePart" name="paymentChargePart" class="input h25" value="${corporationStatusDetail.paymentChargePart}" style="width:230px;">
									</td>
									<th scope="row">연락처</th>
									<td>
										<input type="text" id="paymentChargePhoneNo" name="paymentChargePhoneNo" class="input h25" value="${corporationStatusDetail.paymentChargePhoneNo}" style="width:230px;">
									</td>
								</tr>
								<tr>
									<th scope="row">이름</th>
									<td>
										<input type="text" id="paymentChargeName" name="paymentChargeName" class="input h25" value="${corporationStatusDetail.paymentChargeName}" style="width:230px;">	
									</td>
									<th scope="row">e-mail</th>
									<td>
										<input type="text" id="paymentChargeEmail" name="paymentChargeEmail" class="input h25" value="${corporationStatusDetail.paymentChargeEmail}" style="width:230px;">
									</td>
								</tr>
								<tr>
									<th scope="row">세금계산서(매출전표)</th>
									<th scope="row" class="required">수신용이메일</th>
									<td>
										<input type="text" id="taxReciveEmail" name="taxReciveEmail" class="input h25" value="${corporationStatusDetail.taxReciveEmail}" style="width:230px;">
									</td>
									<th scope="row" class="required">사용여부</th>
									<td>
										<select name="useYn" id="useYn" class="select h30">
											<option value="Y"  <c:if test="${corporationStatusDetail.useYn eq 'Y'}">selected</c:if>>사용</option>
										    <option value="N" <c:if test="${corporationStatusDetail.useYn eq 'N'}">selected</c:if>>사용안함</option>
										</select>
									</td>
								</tr>	
								<tr>
									<th scope="row" class="required">결제수단</th>
									<td colspan="2">
										<select name="paymentMethod" id="paymentMethod" class="select h30">
											<option value="1"  <c:if test="${corporationStatusDetail.paymentMethod eq '1'}">selected</c:if>>현금 결제</option>
										    <option value="2" <c:if test="${corporationStatusDetail.paymentMethod eq '2'}">selected</c:if>>카드 결제</option>
										</select>
									</td>
									<th scope="row" class="required">결제일(매월)</th>
									<td>
										<input type="text" id="paymentDay" name="paymentDay" onkeyup="setNum(this);" class="input h25" value="${corporationStatusDetail.paymentDay}" style="width:50px;"> 일
									</td>												
								</tr>	
								<tr>
									<th scope="row" class="required">할인율</th>
									<td colspan="2">
										<select name="hourPrice" id="hourPrice" class="select h25">
											<option value="">선택</option>
											<c:forEach items="${hourPriceCodeList}" var="list" varStatus="status">
												<option value="${list.codeNameNum}" <c:if test="${customerVO.hourPrice1 eq list.codeNameNum}">selected</c:if>>${list.codeName}</option>
											</c:forEach>
										</select>		
									</td>
									<th scope="row" class="required">이용건수</th>
									<td>0</td>										
								</tr>
								<tr>
									<th scope="row">비고</th>
									<td colspan="4">
										<textarea rows="3" cols="50" class="input type1" id="reMark" value="${corporationStatusDetail.remark}">${corporationStatusDetail.remark}</textarea>
									</td>
								</tr>																																								
							</tbody>
						</table>
					</div>
				</section>
				</c:forEach>	
				<section class="section mar-t10">
					<div class="grid">						
						<div class="col right">
							<button type="button" onclick="updateCorporationStatus();" class="btn type1 primary"><span>수정</span></button>
							<button type="button" onclick="selfClose();" class="btn type1 secondary"><span>닫기</span></button>
						</div>
					</div>
				</section>				
				<!-- <div class="popup-foot grid flex-center">
					<div class="col">				
						<button type="button" onclick="updateCorporationStatus();" class="btn type3 primary"><span>수정</span></button>
						<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
					</div>
				</div> -->
				<section class="section">
					<!-- S: Tab Area -->
					<div class="tab-nav type1">
					    <ul class="tab-list" role="tablist">
					        <li id="tabNav11" role="tab" aria-controls="tabContent11" aria-selected="true" class="tab is-selected">
					        	<button type="button" class="btn"><span>예약된운행</span></button>
					        </li>
					        <li id="tabNav12" role="tab" aria-controls="tabContent12" aria-selected="false" class="tab">
					        	<button type="button" class="btn"><span>종료된운행</span></button>
					        </li>
					        <li id="tabNav13" role="tab" aria-controls="tabContent13" aria-selected="false" class="tab">
					        	<button type="button" class="btn"><span>이용자현황</span></button>
					        </li>
					        <li id="tabNav14" role="tab" aria-controls="tabContent14" aria-selected="false" class="tab">
					        	<button type="button" class="btn"><span>드라이버현황</span></button>
					        </li>	
					        <li id="tabNav15" role="tab" aria-controls="tabContent15" aria-selected="false" class="tab">
					        	<button type="button" class="btn"><span>월결제현황</span></button>
					        </li>				        			        				        
					    </ul>
					</div>
					<div class="tab-container">
					    <!-- S: 예약된운행 -->
					    <div id="tabContent11" aria-labelledby="tabNav11" aria-hidden="false" class="tab-content is-visible">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">예약현황</h2>
								</div>		
							</div>						    
							<div class="gtbl-list l15">
								<table>
									<caption>예약목록</caption>
									<thead>
										<tr>
											<th>번호</th>
											<th>상태</th>
											<th>운행구분</th>
											<th>탑승자</th>
											<th>출발지</th>
											<th>경유지</th>
											<th>종료지</th>
											<th>운행일자</th>
											<th>시작시간</th>
											<th>종료시간</th>
											<th>등록일</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>																								
										</tr>																																																																																																																																																																																																																																																																																										
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div class="col total">페이지 <em>1</em>/129 결과 <em>2,577</em>건</div>
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									        <button type="button" class="btn first" title="처음 페이지" disabled="disabled"><span><i class="arw arw-board1-first gray"></i></span></button>
									        <button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    </div>
										<div class="page-state">
											<span class="curPage">1</span>
											<span class="totPage">3</span>
										</div>	
									    <ul class="num-group">
									        <li><button type="button" class="btn" title="1 페이지" aria-current="true" disabled="disabled"><span>1</span></button></li>
									        <li><button type="button" class="btn" title="2 페이지"><span>2</span></button></li>
									        <li><button type="button" class="btn" title="3 페이지"><span>3</span></button></li>
									    </ul>
									    <div class="page-group">
									        <button type="button" class="btn next" title="다음 페이지"><span><i class="arw arw-board1-next gray"></i></span></button>
									        <button type="button" class="btn last" title="마지막 페이지"><span><i class="arw arw-board1-last gray"></i></span></button>
									    </div>
								    </div>
								</div>
								<div class="col per-page">
									<select name="" id="" class="select h30">
									    <option value="">10</option>
									    <option value="">20</option>
									    <option value="">30</option>
									    <option value="">40</option>
									    <option value="">50</option>
									    <option value="">100</option>
									</select>	
								</div>			
							</div>
					    </div>
					    <!-- E: 예약된운행 -->
					    <!-- S: 종료현황 -->
					    <div id="tabContent12" aria-labelledby="tabNav12" aria-hidden="true" class="tab-content">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">종료현황</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>종료목록</caption>
									<thead>
										<tr>
											<th>번호</th>
											<th>상태</th>
											<th>운행구분</th>
											<th>탑승자</th>
											<th>출발지</th>
											<th>경유지</th>
											<th>종료지</th>
											<th>운행시간</th>
											<th>시작시간</th>
											<th>종료시간</th>
											<th>결재방법</th>
											<th>운행금액</th>
											<th>실비</th>
											<th>결재금액</th>
											<th>등록일</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>										
										</tr>																																																																																																																																																																																																																																																	
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div class="col total">페이지 <em>1</em>/129 결과 <em>2,577</em>건</div>
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									        <button type="button" class="btn first" title="처음 페이지" disabled="disabled"><span><i class="arw arw-board1-first gray"></i></span></button>
									        <button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    </div>
										<div class="page-state">
											<span class="curPage">1</span>
											<span class="totPage">3</span>
										</div>	
									    <ul class="num-group">
									        <li><button type="button" class="btn" title="1 페이지" aria-current="true" disabled="disabled"><span>1</span></button></li>
									        <li><button type="button" class="btn" title="2 페이지"><span>2</span></button></li>
									        <li><button type="button" class="btn" title="3 페이지"><span>3</span></button></li>
									    </ul>
									    <div class="page-group">
									        <button type="button" class="btn next" title="다음 페이지"><span><i class="arw arw-board1-next gray"></i></span></button>
									        <button type="button" class="btn last" title="마지막 페이지"><span><i class="arw arw-board1-last gray"></i></span></button>
									    </div>
								    </div>
								</div>
								<div class="col per-page">
									<select name="" id="" class="select h30">
									    <option value="">10</option>
									    <option value="">20</option>
									    <option value="">30</option>
									    <option value="">40</option>
									    <option value="">50</option>
									    <option value="">100</option>
									</select>	
								</div>			
							</div>
					    </div>
					    <!-- E: 종료현황  -->
					    <!-- S: 이용자현황 -->
					    <div id="tabContent13" aria-labelledby="tabNav13" aria-hidden="true" class="tab-content">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">이용자현황</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>이용자목록</caption>
									<thead>
										<tr>
											<th>번호</th>
											<th>사용자명</th>
											<th>계정권한</th>
											<th>아이디</th>
											<th>전화번호</th>
											<th>차량</th>
											<th>차량번호</th>
											<th>주소</th>
											<th>이용건수</th>
											<th>최종운행일</th>
											<th>등록일</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>										
										</tr>																																																																																																																																																																																																																																																	
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div class="col total">페이지 <em>1</em>/129 결과 <em>2,577</em>건</div>
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									        <button type="button" class="btn first" title="처음 페이지" disabled="disabled"><span><i class="arw arw-board1-first gray"></i></span></button>
									        <button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    </div>
										<div class="page-state">
											<span class="curPage">1</span>
											<span class="totPage">3</span>
										</div>	
									    <ul class="num-group">
									        <li><button type="button" class="btn" title="1 페이지" aria-current="true" disabled="disabled"><span>1</span></button></li>
									        <li><button type="button" class="btn" title="2 페이지"><span>2</span></button></li>
									        <li><button type="button" class="btn" title="3 페이지"><span>3</span></button></li>
									    </ul>
									    <div class="page-group">
									        <button type="button" class="btn next" title="다음 페이지"><span><i class="arw arw-board1-next gray"></i></span></button>
									        <button type="button" class="btn last" title="마지막 페이지"><span><i class="arw arw-board1-last gray"></i></span></button>
									    </div>
								    </div>
								</div>
								<div class="col per-page">
									<select name="" id="" class="select h30">
									    <option value="">10</option>
									    <option value="">20</option>
									    <option value="">30</option>
									    <option value="">40</option>
									    <option value="">50</option>
									    <option value="">100</option>
									</select>	
								</div>			
							</div>
					    </div>
					    <!-- E: 이용자현황  -->	
					    <!-- S: 드라이버현황 -->
					    <div id="tabContent14" aria-labelledby="tabNav14" aria-hidden="true" class="tab-content">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">드라이버현황</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>드라이버목록</caption>
									<thead>
										<tr>
											<th>번호</th>
											<th>드라이버</th>
											<th>연락처</th>
											<th>운행횟수</th>
											<th>등록일</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>									
										</tr>																																																																																																																																																																																																																																																	
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div class="col total">페이지 <em>1</em>/129 결과 <em>2,577</em>건</div>
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									        <button type="button" class="btn first" title="처음 페이지" disabled="disabled"><span><i class="arw arw-board1-first gray"></i></span></button>
									        <button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    </div>
										<div class="page-state">
											<span class="curPage">1</span>
											<span class="totPage">3</span>
										</div>	
									    <ul class="num-group">
									        <li><button type="button" class="btn" title="1 페이지" aria-current="true" disabled="disabled"><span>1</span></button></li>
									        <li><button type="button" class="btn" title="2 페이지"><span>2</span></button></li>
									        <li><button type="button" class="btn" title="3 페이지"><span>3</span></button></li>
									    </ul>
									    <div class="page-group">
									        <button type="button" class="btn next" title="다음 페이지"><span><i class="arw arw-board1-next gray"></i></span></button>
									        <button type="button" class="btn last" title="마지막 페이지"><span><i class="arw arw-board1-last gray"></i></span></button>
									    </div>
								    </div>
								</div>
								<div class="col per-page">
									<select name="" id="" class="select h30">
									    <option value="">10</option>
									    <option value="">20</option>
									    <option value="">30</option>
									    <option value="">40</option>
									    <option value="">50</option>
									    <option value="">100</option>
									</select>	
								</div>			
							</div>
					    </div>
					    <!-- E: 드라이버현황  -->
					    <!-- S: 월결제현황 -->
					    <div id="tabContent15" aria-labelledby="tabNav15" aria-hidden="true" class="tab-content">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">월결제현황</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>종료목록</caption>
									<thead>
										<tr>
											<th>번호</th>
											<th>이용현황</th>
											<th>운행횟수</th>
											<th>결제금액</th>
											<th>결제예정일</th>
											<th>청구서발행일</th>
											<th>결제상태</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>									
										</tr>																																																																																																																																																																																																																																																	
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div class="col total">페이지 <em>1</em>/129 결과 <em>2,577</em>건</div>
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									        <button type="button" class="btn first" title="처음 페이지" disabled="disabled"><span><i class="arw arw-board1-first gray"></i></span></button>
									        <button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    </div>
										<div class="page-state">
											<span class="curPage">1</span>
											<span class="totPage">3</span>
										</div>	
									    <ul class="num-group">
									        <li><button type="button" class="btn" title="1 페이지" aria-current="true" disabled="disabled"><span>1</span></button></li>
									        <li><button type="button" class="btn" title="2 페이지"><span>2</span></button></li>
									        <li><button type="button" class="btn" title="3 페이지"><span>3</span></button></li>
									    </ul>
									    <div class="page-group">
									        <button type="button" class="btn next" title="다음 페이지"><span><i class="arw arw-board1-next gray"></i></span></button>
									        <button type="button" class="btn last" title="마지막 페이지"><span><i class="arw arw-board1-last gray"></i></span></button>
									    </div>
								    </div>
								</div>
								<div class="col per-page">
									<select name="" id="" class="select h30">
									    <option value="">10</option>
									    <option value="">20</option>
									    <option value="">30</option>
									    <option value="">40</option>
									    <option value="">50</option>
									    <option value="">100</option>
									</select>
								</div>			
							</div>
					    </div>
					    <!-- E: 월결제현황  -->				    				    			    
					</div>
					<!-- e: Tab Area -->	
				</section>																		
			</div>
		</div>

		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</body>
</html>