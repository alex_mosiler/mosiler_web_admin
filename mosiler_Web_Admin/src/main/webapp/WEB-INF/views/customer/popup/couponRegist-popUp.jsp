<!-- 프로모션 등록 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 고객관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		var id = "${id}";
		
		if(id == "") {
			location.href = "/";
		} else {
			$("#id").val(id);
		}
		
		var redeemCode = "${redeemCode}";
		$("#redeemCode").val(redeemCode);
	});	

	// 프로모션 쿠폰 추가
	function insertPromotionCoupom() {
		var promotionId 		= $("#promotionId").val();
		var ruleId				= $("#ruleId").val();
		var customerNo		= $("#customerNo").val(); 
		var redeemCode		= $("#redeemCode").val();
		var startDt				= $("#startDate").val();
		var endDt				= $("#endDate").val();
		var isRate				= $('input[name="isRate"]:checked').val();
		var discountValue		= $("#discountValue").val();
		
		if(isRate == 1) {
			discountValue = $("#discountValue").val();

			if(discountValue > 100 || discountValue < 0) {
				alert("할인율을 확인해 주세요.");
				return false;
			}
		} else {
			discountValue = $("#discountValue").val();
		}

		if(customerNo == "") {
			alert("고객을 선택해 주세요.");
			return false;
		}
		if(startDt == "") {
			alert("시작일을 입력해 주세요.");
			return false;
		}
		if(endDt == "") {
			alert("종료일을 입력해 주세요.");
			return false;
		}
		
		$.ajax({
			url: "/customer/insertPromotionCoupon"
			, type: "POST"
			, data: {
				promotionId: promotionId
				, ruleId: ruleId
				, customerNo: customerNo
				, redeemCode: redeemCode
				, startDt: startDt+" 00:00:00.000"
				, endDt: endDt+" 00:00:00.000"
				, isRate: isRate
				, discountValue: discountValue
			}
			, success: function(data) {
				if(data == 200) {
					alert("프로모션이 쿠폰이 등록 되었습니다.");
					
					self.close();
				} else {
					alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
				}

				opener.location.reload();
				self.close();
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
				self.close();
				
				console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			}
		}); 				
	}
	
	// 숫자만 입력 가능
	function setNum(obj) {
		val = obj.value;
		re = /[^0-9]/gi;
		
		obj.value = val.replace(re, "");
	}
	
	// 닫기 버튼
	function selfClose() {
		self.close();
	}
	
	// 고객명 input 필드 내용 삭제
	function selClear() {
		$("#customerName").val("");
		$("#customerPhoneNumber").val("");
	}
</script>

<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
<input type="hidden" id="promotionId" value="${promotionId}">

<!-- 안 쓰는 input 필드임, 고객 서치하는 customerSelect-popUp에 예약이랑 같이 쓰기 위해 임의로 넣는거 -->
	<input type="hidden" name="" id="reservationName" value="" placeholder="" title="" class="input h25 w80">
	<input type="hidden" name="" id="reservationPhoneNumber" value="" placeholder="" title="" class="input h25 w120">
	<input type="hidden" name="" id="ridingName" value="" placeholder="" title="" class="input h25 w80">
	<input type="hidden" name="" id="ridingPhoneNumber" value="" placeholder="" title="" class="input h25 w120">		
<!-- 안 쓰는 input 필드임, 고객 서치하는 customerSelect-popUp에 예약이랑 같이 쓰기 위해 임의로 넣는거 -->

	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">프로모션 쿠폰 등록</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<section class="section">
					<div class="tbl-type1 tbl-lefted">
						<table>
							<colgroup>
								<col width="25%">
								<col width="13%">
								<col width="*">
								<col width="13%">
								<col width="30%">
							</colgroup>
							<tbody>
								<tr>
									<th scope="row" >규칙명</th>
									<td colspan="4">
										<select name="ruleId" id="ruleId" class="select h30" style="width:100%">
											<c:forEach items="${ruleList}" var="list" varStatus="status">
												<option value="${list.ruleId}">${list.promotionCode}</option>
											</c:forEach>
										</select>
									</td>		
								</tr>
								<tr>
									<th scope="row" >고객명</th>
									<td colspan="4">
										<div class="form-controls flexible">
											<input type="hidden" name="" id="customerNo" value="">
											<input type="hidden" name="" id="bizCompNo" value="">
											<input type="text" name="customerName" id="customerName" value="" placeholder="" class="input type1 h25" style="width:115px;" readonly>&nbsp;
											<input type="text" name="customerPhoneNumber" id="customerPhoneNumber" value="" placeholder="" class="input type1 h25" style="width:115px;" readonly>&nbsp;
											<button type="button" onclick="window.open('/customer/customerSelectPopup', '', 'width=550, height=510')" class="btn type1 primary"><span>선택</span></button>&nbsp;
											<button type="button" onclick="selClear();" class="btn type1 primary"><span>삭제</span></button>
										</div>	
									</td>								
								</tr>
								<tr>
									<th scope="row" >RedeemCode</th>
									<td colspan="4">
										<div class="form-controls flexible">
											<input type="text" name="redeemCode" id="redeemCode" value="" placeholder="" class="input type1 h25" readonly>
										</div>	
									</td>								
								</tr>
								<tr>
									<th scope="row">시작일</th>
									<td colspan="4">
										<input type="date" name="startDate" id="startDate" value="" placeholder="" title="" class="input h30 input-date">
									</td>	
								</tr>									
								<tr>
									<th scope="row">종료일</th>
									<td colspan="4">
										<input type="date" name="endDate" id="endDate" value="" placeholder="" title="" class="input h30 input-date">
									</td>
								</tr>	
								<tr>
									<th scope="row" >할인</th>
									<td colspan="4">
										<input type="radio" name="isRate" value="1" checked>율
										<input type="radio" name="isRate" value="0" >양
										&nbsp;
										<input type="text" id="discountValue" name="discountValue" onkeyup="setNum(this);" placeholder="할인 수치 입력" class="input type1 h25" style="width:255px;">
									</td>
								</tr>	
							</tbody>
						</table>
					</div>
				</section>																
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" onclick="insertPromotionCoupom();" class="btn type3 primary"><span>등록</span></button>
				<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</body>
</html>