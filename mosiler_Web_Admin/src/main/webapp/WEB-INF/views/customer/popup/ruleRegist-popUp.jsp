<!-- 프로모션 등록 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 고객관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		var id = "${id}";
		
		if(id == "") {
			location.href = "/";
		} else {
			$("#id").val(id);
		}
	});	

	// 프로모션 규칙 추가
	function insertPromotionRule() {
		var promotionId		= $("#promotionId").val();
		var promotionName = $("#promotionName").val();
		var promotionCode 	= $("#promotionCode").val();
		var conditionType 	= $("#conditionType").val();
		var conditionQty 		= $("#conditionQty").val();
		var expiredDt 			= $("#expiredDt").val();
		var isRepeat;
		
		if($("input:checkbox[name=isRepeat]").is(":checked") == true) {
			isRepeat = 1;
		} else {
			isRepeat = 0;
		}
		
		if(conditionQty == "") {
			conditionQty = 0;	
		}
		
		if(promotionName == "") {
			alert("이름을 입력해 주세요.");
			return false;
		}
		if(promotionCode == "") {
			alert("규칙명을 입력해 주세요.");
			return false;
		}
		if(conditionType == "") {
			alert("조건 유형을 선택해 주세요.");
			return false;
		}
		if(expiredDt == "") {
			alert("만료일을 입력해 주세요.");
			return false;
		}
		
		$.ajax({
			url: "/customer/insertPromotionRule"
			, type: "POST"
			, data: {
				promotionNo: promotionId
				, promotionName: promotionName
				, promotionCode: promotionCode
				, conditionType: conditionType
				, conditionQty: conditionQty
				, expiredDt: expiredDt
				, isRepeat: isRepeat
			}
			, success: function(data) {
				if(data == 200) {
					alert("프로모션이 규칙이 등록 되었습니다.");
					
					self.close();
				} else {
					alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
				}

				opener.location.reload();
				self.close();
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
				self.close();
				
				console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			}
		}); 				
	}
	
	// 숫자만 입력 가능
	function setNum(obj) {
		val = obj.value;
		re = /[^0-9]/gi;
		
		obj.value = val.replace(re, "");
	}
	
	// 닫기 버튼
	function selfClose() {
		self.close();
	}
</script>
<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
<input type="hidden" id="promotionId" value="${promotionId}">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">프로모션 규칙 등록</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<section class="section">
					<div class="tbl-type1 tbl-lefted">
						<table>
							<colgroup>
								<col width="25%">
								<col width="13%">
								<col width="*">
								<col width="13%">
								<col width="30%">
							</colgroup>
							<tbody>
								<tr>
									<th scope="row" >이름</th>
									<td colspan="4">
										<div class="form-controls flexible">
											<input type="text" name="promotionName" id="promotionName" value="" placeholder="" class="input type1 h25">
										</div>	
									</td>								
								</tr>
								<tr>
									<th scope="row" >규칙명</th>
									<td colspan="4">
										<div class="form-controls flexible">
											<input type="text" name="promotionCode" id="promotionCode" value="" placeholder="" class="input type1 h25">
										</div>	
									</td>								
								</tr>
								<tr>
									<th scope="row">조건 유형</th>
									<td colspan="4">
										<select name="conditionType" id="conditionType" class="select h25" style="width:100%;">
											<option value="">선택</option>
											<c:forEach items="${conditionTypeList}" var="list" varStatus="status">
												<option value="${list.code}">${list.codeName}</option>
											</c:forEach>
										</select>		
									</td>		
								</tr>									
								<tr>
									<th scope="row">조건 수량</th>
									<td colspan="4">
										<div class="form-controls flexible">
											<input type="text" name="conditionQty" id="conditionQty" onkeyup="setNum(this);" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
								</tr>	
								<tr>
									<th scope="row" >쿠폰 만료일</th>
									<td colspan="4">
										<div class="form-controls flexible">
											<input type="text" name="expiredDt" id="expiredDt" onkeyup="setNum(this);" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
								</tr>	
								<tr>
									<th scope="row">IsRepeat</th>
									<td colspan="4">
										<input type="checkbox" id="isRepeat" name="isRepeat" value="" placeholder="" title="" class="input h30 input-date">
									</td>												
								</tr>																																	
							</tbody>
						</table>
					</div>
				</section>																
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" onclick="insertPromotionRule();" class="btn type3 primary"><span>등록</span></button>
				<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</body>
</html>