<!-- 고객 상세 팝업 - 예약 등록 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 예약관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
   	
   	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			var currentDate = new Date().toISOString().substring(0, 10);
	   		$("#startDate").val(currentDate);
	   		$("#endDate").val(currentDate);
		});

		// 예약자 - 고객동일 체크 시
		function checkCustomer() {
			if($("input:checkbox[name=checkCustomer1]").is(":checked") == true) {
				$("#reservationName").val($("#customerName").val());
				$("#reservationName").attr("readonly", true);
				
				$("#reservationPhoneNumber").val($("#customerPhoneNumber").val());
				$("#reservationPhoneNumber").attr("readonly", true);
			} else {
				$("#reservationName").val("");
				$("#reservationName").attr("readonly", false);

				$("#reservationPhoneNumber").val("");
				$("#reservationPhoneNumber").attr("readonly", false);
			}
		}

		// 보험 적용 여부 체크 시
		function checkInsurance() { alert("11");
			if($("input:checkbox[name=insuranceYn]").is(":checked") == true) {
				$("#insuranceYn").val("Y");
			} else {
				$("#insuranceYn").val("N");
			}
		}

   		// 예약 위치정보 삭제 버튼
   		function deletePlace(num) {
			$("#address"+num).val("");
			$("#lat"+num).val("");
			$("#lng"+num).val("");
   	   	}
   	   	
   		// 예약하기 시작
		function insertReservation() {
			var driverNo							= $("#driverNo").val();
			var drivingType 						= $("#drivingType").val();
			var insuranceYn						= $("#insuranceYn").val();
			var bizCompNo						= $("#bizCompNo").val();
			var customerNo 						= $("#customerNo").val();
			var customerName 					= $("#customerName").val();
			var customerPhoneNumber 		= $("#customerPhoneNumber").val();
			var reservationName 				= $("#reservationName").val();
			var reservationPhoneNumber 		= $("#reservationPhoneNumber").val();
			var ridingName 						= $("#ridingName").val();
			var ridingPhoneNumber 			= $("#ridingPhoneNumber").val();
			var insurance 							= $("#insurance").val();
			var paymentType						= $("#paymentType").val();
			var couponCnt 						= $("#couponCnt").val();
			var hourPrice							= $("#hourPrice").val();
			var revStartDt							= $("#startDate").val()+" "+$("#startHour").val()+":"+$("#startMin").val()+":00.000";
			var revEndDt							= $("#endDate").val()+" "+$("#endHour").val()+":"+$("#endMin").val()+":00.000";
			var address1 							= $("#address1").val();
			var lat1 									= $("#lat1").val();
			var lng1 									= $("#lng1").val();
			var address2 							= $("#address2").val();
			var lat2 									= $("#lat2").val();
			var lng2 									= $("#lng2").val();
			var address3 							= $("#address3").val();
			var lat3 									= $("#lat3").val();
			var lng3 									= $("#lng3").val();
			var address4 							= $("#address4").val();
			var lat4 									= $("#lat4").val();
			var lng4 									= $("#lng4").val();
			var partnerName 						= $("#userName").val();
			var partnerPhoneNumber	 		= $("#phoneNumber").val();
			var partnerLevel 						= $("#partnerLevel").val();
			//var partnerAccident = ;
			
			var conciergeList = new Array();
			var concierge = document.getElementsByName("concierge");
			for(var i = 0; i < concierge.length; i++) {
				if(concierge[i].checked) {
					conciergeList.push(concierge[i].value);
				}
			}

			var conciergeCodeList 				= conciergeList;
			var customerRequest 				= $("textarea#customerRequest").val();
			var adminRequest 					= $("textarea#adminRequest").val();

			var currentDate = new Date().toISOString().substring(0, 10);
			
   			if(drivingType == "") {
				alert("운행 구분을 선택해 주세요.");
				return false;
			} 
			if(reservationName == "" && reservationPhoneNumber == "") {
				alert("예약자를 확인해 주세요.");
				return false;
			}
			if(ridingName == "" && ridingPhoneNumber == "") {
				alert("탑승자를 확인해 주세요.");
				return false;
			}
			if($("#startDate").val() < currentDate) {
				alert("예약 시작 일시는 현재일보다 작을 수 없습니다.");
				return false;
			}
			if($("#endDate").val() < currentDate) {
				alert("예약 종료 일시는 현재일보다 작을 수 없습니다.");
				return false;
			}
			if($("#startDate").val() == "" || $("#endDate").val() == "") {
				alert("예약 날짜를 확인해 주세요.");
				return false;
			}
			if(paymentType == "") {
				alert("결제 수단을 확인해 주세요.");
				return false;
			}
			if(hourPrice == "") {
				alert("할인율을 확인해 주세요.");
				return false;
			}
			if(address1 == "") {
				alert("출발지를 확인해 주세요.");
				return false;
			}
			if(address4 == "") {
				alert("도착지를 확인해 주세요.");
				return false;
			}
			if(partnerName == "" && partnerPhoneNumber == "") {
				alert("드라이버를 배정해 주세요.");
				return false;
			}   

			jQuery.ajaxSettings.traditional = true;

			$.ajax({
				url: "/reservation/insertReservation"
				, type: "POST"
				, data: {
 					driverNo: driverNo
					, drivingType: drivingType
 					, insuranceYn: insuranceYn
 					, bizCompNo: bizCompNo
					, customerNo: customerNo
					, customerName: customerName
					, customerPhoneNumber: customerPhoneNumber
					, reservationName: reservationName
					, reservationPhoneNumber: reservationPhoneNumber
					, ridingName: ridingName
					, ridingPhoneNumber: ridingPhoneNumber
					, insurance: insurance
					, paymentType: paymentType
					, couponCnt: couponCnt
				  	, hourPrice: $.trim(hourPrice.replace(/,/g, ''))
					, revStartDt: revStartDt
					, revEndDt: revEndDt
					, address1: address1
					, lat1: lat1
					, lng1: lng1
					, address2: address2
					, lat2: lat2
					, lng2: lng2
					, address3: address3
					, lat3: lat3
					, lng3: lng3
					, address4: address4
					, lat4: lat4
					, lng4: lng4
					, partnerName: partnerName
					, partnerPhoneNumber: partnerPhoneNumber
					, partnerLevel: partnerLevel 
//					, partnerAccident: partnerAccident
					, conciergeCodeList: conciergeCodeList
 					, customerRequest: customerRequest
					, adminRequest: adminRequest 		  
				}
				, success: function(data) {
					if(data == 200) {
						alert("예약 정보가 등록 되었습니다.");
						
						self.close();
					} else {
						alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
					}

					opener.location.reload();
					self.close();
				}
				, error: function(data, status, opt) {
					alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
					self.close();
					
					console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
				}
			});
		}

   		// 닫기 버튼
 		function selfClose() {
			self.close();
		}
   	</script>
</head>

<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">예약등록</h3>
		</div>
		<form id="insertReservationForm">
			<div class="popup-body">
				<div class="popup-cont">
					<section class="section">
						<div class="tbl-header grid cross-center">
							<div class="col">
								<h3 class="h3">예약정보</h3>
							</div>	
						</div>
						<div class="tbl-type1 tbl-lefted">
							<table>
								<colgroup>
									<col width="13%">
									<col width="*">
									<col width="13%">
									<col width="35%">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">운행 구분</th>
										<td>
											<select name="drivingType" id="drivingType" class="select h25">
												<option value="">선택</option>
												<c:forEach items="${codeList}" var="list" varStatus="status">
													<option value="${list.code}">${list.codeName}</option>
												</c:forEach>
											</select>		
											<!-- <span class="check type1">
												<input type="hidden" id="insuranceYn" name="InsuranceYn" value="">
											    <input type="checkbox" name="insuranceYn" id="insuranceYn" onclick="javascript:checkInsurance();" checked="" >
											    <label for="check"><span>보험 적용 여부</span></label>
											</span>		 -->									
										</td>
										<th scope="row">고객</th>
										<td>
											<c:forEach items="${customerInfo}" var="list" varStatus="status">
												<!-- <button type="button" class="btn type4 primary" onclick="window.open('M030101-P03.html', '', 'width=550, height=510')">선택</button> -->
												<input type="hidden" name="" id="customerNo" value="${list.customerNo}">
												<input type="hidden" name="" id="bizCompNo" value="${list.bizCompNo}">
												<input type="text" name="" id="customerName" value="${list.customerName}" placeholder="" title="" class="input h25 w80" readonly style="background:#e5e5e5;">
												<input type="text" name="" id="customerPhoneNumber" value="${list.phoneNumber}" placeholder="" title="" class="input h25 w120" readonly style="background:#e5e5e5;">
											</c:forEach>
										</td>										
									</tr>
									<tr>
										<th scope="row">예약자</th>
										<td>
											<c:forEach items="${customerInfo}" var="list" varStatus="status">
												<input type="text" name="" id="reservationName" value="${list.customerName}" placeholder="" title="" class="input h25 w80">
												<input type="text" name="" id="reservationPhoneNumber" value="${list.phoneNumber}" placeholder="" title="" class="input h25 w120">
											</c:forEach>
											<span class="check type1">
											    <input type="checkbox" name="checkCustomer1" id="check" onclick="javascript:checkCustomer();" checked="" >
											    <label for="check"><span>고객동일</span></label>
											</span>											
										</td>
										<th scope="row">탑승자</th>
										<td>
											<c:forEach items="${customerInfo}" var="list" varStatus="status">
												<input type="text" name="" id="ridingName" value="${list.customerName}" placeholder="" title="" class="input h25 w80">
												<input type="text" name="" id="ridingPhoneNumber" value="${list.phoneNumber}" placeholder="" title="" class="input h25 w120">
											</c:forEach>										
										</td>									
									</tr>
									<tr>
										<th scope="row">상품구분</th>
										<td>
											<select name="" id="sSelect1" class="select h25">
											    <option value="">상품선택</option>
											</select>
											(미개발) 										
										</td>
										<th scope="row">보험</th>
										<td>
											<select name="insurance" id="insurance" class="select h25">
											    <option value="">일반보험</option>
											    <option value="">탁송보험</option>
											</select>					
											(미개발)			
										</td>
									</tr>
									<tr>
										<th scope="row">결제 수단</th>
										<td>
											<select name="paymentType" id="paymentType" class="select h25">
												<option value="">선택</option>
												<c:forEach items="${paymentTypeCodeList}" var="list" varStatus="status">
													<option value="${list.code}">${list.codeName}</option>
												</c:forEach>
											</select>	
											<select name="couponCnt" id="couponCnt" class="select h25">
												<c:forEach var="i" begin="0" end="5">
													<option value="${i}">${i}장</option>
												</c:forEach>
											</select>	
										</td>
										<th scope="row">할인율</th>
										<td>
											<select name="hourPrice" id="hourPrice" class="select h25">
												<option value="">선택</option>
												<c:forEach items="${hourPriceCodeList}" var="list" varStatus="status">
													<option value="${list.codeNameNum}">${list.codeName}</option>
												</c:forEach>
											</select>					
										</td>
									</tr>
									<tr>
										<th scope="row">예약 시작 일시</th>
										<td>
											<input type="date" name="startDate" id="startDate" value="${customerVO.startDate }" min="${currentDate}" placeholder="" title="" class="input h30 input-date">
											<select name="startHour" id="startHour" class="select h30">
												<c:forEach var="i" begin="0" end="23">
													<c:if test="${i < 10}">
														<option value="${i}">0${i}시</option>
													</c:if>
													<c:if test="${i >= 10}">
														<option value="${i}">${i}시</option>
													</c:if>
												</c:forEach>
											</select>
											<select name="startMin" id="startMin" class="select h30">
												<option value="0">00분</option>
												<c:forEach var="i" begin="5" end="55">
													<c:if test="${i%5 == 0}">
														<c:if test="${i < 10}">
															<option value="${i}">0${i}분</option>
														</c:if>
														<c:if test="${i >= 10}">
															<option value="${i}">${i}분</option>
														</c:if>
													</c:if>
												</c:forEach>
											</select>	
										</td>		
										<th scope="row">예약 종료 일시</th>
										<td>
											<input type="date" name="endDate" id="endDate" value="${customerVO.endDate }" placeholder="" title="" class="input h30 input-date">
						 					<select name="endHour" id="endHour" class="select h30">
												<c:forEach var="i" begin="0" end="23">
													<c:if test="${i < 10}">
														<option value="${i}">0${i}시</option>
													</c:if>
													<c:if test="${i >= 10}">
														<option value="${i}">${i}시</option>
													</c:if>
												</c:forEach>
											</select>
											<select name="endMin" id="endMin" class="select h30">
												<option value="0">00분</option>
												<c:forEach var="i" begin="5" end="55">
													<c:if test="${i%5 == 0}">
														<c:if test="${i < 10}">
															<option value="${i}">0${i}분</option>
														</c:if>
														<c:if test="${i >= 10}">
															<option value="${i}">${i}분</option>
														</c:if>
													</c:if>
												</c:forEach>
											</select>		
										</td>
									</tr>									
								</tbody>
							</table>
						</div>
					</section>
					<section class="section">
						<div class="tbl-header grid cross-center">
							<div class="col">
								<h3 class="h3">예약 위치정보</h3>
							</div>	
						</div>
						<div class="tbl-type1 tbl-lefted">
							<table>
								<colgroup>
									<col width="13%">
									<col width="*">
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">출발지</th>
										<td>
											<!-- <button type="button" class="btn type4 primary">찾기</button> -->
											<input type="text" name="address1" id="address1" value="" placeholder="지역명 2자리이상 입력하세요" title="" class="input h25 w400">
											<button type="button" class="btn type4" onclick="deletePlace(1)">삭제</button>
											<button type="button" class="btn type4 secondary" onclick="window.open('/customer/reservationMap?num=1','','width=1500,height=950');return true">지도보기</button>
											<input type="text" name="lat1" id="lat1" value="" readonly style="background:#e5e5e5; width:120px;">
											<input type="text" name="lng1" id="lng1" value="" readonly style="background:#e5e5e5; width:120px;">
										</td>									
									</tr>
									<tr>
										<th scope="row">경유지</th>
										<td>
											<!-- <button type="button" class="btn type4 primary">찾기</button> -->
											<input type="text" name="address2" id="address2" value="" placeholder="지역명 2자리이상 입력하세요" title="" class="input h25 w400">
											<button type="button" class="btn type4" onclick="deletePlace(2)">삭제</button>
											<button type="button" class="btn type4 secondary" onclick="window.open('/customer/reservationMap?num=2','','width=1500,height=950');return true">지도보기</button>
											<input type="text" name="lat2" id="lat2" value="" readonly style="background:#e5e5e5; width:120px;">
											<input type="text" name="lng2" id="lng2" value="" readonly style="background:#e5e5e5; width:120px;">
										</td>									
									</tr>
									<tr>
										<th scope="row">경유지</th>
										<td>
											<!-- <button type="button" class="btn type4 primary">찾기</button> -->
											<input type="text" name="address3" id="address3" value="" placeholder="지역명 2자리이상 입력하세요" title="" class="input h25 w400">
											<button type="button" class="btn type4" onclick="deletePlace(3)">삭제</button>
											<button type="button" class="btn type4 secondary" onclick="window.open('/customer/reservationMap?num=3','','width=1500,height=950');return true">지도보기</button>
											<input type="text" name="lat3" id="lat3" value="" readonly style="background:#e5e5e5; width:120px;">
											<input type="text" name="lng3" id="lng3" value="" readonly style="background:#e5e5e5; width:120px;">
										</td>									
									</tr>
									<tr>
										<th scope="row">도착지</th>
										<td>
											<!-- <button type="button" class="btn type4 primary">찾기</button> -->
											<input type="text" name="address4" id="address4" value="" placeholder="지역명 2자리이상 입력하세요" title="" class="input h25 w400">
											<button type="button" class="btn type4" onclick="deletePlace(4)">삭제</button>
											<button type="button" class="btn type4 secondary" onclick="window.open('/customer/reservationMap?num=4','','width=1500,height=950');return true">지도보기</button>
											<input type="text" name="lat4" id="lat4" value="" readonly style="background:#e5e5e5; width:120px;">
											<input type="text" name="lng4" id="lng4" value="" readonly style="background:#e5e5e5; width:120px;">
										</td>									
									</tr>
								</tbody>
							</table>
						</div>
					</section>	
					<section class="section">
						<div class="tbl-header grid cross-center">
							<div class="col">
								<h3 class="h3">파트너 정보</h3>
							</div>
							<div class="col right">
								<button type="button" class="btn type2 primary" onclick="window.open('/partner/partnerAssignment?tabId=1&customerNo=${customerNo}', '', 'width=600, height=570')"><span>파트너배정</span></button>	
								<button type="button" class="btn type2 secondary" onclick="window.open('/partner/partnerMultiAssignment?tabId=1&customerNo=${customerNo}', '', 'width=600, height=570')"><span>파트너멀티배정</span></button>				
							</div>	
						</div>
						<div>
							<div class="tbl-type1 tbl-lefted">
								<table>
									<colgroup>
										<col width="13%">
										<col width="*">
										<col width="13%">
										<col width="35%">
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">파트너명</th>
											<td>
												<input type="hidden" id="driverNo" name="driverNo" value="">
											   	<input type="text" name="name" id="userName" value="" placeholder="" title="" class="input h25" readonly>
											</td>
											<th scope="row">휴대폰번호</th>
											<td>
												<input type="text" name="number" id="phoneNumber" value="" placeholder="" title="" class="input h25" readonly>
											</td>										
										</tr>
										<tr>
											<th scope="row">파트너 등급</th>
											<td>
												<input type="text" name="partnerLv" id="partnerLv" value="" placeholder="" title="" class="input h25" readonly>	
											</td>
											<th scope="row">사고율 (%)</th>
											<td>
												<input type="text" name="" id="inputDate" value="" placeholder="" title="" class="input h25" readonly>
											</td>										
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</section>
					<section class="section">
						<div class="tbl-header grid cross-center">
							<div class="col">
								<h3 class="h3">요청사항</h3>
							</div>
						</div>
						<div>
							<div class="tbl-type1 tbl-lefted">
								<table>
									<colgroup>
										<col width="13%">
										<col width="*">
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">컨시어지</th>
											<td>
												<c:forEach items="${conciergeCodeList}" var="list" varStatus="status">
													<span class="check type1">
														<input type="checkbox" name="concierge" id="${status.count}" value="${list.code}">
													    <label for="${status.count}"><span>${list.codeName}</span></label>
													</span>	
												</c:forEach>																																	
											</td>									
										</tr>							
										<tr>
											<th scope="row">고객 요청사항</th>
											<td><textarea rows="3" id="customerRequest" cols="50" class="input type1"></textarea></td>									
										</tr>
										<tr>
											<th scope="row">관리자 요청사항</th>
											<td><textarea rows="3" id="adminRequest" cols="50" class="input type1"></textarea></td>										
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</section>	
				</div>
			</div>
		</form>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<!-- <button type="button" class="btn type3 primary"><span>결제하기</span></button> -->
				<button type="button" onclick="insertReservation();" class="btn type3 primary"><span>예약하기</span></button>
				<button type="button" onclick="selfClose();"class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</body>
</html>