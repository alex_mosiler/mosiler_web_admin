<!-- 법인 현황 등록 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 고객관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		var id = "${id}";
		
		if(id == "") {
			location.href = "/";
		} else {
			$("#id").val(id);
		}
	});	

	// 법인 현황 등록
	function insertCorporationStatus() {
		var adminName 					= $("#id").val();
		var compName 					= $("#compName").val();
		var bizNumber 					= $("#bizNumber").val();
		var ceoName 						= $("#ceoName").val();
		var usePersonCount 				= $("#usePersonCount").val(); 
		var compAddress 				= $("#compAddress").val();
		var contractChargePart 			= $("#contractChargePart").val();
		var contractChargePhoneNo 	= $("#contractChargePhoneNo").val();
		var contractChargeName 		= $("#contractChargeName").val();
		var contractChargeEmail 		= $("#contractChargeEmail").val();
		var paymentChargePart 		= $("#paymentChargePart").val();
		var paymentChargePhoneNo 	= $("#paymentChargePhoneNo").val();
		var paymentChargeName 		= $("#paymentChargeName").val();
		var paymentChargeEmail 		= $("#paymentChargeEmail").val();
		var taxReciveEmail 				= $("#taxReciveEmail").val();
		var useYn 							= $("#useYn").val();
		var paymentMethod 			= $("#paymentMethod").val();
		var paymentDay 					= $("#paymentDay").val(); 
		var hourPrice 						= $("#hourPrice").val(); 
		var reMark							= $("textarea#reMark").val();

		
		if(compName == "") {
			alert("법인명을 입력해 주세요.");
			return false;
		}
		if(bizNumber == "") {
			alert("사업자 번호를 입력해 주세요.");
			return false;
		}
		if(ceoName == "") {
			alert("대표자명을 입력해 주세요.");
			return false;
		}
		if(usePersonCount == "") {
			alert("사용인원을 확인해 주세요.");
			return false;
		}
		if(taxReciveEmail == "") {
			alert("수신용 이메일을 입력해 주세요.");
			return false;
		}
		if(hourPrice == "") {
			alert("할인율을 확인해 주세요.");
			return false;
		}	
		
		if(paymentDay == "") {
			alert("매월 결제일을 입력해 주세요.");
			return false;
		} else if(paymentDay != "" &&	 (paymentDay <= 0 || paymentDay >31)) {
			alert("결제일은 1 ~ 31 사이의 숫자만 입력해야 합니다.");
			return false;
		}
		
		$.ajax({
			url: "/customer/insertCorporationStatus"
			, type: "POST"
			, data: {
				adminName: adminName
				, compName: compName
				, bizNumber: bizNumber
				, ceoName: ceoName
				, usePersonCount: usePersonCount
				, compAddress: compAddress
				, contractChargePart: contractChargePart
				, contractChargePhoneNo: contractChargePhoneNo
				, contractChargeName: contractChargeName
				, contractChargeEmail: contractChargeEmail
				, paymentChargePart: paymentChargePart
				, paymentChargePhoneNo: paymentChargePhoneNo
				, paymentChargeName: paymentChargeName
				, paymentChargeEmail: paymentChargeEmail
				, taxReciveEmail: taxReciveEmail
				, useYn: useYn
				, paymentMethod: paymentMethod
				, paymentDay: paymentDay
				, hourPrice: $.trim(hourPrice.replace(/,/g, ''))	
				, reMark: reMark
			}
			, success: function(data) {
				if(data == 200) {
					alert("법인 고객정보가 등록 되었습니다.");
					
					self.close();
				} else {
					alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
				}

				opener.location.reload();
				self.close();
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
				self.close();
				
				console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			}
		}); 				
	}
	
	// 숫자만 입력 가능
	function setNum(obj) {
		val = obj.value;
		re = /[^0-9]/gi;
		
		obj.value = val.replace(re, "");
	}
	
	// 닫기 버튼
	function selfClose() {
		self.close();
	}
</script>
<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">법인 고객정보 등록</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<section class="section">
					<div class="tbl-type1 tbl-lefted">
						<table>
							<colgroup>
								<col width="17%">
								<col width="13%">
								<col width="*">
								<col width="13%">
								<col width="30%">
							</colgroup>
							<tbody>
								<tr>
									<th scope="row" class="required">법인명</th>
									<td colspan="2">
										<div class="form-controls flexible">
											<input type="text" name="compName" id="compName" value="" placeholder="" class="input type1 h25">
										</div>	
									</td>
									<th scope="row" class="required">사업자번호</th>
									<td>
										<div class="form-controls flexible">
											<input type="tel" name="bizNumber" id="bizNumber" value="" placeholder="" title="사업자등록번호" class="input type1 h25">
										</div>
									</td>										
								</tr>
								<tr>
									<th scope="row" class="required">대표자</th>
									<td colspan="2">
										<div class="form-controls flexible">
											<input type="text" name="ceoName" id="ceoName" value="" placeholder="" class="input type1 h25">	
										</div>																
									</td>
									<th scope="row">사용인원</th>
									<td>
										<input type="text" name="usePersonCount" id="usePersonCount" onkeyup="setNum(this);" value="" placeholder="" class="input type1 h25 w80 align-r"> 명
									</td>										
								</tr>
								<tr>
									<th scope="row">주소</th>
									<td colspan="4">
										<!-- <div class="row">
											<input type="text" name="sAddress" id="sAddress1" value="" placeholder="" title="우편번호" class="input type1 h25 w100">
											<button type="button" class="btn type4"><span>우편번호</span></button>
										</div> -->
										<!-- <div class="row">
											<input type="text" name="sAddress" id="sAddress2" value="" placeholder="" title="기본주소" class="input type1 h25 W100">
										</div> -->
										<div class="row">
											<input type="text" name="compAddress" id="compAddress" value="" placeholder="" title="상세주소" class="input type1 h25 W100">
										</div>				
									</td>
								</tr>									
								<tr>
									<th scope="row" rowspan="2">계약담당자</th>
									<th scope="row">부서</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="contractChargePart" id="contractChargePart" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
									<th scope="row">연락처</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="contractChargePhoneNo" id="contractChargePhoneNo" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row">이름</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="contractChargeName" id="contractChargeName" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
									<th scope="row">e-mail</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="contractChargeEmail" id="contractChargeEmail" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row" rowspan="2">결제담당자</th>
									<th scope="row">부서</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="paymentChargePart" id="paymentChargePart" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
									<th scope="row">연락처</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="paymentChargePhoneNo" id="paymentChargePhoneNo" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row">이름</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="paymentChargeName" id="paymentChargeName" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
									<th scope="row">e-mail</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="paymentChargeEmail" id="paymentChargeEmail" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row">세금계산서(매출전표)</th>
									<th scope="row" class="required">수신용이메일</th>
									<td>
										<div class="form-controls flexible">
											<input type="text" name="taxReciveEmail" id="taxReciveEmail" value="" placeholder="" class="input type1 h25">
										</div>
									</td>
									<th scope="row" class="required">사용여부</th>
									<td>
										<select name="useYn" id="useYn" class="select h30">
											<option value="Y">사용</option>
										    <option value="N">사용안함</option>
										</select>						
									</td>
								</tr>	
								<tr>
									<th scope="row" class="required">결제수단</th>
									<td colspan="2">
										<select name="paymentMethod" id="paymentMethod" class="select type1 h25">
										    <option value="">현금결제</option>
										    <option value="">카드결제</option>
										</select>								
									</td>
									<th scope="row" class="required">결제일</th>
									<td>
										<span class="static">매월</span>
										<input type="text" name="paymentDay" id="paymentDay" onkeyup="setNum(this);" value="" placeholder="" class="input type1 h25 w50">
										<span class="static">일</span>
									</td>										
								</tr>	
								<tr>
									<th scope="row" class="required">할인율</th>
									<td colspan="4">
										<select name="hourPrice" id="hourPrice" class="select h25">
											<option value="">선택</option>
											<c:forEach items="${hourPriceCodeList}" var="list" varStatus="status">
												<option value="${list.codeNameNum}">${list.codeName}</option>
											</c:forEach>
										</select>							
									</td>									
								</tr>
								<tr>
									<th scope="row">비고</th>
									<td colspan="4">
										<div class="form-controls flexible">
											<textarea rows="3" id="reMark" cols="50" class="input type1" value=""></textarea>
										</div>
									</td>
								</tr>																																								
							</tbody>
						</table>
					</div>
				</section>																
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" onclick="insertCorporationStatus();" class="btn type3 primary"><span>등록</span></button>
				<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</body>
</html>