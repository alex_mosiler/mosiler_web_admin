<!-- 고객 상세 팝업 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 고객관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>

  	<script type="text/javascript">
 		$(document).ready(function(){
			/* $("#couponList").click(function(){
				location.href = "/customer/customerCouponList?customerNo=${customerNo}"
			}); */
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
		}); 
		
		function cntPerPageChange() {
			var cntPerPage = $("#cntPerPage").val();
			location.href = "/customer/customerDetailpopUp?customerNo=${customerNo}&nowPage=${paging.nowPage}&cntPerPage="+cntPerPage;
		}

   		// 닫기 버튼
 		function selfClose() {
			self.close();
		}
	</script>  
</head>

<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">고객상세</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<section class="section">
					<div id=vip class="totalSum">
						<div>
							<span class="class" id=""><a href="javascript:;" onclick="window.open('/customer/customerGrade?customerNo=${customerNo}','','width=500,height=300');return false">VIP</a></span>
						</div>	
						<div class="infoArea">
							<c:forEach var="useInfo" items="${useInfo}" varStatus="idx">
								<ul class="infoList type1">									
									<li class="item-row grid">
										<div class="col left">
											<strong>정액권(시간)</strong>
										</div>
										<div class="col right">
											<span><a href="javascript:;" onclick="window.open('M010101-P03.html','','width=800,height=700');return false">999</a></span>
										</div>										
									</li>									
									<li class="item-row grid">
										<div class="col left">
											<strong>이용금액(원)</strong>
										</div>
										<div class="col right">
											<span>${useInfo.paymentFee}</span>
										</div>	
									</li>
									<li class="item-row grid">
										<div class="col left">
											<strong>이용건수(건)</strong>
										</div>
										<div class="col right">
											<span>${useInfo.useCount}</span>
										</div>										
									</li>
									<li class="item-row grid">
										<div class="col left">
											<strong>최종운행일</strong>
										</div>
										<div class="col right">
											<span>${useInfo.lastDriving}</span>
										</div>										
									</li>
									<li class="item-row grid">
										<div class="col left">
											<strong>상담(건)</strong>
										</div>
										<div class="col right">
											<span><a href="javascript:;" onclick="alert('QNA 이동\n준비중입니다.');">999</a></span>
										</div>										
									</li>
								</ul>		
							</c:forEach>						
						</div>								
					</div>
				</section>
				<section class="section">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h3 class="h3">가입정보</h3>
						</div>	
					</div>
					<div class="tbl-type1 tbl-lefted">
						<table>
							<colgroup>
								<col width="15%">
								<col width="*">
								<col width="15%">
								<col width="35%">
							</colgroup>
							<tbody>
								<c:forEach var="customerInfo" items="${customerInfo}" varStatus="idx">
									<input type="hidden" id="customerNo" value="${customerInfo.customerNo}">
									<tr>
										<th scope="row">고객명</th>
										<td>${customerInfo.customerName}</td>
										<th scope="row">가입일</th>
										<td>${customerInfo.regDt}</td>										
									</tr>
									<tr>
										<th scope="row">휴대폰 번호</th>
										<td>${customerInfo.phoneNumber}</td>
										<th scope="row">이메일</th>
										<td>${customerInfo.email}</td>										
									</tr>											
									<tr>
										<th scope="row">생년월일</th>
										<td><!-- DB에 정보 없음 --></td>												
										<th scope="row">차량정보</th>
										<td>${customerInfo.carNo} / ${customerInfo.carName}</td>									
									</tr>
									<tr>
										<th scope="row">즐겨찾는 장소1</th>
										<td colspan="3"><!-- DB에 정보 없음 --></td>
									</tr>
									<tr>
										<th scope="row">즐겨찾는 장소2</th>
										<td colspan="3"><!-- DB에 정보 없음 --></td>
									</tr>										
									<tr>
										<th scope="row">간편로그인 연동</th>
										<td colspan="3">
											<c:choose>
								    			<c:when test="${customerInfo.accountType eq 'email'}">
								    				<i class="ico ico-sns mail">mail</i>
								    			</c:when>
								    			<c:when test="${customerInfo.accountType eq 'kakao'}">
								    				<i class="ico ico-sns kakao">kakao</i>
								    			</c:when>
								    			<%-- 
								    			<c:when test="${customerInfo.accountType eq 'normal'}">
								    				
								    			</c:when>
								    			<c:when test="${customerInfo.accountType eq 'apple'}">
								    				
								    			</c:when>
								    			 --%>
								    		</c:choose>
											<!-- <i class="ico ico-sns facebook">facebook</i>
											<i class="ico ico-sns mail">mail</i>
											<i class="ico ico-sns navermail">navermail</i>
											<i class="ico ico-sns gmail">gmail</i>
											<i class="ico ico-sns naver">naver</i>
											<i class="ico ico-sns kakao">kakao</i> -->
										</td>			
									</tr>		
								</c:forEach>								
							</tbody>
						</table>
					</div>
				</section>
				<section class="section">
					<!-- S: Tab Area -->
					<div class="tab-nav type1">
					    <ul class="tab-list" role="tablist">
					        <li id="tabNav11" role="tab" aria-controls="tabContent11" aria-selected="true" class="tab is-selected">
					        	<button type="button" class="btn"><span>사용내역</span></button>
					        </li>
					        <li id="tabNav12" role="tab" aria-controls="tabContent12" aria-selected="false" class="tab">
					        	<button type="button" class="btn" id="couponList"><span>쿠폰내역</span></button>
					        </li>
					    </ul>
					</div>
					<div class="tab-container">
					    <!-- S: 사용내역 -->
					    <div id="tabContent11" aria-labelledby="tabNav11" aria-hidden="false" class="tab-content is-visible">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">사용현황</h2>
								</div>
								<div class="col mar-l10">
									<i class="ico ico-user pick">Pick</i>고객직접지정
								</div>	
								<div class="col right">
									<button type="button" class="btn type1 primary" onclick="window.open('/customer/customerReservationpopUp?customerNo=${customerNo}','','width=1000,height=936');return true"><span>예약등록</span></button>
								</div>			
							</div>						    
							<div class="gtbl-list l15">
								<table>
									<caption>사용내역 리스트</caption>
									<thead>
										<tr>
											<th>번호</th>
											<th>예약번호</th>
											<th>상품구분</th>
											<th>예약일시</th>
											<th>예약시간(분)</th>
											<th>운행일시</th>
											<th>운행시간(분)</th>
											<th>최종결제금액(원)</th>
											<th>운행상태</th>
											<th>결제상태</th>
											<th>파트너</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${customerUseInfo}" var="list" varStatus="status">
											<tr>
												<td>${list.rownum}</td>
												<td>${list.drivingNo}</td>
												<td>${list.drivingType}</td>
												<td>${list.revStartDt}</td>
												<td>${list.reservationDt}</td>
												<td>${list.startDt}</td>
												<td>${list.drivingDt}</td>
												<td>${list.paymentFee}</td>
												<td>${list.drivingStatus}</td>
												<td>${list.paymentYn}</td>
												<td>${list.driverName}</td>
											</tr>
										</c:forEach>																																																																																																																																																																																																																																																														
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div class="col total">페이지 <em>${paging.nowPage}</em>/${paging.lastPage} 결과 <em>${paging.total}</em>건</div>
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									       <c:choose>
									    		<c:when test="${paging.nowPage == 1 || paging.total == 0}">
									    			<button type="button" class="btn first" title="처음 페이지" disabled="disabled" ><span><i class="arw arw-board1-first gray"></i></span></button>
									        		<button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    		</c:when>
									    		<c:otherwise>
									    			<button type="button" class="btn first" title="처음 페이지"  onclick="location.href='/customer/customerDetailpopUp?nowPage=1&cntPerPage=${paging.cntPerPage}&customerNo=${customerNo}'"><span><i class="arw arw-board1-first gray"></i></span></button>
									        		<button type="button" class="btn prev" title="이전 페이지" onclick="location.href='/customer/customerDetailpopUp?nowPage=${paging.nowPage-1}&cntPerPage=${paging.cntPerPage}&customerNo=${customerNo}'"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    		</c:otherwise>
							    			</c:choose>	
									    </div>
									    <ul class="num-group">
									      <c:forEach begin="${paging.startPage}" end="${paging.endPage}" var="page"> 
							   					<c:choose>
							   						<c:when test="${page == paging.nowPage}">
							   							<li><button type="button" class="btn" aria-current="true" disabled="disabled"><span>${page}</span></button></li>
							   						</c:when>
									   				<c:when test="${page != paging.nowPage}">
									   					<li><button type="button" class="btn" onclick="location.href='/customer/customerDetailpopUp?nowPage=${page}&cntPerPage=${paging.cntPerPage}&customerNo=${customerNo}'"><span>${page}</span></button></li>
									   				</c:when>
							   					</c:choose>
							   				</c:forEach> 
									    </ul>
									    <div class="page-group">
									    	<c:choose>
									    		<c:when test="${paging.nowPage == paging.lastPage || paging.total == 0}">
									    			<button type="button" class="btn next" title="다음 페이지" disabled="disabled" ><span><i class="arw arw-board1-next gray"></i></span></button>
									        		<button type="button" class="btn last" title="마지막 페이지" disabled="disabled"><span><i class="arw arw-board1-last gray"></i></span></button>
									    		</c:when>
									    		<c:otherwise>
									    			<button type="button" class="btn next" title="다음 페이지" onclick="location.href='/customer/customerDetailpopUp?nowPage=${paging.nowPage+1}&cntPerPage=${paging.cntPerPage}&customerNo=${customerNo}'"><span><i class="arw arw-board1-next gray"></i></span></button>
									        		<button type="button" class="btn last" title="마지막 페이지" onclick="location.href='/customer/customerDetailpopUp?nowPage=${paging.lastPage}&cntPerPage=${paging.cntPerPage}&customerNo=${customerNo}'"><span><i class="arw arw-board1-last gray"></i></span></button>
									    		</c:otherwise>
									    	</c:choose>
									    </div>
								    </div>
								</div>
								<div class="col per-page">
									<select name="cntPerPage" id="cntPerPage" class="select h30" onChange="cntPerPageChange()">
									    <option value="10" <c:if test="${paging.cntPerPage == 10}">selected</c:if>>10</option>
									    <option value="20" <c:if test="${paging.cntPerPage == 20}">selected</c:if>>20</option>
									    <option value="30" <c:if test="${paging.cntPerPage == 30}">selected</c:if>>30</option>
									    <option value="40" <c:if test="${paging.cntPerPage == 40}">selected</c:if>>40</option>
									    <option value="50" <c:if test="${paging.cntPerPage == 50}">selected</c:if>>50</option>
									    <option value="100" <c:if test="${paging.cntPerPage == 100}">selected</c:if>>100</option>
									</select>	
								</div>			
							</div>
					    </div>
					    <!-- E: 사용내역 -->
					    <!-- S: 쿠폰내역 -->
					    <div id="tabContent12" aria-labelledby="tabNav12" aria-hidden="true" class="tab-content">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">쿠폰사용현황</h2>
								</div>		
							</div>							    
							<div class="gtbl-list l15">
								<table>
									<caption>쿠폰내역 리스트</caption>
									<thead>
										<tr>
											<th>번호</th>
											<th>쿠폰코드</th>
											<th>쿠폰명</th>
											<th>내용</th>
											<th>할인율</th>
											<th>할인가</th>
											<th>유효기간</th>
											<th>발급일</th>
											<th>사용여부</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${couponList}" var="list" varStatus="status">
											<tr>
												<td>${list.rownum}</td>
												<td>${list.redeemCode}</td>
												<td>${list.promotionCode}</td>
												<td>${list.description}</td>
												<td>${list.disCountRate}</td>
												<td>${list.disCountAmount}</td>
												<td>${list.expiredDt}</td>
												<td>${list.activatedDt}</td>
												<td>-</td>
											</tr>
										</c:forEach>																																																																																																																																																																																																																																													
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div class="col total">페이지 <em>1</em>/129 결과 <em>2,577</em>건</div>
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									        <button type="button" class="btn first" title="처음 페이지" disabled="disabled"><span><i class="arw arw-board1-first gray"></i></span></button>
									        <button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    </div>
										<div class="page-state">
											<span class="curPage">1</span>
											<span class="totPage">3</span>
										</div>	
									    <ul class="num-group">
									        <li><button type="button" class="btn" title="1 페이지" aria-current="true" disabled="disabled"><span>1</span></button></li>
									        <li><button type="button" class="btn" title="2 페이지"><span>2</span></button></li>
									        <li><button type="button" class="btn" title="3 페이지"><span>3</span></button></li>
									    </ul>
									    <div class="page-group">
									        <button type="button" class="btn next" title="다음 페이지"><span><i class="arw arw-board1-next gray"></i></span></button>
									        <button type="button" class="btn last" title="마지막 페이지"><span><i class="arw arw-board1-last gray"></i></span></button>
									    </div>
								    </div>
								</div>
								<div class="col per-page">
									<select name="" id="" class="select h30">
									    <option value="">10</option>
									    <option value="">20</option>
									    <option value="">30</option>
									    <option value="">40</option>
									    <option value="">50</option>
									    <option value="">100</option>
									</select>	
								</div>			
							</div>
					    </div>
					    <!-- E: 쿠폰내역  -->
					</div>
					<!-- e: Tab Area -->	
				</section>
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" onclick="selfClose();" class="btn type3 primary"><span>확인</span></button>
				<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>
</div>
</body>
</html>