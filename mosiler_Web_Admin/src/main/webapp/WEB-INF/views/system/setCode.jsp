<!-- 코드 관리 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 시스템</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>  
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>    	
   	<script src="/js/util.js"></script>
   	
	<!-- 메뉴구현부  -->
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			var cateList = ${cateList};
			var depthList = ${depthList};
			
			var cateMenuCode = 'M'+'07'+'0000';
			var depthMenuCode1 = 'M'+'07'+'09'+'00';
			var depthMenuCode2 = 'M070901';
	
			makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2);
		});
	</script>   	
</head>

<body>
	<div class="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">					
		
			<%@ include file="../header2.jsp" %>
			<!-- Content -->
			<div id="content" class="in-sec mar-t-10">
				<section class="section grid gut-20">					
					<div class="col col-8">
						<!-- 상품 현황 -->
						<section>
							<div class="form-search grid flex-center cross-center">
								<ul>
									<li>
										<label class="form-tit">분류코드</label>
										<div class="data-group">
											<select name="sSelect1" id="sSelect1" class="select h30">
											    <option value="">전체</option>
											</select>									
											<input type="text" name="" id="inputDate" value="코드ID/코드ID명" placeholder="" title="" class="input h30 w200">
										</div>
									</li>								
								</ul>
								<div>
									<button type="button" class="btn-search"><span>검색</span></button>
								</div>					
							</div>
						</section>
						<!-- section -->
						<section class="section">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">공통코드</h2>
								</div>
								<div class="col right">
									<button type="button" class="btn type1 primary"><span>등록</span></button>
									<button type="button" class="btn type1 primary"><span>삭제</span></button>
								</div>			
							</div>
							<div class="Mguide">
		   						모바일에서 표를 좌우로 스크롤 할 수 있습니다. 
		  					</div>
							<div class="gtbl-list l15">
								<table class="x-auto">
									<caption></caption>
									<thead>
										<tr>
											<th scope="col">
												<span class="check type1">
												    <input type="checkbox" name="sCheckall" id="sCheckall" value="">
												    <label for="sCheckall"><span class="blind">전체선택</span></label>
												</span>									
											</th>	
											<th scope="col">분류코드</th>
											<th scope="col">코드ID</th>
											<th scope="col">코드ID명</th>
											<th scope="col">정렬순서</th>
											<th scope="col">금액</th>
											<th scope="col">코드ID설명</th>
											<th scope="col">사용여부</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck01" id="sCheck01" value="">
												    <label for="sCheck01"><span class="blind">선택</span></label>
												</span>									
											</td>	
											<td>1</td>
											<td>1</td>
											<td>운행상태</td>
											<td>1</td>
											<td class="align-r">75,000</td>
											<td class="align-l">코드의 코드명은 드라이버 기준이며 고객 기준은 서비스에 별도로 정의함</td>
											<td>ON</td>
										</tr>																																																																																																																																																																																																																																																																																																																																																																																																																											
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div class="col total">페이지 <em>1</em>/129 결과 <em>2,577</em>건</div>
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									        <button type="button" class="btn first" title="처음 페이지" disabled="disabled"><span><i class="arw arw-board1-first gray"></i></span></button>
									        <button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    </div>
										<div class="page-state">
											<span class="curPage">1</span>
											<span class="totPage">3</span>
										</div>	
									    <ul class="num-group">
									        <li><button type="button" class="btn" title="1 페이지" aria-current="true" disabled="disabled"><span>1</span></button></li>
									        <li><button type="button" class="btn" title="2 페이지"><span>2</span></button></li>
									        <li><button type="button" class="btn" title="3 페이지"><span>3</span></button></li>
									    </ul>
									    <div class="page-group">
									        <button type="button" class="btn next" title="다음 페이지"><span><i class="arw arw-board1-next gray"></i></span></button>
									        <button type="button" class="btn last" title="마지막 페이지"><span><i class="arw arw-board1-last gray"></i></span></button>
									    </div>
								    </div>
								</div>
								<div class="col per-page">
									<select name="" id="" class="select h30">
									    <option value="">10</option>
									    <option value="">20</option>
									    <option value="">30</option>
									    <option value="">40</option>
									    <option value="">50</option>
									    <option value="">100</option>
									</select>	
								</div>			
							</div>		
						</section>
						<!-- //section -->						
						<!-- //상품 현황 -->
					</div>
					<div class="col col-4">
						<!-- 상품 등록 -->
						<section>
							<div class="tbl-type1 tbl-lefted">
								<table>
									<colgroup>
										<col width="20%">
										<col width="*">
										<col width="20%">
										<col width="30%">										
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">분류코드</th>
											<td colspan="3">
												<select name="sSelect1" id="sSelect1" class="select h25">
												    <option value="">전체</option>
												</select>
											</td>						
										</tr>
										<tr>
											<th scope="row">코드ID</th>
											<td colspan="3">
												<input type="text" name="" id="" value="" placeholder="" title="" class="input h25 W100">
											</td>						
										</tr>
										<tr>
											<th scope="row">코드ID명</th>
											<td colspan="3">
												<input type="text" name="" id="" value="" placeholder="" title="" class="input h25 W100">
											</td>						
										</tr>										
										<tr>
											<th scope="row">정렬순서</th>
											<td>
												<input type="text" name="" id="" value="" placeholder="0" title="" class="input h25 w120">
											</td>
											<th scope="row">금액</th>
											<td>
												<input type="text" name="" id="" value="" placeholder="0" title="" class="input h25 w120 align-r">									
											</td>																		
										</tr>																																	
										<tr>
											<th scope="row">코드ID설명</th>
											<td colspan="3">
												<textarea rows="3" cols="50" class="input type1"></textarea>									
											</td>
										</tr>
										<tr>
											<th scope="row">사용여부</th>
											<td colspan="3">
												<span class="switch type1">
												    <input type="checkbox" name="sSwitch2" id="sSwitch22" value="" checked="">
												    <label for="sSwitch22"></label>
												</span>											
											</td>																		
										</tr>						
									</tbody>
								</table>
							</div>
						</section>	
						<section class="section">	
							<div class="grid cross-center">
								<div class="col right">
									<button type="button" class="btn type1 primary"><span>저장</span></button>
									<button type="button" class="btn type1 secondary"><span>취소</span></button>
								</div>			
							</div>
						</section>											
						<!-- //상품 등록 -->					
					</div>
				</section>			

			</div>	
			<!-- //Content -->
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='../index.html'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="window.open('M040101-P01.html','','width=100%,height=100%'); return false;">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->		
	</div>
</body>
</html>