<!-- 권한 관리 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 시스템</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>  
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>    
   	<script src="/js/util.js"></script>
   	
   	<!-- 메뉴구현부  -->
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			var cateList = ${cateList};
			var depthList = ${depthList};
			
			var cateMenuCode = 'M'+'07'+'0000';
			var depthMenuCode1 = 'M'+'07'+'02'+'00';
			var depthMenuCode2 = 'M070201';
	
			makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2);
		});
	</script>   	
</head>

<body>
	<div class="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">					
			<%@ include file="../header2.jsp" %>
			<!-- Content -->
			<div id="content" class="in-sec mar-t-10">
				<section class="section grid gut-20">					
					<div class="col col-6">
						<!-- 운영자관리 현황 -->
						<section>
							<div class="form-search grid flex-center cross-center">
								<ul>
									<li>
										<label class="form-tit">소속</label>
										<div class="data-group">
											<select name="" id="sSelect1" class="select h30">
											    <option value="">전체</option>
											</select>									
										</div>
									</li>
									<li>
										<label class="form-tit">검색</label>
										<div class="data-group">
											<select name="sSelect1" id="sSelect1" class="select h30">
											    <option value="">선택일</option>
											</select>									
											<input type="text" name="" id="inputDate" value="고객명/휴대폰번호/이메일" placeholder="" title="" class="input h30 w200">
										</div>
									</li>								
								</ul>
								<div>
									<button type="button" class="btn-search"><span>검색</span></button>
								</div>					
							</div>
						</section>
						<!-- section -->
						<section class="section">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">권한관리현황</h2>
								</div>
								<div class="col right">
									<button type="button" class="btn type1 secondary"><span>엑셀다운로드</span></button>
								</div>			
							</div>
							<div class="Mguide">
		   						모바일에서 표를 좌우로 스크롤 할 수 있습니다. 
		  					</div>
							<div class="gtbl-list l15">
								<table>
									<caption></caption>
									<thead>
										<tr>
											<th scope="col">번호</th>
											<th scope="col">소속</th>
											<th scope="col">접근가능메뉴</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>99</td>
											<td>전체관리</td>
											<td>보기</td>
										</tr>
										<tr>
											<td>98</td>
											<td>총무회계팀</td>
											<td>보기</td>
										</tr>	
										<tr>
											<td>97</td>
											<td>교육팀</td>
											<td>보기</td>
										</tr>	
										<tr>
											<td>96</td>
											<td>인재개발팀</td>
											<td>보기</td>
										</tr>																																																																																																																																																																																																																																																																																																																																																																																																		
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div class="col total">페이지 <em>1</em>/129 결과 <em>2,577</em>건</div>
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									        <button type="button" class="btn first" title="처음 페이지" disabled="disabled"><span><i class="arw arw-board1-first gray"></i></span></button>
									        <button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    </div>
										<div class="page-state">
											<span class="curPage">1</span>
											<span class="totPage">3</span>
										</div>	
									    <ul class="num-group">
									        <li><button type="button" class="btn" title="1 페이지" aria-current="true" disabled="disabled"><span>1</span></button></li>
									        <li><button type="button" class="btn" title="2 페이지"><span>2</span></button></li>
									        <li><button type="button" class="btn" title="3 페이지"><span>3</span></button></li>
									    </ul>
									    <div class="page-group">
									        <button type="button" class="btn next" title="다음 페이지"><span><i class="arw arw-board1-next gray"></i></span></button>
									        <button type="button" class="btn last" title="마지막 페이지"><span><i class="arw arw-board1-last gray"></i></span></button>
									    </div>
								    </div>
								</div>
								<div class="col per-page">
									<select name="" id="" class="select h30">
									    <option value="">10</option>
									    <option value="">20</option>
									    <option value="">30</option>
									    <option value="">40</option>
									    <option value="">50</option>
									    <option value="">100</option>
									</select>	
								</div>			
							</div>		
						</section>
						<!-- //section -->						
						<!-- //운영자관리 현황 -->
					</div>
					<div class="col col-6">
						<!-- 운영자관리 등록 -->
						<section>
							<div class="form-search grid flex-center cross-center">
								<ul>
									<li>
										<label class="form-tit">소속</label>
										<div class="data-group">
											<select name="" id="sSelect1" class="select h30">
											    <option value="">전체관리</option>
											    <option value="">총무회계팀</option>
											    <option value="">교육팀</option>
											    <option value="">인재개발팀</option>
											</select>										
										</div>
									</li>
									<li>
										<label class="form-tit">개인정보접근</label>
										<div class="data-group">
											<span class="radio type1">
										    	<input type="radio" name="sRadioNo" id="sRadioNo" value="">
										    	<label for="sRadioNo"><span>불가</span></label>
											</span>
											<span class="radio type1">
										    	<input type="radio" name="sRadioYes" id="sRadioYes" value="" checked="">
										    	<label for="sRadioYes"><span>가능</span></label>
											</span>											
										</div>
									</li>								
								</ul>					
							</div>
						</section>						
						<section class="section">
							<div class="tbl-type1 tbl-lefted">
								<table>
									<colgroup>
										<col width="20%">
										<col width="*">								
									</colgroup>
									<thead>
										<tr>
											<th scope="col">1Depth</th>
											<th scope="col">2Depth</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck10" id="sCheck10" value="" checked="">
												    <label for="sCheck10"><span>고객관리</span></label>
												</span>											
											</td>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck11" id="sCheck11" value="">
												    <label for="sCheck11"><span>고객</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck12" id="sCheck12" value="">
												    <label for="sCheck12"><span>Biz</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck13" id="sCheck13" value="">
												    <label for="sCheck13"><span>임시</span></label>
												</span>	
												<span class="check type1">
												    <input type="checkbox" name="sCheck14" id="sCheck14" value="">
												    <label for="sCheck14"><span>휴먼탈퇴제명</span></label>
												</span>																																																
											</td>
										</tr>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck20" id="sCheck20" value="" checked="">
												    <label for="sCheck20"><span>교육관리</span></label>
												</span>											
											</td>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck21" id="sCheck21" value="">
												    <label for="sCheck21"><span>교육일정</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck22" id="sCheck22" value="">
												    <label for="sCheck22"><span>커리큘럼관리</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck23" id="sCheck23" value="">
												    <label for="sCheck23"><span>교육신청현황</span></label>
												</span>	
												<span class="check type1">
												    <input type="checkbox" name="sCheck24" id="sCheck24" value="">
												    <label for="sCheck24"><span>파트너평가서</span></label>
												</span>	
												<span class="check type1">
												    <input type="checkbox" name="sCheck25" id="sCheck25" value="">
												    <label for="sCheck25"><span>교육결과</span></label>
												</span>																																																											
											</td>
										</tr>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck30" id="sCheck30" value="" checked="">
												    <label for="sCheck30"><span>종합현황</span></label>
												</span>											
											</td>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck31" id="sCheck31" value="">
												    <label for="sCheck31"><span>예약현황</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck32" id="sCheck32" value="">
												    <label for="sCheck32"><span>운행중현황</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck33" id="sCheck33" value="">
												    <label for="sCheck33"><span>운행완료현황</span></label>
												</span>																																																										
											</td>
										</tr>							
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck40" id="sCheck40" value="" checked="">
												    <label for="sCheck40"><span>결제관리</span></label>
												</span>											
											</td>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck41" id="sCheck41" value="">
												    <label for="sCheck41"><span>결제현황</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck42" id="sCheck42" value="">
												    <label for="sCheck42"><span>Biz결제관리</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck43" id="sCheck43" value="">
												    <label for="sCheck43"><span>정액권결제</span></label>
												</span>	
												<span class="check type1">
												    <input type="checkbox" name="sCheck44" id="sCheck44" value="">
												    <label for="sCheck44"><span>정기권결제</span></label>
												</span>																																																											
											</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck50" id="sCheck50" value="" checked="">
												    <label for="sCheck50"><span>정산관리</span></label>
												</span>											
											</td>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck51" id="sCheck51" value="">
												    <label for="sCheck51"><span>파트너정산서</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck52" id="sCheck52" value="">
												    <label for="sCheck52"><span>보증금공제</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck53" id="sCheck53" value="">
												    <label for="sCheck53"><span>사고공제</span></label>
												</span>	
												<span class="check type1">
												    <input type="checkbox" name="sCheck54" id="sCheck54" value="">
												    <label for="sCheck54"><span>법칙금공제</span></label>
												</span>	
												<span class="check type1">
												    <input type="checkbox" name="sCheck55" id="sCheck55" value="">
												    <label for="sCheck55"><span>위반이력</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck56" id="sCheck56" value="">
												    <label for="sCheck56"><span>재고관리</span></label>
												</span>																																																																																				
											</td>
										</tr>
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck60" id="sCheck60" value="" checked="">
												    <label for="sCheck60"><span>통계</span></label>
												</span>											
											</td>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck61" id="sCheck61" value="">
												    <label for="sCheck61"><span>매출통계</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck62" id="sCheck62" value="">
												    <label for="sCheck62"><span>운행통계</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck63" id="sCheck63" value="">
												    <label for="sCheck63"><span>파트너통계</span></label>
												</span>	
												<span class="check type1">
												    <input type="checkbox" name="sCheck64" id="sCheck64" value="">
												    <label for="sCheck64"><span>쿠폰통계</span></label>
												</span>																																																																																					
											</td>
										</tr>	
										<tr>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck70" id="sCheck70" value="" checked="">
												    <label for="sCheck70"><span>시스템</span></label>
												</span>											
											</td>
											<td>
												<span class="check type1">
												    <input type="checkbox" name="sCheck71" id="sCheck71" value="">
												    <label for="sCheck71"><span>운영자관리</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck72" id="sCheck72" value="">
												    <label for="sCheck72"><span>권한관리</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck73" id="sCheck73" value="">
												    <label for="sCheck73"><span>쿠폰관리</span></label>
												</span>	
												<span class="check type1">
												    <input type="checkbox" name="sCheck74" id="sCheck74" value="">
												    <label for="sCheck74"><span>설문지관리</span></label>
												</span>	
												<span class="check type1">
												    <input type="checkbox" name="sCheck75" id="sCheck75" value="">
												    <label for="sCheck75"><span>공지사항</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck76" id="sCheck76" value="">
												    <label for="sCheck76"><span>QNA</span></label>
												</span>
												<span class="check type1">
												    <input type="checkbox" name="sCheck64" id="sCheck77" value="">
												    <label for="sCheck77"><span>FAQ</span></label>
												</span>																																																																																																																								
											</td>
										</tr>																																												
									</tbody>
								</table>
							</div>
						</section>	
						<section class="section">	
							<div class="grid cross-center">
								<div class="col right">
									<button type="button" class="btn type1 primary"><span>저장</span></button>
									<button type="button" class="btn type1 secondary"><span>취소</span></button>
								</div>			
							</div>
						</section>											
						<!-- //운영자관리 등록 -->					
					</div>
				</section>

			</div>	
			<!-- //Content -->
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='../index.html'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="window.open('M040101-P01.html','','width=100%,height=100%'); return false;">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->			
	</div>
</body>
</html>