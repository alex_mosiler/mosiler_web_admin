<!-- 상품 관리 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 시스템</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>   
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>    	
   	<script src="/js/util.js"></script> 	
   	
	<!-- 메뉴구현부  -->
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			var cateList = ${cateList};
			var depthList = ${depthList};
			
			var cateMenuCode = 'M'+'07'+'0000';
			var depthMenuCode1 = 'M'+'07'+'03'+'00';
			var depthMenuCode2 = 'M070301';
	
			makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2);
		});
	</script>   	
</head>

<body>
	<div class="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">					
			<%@ include file="../header2.jsp" %>
			<!-- Content -->
			<div id="content" class="in-sec mar-t-10">
				<section class="section grid gut-20">					
					<div class="col col-8">
						<!-- 상품 현황 -->
						<section>
							<div class="form-search grid flex-center cross-center">
								<ul>
									<li>
										<label class="form-tit">검색</label>
										<div class="data-group">
											<select name="sSelect1" id="sSelect1" class="select h30">
											    <option value="">선택일</option>
											</select>									
											<input type="text" name="" id="inputDate" value="고객명/휴대폰번호/이메일" placeholder="" title="" class="input h30 w200">
										</div>
									</li>								
								</ul>
								<div>
									<button type="button" class="btn-search"><span>검색</span></button>
								</div>					
							</div>
						</section>
						<!-- section -->
						<section class="section">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">상품현황</h2>
								</div>
								<div class="col right">
									<button type="button" class="btn type1 secondary"><span>엑셀다운로드</span></button>
								</div>			
							</div>
							<div class="Mguide">
		   						모바일에서 표를 좌우로 스크롤 할 수 있습니다. 
		  					</div>
							<div class="gtbl-list l15">
								<table class="x-auto">
									<caption></caption>
									<thead>
										<tr>
											<th scope="col">번호</th>
											<th scope="col">카테고리</th>
											<th scope="col">상품코드</th>
											<th scope="col">상품명</th>
											<th scope="col">기본시간</th>
											<th scope="col">기본금액</th>
											<th scope="col">추가계산</th>
											<th scope="col">반영일시</th>
											<th scope="col">설명</th>
											<th scope="col">등록일</th>
											<th scope="col">등록자</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>99</td>
											<td>특화</td>
											<td>golf-01</td>
											<td>골프</td>
											<td>10시간</td>
											<td>160,000</td>
											<td>300원/분</td>
											<td>YYYY-MM-DD hh:mm</td>
											<td class="align-l">골프장 운행 최고의 파트너<br>18홀 65타. 최상의 컨디션을 만들어 드리겠습니다.</td>
											<td>YYYY-MM-DD</td>
											<td>admin</td>
										</tr>
										<tr>
											<td>98</td>
											<td>일반</td>
											<td>kids-01</td>
											<td>키즈</td>
											<td>2시간</td>
											<td>40,000</td>
											<td>300원/분</td>
											<td>YYYY-MM-DD hh:mm</td>											
											<td class="align-l">우리 아이들의 로드매니저<br>내 아이에게 특화된 운행 서비스를 제공합니다.</td>
											<td>YYYY-MM-DD</td>
											<td>admin</td>
										</tr>
										<tr>
											<td>97</td>
											<td>일반</td>
											<td>air-01</td>
											<td>에어</td>
											<td>4시간</td>
											<td>80,000</td>
											<td>300원/분</td>
											<td>YYYY-MM-DD hh:mm</td>											
											<td class="align-l">나의 공항리무진 드라이버 <br>이동은 내차로 편하게 주차는 집앞에 안전하게</td>
											<td>YYYY-MM-DD</td>
											<td>admin</td>
										</tr>	
										<tr>
											<td>96</td>
											<td>일반</td>
											<td>silver-01</td>
											<td>실버</td>
											<td>6시간</td>
											<td>120,000</td>
											<td>300원/분</td>
											<td>YYYY-MM-DD hh:mm</td>											
											<td class="align-l">우리 부모님의 일일 집사  <br>병원 동행으로 진료표 작성부터 소견전달까지</td>
											<td>YYYY-MM-DD</td>
											<td>admin</td>
										</tr>
										<tr>
											<td>95</td>
											<td>제휴</td>
											<td>ebridge-01</td>
											<td>비즈</td>
											<td>2시간</td>
											<td>40,000</td>
											<td>300원/분</td>
											<td>YYYY-MM-DD hh:mm</td>											
											<td class="align-l">주 52시간 제약없는 솔루션<br>드라이버 주52시간문제 모시러가 해결해 드립니다. </td>
											<td>YYYY-MM-DD</td>
											<td>admin</td>
										</tr>																																																																																																																																																																																																																																																																																																																																																																																																																														
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div class="col total">페이지 <em>1</em>/129 결과 <em>2,577</em>건</div>
								<div class="col">
									<div class="paging paging-basic">
									    <div class="page-group">
									        <button type="button" class="btn first" title="처음 페이지" disabled="disabled"><span><i class="arw arw-board1-first gray"></i></span></button>
									        <button type="button" class="btn prev" title="이전 페이지" disabled="disabled"><span><i class="arw arw-board1-prev gray"></i></span></button>
									    </div>
										<div class="page-state">
											<span class="curPage">1</span>
											<span class="totPage">3</span>
										</div>	
									    <ul class="num-group">
									        <li><button type="button" class="btn" title="1 페이지" aria-current="true" disabled="disabled"><span>1</span></button></li>
									        <li><button type="button" class="btn" title="2 페이지"><span>2</span></button></li>
									        <li><button type="button" class="btn" title="3 페이지"><span>3</span></button></li>
									    </ul>
									    <div class="page-group">
									        <button type="button" class="btn next" title="다음 페이지"><span><i class="arw arw-board1-next gray"></i></span></button>
									        <button type="button" class="btn last" title="마지막 페이지"><span><i class="arw arw-board1-last gray"></i></span></button>
									    </div>
								    </div>
								</div>
								<div class="col per-page">
									<select name="" id="" class="select h30">
									    <option value="">10</option>
									    <option value="">20</option>
									    <option value="">30</option>
									    <option value="">40</option>
									    <option value="">50</option>
									    <option value="">100</option>
									</select>	
								</div>			
							</div>		
						</section>
						<!-- //section -->						
						<!-- //상품 현황 -->
					</div>
					<div class="col col-4">
						<!-- 상품 등록 -->
						<section>
							<div class="tbl-type1 tbl-lefted">
								<table>
									<colgroup>
										<col width="20%">
										<col width="*">
										<col width="20%">
										<col width="30%">										
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">카테고리</th>
											<td>
												<select name="" id="sSelect1" class="select h25">
												    <option value="">일반</option>
												    <option value="">특화</option>
												    <option value="">제휴</option>
												</select>																				
											</td>	
											<th scope="row">상품코드</th>
											<td>
												<select name="" id="sSelect1" class="select h25">
												    <option value="">kids-01</option>
												    <option value="">silver-01</option>
												    <option value="">air-01</option>
												    <option value="">shopping-01</option>
												</select>										
											</td>									
										</tr>
										<tr>
											<th scope="row">상품명</th>
											<td colspan="3">
												<input type="text" name="" id="" value="" placeholder="" title="" class="input h25 W100">
											</td>																		
										</tr>
										<tr>
											<th scope="row">기본시간</th>
											<td>
												<select name="" id="sSelect1" class="select h25">
												    <option value="">1시간</option>
												    <option value="">2시간</option>
												    <option value="">3시간</option>
												    <option value="">4시간</option>
												    <option value="">5시간</option>
												</select>	
											</td>
											<th scope="row">기본금액(원)</th>
											<td>
												<input type="text" name="" id="" value="" placeholder="" title="" class="input h25 W100">
											</td>																			
										</tr>	
										<tr>
											<th scope="row">추가계산(원)</th>
											<td>
												<input type="text" name="" id="" value="" placeholder="" title="" class="input h25 w100">
												<span class="static">/분</span>
											</td>
											<th scope="row">반영여부</th>
											<td>
												<span class="switch type1">
												    <input type="checkbox" name="sSwitch2" id="sSwitch22" value="" checked="">
												    <label for="sSwitch22"></label>
												</span>												
											</td>																			
										</tr>																			
										<tr>
											<th scope="row">상품설명</th>
											<td colspan="3">
												<textarea rows="3" cols="50" class="input type1"></textarea>									
											</td>
										</tr>						
									</tbody>
								</table>
							</div>
						</section>	
						<section class="section">	
							<div class="grid cross-center">
								<div class="col right">
									<button type="button" class="btn type1 primary"><span>저장</span></button>
									<button type="button" class="btn type1 secondary"><span>취소</span></button>
								</div>			
							</div>
						</section>											
						<!-- //상품 등록 -->					
					</div>
				</section>			

			</div>	
			<!-- //Content -->
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='../index.html'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="window.open('M040101-P01.html','','width=100%,height=100%'); return false;">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->			
	</div>
</body>
</html>