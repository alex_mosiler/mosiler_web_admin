<!-- 운영자 관리 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 시스템</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>  
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>    	
   	<script src="/js/util.js"></script>
   	
	<!-- 메뉴구현부  -->
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			var adminUserList = ${adminUserList};	
			var cateList = ${cateList};
			var depthList = ${depthList};
			
			var cateMenuCode = 'M'+'07'+'0000';
			var depthMenuCode1 = 'M'+'07'+'01'+'00';
			var depthMenuCode2 = 'M070101';
			
			makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2);
			
			// 현재 페이지 번호 세팅
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			// 엔터키 이벤트 
		 	$("#searchKeyword").keypress(function(e) {
				if(e.which == 13) {
					search();
				}
			}); 
			
			// 리스트
			makeContents(adminUserList);
			// 페이징
			makePagingVar(${pagingVO.nowPage}, ${pagingVO.startPage}, ${pagingVO.endPage}, ${pagingVO.lastPage});
		});
		
		// 리스트
		function makeContents(adminUserList) {
			var html = "";
			var cntPerPage = ${pagingVO.cntPerPage};
			
			for(var i = 0; i < cntPerPage; i++) {
				if(adminUserList[i] == null) {
					break;
				} else {
					html += "<tr style='cursor: pointer' onclick='javascript:selectUser("+i+")'>";
					html += "	<input type='hidden' id='otpKey"+i+"' value="+adminUserList[i].otpKey+" >";
					html += "	<input type='hidden' id='user_no"+i+"' value="+adminUserList[i].userNo+" >";
					html += "	<td>"+adminUserList[i].rownum+"</td>";
					html += "	<td id='user_id"+i+"'>"+adminUserList[i].userId+"</a></td>"
					html += "	<td id='user_name"+i+"'>"+adminUserList[i].userName+"</td>";
					html += "	<td id='user_email"+i+"'>"+adminUserList[i].email+"</td>";
					html += "	<td id='user_phoneNo"+i+"'>"+adminUserList[i].phoneNumber+"</td>";
					html += "	<td style='width:30px;'><button type='button' class='btn-search' onclick='pwdReset("+i+");'><span>초기화</span></button></td>"; 
					html += "<tr>";
				}
			}
			
			$("#listDiv").append(html);
		}

		// 페이징 바
		function makePagingVar() {
			var nowPage = ${pagingVO.nowPage};
			var startPage = ${pagingVO.startPage};
			var endPage = ${pagingVO.endPage};
			var lastPage = ${pagingVO.lastPage};
		
			var html = "";
			
			html += "<div class=\"col'\">";
			html += "	<div class=\"paging paging-basic\">";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == 1) {
				html += "		<button type=\"button\" class=\"btn first\" title=\"처음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn prev\" title=\"이전 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"firstPage\" class=\"btn first\" title=\"처음 페이지\"  onclick='arrowBtn(1);'><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" id=\"prevPage\" class=\"btn prev\" title=\"이전 페이지\" onclick='arrowBtn(2);'><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			}
			
			html += "		</div>";
			html += "		<ul class=\"num-group\">";
			
			for(var i = startPage; i <= endPage; i++) {
				if(i == nowPage) {
					html += "	<li><button type=\"button\" class=\"btn\" aria-current=\"true\" disabled=\"disabled\"><span>"+i+"</span></button></li>";
				} else {
					html += "	<li><button type=\"button\" id=\"e\" class=\"btn\" onclick='numberBtn("+i+");'><span>"+i+"</span></button></li>"
				}
			}
			
			html += "		</ul>";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == lastPage) {
				html += "		<button type=\"button\" class=\"btn next\" title=\"다음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-next gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn last\" title=\"마지막 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"nextPage\" class=\"btn next\" title=\"다음 페이지\" onclick='arrowBtn(3);'><span><i class=\"arw arw-board1-next gray\"></i></span></button>";			
				html += "		<button type=\"button\" id=\"lastPage\" class=\"btn last\" title=\"마지막 페이지\" onclick='arrowBtn(4);'><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			}
			
			html += "		</div>";		
			html += "	</div>";
			html += "</div>";	
			
			$("#pagingVar").append(html);
		}
		
		// 유저 선택 시 상세 표시
		function selectUser(num) {
			$("#selectUserNo").val($("#user_no"+num).val());
			$("#sOtpKey").val($("#otpKey"+num).val());
			
			$("#sId").val($("#user_id"+num).text());
			$("#sName").val($("#user_name"+num).text());
			$("#sEmail").val($("#user_email"+num).text());
			$("#sPhoneNo").val($("#user_phoneNo"+num).text());
		}
		
		// 검색
 		function search() {
			//submit
			document.adminUserForm.submit();
		} 
		
		// 검색 초기화
		function keywordReset() {
			$("#searchType option:eq(0)").prop("selected", true);
			$("#searchKeyword").val("");
		}		
		
		// 한 페이지에 보여줄 row수 전환 
		function cntPerPageChange() {
			// 현재 페이지 번호 저장
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			// submit
			document.adminUserForm.submit(); 
		}

		// 페이징 번호 버튼
		function numberBtn(num) {
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(num);
			
			// submit
			document.adminUserForm.submit(); 
		}
		
		// 페이징 화살표 버튼
		function arrowBtn(num) {
			var nowPage = ${pagingVO.nowPage};
			
			if(num == 1) {
				// 처음
				nowPage = 1;
			} else if(num == 2) {
				// 이전
				nowPage = nowPage - 1;
			} else if(num == 3) {
				// 다음
				nowPage = nowPage + 1;
			} else {
				// 마지막
				nowPage = ${pagingVO.lastPage};
			}
			
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(nowPage);
			
			// submit
			document.adminUserForm.submit(); 
		}
		
		// 비밀번호 초기화 버튼
		function pwdReset(num) {
			var phoneNumber 	= $("#user_phoneNo"+num).text();
			var userNo 			  	= $("#user_no"+num).val();
			
			if(phoneNumber == "") {
				alert("등록된 연락처가 없으므로 비밀번호를 초기화 할 수 없습니다.");
				return false;
			} else {
				if(confirm("비밀번호가 연락처로 초기화 됩니다. 계속 하시겠습니까?")) {
					$.ajax({
						url: "/system/pwdReset"
						, type: "POST"
						, data: {
							phoneNumber : phoneNumber
							, userNo : userNo
						}, success: function(resultCode){
							if(resultCode == 200) {
								alert("초기화 되었습니다.");
								location.reload();
							} else {
								alert("초기화에 실패 하였습니다.");
							}
						}, error:  function(data, status, opt) {
							alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
					        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
					    }
					});
				}
			}
		}
		
		// 어드민 유저 정보 수정
		function updateChangeUser() {
			var userNo 				= $("#selectUserNo").val();
			var userId 				= $("#sId").val();
			var userPw 				= $("#sPw").val();
			var userName 			= $("#sName").val();
			var email 				= $("#sEmail").val();
			var phoneNumber 	= $("#sPhoneNo").val();
			var otpKey 				= $("#sOtpKey").val();
			
			if(phoneNumber == "") {
				alert("연락처는 필수 값입니다.");
				return false;
			}
			
			if(confirm("수정 하시겠습니까?")) {
				$.ajax({
					url: "/system/updateAdminUser"
					, type: "POST"
					, data: {
						userNo : userNo
						, userId : userId
						, userPw : userPw
						, userName : userName
						, email : email
						, phoneNumber : phoneNumber
						, otpKey : otpKey
					}
					, success: function(resultCode) {
						if(resultCode == 200) {
							alert("수정 되었습니다.");
							location.reload();
						} else {
							alert("정보 수정에 실패 하였습니다.");
						}
					}
					, error: function(data, status, opt) {
						alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
				        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
				    }
				});
			} 
		}
		
		// OTP 키 발급
		function getOtpKey() {
			var userNo = $("#selectUserNo").val();
			
			if(userNo == "" || userNo == null || userNo == undefined) {
				alert("발급 받을 운영자를 선택해 주세요.");
				return false;
			} else if((userNo != "" || userNo != null || userNo != undefined) && $("#sOtpKey").val() != "") {
				if(confirm("이미 인증키가 있는 경우 재발급 시 구글 OTP 앱에서 재등록이 필요합니다. 정말 재발급 하시겠습니까?")) {
					$.ajax({
						url: "/system/getOtpKey"
						, type: "POST"
						, success: function(otpKey) {
							$("#sOtpKey").val(otpKey);
						}
						, error: function(data, status, opt) {
							alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
					        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
					    }
					});				
				}
			} else {
				$.ajax({
					url: "/system/getOtpKey"
					, type: "POST"
					, success: function(otpKey) {
						$("#sOtpKey").val(otpKey);
					}
					, error: function(data, status, opt) {
						alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
				        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
				    }
				});		
			}
		}
	</script>   	
</head>

<body>
<form name="adminUserForm" id="adminUserForm" method="post" action="/system/setAdminUser" onsubmit="return false">
	<div class="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">					
			<%@ include file="../header2.jsp" %>
			<!-- Content -->
			<div id="content" class="in-sec mar-t-10">
				<section class="section grid gut-20">	
					<input type="hidden" name="nowPage">				
					<div class="col col-8">
						<!-- 운영자관리 현황 -->
						<section>
							<div class="form-search grid flex-center cross-center">
								<ul>
									<!-- <li>
										<label class="form-tit">소속</label>
										<div class="data-group">
											<select name="" id="sSelect1" class="select h30">
											    <option value="">전체</option>
											</select>									
										</div>
									</li> -->
									<li>
										<label class="form-tit">검색</label>
										<div class="data-group">
											<select name="searchType" id="searchType" class="select h30">
												<option value="userId"			<c:if test="${systemVO.searchType eq 'userId'}">selected</c:if>>아이디</option>
											    <option value="userName"	<c:if test="${systemVO.searchType eq 'userName'}">selected</c:if>>이름</option>
											</select>									
											<input type="text" name="searchKeyword" id="searchKeyword" value="${systemVO.searchKeyword}" placeholder="고객명/휴대폰번호/이메일" title="" class="input h30 w200">
										</div>
									</li>								
								</ul>
								<div>
									<button type="button" class="btn-search" onclick="search();"><span>검색</span></button>
								</div>&emsp;
								<div>
									<button type="button" class="btn-search" onclick="keywordReset();"><span id="searchButton">초기화</span></button>
								</div>								
							</div>
						</section>
						<!-- section -->
						<section class="section">
							<div class="tbl-header grid cross-center">
								<div class="col">
									<h2 class="h2">운영자현황</h2>
								</div>
								<div class="col right">
									<button type="button" class="btn type1 secondary"><span>엑셀다운로드</span></button>
								</div>			
							</div>
							<div class="Mguide">
		   						모바일에서 표를 좌우로 스크롤 할 수 있습니다. 
		  					</div>
							<div class="gtbl-list l15">
								<table>
									<caption></caption>
									<thead>
										<tr>
											<th scope="col">번호</th>
											<th scope="col">아이디</th>
											<th scope="col">이름</th>
											<th scope="col">이메일</th>
											<th scope="col">연락처</th>
											<th scope="col">비밀번호</th>
										</tr>
									</thead>
									<tbody id="listDiv">																																																																																																																																																																																																																																																																																																																																																																																									
									</tbody>	
								</table>
							</div>
							<div class="tbl-footer grid flex-between cross-center">
								<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
								<div id="pagingVar" class="col">
								</div>
								<div class="col per-page">
									<select name="cntPerPage" id="cntPerPage" class="select h30" onChange="cntPerPageChange()">
									    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
									    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
									    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
									    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
									    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
									    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
									</select>	
								</div>			
							</div>		
						</section>
						<!-- //section -->						
						<!-- //운영자관리 현황 -->
					</div>
					<div class="col col-4">
						<!-- 운영자관리 등록 -->
						<section>
							<input type="hidden" id="selectUserNo" name="selectUserNo" >
							<div class="tbl-type1 tbl-lefted">
								<table>
									<colgroup>
										<col width="30%">
										<col width="*">
									</colgroup>
									<tbody>
										<!--  <tr>
											<th scope="row">소속</th>
											<td>
												<select name="" id="sSelect1" class="select h25">
												    <option value="">전체관리</option>
												    <option value="">총무회계팀</option>
												    <option value="">교육팀</option>
												    <option value="">인재개발팀</option>
												</select>										
											</td>									
										</tr>  -->
										<tr>
											<th scope="row">아이디</th>
											<td>
												<input type="text" name="" id="sId" value="" placeholder="" title="" class="input h25 W100">
											</td>								
										</tr>
										<tr>
											<th scope="row">비밀번호 변경</th>
											<td>
												<input type="text" name="" id="sPw" value="" placeholder="변경시에만 입력" title="" class="input h25 W100">
											</td>								
										</tr>
										<tr>
											<th scope="row">이름</th>
											<td>
												<input type="text" name="" id="sName" value="" placeholder="" title="" class="input h25 W100">										
											</td>
										</tr>
										<tr>
											<th scope="row">이메일</th>
											<td>
												<input type="text" name="" id="sEmail" value="" placeholder="" title="" class="input h25 W100">										
											</td>
										</tr>		
										<tr>
											<th scope="row">연락처</th>
											<td>
												<input type="text" name="" id="sPhoneNo" value="" placeholder="'-' 제외하고 숫자만 입력" title="" class="input h25 W100">										
											</td>
										</tr>	
										<tr>
											<th scope="row">인증키</th>
											<td>
												<input type="text" name="" id="sOtpKey" value="" placeholder="" title="" class="input h25 W100" style="width:244px;" disabled>	
												<button type='button' class='btn-search' onclick='getOtpKey();'><span>키 발급</span></button>									
											</td>
										</tr>							
									</tbody>
								</table>
							</div>
						</section>	
						<section class="section">	
							<div class="grid cross-center">
								<div class="col right">
									<button type="button" class="btn type1 primary" onclick="updateChangeUser();"><span>저장</span></button>
									<button type="button" class="btn type1 secondary"><span>취소</span></button>
								</div>			
							</div>
						</section>											
						<!-- //운영자관리 등록 -->					
					</div>
				</section>

			</div>	
			<!-- //Content -->
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='../index.html'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="window.open('M040101-P01.html','','width=100%,height=100%'); return false;">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->		
	</div>
</form>
</body>
</html>