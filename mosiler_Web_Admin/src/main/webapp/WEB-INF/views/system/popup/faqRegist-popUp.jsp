<!-- FAQ 등록 팝업 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 시스템</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
   	<script type="text/javascript" src="/ckeditor/ckeditor.js" charset="utf-8"></script>
</head>
<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">FAQ 등록</h3>
		</div>
		<input type="hidden" id="id" value="">
		<div class="popup-body">
			<div class="popup-cont">
				<!-- section -->
				<section class="section">
					<div class="tbl tbl-view">
						<table class="x-auto">
							<caption></caption>
							<colgroup>
								<col width="10%">
								<col width="">
								
								<col width="10%">
								<col width="40%">							
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">카테고리</th>
									<td>
										<select name="faqParentCategory" id="faqParentCategory" class="select h30" style="width: 163px;">
										    <option value="">전체</option>
										    <option value="web">Web</option>
										    <option value="customer">Customer App</option>
										    <option value="driver">Driver App</option>
										</select>&nbsp;&nbsp;
										<select name="faqCategory" id="faqCategory" class="select h30" style="width: 163px;">
										    <option value="">대분류 선택</option>
										</select>
									</td>
									<th scope="row">Top 여부</th>
									<td>
										<select name="faqTopYn" id="faqTopYn" class="select h30">
										    <option value="">전체</option>
										    <option value="Y">사용</option>
										    <option value="N">미사용</option>
										</select>
									</td>
								</tr>							
								<tr>
									<th scope="row">제목</th>
									<td>
										<input type="text" name="faqQuestion" id="faqQuestion" value="" placeholder="" title="" class="input h30 w200" style="width:350px;">
									</td>
									<th scope="row">사용 여부</th>
									<td>
										<select name="useYn" id="useYn" class="select h30">
										    <option value="">전체</option>
										    <option value="Y">사용</option>
										    <option value="N">미사용</option>
										</select>
									</td>
								</tr>
								<!-- <tr>
									<th scope="row">등록일</th>
									<td></td>
									<th scope="row">등록자</th>
									<td></td>
								</tr> -->
								<tr>
									<th scope="row">내용</th>
									<td colspan="3" class="view-text">
										<textarea style="height:400px;" id="faqAnswer" name="faqAnswer" cols="50" class="input type1" ></textarea>
									</td>
								</tr>																																																																																																																																																																																																																																																																																															
							</tbody>	
						</table>
					</div>			
				</section>
				<!-- //section -->								
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">				
				<button type="button" onclick="insertFaq();" class="btn type3 primary"><span>등록</span></button>
				<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</body>
<script>
 	var ckeditor_config = {
 		height: 375,
   		resize_enaleb : false,
   		enterMode : CKEDITOR.ENTER_BR,
   		shiftEnterMode : CKEDITOR.ENTER_P,
   		filebrowserUploadUrl : "${pageContext.request.contextPath}/system/multiFileUpload"
 	};
 
	CKEDITOR.replace("faqAnswer", ckeditor_config);
</script>
<script type="text/javascript">
	$(document).ready(function(){
		var id = "${id}";
		
		if(id == "") {
			location.href = "/";
		} else {
			$("#id").val(id);
		}
		
		$("#faqParentCategory").change(function() {
			$("#faqCategory").children("option").remove();
			
			changeFaqCategory(this.value);
		});
	});
	
	// select option 변경
	function changeFaqCategory(value) {
		var web			= ["가입/탈퇴", "예약/취소", "요금/결제", "보험/사고", "등하교 헬퍼", "실버 케어", "파트너"];
		var customer	= ["고객1", "고객2", "고객3", "고객4"];
		var driver		= ["파트너1", "파트너2", "파트너3", "파트너4", "파트너5"];
		var changeValue;
		
		if(value == "web") {
			changeValue = web;	
		} else if (value == "customer") {
			changeValue = customer;
		} else if (value == "driver") {
			changeValue = driver;
		}
		
		for(var i = 1; i <= changeValue.length; i++) {
			var option;
			
			if(i == 0) {
				option = $("<option value=''>전체</option>");
			} else {
				option = $("<option value="+i+">"+changeValue[i-1]+"</option>");
			}
			
			$("#faqCategory").append(option);
		}
	}
	
	// Faq Insert
	function insertFaq() {
		var id 						= $("#id").val();
		var faqParentCategory 	= $("#faqParentCategory").val();
		var faqCategory 			= $("#faqCategory").val();
		var faqTopYn 				= $("#faqTopYn").val();
		var faqQuestion 			= $("#faqQuestion").val();
		var useYn					= $("#useYn").val();
		var faqAnswer				= CKEDITOR.instances.faqAnswer.getData();
		
		if(faqParentCategory == "") {
			alert("대분류 카테고리를 선택해 주세요.");
			return false;
		}
		if(faqCategory == "") {
			alert("카테고리를 선택해 주세요.");
			return false;
		}
		if(faqTopYn == "") {
			alert("Top 여부를 선택해 주세요.");
			return false;
		}
		if(faqQuestion == "") {
			alert("제목을 입력해 주세요.");
			return false;
		}
		if(useYn == "") {
			alert("사용여부를 선택해 주세요.");
			return false;
		}
		if(faqAnswer == "") {
			alert("내용을 입력해 주세요.");
			return false;
		}
		
		$.ajax({
			url: "/system/insertFaq"
			, type: "POST"
			, data: {
				id: id
				, faqParentCategory: faqParentCategory
				, faqCategory: faqCategory
				, faqTopYn: faqTopYn
				, faqQuestion: faqQuestion
				, useYn: useYn
				, faqAnswer: faqAnswer
			}
			, success: function(data) {
				if(data == 200) {
					alert("FAQ가 등록 되었습니다.");
					
					self.close();
				} else {
					alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
				}

				opener.location.reload();
				self.close();
			}
			, error: function(data, status, opt) {
				alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
				self.close();
				
				console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
			}
		});
	}
	
	// 닫기 버튼
	function selfClose() {
		self.close();
	}
</script>
</html>