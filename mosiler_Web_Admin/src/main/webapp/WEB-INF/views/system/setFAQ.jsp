<!-- FAQ -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 시스템</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>   
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>    	
   	<script src="/js/util.js"></script> 	
   	
	<!-- 메뉴구현부  -->
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			var faqList = ${faqList};
			var cateList = ${cateList};
			var depthList = ${depthList};
			
			var cateMenuCode = 'M'+'07'+'0000';
			var depthMenuCode1 = 'M'+'07'+'07'+'00';
			var depthMenuCode2 = 'M070701';

			makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2);
			
			// 현재 페이지 번호 세팅
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			// 엔터키 이벤트 
		 	$("#searchKeyword").keypress(function(e) {
				if(e.which == 13) {
					search();
				}
			}); 

			// 리스트
			makeContents(faqList);
			// 페이징
			makePagingVar();
		});
		
		// 리스트
		function makeContents(faqList) {
			var html = "";
			var cntPerPage = ${pagingVO.cntPerPage};
			
			for(var i = 0; i < cntPerPage; i++) {
				if(faqList[i] == null) {
					break;
				} else {
					var parentCategory;
					var category;
					var topYn;
					var useYn;
					
					if(faqList[i].faqParentCategory == "web") {
						parentCategory = "홈페이지";
						
						if(faqList[i].faqCategory == "1") {
							category = "가입/탈퇴";
						} else if(faqList[i].faqCategory == "2") {
							category = "예약/취소";
						} else if(faqList[i].faqCategory == "3") {
							category = "요금/결제";
						} else if(faqList[i].faqCategory == "4") {
							category = "보험/사고";
						} else if(faqList[i].faqCategory == "5") {
							category = "등하교 헬퍼";
						} else if(faqList[i].faqCategory == "6") {
							category = "실버 케어";
						} else if(faqList[i].faqCategory == "7") {
							category = "파트너";
						}
					} else if(faqList[i].faqParentCategory == "customer") {
						parentCategory = "고객";
						
						if(faqList[i].faqCategory == "1") {
							category = "고객1";
						} else if(faqList[i].faqCategory == "2") {
							category = "고객2";
						} else if(faqList[i].faqCategory == "3") {
							category = "고객3";
						} else if(faqList[i].faqCategory == "4") {
							category = "고객4";
						}
					} else if(faqList[i].faqParentCategory == "driver") {
						parentCategory = "파트너";
						
						if(faqList[i].faqCategory == "1") {
							category = "파트너1";
						} else if(faqList[i].faqCategory == "2") {
							category = "파트너2";
						} else if(faqList[i].faqCategory == "3") {
							category = "파트너3";
						} else if(faqList[i].faqCategory == "4") {
							category = "파트너4";
						} else if(faqList[i].faqCategory == "5") {
							category = "파트너5";
						}
					}
					
					if(faqList[i].faqTopYn == "Y") {
						topYn = "사용";
					} else if(faqList[i].faqTopYn == "N") {
						topYn = "미사용";
					}
					
					if(faqList[i].useYn == "Y") {
						useYn = "사용";
					} else if(faqList[i].useYn == "N") {
						useYn = "미사용";
					}
 					
					html +=  "<tr style='cursor: pointer' onclick=\"window.open('/system/faqDetailpopUp?"
						 			+"faqNo="+faqList[i].faqNo+"','','width=1035,height=740');return true\">";
					html += "	<td>"+faqList[i].rownum+"</td>";
					html += "	<td>"+parentCategory+"</td>";
					html += "	<td>"+category+"</td>";
					html += "	<td class='align-l'>"+faqList[i].faqQuestion+"</td>";
					html += "	<td>"+topYn+"</td>";
					html += "	<td>"+useYn+"</td>";
					html += "	<td>"+faqList[i].regDt+"</td>";
					html += "	<td>"+faqList[i].regId2+"</td>";
					html += "<tr>";
				}
			}
			
			$("#listDiv").append(html);			
		}
		
		// 페이징 바
		function makePagingVar() {
			var nowPage = ${pagingVO.nowPage};
			var startPage = ${pagingVO.startPage};
			var endPage = ${pagingVO.endPage};
			var lastPage = ${pagingVO.lastPage};
		
			var html = "";
			
			html += "<div class=\"col'\">";
			html += "	<div class=\"paging paging-basic\">";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == 1) {
				html += "		<button type=\"button\" class=\"btn first\" title=\"처음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn prev\" title=\"이전 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"firstPage\" class=\"btn first\" title=\"처음 페이지\"  onclick='arrowBtn(1);'><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" id=\"prevPage\" class=\"btn prev\" title=\"이전 페이지\" onclick='arrowBtn(2);'><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			}
			
			html += "		</div>";
			html += "		<ul class=\"num-group\">";
			
			for(var i = startPage; i <= endPage; i++) {
				if(i == nowPage) {
					html += "	<li><button type=\"button\" class=\"btn\" aria-current=\"true\" disabled=\"disabled\"><span>"+i+"</span></button></li>";
				} else {
					html += "	<li><button type=\"button\" id=\"e\" class=\"btn\" onclick='numberBtn("+i+");'><span>"+i+"</span></button></li>"
				}
			}
			
			html += "		</ul>";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == lastPage) {
				html += "		<button type=\"button\" class=\"btn next\" title=\"다음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-next gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn last\" title=\"마지막 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"nextPage\" class=\"btn next\" title=\"다음 페이지\" onclick='arrowBtn(3);'><span><i class=\"arw arw-board1-next gray\"></i></span></button>";			
				html += "		<button type=\"button\" id=\"lastPage\" class=\"btn last\" title=\"마지막 페이지\" onclick='arrowBtn(4);'><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			}
			
			html += "		</div>";		
			html += "	</div>";
			html += "</div>";	
			
			$("#pagingVar").append(html);
		}
		
		// 검색
 		function search() {
			//submit
			document.faqForm.submit();
		} 
		
		// 검색 초기화
		function keywordReset() {
			$("#faqCategory").val("");
			$("#faqTopYn").val("");
			$("#useYn").val("");
			$("#searchKeyword").val("");
		}
		
		// 한 페이지에 보여줄 row수 전환 
		function cntPerPageChange() {
			// 현재 페이지 번호 저장
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			// submit
			document.faqForm.submit(); 
		}

		// 페이징 번호 버튼
		function numberBtn(num) {
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(num);
			
			// submit
			document.faqForm.submit(); 
		}
		
		// 페이징 화살표 버튼
		function arrowBtn(num) {
			var nowPage = ${pagingVO.nowPage};
			
			if(num == 1) {
				// 처음
				nowPage = 1;
			} else if(num == 2) {
				// 이전
				nowPage = nowPage - 1;
			} else if(num == 3) {
				// 다음
				nowPage = nowPage + 1;
			} else {
				// 마지막
				nowPage = ${pagingVO.lastPage};
			}
			
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(nowPage);
			
			// submit
			document.faqForm.submit(); 
		}		
	</script>   	
</head>

<body>
<form name="faqForm" id="faqForm" method="post" action="/system/setFAQ" onsubmit="return false">
	<div class="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">					
			<%@ include file="../header2.jsp" %>
			<!-- Content -->
			<div id="content" class="in-sec">
				<section class="section">			
					<input type="hidden" name="nowPage">		
					<div class="form-search grid flex-center cross-center">
						<ul>
							<li>
								<label class="form-tit">카테고리</label>
								<div class="data-group">
									<select name="faqCategory" id="faqCategory" class="select h30">
									    <option value="">전체</option>
									    <option value="1" <c:if test="${systemVO.faqCategory eq '1'}">selected</c:if>>가입/탈퇴</option>
									    <option value="2" <c:if test="${systemVO.faqCategory eq '2'}">selected</c:if>>예약/취소</option>
									    <option value="3" <c:if test="${systemVO.faqCategory eq '3'}">selected</c:if>>요금/결제</option>
									    <option value="4" <c:if test="${systemVO.faqCategory eq '4'}">selected</c:if>>보험/사고</option>
									    <option value="5" <c:if test="${systemVO.faqCategory eq '5'}">selected</c:if>>등하교 헬퍼</option>
									    <option value="6" <c:if test="${systemVO.faqCategory eq '6'}">selected</c:if>>실버 케어</option>
									    <option value="7" <c:if test="${systemVO.faqCategory eq '7'}">selected</c:if>>파트너</option>
									</select>
								</div>
							</li>								
							<li>
								<label class="form-tit">Top 여부</label>
								<div class="data-group">
									<select name="faqTopYn" id="faqTopYn" class="select h30">
									    <option value="">전체</option>
									    <option value="Y" <c:if test="${systemVO.faqTopYn eq 'Y'}">selected</c:if>>사용</option>
									    <option value="N" <c:if test="${systemVO.faqTopYn eq 'N'}">selected</c:if>>미사용</option>
									</select>
								</div>
							</li>			
							<li>
								<label class="form-tit">사용 여부</label>
								<div class="data-group">
									<select name="useYn" id="useYn" class="select h30">
									    <option value="">전체</option>
									    <option value="Y" <c:if test="${systemVO.useYn eq 'Y'}">selected</c:if>>사용</option>
									    <option value="N" <c:if test="${systemVO.useYn eq 'N'}">selected</c:if>>미사용</option>
									</select>
								</div>
							</li>
							<li>
								<label class="form-tit">검색</label>
								<div class="data-group">
									<input type="text" name="searchKeyword" id="searchKeyword" value="${systemVO.searchKeyword}" placeholder="질문 내용으로 검색" title="" class="input h30 w200">
								</div>
							</li>									
						</ul>
						<div>
							<button type="button" class="btn-search" onclick="search();"><span>검색</span></button>
						</div>&emsp;
						<div class="Mmar-t10">
							<button type="button" class="btn-search" onclick="keywordReset();"><span id="searchButton">초기화</span></button>
						</div>			
					</div>
				</section>			
				<!-- section -->
				<section class="section">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h2 class="h2">FAQ</h2>
						</div>
						<div class="col right">
							<button type="button" class="btn type1 primary" onclick="window.open('/system/faqRegistPopUp', '', 'width=1000, height=700')"><span>등록</span></button>
							<button type="button" class="btn type1 secondary"><span>엑셀다운로드</span></button>
						</div>			
					</div>
					<div class="Mguide">
   						모바일에서 표를 좌우로 스크롤 할 수 있습니다. 
  					</div>
					<div class="gtbl-list l15">
						<table class="x-auto">
							<caption></caption>
							<colgroup>
								<col width="8%">
								<col width="8%">
								<col width="8%">
								<col width="*">
								<col width="8%">
								<col width="8%">
								<col width="10%">
								<col width="10%">							
							</colgroup>
							<thead>
								<tr>
									<th scope="col">번호</th>
									<th scope="col">대분류</th>
									<th scope="col">카테고리</th>
									<th scope="col">질문 내용</th>
									<th scope="col">Top 여부</th>
									<th scope="col">사용 여부</th>
									<th scope="col">등록일</th>
									<th scope="col">등록자</th>
								</tr>
							</thead>
							<tbody id="listDiv">
							</tbody>
						</table>
					</div>
					<div class="tbl-footer grid flex-between cross-center">
						<div id="pageNumber" class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
						<div id="pagingVar" class="col">
						</div>
						<div class="col per-page">
							<select name="cntPerPage" id="cntPerPage" class="select h30" onChange="cntPerPageChange()">
							    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
							    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
							    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
							    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
							    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
							    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
							</select>	
						</div>			
					</div>		
				</section>
				<!-- //section -->		
			</div>	
			<!-- //Content -->
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='../index.html'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="window.open('M040101-P01.html','','width=100%,height=100%'); return false;">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->			
	</div>
</form>
</body>
</html>