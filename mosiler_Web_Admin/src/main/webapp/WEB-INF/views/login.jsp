<!-- 로그인 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 | 로그인</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="../images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="../images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="../css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="../css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="../css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="../js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="../js/egovframework/com/adm/cmm/libs/librarys.min.js"></script>
    <script src="../js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="../js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="../js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="../js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
	<style>
		html, body {height:100%;}	
	</style>    
	
	<script type="text/javascript">
		$(document).ready(function(){
			var msg = "${msg}";
			
			if(msg != "") {
				alert(msg);
			}
			
			var key = getCookie("key");

			$("#id").val(key); 

		    // 아이디가 있을 경우 체크박스 true
			if($("#id").val() != "") { 
		        $("#sSaveId").attr("checked", true); 
		    }

		    // 체크 박스 변화 시
		    $("#sSaveId").change(function() { 
		        if($("#sSaveId").is(":checked")) { 
		            setCookie("key", $("#id").val(), 300);
		        } else { 	
		            deleteCookie("key");
		        }
		    });

		    // 아이디 입력할 때 
		    $("#id").keyup(function() { 
		        if($("#sSaveId").is(":checked")) {
		            setCookie("key", $("#id").val(), 300);
		        }
		    });

			function setCookie(cookieName, value, exdays) {
		        var exdate = new Date();
		        exdate.setDate(exdate.getDate() + exdays);

		        var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
		        document.cookie = cookieName + "=" + cookieValue;
		    }
		     
		    function deleteCookie(cookieName) {
		        var expireDate = new Date();
		        expireDate.setDate(expireDate.getDate() - 1);

		        document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
		    }
		     
		    function getCookie(cookieName) {
		        cookieName = cookieName + '=';
		        
		        var cookieData = document.cookie;
		        var start = cookieData.indexOf(cookieName);
		        var cookieValue = '';
		        
		        if(start != -1) {
		            start += cookieName.length;
		            var end = cookieData.indexOf(';', start);

		            if(end == -1) {
			            end = cookieData.length;
			        }

		            cookieValue = cookieData.substring(start, end);
		        }
		        
		        return unescape(cookieValue);
		    }

			// 엔터키 이벤트 
			$("#id").keypress(function(e) {
				if(e.which == 13) {
					procLogin()
				}
			});

			// 엔터키 이벤트 
			$("#pwd").keypress(function(e) {
				if(e.which == 13) {
					procLogin()
				}
			});
			
			// 엔터키 이벤트 
			$("#otpKey").keypress(function(e) {
				if(e.which == 13) {
					procLogin()
				}
			});

			// 버튼 클릭 이벤트
			$("#loginBtn").click(function(){
				procLogin()
			});

			// 로그인
			function procLogin() {
				var id = $("#id").val();
				var pwd = $("#pwd").val();
				var otpKey = $("#otpKey").val();
				
				if(id == "" || pwd == "") {
					alert("아이디 또는 비밀번호를 입력해 주세요.");
					return false;
				}  
				
				if(otpKey == "") {
					alert("OTP 키를 입력해 주세요.");
					return false;
				}
				
				//submit
				document.loginForm.submit();
			}
			
			// 로그인
/* 			function procLogin() {
				var id = $("#id").val();
				var pwd = $("#pwd").val();
				var otpKey = $("#otpKey").val();
				
				if(id == "" || pwd == "") {
					alert("아이디 또는 비밀번호를 입력해 주세요.");
					return false;
				}  
				
				if(otpKey == "") {
					alert("OTP 키를 입력해 주세요.");
					return false;
				}
				
				$.ajax({
					url: "/procLogin"
					, type: "POST"
					, data: {
						id : id
						, pwd : pwd
						, otpKey: otpKey
					}
					, success: function(resultCode) {
						
						
						if(resultCode == 200) {
							location.href = "/index";
						} else if(resultCode == 400) {
							alert("운영자 정보가 존재하지 않습니다. 다시 입력해 주세요.");
						} else {
							alert("OTP가 일치하지 않습니다. OTP 확인 바랍니다.");
						}
					}
					, error: function(data, status, opt) {
						alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
				        console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
				    }
				}); 
			} */
		});
	</script>
</head>
<c:if test="${id == null}">
	<body class="login-body">
		<div class="login-wrapper">
			<div class="container">
				<div class="content">
					<section class="login-sec">
		           		<div class="section-title">
		             		<h1><span class="blind">모시러</span></h1>
		           		</div>
		           		<div class="section-body">
				            <form name="loginForm" id="loginForm" method="post" action="/procLogin" onsubmit="return false">
				            	<fieldset class="fld_login">
									<legend class="blind">모시러 로그인 폼</legend>
				                    <div class="login-form-controls">
										<input type="text" name="id" id="id" value="" placeholder="아이디" class="input login">
									</div>
									<div class="login-form-controls">
										<input type="password" name="pwd" id="pwd" value="" placeholder="비밀번호" class="input login">
									</div>	
									<div class="login-form-controls">
										<input type="text" name="otpKey" id="otpKey" value="" placeholder="OTP 입력" class="input login">
									</div>
									<div class="login-form-controls">
										<span class="check type1">
										    <input type="checkbox" name="sSaveId" id="sSaveId" value="">
										    <label for="sSaveId"><span style="letter-spacing: -1px;color: #666;">아이디 저장</span></label>
										</span>	
									</div>	
									<div class="btn-area vertical">
									    <button type="button" class="btn login" id="loginBtn"><span>로그인</span></button>
									</div>
									<div class="form-info mar-t5">
									    <p>ID 및 비밀번호 분실시 관리자에게 문의바랍니다.</p>
									</div>						
								</fieldset>	
				            </form>           		
		           		</div>
					</section>
				</div>
			</div>
		</div>
	</body>
</c:if>

<c:if test="${id != null}">
	<c:redirect url="/index"></c:redirect>
</c:if>
</html>