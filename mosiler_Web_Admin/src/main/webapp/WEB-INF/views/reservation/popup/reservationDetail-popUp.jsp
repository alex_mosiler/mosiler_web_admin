<!-- 예약 현황 상세 팝업 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 예약관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
   	
   	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			// 컨시어지 리스트
			var concierge = ${conciergeObject};

			// 기존 저장된 컨시어지 체크 
			$(concierge).each(function(index, item) {
				var value = item.payServiceNo;

				$('input:checkbox[name="concierge"]').each(function(index) {
					var i = index+1;

					if($("#"+i).val() == value) {
						this.checked = true;
					}
				});
			});
		});

   		// 예약 위치정보 삭제 버튼
		function deletePlace(num) {
			$("#address"+num).val("");
			$("#lat"+num).val("");
			$("#lng"+num).val("");
	   	}

   		// 예약 수정
		function updateReservation() {
			var customerNo 						= ${customerNo};
			var customerName 					= $("#customerName").val();
			var customerPhoneNo 				= $("#customerPhoneNo").val();
			var drivingType 						= $("#drivingType").val();
			var revStartDt							= $("#startDate").val()+" "+$("#startHour").val()+":"+$("#startMin").val()+":00.000";
			var revEndDt							= $("#endDate").val()+" "+$("#endHour").val()+":"+$("#endMin").val()+":00.000";
			// 출발지
			var address1 							= $("#address1").val();
			var lat1 									= $("#lat1").val();
			var lng1 								= $("#lng1").val();
			// 경유지1
			var address2 							= $("#address2").val();
			var lat2 									= $("#lat2").val();
			var lng2 								= $("#lng2").val();
			// 경유지2
			var address3 							= $("#address3").val();
			var lat3 									= $("#lat3").val();
			var lng3 								= $("#lng3").val();
			// 도착지
			var address4 							= $("#address4").val();
			var lat4 									= $("#lat4").val();
			var lng4 								= $("#lng4").val();
			var paymentType						= $("#paymentType").val();
			var couponCnt 						= $("#couponCnt").val();
			var mosilerComment					= $("textarea#adminRequest").val();
			var customerComment				= $("textarea#customerRequest").val();
			var drivingNo							= $("#drivingNo").val();
			var driverNo							= $("#driverNo").val();
			var passenger							= $("#passenger").val();
			var passengerPhoneNo				= $("#passengerPhoneNo").val();
			var hourPrice							= $("#hourPrice").val();
			var couponCnt 						= $("#couponCnt").val();
			var bizCompNo 						= $("#bizCompNo").val();
			var partnerName						= $("#userName").val();
			var partnerPhoneNumber			= $("#phoneNumber").val();

			var conciergeList = new Array();
			var concierge = document.getElementsByName("concierge");
			for(var i = 0; i < concierge.length; i++) {
				if(concierge[i].checked) {
					conciergeList.push(concierge[i].value);
				}
			}

			var conciergeCodeList 				= conciergeList;
			var currentDate = new Date().toISOString().substring(0, 10);

   			if(drivingType == "") {
				alert("운행 구분을 선택해 주세요.");
				return false;
			} 
			if(passenger == "" && passengerPhoneNo == "") {
				alert("탑승자를 확인해 주세요.");
				return false;
			}
			if($("#startDate").val() < currentDate) {
				alert("예약 시작 일시는 현재일보다 작을 수 없습니다.");
				return false;
			}
			if($("#endDate").val() < currentDate) {
				alert("예약 종료 일시는 현재일보다 작을 수 없습니다.");
				return false;
			}
			if($("#startDate").val() == "" || $("#endDate").val() == "") {
				alert("예약 날짜를 확인해 주세요.");
				return false;
			}
			if(paymentType == "") {
				alert("결제 수단을 확인해 주세요.");
				return false;
			}
			if(hourPrice == "") {
				alert("할인율을 확인해 주세요.");
				return false;
			}
			if(address1 == "") {
				alert("출발지를 확인해 주세요.");
				return false;
			}
			if(address4 == "") {
				alert("도착지를 확인해 주세요.");
				return false;
			}
			if(partnerName == "" && partnerPhoneNumber == "") {
				alert("드라이버를 배정해 주세요.");
				return false;
			}   			
			
			jQuery.ajaxSettings.traditional = true;

 			$.ajax({
				url: "/reservation/updateReservation"
				, type: "POST"
				, data: {
					customerNo: customerNo
					, customerName: customerName
					, customerPhoneNo: customerPhoneNo
					, drivingType: drivingType 
					, revStartDt: revStartDt
					, revEndDt: revEndDt	
					, address1: address1 					
					, lat1: lat1
					, lng1: lng1
					, address2: address2
					, lat2: lat2
					, lng2: lng2
					, address3: address3
					, lat3: lat3
					, lng3: lng3
					, address4: address4
					, lat4: lat4
					, lng4: lng4
					, paymentType: paymentType	
					, couponCnt: couponCnt
					, mosilerComment: mosilerComment
					, customerComment: customerComment
					, drivingNo: drivingNo
					, driverNo: driverNo							
					, passenger: passenger						
					, passengerPhoneNo: passengerPhoneNo			
					, hourPrice: $.trim(hourPrice.replace(/,/g, ''))	
					, couponCnt: couponCnt 						
					, bizCompNo: bizCompNo 	
					, conciergeCodeList: conciergeCodeList			
				}
				, success: function(data) {
					if(data == 200) {
						alert("예약 정보가 수정 되었습니다.");
						
						self.close();
					} else {
						alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
					}

					opener.location.reload();
					self.close();
				}
				, error: function(data, status, opt) {
					alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
					self.close();
					
					console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
				}
			}); 
		}
		
		// 예약 취소
		function cancelReservation() {
			var drivingNo = $("#drivingNo").val();
			var regId = $("#regId").val();
			
			$.ajax({
				url: "/reservation/cancelReservation"
				, type: "POST"
				, data: {
					drivingNo: drivingNo
					, regId: regId
				}
				, success: function(data) {
					if(data == 200) {
						alert("예약 취소 되었습니다.");
						
						self.close();
					} else {
						alert("저장 중 에러가 발생하였습니다. 사이트 관리자에게 문의 바랍니다.");
					}

					opener.location.reload();
					self.close();
				}
				, error: function(data, status, opt) {
					alert("서비스 장애가 발생 하였습니다. 다시 시도해 주세요.");
					self.close();
					
					console.log("code:"+data.status+"\n"+"message:"+data.responseText+"\n"+"error:"+opt);
				}
			}); 
		}
		
   		// 닫기 버튼
 		function selfClose() {
			self.close();
		}
   	</script>
</head>

<body>
<div id="popupBasic" class="popup-wrap type-basic is-active" role="dialog" aria-hidden="false" style="z-index: 1001;">
	<section class="popup type1" tabindex="0">
		<div class="popup-head">
			<h3 id="popupContentTitle">예약상세</h3>
		</div>
		<div class="popup-body">
			<div class="popup-cont">
				<section class="section">
					<div class="status-board grid flex-center cross-center">
						<c:forEach var="reservationDetail" items="${reservationDetail}" varStatus="idx">
							<ul>
								<li>
									<label class="">운행 구분</label>
									<div class="data-group">
										<c:if test="${reservationDetail.drivingType == 1}">일반</c:if> 
										<c:if test="${reservationDetail.drivingType == 3}">골프</c:if>
										<c:if test="${reservationDetail.drivingType == 21}">키즈</c:if>
										<c:if test="${reservationDetail.drivingType == 22}">실버</c:if>
										<c:if test="${reservationDetail.drivingType == 23}">에어</c:if>
										<c:if test="${reservationDetail.drivingType == 24}">비즈</c:if>
										<c:if test="${reservationDetail.drivingType == 25}">공항 홈발렛</c:if>
										<c:if test="${reservationDetail.drivingType == 300}">고정 운행</c:if>
									</div>
								</li>
								<li>
									<label class="">예약번호</label>
									<input type="hidden" id="drivingNo" name="drivingNo" value="${reservationDetail.drivingNo}">
									<div class="data-group">${reservationDetail.drivingNo}</div>
								</li>
								<!-- <li>
									<label class="">상품구분</label>
									<div class="data-group">상품A</div>
								</li>	 -->
								<li>
									<label class="">배차 상태</label>
									<div class="data-group">
										<span class="text-matching">
											<c:if test="${reservationDetail.drivingStatus == 1}">배차신청</c:if> 
											<c:if test="${reservationDetail.drivingStatus == 2}">배차확정</c:if>
											<c:if test="${reservationDetail.drivingStatus == 3}">예약취소</c:if>
											<c:if test="${reservationDetail.drivingStatus == 8}">배차확인</c:if> 
											<c:if test="${reservationDetail.drivingStatus == 9}">취소요청</c:if>   
										</span>
									</div>
								</li>					
							</ul>
						</c:forEach>
						<div class="Mmar-t10 right">
							<button type="button" class="btn-search" onclick="window.open('/partner/partnerAssignment?tabId=1&flag=Y&customerNo=${customerNo}', '', 'width=600, height=570')"><span>매칭중 파트너</span></button>
							<button type="button" class="btn-search" onclick="window.open('/partner/partnerAssignment?tabId=2&flag=Y&customerNo=${customerNo}', '', 'width=600, height=570')"><span>강제매칭</span></button>
						</div>											
					</div>
				</section>	
				<section class="section">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h3 class="h3">예약정보</h3>
						</div>	
					</div>
					<div style="position:relative;">
						<div class="tbl-type1 tbl-lefted">
							<table>
								<colgroup>
									<col width="17%">
									<col width="*">
									<col width="17%">
									<col width="20%">
								</colgroup>
								<tbody>
									<c:forEach var="reservationDetail" items="${reservationDetail}" varStatus="idx">
										<tr>
											<th scope="row">예약자 정보</th>
												<td>
													<input type="hidden" id="customerName" name="customerName" value="${reservationDetail.customerName}">
													<input type="hidden" id="bizCompNo" name="bizCompNo" value="${reservationDetail.bizCompNo}">
													<input type="hidden" id="regId" name="regId" value="${reservationDetail.regId }">
													${reservationDetail.customerName} 
												</td>
											<th scope="row">예약자 연락처</th>
												<input type="hidden" id="customerPhoneNo" name="customerPhoneNo" value="${reservationDetail.customerPhoneNo}">
												<td>
													${reservationDetail.customerPhoneNo}	
												</td>
											
											<%-- <th scope="row">탑승자 정보</th>
												<td>
													${reservationDetail.passenger} / ${reservationDetail.passengerPhoneNo}
													<input type="text" id="passenger" name="passenger" value="${reservationDetail.passenger}">/
													<input type="text" id="passengerPhoneNo" name="passengerPhoneNo" value="${reservationDetail.passengerPhoneNo}">
												</td>	 --%>									
										</tr>
										<tr>
											<th scope="row">탑승자 정보</th>
												<td>
													<input type="text" id="passenger" name="passenger" class="input h25" value="${reservationDetail.passenger}">
												</td>
											<th scope="row">탑승자 연락처</th>
												<td>
													<input type="text" id="passengerPhoneNo" name="passengerPhoneNo" class="input h25" value="${reservationDetail.passengerPhoneNo}">
												</td>
										</tr>
										<tr>
											<th scope="row">운행 구분</th>
											<td>
												<select name="drivingType" id="drivingType" class="select h25">
													<option value="">선택</option>
													<c:forEach items="${codeList}" var="list" varStatus="status">
														<option value="${list.code}" <c:if test="${reservationDetail.drivingType eq list.code}">selected</c:if>>${list.codeName}</option>
													</c:forEach>
												</select>	
											</td>			
											<th scope="row">차량정보</th>
											<td>${reservationDetail.carModel}</td>
										</tr>
										<tr>
											<th scope="row">예약 시작 일시</th>
												<td>
													<input type="date" name="startDate" id="startDate" value="${fn:substring(reservationDetail.revStartDt, 0, 10)}" placeholder="" title="" class="input h30 input-date">
													<select name="startHour" id="startHour" class="select h30">
														<c:forEach var="i" begin="0" end="23">
															<c:if test="${i < 10}">
																<option value="${i}" <c:if test="${fn:substring(reservationDetail.revStartDt, 12, 13) eq i}">selected</c:if>>0${i}시</option>
															</c:if>
															<c:if test="${i >= 10}">
																<option value="${i}" <c:if test="${fn:substring(reservationDetail.revStartDt, 11, 13) eq i}">selected</c:if>>${i}시</option>
															</c:if>
														</c:forEach>
													</select>
													<select name="startMin" id="startMin" class="select h30"> 
														<option value="0">00분</option>
														<c:forEach var="i" begin="5" end="55">
															<c:if test="${i%5 == 0}">
																<c:if test="${i < 10}">
																	<option value="${i}" <c:if test="${fn:substring(reservationDetail.revStartDt, 15, 16) eq i}">selected</c:if>>0${i}분</option>
																</c:if>
																<c:if test="${i >= 10}">
																	<option value="${i}" <c:if test="${fn:substring(reservationDetail.revStartDt, 14, 16) eq i}">selected</c:if>>${i}분</option>
																</c:if>
															</c:if>
														</c:forEach>
													</select>	
												</td>
											<th scope="row">예약시간</th>
												<td>${reservationDetail.timeStamp}분</td>
										</tr>		
										<tr>
											<th scope="row">예약 종료 일시</th>
												<td>
													<input type="date" name="endDate" id="endDate" value="${fn:substring(reservationDetail.revEndDt, 0, 10)}" placeholder="" title="" class="input h30 input-date">
													<select name="endHour" id="endHour" class="select h30">
														<c:forEach var="i" begin="0" end="23">
															<c:if test="${i < 10}">
																<option value="${i}" <c:if test="${fn:substring(reservationDetail.revEndDt, 12, 13) eq i}">selected</c:if>>0${i}시</option>
															</c:if>
															<c:if test="${i >= 10}">
																<option value="${i}" <c:if test="${fn:substring(reservationDetail.revEndDt, 11, 13) eq i}">selected</c:if>>${i}시</option>
															</c:if>
														</c:forEach>
													</select>
													<select name="endMin" id="endMin" class="select h30"> 
														<option value="0">00분</option>
														<c:forEach var="i" begin="5" end="55">
															<c:if test="${i%5 == 0}">
																<c:if test="${i < 10}">
																	<option value="${i}" <c:if test="${fn:substring(reservationDetail.revEndDt, 15, 16) eq i}">selected</c:if>>0${i}분</option>
																</c:if>
																<c:if test="${i >= 10}">
																	<option value="${i}" <c:if test="${fn:substring(reservationDetail.revEndDt, 14, 16) eq i}">selected</c:if>>${i}분</option>
																</c:if>
															</c:if>
														</c:forEach>
													</select>													
												</td>
										</tr>							
										<tr>
											<th scope="row">출발지</th>
												<td colspan="3">
													<input type="text" name="address1" id="address1" value="${reservationDetail.revStartAddress}" placeholder="지역명 2자리이상 입력하세요" title="" class="input h25 w400">
													<button type="button" class="btn type4" onclick="deletePlace(1)">삭제</button>
													<button type="button" class="btn type4 secondary" onclick="window.open('/customer/reservationMap?num=1','','width=1500,height=950');return true">지도보기</button>
													<input type="text" name="lat1" id="lat1" value="${reservationDetail.revStartLat}" readonly style="background:#e5e5e5; width:120px;">
													<input type="text" name="lng1" id="lng1" value="${reservationDetail.revStartLng}" readonly style="background:#e5e5e5; width:120px;">
												</td>
										</tr>
										<tr>
											<th scope="row">도착지</th>
												<td colspan="3">
													<input type="text" name="address4" id="address4" value="${reservationDetail.revEndAddress}" placeholder="지역명 2자리이상 입력하세요" title="" class="input h25 w400">
													<button type="button" class="btn type4" onclick="deletePlace(4)">삭제</button>
													<button type="button" class="btn type4 secondary" onclick="window.open('/customer/reservationMap?num=4','','width=1500,height=950');return true">지도보기</button>
													<input type="text" name="lat4" id="lat4" value="${reservationDetail.revEndLat}" readonly style="background:#e5e5e5; width:120px;">
													<input type="text" name="lng4" id="lng4" value="${reservationDetail.revEndLng}" readonly style="background:#e5e5e5; width:120px;">
												</td>
										</tr>
										<tr>
											<th scope="row">경유지1</th>
												<c:choose>
													<c:when test="${(reservationWayPoint == null or fn:length(reservationWayPoint) == 0) and reservationDetail.wayPointAddress == null}">
														<td colspan="3">
															<input type="text" name="address2" id="address2" value="" placeholder="지역명 2자리이상 입력하세요" title="" class="input h25 w400">
															<button type="button" class="btn type4" onclick="deletePlace(2)">삭제</button>
															<button type="button" class="btn type4 secondary" onclick="window.open('/customer/reservationMap?num=2','','width=1500,height=950');return true">지도보기</button>
															<input type="text" name="lat2" id="lat2" value="" readonly style="background:#e5e5e5; width:120px;">
															<input type="text" name="lng2" id="lng2" value="" readonly style="background:#e5e5e5; width:120px;">
														</td>
													</c:when>
													<c:when test="${reservationDetail.wayPointAddress != null}">
														<td colspan="3">
															<input type="text" name="address2" id="address2" value="${reservationDetail.wayPointAddress}" placeholder="지역명 2자리이상 입력하세요" title="" class="input h25 w400">
															<button type="button" class="btn type4" onclick="deletePlace(2)">삭제</button>
															<button type="button" class="btn type4 secondary" onclick="window.open('/customer/reservationMap?num=2','','width=1500,height=950');return true">지도보기</button>
															<input type="text" name="lat2" id="lat2" value="${reservationDetail.wayPointLat}" readonly style="background:#e5e5e5; width:120px;">
															<input type="text" name="lng2" id="lng2" value="${reservationDetail.wayPointLng}" readonly style="background:#e5e5e5; width:120px;">
														</td>
													</c:when>
													<c:otherwise>
														<c:forEach var="reservationWayPoint" items="${reservationWayPoint}" varStatus="idx">
															<td colspan="3">
																<input type="text" name="address2" id="address2" value="${reservationWayPoint.wayPointAddress01}" placeholder="지역명 2자리이상 입력하세요" title="" class="input h25 w400">
																<button type="button" class="btn type4" onclick="deletePlace(2)">삭제</button>
																<button type="button" class="btn type4 secondary" onclick="window.open('/customer/reservationMap?num=2','','width=1500,height=950');return true">지도보기</button>
																<input type="text" name="lat2" id="lat2" value="${reservationWayPoint.wayPointLat01}" readonly style="background:#e5e5e5; width:120px;">
																<input type="text" name="lng2" id="lng2" value="${reservationWayPoint.wayPointLng01}" readonly style="background:#e5e5e5; width:120px;">
															</td>
														</c:forEach>													
													</c:otherwise>
												</c:choose>
										</tr>
										<tr class="expand-item" data-name="expandTable" aria-hidden="true">
											<th scope="row">경유지2</th>
												<c:choose>
													<c:when test="${reservationWayPoint == null or fn:length(reservationWayPoint) == 0}">
														<td colspan="3">
															<input type="text" name="address3" id="address3" value="" placeholder="지역명 2자리이상 입력하세요" title="" class="input h25 w400">
															<button type="button" class="btn type4" onclick="deletePlace(3)">삭제</button>
															<button type="button" class="btn type4 secondary" onclick="window.open('/customer/reservationMap?num=3','','width=1500,height=950');return true">지도보기</button>
															<input type="text" name="lat3" id="lat3" value="" readonly style="background:#e5e5e5; width:120px;">
															<input type="text" name="lng3" id="lng3" value="" readonly style="background:#e5e5e5; width:120px;">
														</td>
													</c:when>
													<c:otherwise>
														<c:forEach var="reservationWayPoint" items="${reservationWayPoint}" varStatus="idx">
															<td colspan="3">
																<input type="text" name="address3" id="address3" value="${reservationWayPoint.wayPointAddress02}" placeholder="지역명 2자리이상 입력하세요" title="" class="input h25 w400">
																<button type="button" class="btn type4" onclick="deletePlace(3)">삭제</button>
																<button type="button" class="btn type4 secondary" onclick="window.open('/customer/reservationMap?num=3','','width=1500,height=950');return true">지도보기</button>
																<input type="text" name="lat3" id="lat3" value="${reservationWayPoint.wayPointLat02}" readonly style="background:#e5e5e5; width:120px;">
																<input type="text" name="lng3" id="lng3" value="${reservationWayPoint.wayPointLng02}" readonly style="background:#e5e5e5; width:120px;">
															</td>
														</c:forEach>													
													</c:otherwise>
												</c:choose>
										</tr>
										
										<!-- <tr class="expand-item" data-name="expandTable" aria-hidden="true">
											<th scope="row">경유지3</th>
												<td colspan="3">
												
												</td>
										</tr> -->
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div class="align-c">
							<button type="button" class="btn-ico expand-toggle" data-type="items" data-target="expandTable" aria-expanded="false">
								<span><i class="arw arw-toggle1 dark">Example</i></span>
							</button>
						</div>
					</div>
				</section>
				<section class="section mar-t0">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h3 class="h3">예약 결제정보</h3>
						</div>	
					</div>
					<div style="position:relative;">
						<div class="tbl-type1 tbl-lefted">
							<table>
								<colgroup>
									<col width="17%">
									<col width="*">
									<col width="17%">
									<col width="20%">
								</colgroup>
								<tbody>
									<c:forEach var="reservationDetail" items="${reservationDetail}" varStatus="idx">
										<tr>
											<th scope="row">예약운임</th>
											<td colspan="3"><c:out value="${reservationDetail.reserveCharge}"></c:out>  </td>
											<th scope="row">실결제금액</th>
											<td><c:out value="${reservationDetail.paymentFee}"></c:out></td>										
										</tr>
										<tr>
											<th scope="row">예약 결제수단</th>
											<td colspan="3">
												<select name="paymentType" id="paymentType" class="select h25">
													<option value="">선택</option>
													<c:forEach items="${paymentTypeCodeList}" var="list" varStatus="status">
														<option value="${list.code}" <c:if test="${reservationDetail.paymentType eq list.code}">selected</c:if>>${list.codeName}</option>
													</c:forEach>
												</select>	
												<select name="couponCnt" id="couponCnt" class="select h25">
													<option value="">쿠폰</option>
													<c:forEach var="i" begin="0" end="5">
														<option value="${i}" <c:if test="${reservationDetail.couponCnt eq i}">selected</c:if>>${i}장</option>
													</c:forEach>
												</select>
											</td>	
											<th scope="row">할인율</th>	
											<td>
												<select name="hourPrice" id="hourPrice" class="select h25">
													<option value="">선택</option>
													<c:forEach items="${hourPriceCodeList}" var="list" varStatus="status">
														<option value="${list.codeNameNum}" <c:if test="${reservationVO.hourPrice1 eq list.codeNameNum}">selected</c:if>>${list.codeName}</option>
													</c:forEach>
												</select>					
											</td>
											<%-- <fmt:formatNumber value="${reservationDetail.hourPrice}" pattern="#,###" /> --%>
										</tr>	
										<!-- <tr>
											<th scope="row">쿠폰 ??????</th>
											<td colspan="3">10,000</td>		
										</tr>	 -->																				
										<tr>
										<!-- <tr class="expand-item" data-name="expandTable2" aria-hidden="true"> -->
											<th scope="row">컨시어지</th><!-- 펼친 후 포커스 folder-focus -->
											<td colspan="3">
												<c:forEach items="${conciergeCodeList}" var="list" varStatus="status">
													<span class="check type1">
														<input type="checkbox" name="concierge" <%-- id="concierge${list.code}"  --%> id="${status.count}" value="${list.code}" <%-- <c:if test="${concierge.payServiceNo eq list.code}">checked</c:if> --%>>
													    <label for="${status.count}"><span>${list.codeName}</span></label>
													</span>	
												</c:forEach>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div class="align-c">
							<button type="button" class="btn-ico expand-toggle" data-type="items" data-target="expandTable2" aria-expanded="false">
								<span></span>
							</button>
						</div>
					</div>
				</section>	
				<section class="section mar-t0">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h3 class="h3">파트너 정보</h3>
						</div>
						<div class="col right">									
							<button type="button" class="btn type2 primary" onclick="window.open('/partner/partnerAssignment?tabId=1&customerNo=${customerNo}', '', 'width=600, height=570')"><span>파트너변경</span></button>
							<button type="button" class="btn type2 cancel"><span>배차취소</span></button>
						</div>	
					</div>
					<div>
						<div class="tbl-type1 tbl-lefted">
							<table>
								<colgroup>
									<col width="17%">
									<col width="*">
									<col width="17%">
									<col width="20%">
								</colgroup>
								<tbody>
									<c:forEach var="reservationDetail" items="${reservationDetail}" varStatus="idx">
										<tr>
											<th scope="row">파트너명</th>
											<td>
												<input type="hidden" id="driverNo" name="driverNo" value="${reservationDetail.driverNo}"/>
											   	<input type="text" name="userName" id="userName" value="${reservationDetail.driverName}" placeholder="" title="" class="input h25" readonly>
											</td>	
											<th scope="row">휴대폰번호</th>
											<td>
												<input type="text" name="phoneNumber" id="phoneNumber" value="${reservationDetail.driverPhoneNo}" placeholder="" title="" class="input h25" readonly>							
											</td>								
										</tr>
										<tr>
											<th scope="row">파트너 등급</th>
												<td>
													<input type="text" name="partnerLv" id="partnerLv" value="${reservationVO.partnerLv}" placeholder="" title="" class="input h25" readonly>	
												</td>
											<th scope="row">사고율 (%)</th>
												<td></td>										
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</section>
				<section class="section">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h3 class="h3">요청사항</h3>
						</div>
					</div>
					<div>
						<div class="tbl-type1 tbl-lefted">
							<table>
								<colgroup>
									<col width="17%">
									<col width="*">
								</colgroup>
								<tbody>
									<c:forEach var="reservationDetail" items="${reservationDetail}" varStatus="idx">
										<tr>
											<th scope="row">고객 요청사항</th>
											<td><textarea rows="3" id="customerRequest" cols="50" class="input type1" value="${reservationDetail.customerComment}">${reservationDetail.customerComment}</textarea></td>							
										</tr>
										<tr>
											<th scope="row">관리자 요청사항</th>
											<td><textarea rows="3" id="adminRequest" cols="50" class="input type1" value="${reservationDetail.mosilerComment}">${reservationDetail.mosilerComment}</textarea></td>								
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</section>																
			</div>
		</div>
		<div class="popup-foot grid flex-center">
			<div class="col">
				<button type="button" onclick="cancelReservation();" class="btn type3 primary" style="background-color:red; margin-left: -425px;;"><span>예약 취소</span></button>		
				<button type="button" onclick="updateReservation();" class="btn type3 primary" style="margin-left: 350px;"><span>저장</span></button>
				<button type="button" onclick="selfClose();" class="btn type3 secondary"><span>취소</span></button>
			</div>
		</div>
		<div class="popup-close">
			<button type="button" class="btn-ico btn-close" title="팝업닫기" onclick="window.close();"><span><i class="ico ico-close1 white">Close</i></span></button>
		</div>
	</section>	
</div>
</body>
</html>