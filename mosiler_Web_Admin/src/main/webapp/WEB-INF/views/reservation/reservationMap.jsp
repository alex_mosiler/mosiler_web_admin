<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>모시러 ㅣ 예약장소 선택</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">    
    <style>
		body, html {height: 100%;margin:0;padding:0;letter-spacing: -1px;}
		.sidebar2 {display: block;position: absolute;z-index: 200;left: 0px;width: 390px;background-color: #fff;box-shadow: 0 0 10px rgba(0, 0, 0, 0.3); }
		.sidebar2 .search2 {background-color: #1a73e8;height: 70px;padding-top: 7px;}
		.type-search2 {margin: 10px 20px;width: 300px;height: 40px;border-radius: 3px;background-color: #fff;-webkit-box-shadow: 0 2px 1px 0 rgba(0,0,0,.15);-moz-box-shadow: 0 2px 1px 0 rgba(0,0,0,.15);box-shadow: 0 2px 1px 0 rgba(0,0,0,.15);}
		.input-search2 {width: 230px;padding: 8px 0;border: 0 none;font-weight: bold;font-size: 16px;background-color: transparent;outline: 0;}
		.item-summary2 {height: calc(100% - 70px);overflow-x: hidden;overflow-y: auto;position: relative;}
		.bAddr {margin:10px !important;padding: 5px 5px 0 !important;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;z-index: 1000 !important}
		.bLong {margin:10px !important;padding: 5px !important;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;z-index: 1000 !important;border-top: 1px solid #ccc;;letter-spacing: 0;}
		.map_wrap a, .map_wrap a:hover, .map_wrap a:active{color:#000;text-decoration: none;}
		.map_wrap {position:relative;width:100%;height:100%;}
		#menu_wrap {position:absolute;top:0;left:0;bottom:0;width:340px;overflow-y:auto;background:rgba(255, 255, 255, 1);z-index: 1;}
		.bg_white {background:#fff;}
		#menu_wrap hr {display: block; height: 1px;border: 0; border-top: 2px solid #5F5F5F;margin:3px 0;}
		#menu_wrap .option{text-align: center;}
		#menu_wrap .option p {margin:10px 0;}  
		#menu_wrap .option button {margin-left:20px;}
		#placesList li {list-style: none;}
		#placesList .item {position:relative;border-bottom:1px solid #ccc;overflow: hidden;cursor: pointer;min-height: 65px;}
		#placesList .item span {display: block;margin-top:4px;}
		#placesList .item h5 {font-weight: bold;font-size: 16px;}
		#placesList .item h5, #placesList .item .info {text-overflow: ellipsis;overflow: hidden;white-space: nowrap;}
		#placesList .item .info{padding:10px 0 10px 50px;}
		#placesList .info .gray {color:#8a8a8a;}
		#placesList .info .juso {padding-left:50px;background:url(/images/egovframework/com/adm/places_juso.png) no-repeat;width: 45px;height: 20px;}	
		#placesList .info .jibun {padding-left:50px;background:url(/images/egovframework/com/adm/places_jibun.png) no-repeat;width: 45px;height: 20px;}
		#placesList .info .tel {padding-left:50px;background:url(/images/egovframework/com/adm/places_tel.png) no-repeat;width: 45px;height: 20px;letter-spacing: -.5px;font-size: 13px;font-family:'tahoma'}
		#placesList .item .markerbg {float:left;position:absolute;width:36px; height:37px;margin:10px 0 0 10px;background:url(https://t1.daumcdn.net/localimg/localimages/07/mapapidoc/marker_number_blue.png) no-repeat;}
		#placesList .item .marker_1 {background-position: 0 -10px;}
		#placesList .item .marker_2 {background-position: 0 -56px;}
		#placesList .item .marker_3 {background-position: 0 -102px}
		#placesList .item .marker_4 {background-position: 0 -148px;}
		#placesList .item .marker_5 {background-position: 0 -194px;}
		#placesList .item .marker_6 {background-position: 0 -240px;}
		#placesList .item .marker_7 {background-position: 0 -286px;}
		#placesList .item .marker_8 {background-position: 0 -332px;}
		#placesList .item .marker_9 {background-position: 0 -378px;}
		#placesList .item .marker_10 {background-position: 0 -423px;}
		#placesList .item .marker_11 {background-position: 0 -470px;}
		#placesList .item .marker_12 {background-position: 0 -516px;}
		#placesList .item .marker_13 {background-position: 0 -562px;}
		#placesList .item .marker_14 {background-position: 0 -608px;}
		#placesList .item .marker_15 {background-position: 0 -654px;}
		#pagination {margin:10px auto;text-align: center;font-size: 14px;font-family: 'tahoma';}
		#pagination a {display:inline-block;margin-right:10px;}
		#pagination .on {font-weight: bold; cursor: default;color:#1a73e8;}	
	</style>
</head>
<body style="height:100%">
<div class="map_wrap">
    <div id="map" style="width:100%;height:100%;position:relative;overflow:hidden;"></div>
    <div id="menu_wrap" class="sidebar2">
        <div class="option search2">
			<form onsubmit="searchPlaces(); return false;">
				<fieldset class="form-controls flexible">
					<span class="input-group2 type-search2">
						<input type="text" name="" id="keyword" value="" placeholder="검색명 입력" class="input-search2">
						<span class="append"><button type="button" class="btn" onclick="searchPlaces();"><span><i class="ico ico-search1">search</i></span></button></span>
					</span>
				</fieldset>  
			</form>
        </div>
        <div class="item-summary2">
        	<ul id="placesList"></ul>
        	<div id="pagination"></div>
        </div>
    </div>
</div>

<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=8b0f41af31fdb6d393a5383d14d4c12e&libraries=services"></script>

<script>
<%
	String num = request.getParameter("num");
%>
var num = <%=num%>;

// 마커를 담을 배열입니다
var markers = [];

var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = {
        center: new kakao.maps.LatLng(37.566826, 126.9786567), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };  

// 지도를 생성합니다    
var map = new kakao.maps.Map(mapContainer, mapOption); 

//주소-좌표 변환 객체를 생성합니다
var geocoder = new kakao.maps.services.Geocoder();

// 장소 검색 객체를 생성합니다
var ps = new kakao.maps.services.Places();  

// 검색 결과 목록이나 마커를 클릭했을 때 장소명을 표출할 인포윈도우를 생성합니다
var infowindow = new kakao.maps.InfoWindow({zIndex:1});

var marker = new kakao.maps.Marker(), // 클릭한 위치를 표시할 마커입니다
infowindow1 = new kakao.maps.InfoWindow({zindex:1}); // 클릭한 위치에 대한 주소를 표시할 인포윈도우입니다

// 키워드로 장소를 검색합니다
//searchPlaces();

// 키워드 검색을 요청하는 함수입니다
function searchPlaces() {

    var keyword = document.getElementById('keyword').value;

    if (!keyword.replace(/^\s+|\s+$/g, '')) {
        alert('키워드를 입력해주세요!');
        return false;
    }

    // 장소검색 객체를 통해 키워드로 장소검색을 요청합니다
    ps.keywordSearch( keyword, placesSearchCB); 
}

// 장소검색이 완료됐을 때 호출되는 콜백함수 입니다
function placesSearchCB(data, status, pagination) {
    if (status === kakao.maps.services.Status.OK) {

        // 정상적으로 검색이 완료됐으면
        // 검색 목록과 마커를 표출합니다
        displayPlaces(data);

        // 페이지 번호를 표출합니다
        displayPagination(pagination);

    } else if (status === kakao.maps.services.Status.ZERO_RESULT) {

        alert('검색 결과가 존재하지 않습니다.');
        return;

    } else if (status === kakao.maps.services.Status.ERROR) {

        alert('검색 결과 중 오류가 발생했습니다.');
        return;

    }
}

// 검색 결과 목록과 마커를 표출하는 함수입니다
function displayPlaces(places) {

    var listEl = document.getElementById('placesList'), 
    menuEl = document.getElementById('menu_wrap'),
    fragment = document.createDocumentFragment(), 
    bounds = new kakao.maps.LatLngBounds(), 
    listStr = '';
    
    // 검색 결과 목록에 추가된 항목들을 제거합니다
    removeAllChildNods(listEl);

    // 지도에 표시되고 있는 마커를 제거합니다
    removeMarker();
    
    for ( var i=0; i<places.length; i++ ) {

        // 마커를 생성하고 지도에 표시합니다
        var placePosition = new kakao.maps.LatLng(places[i].y, places[i].x),
            marker = addMarker(placePosition, i), 
            itemEl = getListItem(i, places[i]); // 검색 결과 항목 Element를 생성합니다

        // 검색된 장소 위치를 기준으로 지도 범위를 재설정하기위해
        // LatLngBounds 객체에 좌표를 추가합니다
        bounds.extend(placePosition);

        // 마커와 검색결과 항목에 mouseover 했을때
        // 해당 장소에 인포윈도우에 장소명을 표시합니다
        // mouseout 했을 때는 인포윈도우를 닫습니다
        (function(marker, title) {
            kakao.maps.event.addListener(marker, 'mouseover', function() {
                displayInfowindow(marker, title);
            });

            kakao.maps.event.addListener(marker, 'mouseout', function() {
                infowindow.close();
            });

            itemEl.onmouseover =  function () {
                displayInfowindow(marker, title);
            };

            itemEl.onmouseout =  function () {
                infowindow.close();
            };
        })(marker, places[i].place_name);

        fragment.appendChild(itemEl);
    }

    // 검색결과 항목들을 검색결과 목록 Elemnet에 추가합니다
    listEl.appendChild(fragment);
    menuEl.scrollTop = 0;

    // 검색된 장소 위치를 기준으로 지도 범위를 재설정합니다
    map.setBounds(bounds);
}

// 검색결과 항목을 Element로 반환하는 함수입니다
function getListItem(index, places) {
    var el = document.createElement('li'),
    itemStr = '<span class="markerbg marker_' + (index+1) + '"></span>' +
                '<div class="info">' +
                '   <h5>' + places.place_name + '</h5>';
                             
    if (places.road_address_name) {
        itemStr += "    <span style='cursor:pointer;' class='juso' onclick='selAddress("+places.y+","+places.x+", \""+places.road_address_name+"\");'>" + places.road_address_name + '</span>' +
                    "   <span style='cursor:pointer;' class='jibun' onclick='selAddress("+places.y+","+places.x+",\""+places.address_name+"\");'>" +  places.address_name  + '</span>';
    } else {
        itemStr += "    <span style='cursor:pointer;' class='jibun' onclick='selAddress("+places.y+","+places.x+",\""+places.address_name+"\");'> "+  places.address_name  + '</span>'; 
    }
                 
      itemStr += '  <span class="tel">' + places.phone  + '</span>' +
                '</div>';           

    el.innerHTML = itemStr;
    el.className = 'item';

    return el;
}

// 마커를 생성하고 지도 위에 마커를 표시하는 함수입니다
function addMarker(position, idx, title) {
    var imageSrc = 'https://t1.daumcdn.net/localimg/localimages/07/mapapidoc/marker_number_blue.png', // 마커 이미지 url, 스프라이트 이미지를 씁니다
        imageSize = new kakao.maps.Size(36, 37),  // 마커 이미지의 크기
        imgOptions =  {
            spriteSize : new kakao.maps.Size(36, 691), // 스프라이트 이미지의 크기
            spriteOrigin : new kakao.maps.Point(0, (idx*46)+10), // 스프라이트 이미지 중 사용할 영역의 좌상단 좌표
            offset: new kakao.maps.Point(13, 37) // 마커 좌표에 일치시킬 이미지 내에서의 좌표
        },
        markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imgOptions),
            marker = new kakao.maps.Marker({
            position: position, // 마커의 위치
            image: markerImage 
        });

    marker.setMap(map); // 지도 위에 마커를 표출합니다
    markers.push(marker);  // 배열에 생성된 마커를 추가합니다

    return marker;
}

// 지도 위에 표시되고 있는 마커를 모두 제거합니다
function removeMarker() {
    for ( var i = 0; i < markers.length; i++ ) {
        markers[i].setMap(null);
    }   
    markers = [];
}

// 검색결과 목록 하단에 페이지번호를 표시는 함수입니다
function displayPagination(pagination) {
    var paginationEl = document.getElementById('pagination'),
        fragment = document.createDocumentFragment(),
        i; 

    // 기존에 추가된 페이지번호를 삭제합니다
    while (paginationEl.hasChildNodes()) {
        paginationEl.removeChild (paginationEl.lastChild);
    }

    for (i=1; i<=pagination.last; i++) {
        var el = document.createElement('a');
        el.href = "#";
        el.innerHTML = i;

        if (i===pagination.current) {
            el.className = 'on';
        } else {
            el.onclick = (function(i) {
                return function() {
                    pagination.gotoPage(i);
                }
            })(i);
        }

        fragment.appendChild(el);
    }
    paginationEl.appendChild(fragment);	
}

// 검색결과 목록 또는 마커를 클릭했을 때 호출되는 함수입니다
// 인포윈도우에 장소명을 표시합니다
function displayInfowindow(marker, title) {
    var content = '<div style="padding:5px;z-index:1;">' + title + '</div>';

    infowindow.setContent(content);
    infowindow.open(map, marker);
}

 // 검색결과 목록의 자식 Element를 제거하는 함수입니다
function removeAllChildNods(el) {   
    while (el.hasChildNodes()) {
        el.removeChild (el.lastChild);
    }
}

//지도를 클릭했을 때 클릭 위치 좌표에 대한 주소정보를 표시하도록 이벤트를 등록합니다
kakao.maps.event.addListener(map, 'click', function(mouseEvent) {
    searchDetailAddrFromCoords(mouseEvent.latLng, function(result, status) {
        console.log("lng : " + mouseEvent.latLng.La);
        console.log("lat : " + mouseEvent.latLng.Ma);

        if (status === kakao.maps.services.Status.OK) {
            var detailAddr = !!result[0].road_address ? "<div style='cursor:pointer;' onclick='selAddress("+mouseEvent.latLng.Ma+","+mouseEvent.latLng.La+",\""+result[0].road_address.address_name+"\");'>- [도로명주소] " + result[0].road_address.address_name + '</div>' : '';
            detailAddr += "<div style='cursor:pointer;' onclick='selAddress("+mouseEvent.latLng.Ma+","+mouseEvent.latLng.La+",\""+result[0].address.address_name+"\");'>- [지번 주소] " + result[0].address.address_name + '</div>';
			var content = '<div class="bAddr">' +detailAddr + '</div>';
            
            content += '<div class="bLong"><div>- Lng : '+ mouseEvent.latLng.La + "</div>"; 
			content += '<div>- Lat : ' + mouseEvent.latLng.Ma + "</div></div>";
			

            // 마커를 클릭한 위치에 표시합니다 
            marker.setPosition(mouseEvent.latLng);
            marker.setMap(map);

            // 인포윈도우에 클릭한 위치에 대한 법정동 상세 주소정보를 표시합니다
            infowindow1.setContent(content);
            infowindow1.open(map, marker);
        }   
    });
});

function searchDetailAddrFromCoords(coords, callback) {
    // 좌표로 법정동 상세 주소 정보를 요청합니다
    geocoder.coord2Address(coords.getLng(), coords.getLat(), callback);
}

function selAddress(x, y, address) {
	opener.document.getElementById("address"+num).value = address;
	opener.document.getElementById("lat"+num).value = x;
	opener.document.getElementById("lng"+num).value = y;

	self.close();

//	console.log(x);
//	console.log(y);
//	console.log(address);
}

</script>
</body>
</html>