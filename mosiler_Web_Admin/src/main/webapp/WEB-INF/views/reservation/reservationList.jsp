<!-- 전체 예약 현황 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>모시러 ㅣ 예약관리</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta name="keywords" content="모시러, mosiler, 운전동행">
	<meta name="description" content="고객의 마음까지 운전합니다. 운전동행 서비스 모시러">	
	<link rel="icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/images/egovframework/com/adm/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="/css/egovframework/com/adm/base.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/standard.ui.css">
	<link rel="stylesheet" href="/css/egovframework/com/adm/lightslider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<script src="/js/egovframework/com/adm/cmm/libs/jquery.min.js"></script>
    <script src="/js/egovframework/com/adm/cmm/libs/librarys.min.js"></script> 
    <script src="/js/egovframework/com/adm/cmm/ui/ui.common.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/ui.utility.js"></script>
    <script src="/js/egovframework/com/adm/cmm/ui/sidebar_over.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/ui/lightslider.js"></script>
   	<script src="/js/egovframework/com/adm/cmm/FnMenu.data.js"></script>    	
   	<script src="/js/util.js"></script>
   	
	<!-- 메뉴구현부  -->
	<script type="text/javascript">
		$(document).ready(function(){
			var id = "${id}";
			
			if(id == "") {
				location.href = "/";
			}
			
			var reservationList = ${reservationList};	
			var cateList = ${cateList};
			var depthList = ${depthList};
			
			var cateMenuCode = 'M'+'03'+'0000';
			var depthMenuCode1 = 'M'+'03'+'01'+'00';
			var depthMenuCode2 = 'M030101';
			
			makeMenuBar(cateList, depthList, cateMenuCode, depthMenuCode1, depthMenuCode2);

			// 현재 페이지 번호 세팅
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			// 엔터키 이벤트 
			$("#searchKeyword1").keypress(function(e) {
				if(e.which == 13) {
					search();
				}
			});

			// 엔터키 이벤트 
			$("#searchKeyword2").keypress(function(e) {
				if(e.which == 13) {
					search();
				}
			});

			// 리스트
			makeContents(reservationList);
			// 페이징
			makePagingVar();
		});

		// 리스트
		function makeContents(reservationList) {
			var html = "";
			var total = ${pagingVO.total};
			var nowPage = ${pagingVO.nowPage};
			var cntPerPage = ${pagingVO.cntPerPage};
			
			for(var i = 0; i < cntPerPage; i++) {
				if(reservationList[i] == null) {
					break;
				} else {
					var drivingType;
					var drivingStatus;
					
					// drivingType 세팅
					if(reservationList[i].drivingType == 1) {
						drivingType = "일반";
					} else if(reservationList[i].drivingType == 3) {
						drivingType = "골프";
					} else if(reservationList[i].drivingType == 21) {
						drivingType = "키즈";
					} else if(reservationList[i].drivingType == 22) {
						drivingType = "실버";
					} else if(reservationList[i].drivingType == 23) {
						drivingType = "에어";
					} else if(reservationList[i].drivingType == 24) {
						drivingType = "비즈";
					} else if(reservationList[i].drivingType == 25) {
						drivingType = "쇼핑";
					} else if(reservationList[i].drivingType == 26) {
						drivingType = "공항 홈발렛";
					} else if(reservationList[i].drivingType == 300) {
						drivingType = "고정운행";
					}
					
					// drivingStatus 세팅
					if(reservationList[i].drivingStatus == 1) {
						drivingStatus = "배차신청";
					} else if(reservationList[i].drivingStatus == 2) {
						drivingStatus = "배차확정";
					} else if(reservationList[i].drivingStatus == 3) {
						drivingStatus = "예약취소";
					} else if(reservationList[i].drivingStatus == 8) {
						drivingStatus = "배차확인";
					} else if(reservationList[i].drivingStatus == 9) {
						drivingStatus = "취소요청";
					}
					
					html += "<tr style='cursor: pointer' onclick=\"window.open('/reservation/reservationDetailpopUp?"
								+"drivingNo="+reservationList[i].drivingNo+"','','width=1130,height=900');return true\">";
					html += "	<td>"+(total - ((nowPage -1) * cntPerPage + i))+"</td>";
					html += "	<td>"+drivingType+"</td>";
					html += "	<td>"+reservationList[i].drivingNo+"</td>";
					html += "	<td>"+reservationList[i].customerName+"</td>"          
					html += "	<td class='align-l'>"+reservationList[i].revStartAddress+"</td>";
					html += "	<td>"+drivingStatus+"</td>";
					html += "	<td>"+reservationList[i].revStartDt.substring(0, 10)+"</td>";
					html += "	<td>"+reservationList[i].revStartDt.substring(11, 16)+"</td>";
					html += "	<td>"+reservationList[i].revEndDt.substring(11, 16)+"</td>";
					html += "	<td>"+reservationList[i].timeStamp+"분</td>";
					html += "	<td>"+reservationList[i].driverName+"</td>";
					html += "	<td>"+reservationList[i].regDt+"</td>";
					html += "<tr>";
				}
			}
			
			$("#listDiv").append(html);
		}
		
		// 페이징 바
		function makePagingVar() {
			var nowPage = ${pagingVO.nowPage};
			var startPage = ${pagingVO.startPage};
			var endPage = ${pagingVO.endPage};
			var lastPage = ${pagingVO.lastPage};
		
			var html = "";
			
			html += "<div class=\"col'\">";
			html += "	<div class=\"paging paging-basic\">";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == 1) {
				html += "		<button type=\"button\" class=\"btn first\" title=\"처음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn prev\" title=\"이전 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"firstPage\" class=\"btn first\" title=\"처음 페이지\"  onclick='arrowBtn(1);'><span><i class=\"arw arw-board1-first gray\"></i></span></button>";
				html += "		<button type=\"button\" id=\"prevPage\" class=\"btn prev\" title=\"이전 페이지\" onclick='arrowBtn(2);'><span><i class=\"arw arw-board1-prev gray\"></i></span></button>";
			}
			
			html += "		</div>";
			html += "		<ul class=\"num-group\">";
			
			for(var i = startPage; i <= endPage; i++) {
				if(i == nowPage) {
					html += "	<li><button type=\"button\" class=\"btn\" aria-current=\"true\" disabled=\"disabled\"><span>"+i+"</span></button></li>";
				} else {
					html += "	<li><button type=\"button\" id=\"e\" class=\"btn\" onclick='numberBtn("+i+");'><span>"+i+"</span></button></li>"
				}
			}
			
			html += "		</ul>";
			html += "		<div class=\"page-group\">";
			
			if(nowPage == lastPage) {
				html += "		<button type=\"button\" class=\"btn next\" title=\"다음 페이지\" disabled=\"disabled\" ><span><i class=\"arw arw-board1-next gray\"></i></span></button>";
				html += "		<button type=\"button\" class=\"btn last\" title=\"마지막 페이지\" disabled=\"disabled\"><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			} else {
				html += "		<button type=\"button\" id=\"nextPage\" class=\"btn next\" title=\"다음 페이지\" onclick='arrowBtn(3);'><span><i class=\"arw arw-board1-next gray\"></i></span></button>";			
				html += "		<button type=\"button\" id=\"lastPage\" class=\"btn last\" title=\"마지막 페이지\" onclick='arrowBtn(4);'><span><i class=\"arw arw-board1-last gray\"></i></span></button>";
			}
			
			html += "		</div>";		
			html += "	</div>";
			html += "</div>";	
			
			$("#pagingVar").append(html);
		}

		// 검색
		function search() {
			// 날짜 세팅
			setDate();
			
			//submit
			document.reservationForm.submit();
		}
		
		// 검색 초기화
		function keywordReset() {
			$("#drivingStatus").val("");
			$("#drivingType").val("");
		 	$("#searchType1").val("");
			$("#searchKeyword1").val("");
			$("#searchType2").val("");
			$("#searchKeyword2").val("");
			
			$("#startDate").val("");
			$("#startHour").val("00");
			$("#startMin").val(0);
			$("#endDate").val("")
			$("#endHour").val("00");
			$("#endMin").val(0);
	
			$("#drivingStatus option:eq(0)").prop("selected", true);
			$("#drivingType option:eq(0)").prop("selected", true);
			$("#searchType1 option:eq(0)").prop("selected", true);
			$("#searchType2 option:eq(0)").prop("selected", true);
		}
		
		// 한 페이지에 보여줄 row수 전환 
		function cntPerPageChange() {
			// 날짜 세팅
			setDate();
			
			// 현재 페이지 번호 저장
			$("input[name=nowPage]").val(${pagingVO.nowPage});
			
			// submit
			document.reservationForm.submit(); 
		}
		
		// 페이징 번호 버튼
		function numberBtn(num) {
			// 날짜 세팅
			setDate();
			
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(num);
			
			// submit
			document.reservationForm.submit(); 
		}
		
		// 페이징 화살표 버튼
		function arrowBtn(num) {
			// 날짜 세팅
			setDate();
			
			var nowPage = ${pagingVO.nowPage};
			
			if(num == 1) {
				// 처음
				nowPage = 1;
			} else if(num == 2) {
				// 이전
				nowPage = nowPage - 1;
			} else if(num == 3) {
				// 다음
				nowPage = nowPage + 1;
			} else {
				// 마지막
				nowPage = ${pagingVO.lastPage};
			}
			
			// 전활될 페이지 번호
			$("input[name=nowPage]").val(nowPage);
			
			// submit
			document.reservationForm.submit(); 
		}
		
		// 날짜 세팅 (페이지 전환 시, 검색 시 등등)
		function setDate() {
			var startYmd = "";
			var endYmd = "";
			
			if($("#startDate").val() == "") {
				startYmd = "2000-01-01";
			} else {
				startYmd = $("#startDate").val();
			}
			
			if($("#endDate").val() == "") {
				endYmd = "2999-12-31";
			} else {
				endYmd = $("#endDate").val();
			}
			
			var startDate = startYmd+" "+$("#startHour").val()+":"+$("#startMin").val()+":00.000";
			var endDate = endYmd+" "+$("#endHour").val()+":"+$("#endMin").val()+":00.000";
			
			// 날짜 세팅
			$("input[name=hiddenStartDate]").val(startDate);
			$("input[name=hiddenEndDate]").val(endDate);	
		}
	</script>   	
</head>

<body>
<form name="reservationForm" id="reservationForm" method="post" action="/reservation/reservationList" onsubmit="return false">
	<div class="wrapper">
		<%@ include file="../header.jsp" %>
		<!-- Container -->
		<div id="container">					
			<%@ include file="../header2.jsp" %>
			<!-- Content -->
			<div id="content" class="in-sec">
				<!-- section -->
				<section class="section">
					<input type="hidden" name="nowPage">
					<input type="hidden" name="hiddenStartDate">
					<input type="hidden" name="hiddenEndDate">
					<div class="status-box green">
						<ul>
							<li>
								<a href="javascript:void(0);" class="status-link">
									<span class="status-txt">매칭전</span>
									<span class="status-qty">1,050</span> 건
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="status-link">
									<span class="status-txt">매칭중</span>
									<span class="status-qty">250</span> 건
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="status-link">
									<span class="status-txt">매칭완료</span>
									<span class="status-qty">1,250</span> 건
								</a>
							</li>					
						</ul>										
					</div>
				</section>
				<!-- //section -->			
				<section class="section mar-t10">
					<div class="form-search grid flex-center cross-center">
						<ul>
							<li>
								<label class="form-tit">배차상태</label>
								<div class="data-group">
									<select name="drivingStatus" id="drivingStatus" class="select h30">
									    <option value="">전체</option>
									    <c:forEach items="${codeList1}" var="list" varStatus="status">
											<option value="${list.code}" <c:if test="${reservationVO.state eq list.code}">selected</c:if>>${list.codeName}</option>
										</c:forEach>
									</select>									
								</div>
							</li>
							<li>
								<label class="form-tit">운행구분</label>
								<div class="data-group">
									<select name="drivingType" id="drivingType" class="select h30">
									    <option value="">전체</option>
									    <c:forEach items="${codeList2}" var="list" varStatus="status">
											<option value="${list.code}" <c:if test="${reservationVO.drivingType eq list.code}">selected</c:if>>${list.codeName}</option>
										</c:forEach>
									</select>
								</div>
							</li>							
							<li>
								<label class="form-tit">검색</label>
								<div class="data-group">
									<select name="searchType1" id="searchType1" class="select h30">
									    <!-- <option value="">전체</option> -->
									    <option value="customer" <c:if test="${reservationVO.gubun eq 'customer'}">selected</c:if>>고객명</option>
									    <option value="phone" 	 <c:if test="${reservationVO.gubun eq 'phone'}">selected</c:if>>연락처(고객)</option>
									    <option value="driver"	 	 <c:if test="${reservationVO.gubun eq 'driver'}">selected</c:if>>드라이버명</option>
									</select>
									<input type="text" name="searchKeyword1" id="searchKeyword1" value="${reservationVO.keyword}" placeholder="고객명/연락처/드라이버명" title="" class="input h30 w200">
								</div>
							</li>
							<li>
								<label class="form-tit">위치</label>
								<div class="data-group">
									<select name="searchType2" id="searchType2" class="select h30">
										<!-- <option value="">전체</option> -->
									    <option value="revStartAddress"		 <c:if test="${reservationVO.address eq 'revStartAddress'}">selected</c:if>>출발지</option>
									    <option value="revWaypointAddress" <c:if test="${reservationVO.address eq 'revWaypointAddress'}">selected</c:if>>경유지</option>
									    <option value="revEndAddress"			 <c:if test="${reservationVO.address eq 'revEndAddress'}">selected</c:if>>도착지</option>
									</select>
									<input type="text" name="searchKeyword2" id="searchKeyword2" value="${reservationVO.keywordAddress}" placeholder="출발지/경유지/도착지" title="" class="input h30 w200">
								</div>
							</li>																			
							<li>
								<label class="form-tit">기간</label>
								<div class="data-group">
									<!-- <select name="sSelect1" id="sSelect1" class="select h30">
									    <option value="">예약일</option>
									</select> -->							
									<input type="date" name="startDate" id="startDate" value="${fn:substring(reservationVO.startDt,0,10)}" placeholder="" title="" class="input h30 input-date">
									<select name="startHour" id="startHour" class="select h30">
										<c:forEach var="i" begin="0" end="23">
											<c:if test="${i < 10}">
												<option value="0${i}" <c:if test="${fn:substring(reservationVO.startDt, 12, 13) eq i}">selected</c:if>>0${i}시</option>
											</c:if>
											<c:if test="${i >= 10}">
												<option value="${i}" <c:if test="${fn:substring(reservationVO.startDt, 11, 13) eq i}">selected</c:if>>${i}시</option>
											</c:if>
										</c:forEach>
									</select>
									<select name="startMin" id="startMin" class="select h30">
										<option value="0">00분</option>
										<c:forEach var="i" begin="5" end="55">
											<c:if test="${i%5 == 0}">
												<c:if test="${i < 10}">
													<option value="0${i}" <c:if test="${fn:substring(reservationVO.startDt, 15, 16) eq i}">selected</c:if>>0${i}분</option>
												</c:if>
												<c:if test="${i >= 10}">
													<option value="${i}" <c:if test="${fn:substring(reservationVO.startDt, 14, 16) eq i}">selected</c:if>>${i}분</option>
												</c:if>
											</c:if>
										</c:forEach>
									</select>									
									<span class="form-split">-</span>
									<input type="date" name="endDate" id="endDate" value="${fn:substring(reservationVO.endDt,0,10)}" placeholder="" title="" class="input h30 input-date">
									<select name="endHour" id="endHour" class="select h30">
										<c:forEach var="i" begin="0" end="23">
											<c:if test="${i < 10}">
												<option value="0${i}"  <c:if test="${fn:substring(reservationVO.endDt, 12, 13) eq i}">selected</c:if>>0${i}시</option>
											</c:if>
											<c:if test="${i >= 10}">
												<option value="${i}"  <c:if test="${fn:substring(reservationVO.endDt, 11, 13) eq i}">selected</c:if>>${i}시</option>
											</c:if>
										</c:forEach>
									</select>
									<select name="endMin" id="endMin" class="select h30">
										<option value="0">00분</option>
										<c:forEach var="i" begin="5" end="55">
											<c:if test="${i%5 == 0}">
												<c:if test="${i < 10}">
													<option value="0${i}" <c:if test="${fn:substring(reservationVO.endDt, 15, 16) eq i}">selected</c:if>>0${i}분</option>
												</c:if>
												<c:if test="${i >= 10}">
													<option value="${i}" <c:if test="${fn:substring(reservationVO.endDt, 14, 16) eq i}">selected</c:if>>${i}분</option>
												</c:if>
											</c:if>
										</c:forEach>
									</select>		
								</div>
							</li>						
						</ul>
						<div class="Mmar-t10">
							<button type="button" class="btn-search" onclick="search();"><span>검색</span></button>
						</div>&emsp;
						<div class="Mmar-t10">
							<button type="button" class="btn-search" onclick="keywordReset();"><span id="searchButton">초기화</span></button>
						</div>
					</div>
				</section>
				<!-- section -->
				<section class="section">
					<div class="tbl-header grid cross-center">
						<div class="col">
							<h2 class="h2">전체예약현황</h2>
						</div>
						<div class="col right">
							<button type="button" class="btn type1 primary" onclick="window.open('/reservation/reservationRegistPopUp', '', 'width=1000, height=936')"><span>예약등록</span></button>
							<button type="button" class="btn type1 secondary"><span>엑셀다운로드</span></button>
						</div>			
					</div>
					<div class="Mguide">
   						모바일에서 표를 좌우로 스크롤 할 수 있습니다. 
  					</div>
					<div class="gtbl-list l15">
						<table class="x-auto">
							<caption></caption>
							<thead>
								<tr>
									<th scope="col">번호</th>
									<th scope="col">운행 구분</th>
									<th scope="col">예약 번호</th>
									<th scope="col">고객명</th>
									<th scope="col">출발지</th>
									<th scope="col">배차 상태</th>
									<th scope="col">예약일</th>
									<th scope="col">시작</th>
									<th scope="col">종료</th>
									<th scope="col">예약시간(분)</th>
									<th scope="col">파트너</th>
									<th scope="col">등록일</th>
								</tr>
							</thead>
							<tbody id="listDiv">
																																																																																																																																																																																																																																																															
							</tbody>	
						</table>
					</div>
					<div class="tbl-footer grid flex-between cross-center">
						<div class="col total">페이지 <em>${pagingVO.nowPage}</em>/${pagingVO.lastPage} 결과 <em>${pagingVO.total}</em>건</div>
						<div id="pagingVar" class="col">

						</div>
						<div class="col per-page">
							<select name="cntPerPage" id="cntPerPage" class="select h30" onChange="cntPerPageChange()">
							    <option value="10" <c:if test="${pagingVO.cntPerPage == 10}">selected</c:if>>10</option>
							    <option value="20" <c:if test="${pagingVO.cntPerPage == 20}">selected</c:if>>20</option>
							    <option value="30" <c:if test="${pagingVO.cntPerPage == 30}">selected</c:if>>30</option>
							    <option value="40" <c:if test="${pagingVO.cntPerPage == 40}">selected</c:if>>40</option>
							    <option value="50" <c:if test="${pagingVO.cntPerPage == 50}">selected</c:if>>50</option>
							    <option value="100" <c:if test="${pagingVO.cntPerPage == 100}">selected</c:if>>100</option>
							</select>	
						</div>			
					</div>		
				</section>
				<!-- //section -->
			</div>	
			<!-- //Content -->
		</div>
		<!-- //Container -->
		<!-- Sidebar -->
		<aside id="quickmenu">
			<div class="quick-control">
				<a href="javascript:void(0);" class="quick-left" style="font-weight:normal;line-height:11px;">Quick<br>Menu</a>
				<a href="javascript:void(0);" class="quick-right">닫기</a>
			</div>
			<ul class="quick-list">
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/index'">
						<i class="ico ico-quick totalchart"></i>
						<p>종합현황</p>
					</a>
				</li>				
				<li>				
					<a href="javascript:void(0);" class="quick-link" onclick="location.href='/operation/realTimeControll'">
						<i class="ico ico-quick map02"></i>
						<p>실시간관제</p>
					</a>
				</li>																				
			</ul>
			<div>
				<a href="#" class="quick-scroll-top">TOP</a>
			</div>
		</aside>
		<!-- //Sidebar -->			
	</div>
</form>
</body>
</html>