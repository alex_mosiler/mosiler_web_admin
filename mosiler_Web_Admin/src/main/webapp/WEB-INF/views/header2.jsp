<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- breadcrumb -->
	<div class="breadcrumb">
		<div class="in-sec">
			<div class="home"><a href="/index"></a></div>
			<div class="bread-menu">
				<div class="depth dep01" id="cateDiv"></div>
				<div class="depth dep02" id="depthDiv1"></div>					
			</div>
			<div id="menu-container" class="is-hidden">
	            <div id="menu-wrapper">
	                <div id="hamburger-menu"><span></span><span></span><span></span></div>
	                <!-- hamburger-menu -->
	            </div>
	            <!-- menu-wrapper -->
	            <ul class="menu-list accordion">
	                <li id="nav1" class="toggle accordion-toggle"> 
	                    <span class="icon-plus"></span>
	                    <a class="menu-link" href="/customer/customerList">고객관리</a>
	                </li>
	                <ul class="menu-submenu accordion-content">
	                    <li><a class="head" href="/customer/customerList">고객현황</a></li>
	                    <li><a class="head" href="/customer/applyServiceUser">서비스신청자</a></li>
	                    <li><a class="head" href="/customer/corporationStatus">법인현황</a></li>
	                </ul> 
	                <li id="nav2" class="toggle accordion-toggle"> 
	                    <span class="icon-plus"></span>
	                    <a class="menu-link" href="/partner/partnerList">파트너관리</a>
	                </li>
                    <ul class="menu-submenu accordion-content">
                        <li><a class="head" href="/partner/partnerList">파트너현황</a></li>
                        <li><a class="head" href="/partner/temporaryPartner">임시파트너</a></li>
                        <li><a class="head" href="/partner/failPartner">불합격자</a></li>
                        <li><a class="head" href="/partner/setDispatch">배차설정</a></li>
                    </ul>
	                <li id="nav3" class="toggle accordion-toggle"> 
	                    <span class="icon-plus"></span>
	                    <a class="menu-link" href="/reservation/reservationList">예약관리</a>
	                </li>
                    <ul class="menu-submenu accordion-content">
                        <li><a class="head" href="/reservation/reservationList">전체예약현황</a></li>
                    </ul>
	                <li id="nav4" class="toggle accordion-toggle"> 
	                    <span class="icon-plus"></span>
	                    <a class="menu-link" href="/operation/operationDayList">운행관리</a>
	                </li>
                    <ul class="menu-submenu accordion-content">
                        <li><a class="head" href="/operation/realTimeControll">실시간관제</a></li>
                        <li><a class="head" href="/operation/operationDayList">일별운행현황</a></li>
                        <li><a class="head" href="/operation/operationList">전체운행현황</a></li>
                        <li><a class="head" href="/operation/workOnOffTime">출퇴근관리</a></li>
                        <li><a class="head" href="/operation/operationAccident">사고관리</a></li>
                        <li><a class="head" href="/operation/operationClaim">선클레임</a></li>
                    </ul>	
	                <li id="nav5" class="toggle accordion-toggle"> 
	                    <span class="icon-plus"></span>
	                    <a class="menu-link" href="/payment/paymentList">결제관리</a>
	                </li>
                    <ul class="menu-submenu accordion-content">
                        <li><a class="head" href="/payment/paymentList">결제현황</a></li>
                        <li><a class="head" href="/payment/commuterTicket">정액권결제</a></li>
                        <li><a class="head" href="/payment/productFee">상품요금</a></li>
                    </ul>
	                <li id="nav6" class="toggle accordion-toggle"> 
	                    <span class="icon-plus"></span>
	                    <a class="menu-link" href="/calculate/calculatePartner">정산관리</a>
	                </li>
                    <ul class="menu-submenu accordion-content">
                        <li><a class="head" href="/calculate/calculatePartner">파트너정산서</a></li>
                        <li><a class="head" href="/calculate/depositDeduction">보증금공제</a></li>
                        <li><a class="head" href="/calculate/accidentDeduction">사고공제</a></li>
                        <li><a class="head" href="/calculate/penaltyDeduction">법칙금공제</a></li>
                        <li><a class="head" href="/calculate/violationHistory">위반이력</a></li>
                        <li><a class="head" href="/calculate/setFee">금액설정</a></li>
                    </ul>	
	                <li id="nav7" class="toggle accordion-toggle"> 
	                    <span class="icon-plus"></span>
	                    <a class="menu-link" href="/system/setAdminUser">시스템</a>
	                </li>
                    <ul class="menu-submenu accordion-content">
                        <li><a class="head" href="/system/setAdminUser">운영자관리</a></li>
                        <li><a class="head" href="/system/setAuthority">권한관리</a></li>
                        <li><a class="head" href="/system/setProductFee">상품관리</a></li>
                        <li><a class="head" href="/system/setConcierge">컨시어지관리</a></li>
                        <li><a class="head" href="/system/setCoupon">쿠폰관리</a></li>
                        <li><a class="head" href="/system/setCode">코드관리</a></li>
                        <li><a class="head" href="/system/setCode">공지사항</a></li>
                        <li><a class="head" href="/system/setFAQ">FAQ</a></li>
                        <li><a class="head" href="/system/setFAQ">QNA</a></li>
                    </ul>		                    	                    		                    	                    
	            </ul>
	            <!-- menu-list accordion-->
	        </div>
		</div>
	</div>
	<!-- //breadcrumb -->
</body>
</html>