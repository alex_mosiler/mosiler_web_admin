<!-- 최상단 헤더 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	function logout() {
		location.href = "/logout";	
	}
</script>
</head>
<body>
	 <!-- Header -->	
	<div class="header-top in-sec">
		<h1 class="header-logo">
			<a href="/index">
				<i class="ico ico-logo">모시러 로고</i>
				<span class="logo-bi">모시러관리시스템</span>
			</a>
		</h1>		
		<div class="header-link">
			<ul class="navbar-nv">
				<li class="nav-item drop down mar-r30">
					<!-- <div class="smsPossible">
	                	<span>SMS 발송가능</span> <span class="txt-red01" id="sms_cnt">11,954</span>
	                </div> -->					
				</li>
				<li class="nav-item drop down">
					<button type="button" aria-controls="drop1" aria-haspopup="listbox" class="btn-ico drop-toggle" title="amy">
						<span><i class="ico ico-nav user">amy</i></span>
					</button>
					<div id="drop1" class="drop-menu" style="width: 200px">
						<ul class="drop-list" role="listbox">
							<li class="drop-item" role="option"><a href="javascript:;" onclick="window.open('profile-P01.html','','width=600,height=400');return false">프로필 보기</a></li>
							<li class="drop-item" role="option"><a href="javascript:;" onclick="window.open('profile-P02.html','','width=600,height=600');return false">프로필 편집</a></li>
							<li class="drop-item" role="option"><a href="javascript:;" onclick="logout();"><i class="ico ico-logout"></i>로그아웃</a></li>
						</ul>							
					</div>						
				</li>
				<!-- <li class="nav-item drop down">
					<button type="button" aria-controls="drop3" aria-haspopup="listbox" class="btn-ico drop-toggle" title="공지사항">
						<span><i class="ico ico-nav notice"></i></span>
						<span class="badge_box"><i class="badge_new">1</i></span>
					</button>						
					<div id="drop3" class="drop-menu" style="width: 400px">
						<ul class="drop-list" role="listbox">
							<li class="drop-item" role="option">
							    <ul class="list-item list drop">
							        <li class="item header">
										<h5 class="date">2020년 8월 11일</h5>
										<h3 class="title">긴급시스템 점검 안내</h3>									        
							        </li>
							        <li class="item body">
										<p>IDC장애로 인해 모시러 서비스 이용이 불가능합니다. 정상화에 힘을 쏟아 빠른시간 내에 서비스를 이용하실 수 있도록 최선을 다하겠습니다. 이용에 불편을 드려 대단히 죄송합니다. 문의사항은 모시러 고객센터(1522-4556)로 연락바랍니다. 감사합니다.</p>	
							        </li>
							        <li class="item footer">
							        	<a href="javascript:;" onclick="window.open('M070601-P01.html','','width=1000,height=700');return false" class="item-link">자세히보기<span><i class="arw-more1 gray"></i></span></a>
							        </li>
							    </ul>
							</li>
						</ul>							
					</div>						
				</li>					
				<li class="nav-item drop down">
					<button type="button" aria-controls="drop2" aria-haspopup="listbox" class="btn-ico drop-toggle" title="QNA">
						<span><i class="ico ico-nav chat"></i></span>
						<span class="badge_box"><i class="badge_new">2</i></span>
					</button>						
					<div id="drop2" class="drop-menu" style="width: 400px">
						<ul class="drop-list" role="listbox">
							<li class="drop-item" role="option">
							    <ul class="list-item list drop unanswer">
							        <li class="item header">
										<h5 class="date">2019년 12월 2일</h5>
										<h3 class="title">골프장 이용 요금 알려주세요?</h3>									        
							        </li>								        
							        <li class="item footer">
							        	<a href="javascript:;" onclick="window.open('M070801-P02.html','','width=1000,height=700');return false" class="item-link">답글쓰기<span><i class="arw-more1 primary"></i></span></a>
							        </li>
							    </ul>
							</li>							
							<li class="drop-item" role="option">
							    <ul class="list-item list drop">
							        <li class="item header">
										<h5 class="date">2020년 9월 4일</h5>
										<h3 class="title">모시러 이용 가능 지역 알려주세요?</h3>									        
							        </li>
							        <li class="item body">
										<p>현재로서는 수도권 지역에서만 이용이 가능하며, 경기/인천 중 일부 지역은 서비스가 제한될 수 있는 점 양해 부탁드리며, 입력하신 출발 또는 종료지가 이용 불가한 지역일 경우 고객센터에서 별도의 연락을 드립니다. 문의사항은 모시러 고객센터(1522-4556)로 연락바랍니다. 감사합니다.</p>	
							        </li>
							        <li class="item footer">
							        	<a href="javascript:;" onclick="window.open('M070801-P03.html','','width=1000,height=700');return false" class="item-link">자세히보기<span><i class="arw-more1 gray"></i></span></a>
							        </li>
							    </ul>
							</li>
						</ul>							
					</div>												
				</li> -->
			</ul>
		</div>									
	</div>
	<header id="header">	
		<nav class="gnb gnb-fulldown in-sec" id="gnb">
			<ul class="node1-list" id="gnbMenuUI">
				<li class="node1-item is-active">
					<a href="javascript:void(0)" class="node1-link">고객관리</a>
					<div class="node2-menu">
						<ul class="node2-list is-entered is-leaved">
							<li class="node2-item is-active">
								<a href="/customer/customerList" class="node2-link">고객현황</a>
							</li>
							<li class="node2-item">
								<a href="/customer/applyServiceUser" class="node2-link">서비스신청자</a>
							</li>
							<li class="node2-item">
								<a href="/customer/corporationStatus" class="node2-link">법인현황</a>
							</li>																
							<li class="node2-item">
								<a href="/customer/promotion" class="node2-link">프로모션</a>
							</li>	
						</ul>
					</div>
				</li>
				<li class="node1-item">
					<a href="javascript:void(0)" class="node1-link">파트너관리</a>
					<div class="node2-menu">
						<ul class="node2-list is-entered is-leaved">
							<li class="node2-item">
								<a href="/partner/partnerList" class="node2-link">파트너현황</a>
							</li>
							<li class="node2-item">
								<!-- <a href="/partner/temporaryPartner" class="node2-link">임시파트너</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/partner/failPartner" class="node2-link">불합격자</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/partner/setDispatch" class="node2-link">배차설정</a>  -->
							</li>																
						</ul>
					</div>
				</li>
				<li class="node1-item">
					<a href="javascript:void(0)" class="node1-link">예약관리</a>
					<div class="node2-menu">
						<ul class="node2-list is-entered is-leaved">
							<li class="node2-item">
								<a href="/reservation/reservationList" class="node2-link">전체예약현황</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="node1-item">
					<a href="javascript:void(0)" class="node1-link">운행관리</a>
					<div class="node2-menu">
						<ul class="node2-list is-entered is-leaved">
							<li class="node2-item">
								<a href="/operation/realTimeControll" class="node2-link">실시간관제</a>
							</li>
							<li class="node2-item">
								<a href="/operation/operationDayList" class="node2-link">일별운행현황</a>
							</li>								
							<li class="node2-item">
								<!-- <a href="/operation/operationList" class="node2-link">전체운행현황</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/operation/workOnOffTime" class="node2-link">출퇴근관리</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/operation/operationAccident" class="node2-link">사고관리</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/operation/operationClaim" class="node2-link">선클레임</a> -->
							</li>																																							
						</ul>
					</div>
				</li>
				<li class="node1-item">
					<a href="javascript:void(0)" class="node1-link">결제관리</a>
					<div class="node2-menu">
						<ul class="node2-list is-entered is-leaved">
							<li class="node2-item">
								<!-- <a href="/payment/paymentList" class="node2-link">결제현황</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/payment/commuterTicket" class="node2-link">정액권결제</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/payment/productFee" class="node2-link">상품요금</a> -->
							</li>																																							
						</ul>
					</div>
				</li>										
				<li class="node1-item">
					<a href="javascript:void(0)" class="node1-link">정산관리</a>
					<div class="node2-menu">
						<ul class="node2-list is-entered is-leaved">
							<li class="node2-item">
								<!-- <a href="/calculate/calculatePartner" class="node2-link">파트너정산서</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/calculate/depositDeduction" class="node2-link">보증금공제</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/calculate/accidentDeduction" class="node2-link">사고공제</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/calculate/penaltyDeduction" class="node2-link">범칙금공제</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/calculate/violationHistory" class="node2-link">위반이력</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/calculate/setFee" class="node2-link">금액설정</a> -->
							</li>																								
						</ul>
					</div>
				</li>
				<li class="node1-item">
					<a href="javascript:void(0)" class="node1-link">시스템</a>
					<div class="node2-menu">
						<ul class="node2-list is-entered is-leaved">
							<li class="node2-item">
								<a href="/system/setAdminUser" class="node2-link">운영자관리</a>
							</li>
							<li class="node2-item">
								<!-- <a href="/system/setAuthority" class="node2-link">권한관리</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/system/setProductFee" class="node2-link">상품관리</a> -->
							</li>
							<li class="node2-item">
								<!-- <a href="/system/setConcierge" class="node2-link">컨시어지관리</a> -->
							</li>
							<li class="node2-item">
								<a href="/system/setCoupon" class="node2-link">쿠폰관리</a>
							</li>
							<li class="node2-item">
								<!-- <a href="/system/setCode" class="node2-link">코드관리</a> -->
							</li>								
							<li class="node2-item">
								<!-- <a href="/system/setNotice" class="node2-link">공지사항</a> -->
							</li>								
							<li class="node2-item">
								<a href="/system/setFAQ" class="node2-link">FAQ</a>
							</li>
							<li class="node2-item">
								<!-- <a href="/system/setQNA" class="node2-link">QNA</a> -->
							</li>																																																
						</ul>
					</div>
				</li>														
			</ul>
			<div class="gnb-bg"></div>
		</nav>
	</header>	
	<!-- //Header -->	
</body>
</html>